LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PRELINK_MODULE := false

LOCAL_SRC_FILES:= \
    libml.c

LOCAL_C_INCLUDES += \
    $(JNI_H_INCLUDE) \
    external/libusb/libusb

LOCAL_SHARED_LIBRARIES := \
    libusb \
    libcutils \
    libutils

LOCAL_MODULE := liblauncher_jni
LOCAL_MODULE_TAGS := optional

include $(BUILD_SHARED_LIBRARY)
