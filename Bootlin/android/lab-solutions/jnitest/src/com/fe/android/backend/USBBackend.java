package com.fe.android.backend;

import android.app.Activity;

import android.os.Bundle;
import android.os.AsyncTask;

import android.util.Log;

import android.widget.TextView;
import android.widget.Toast;

public class USBBackend extends Activity {
	private final String TAG = getClass().getSimpleName();

	private class USBTask extends AsyncTask<Void, Void, Void> {
		protected void onPreExecute() {
			super.onPreExecute();

			Toast.makeText(getApplicationContext(),
				       "Starting Test",
				       Toast.LENGTH_LONG).show();
		}

		protected Void doInBackground(Void... args) {
			try {
				moveDown();
				Thread.sleep(2000);

				fire();
				Thread.sleep(4000);

				stop();
			} catch (Exception e) {}

			return null;
		}

		protected void onPostExecute(Void result) {
			TextView textView = (TextView) findViewById(R.id.usb_view_id);
			textView.setText(R.string.usb_view_done);

			Toast.makeText(getApplicationContext(),
				       "Test done",
				       Toast.LENGTH_LONG).show();
		}
	}

	private native void fire();
	private native void stop();

	private native int freeUSB();
	private native int initUSB();
	private native int moveDown();
	private native int moveLeft();
	private native int moveRight();
	private native int moveUp();

	public USBBackend() {
		initUSB();
	}

	public void finalize() {
		freeUSB();
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.usb_layout);

		new USBTask().execute();
	}

	static {
		System.loadLibrary("launcher_jni");
	}

}
