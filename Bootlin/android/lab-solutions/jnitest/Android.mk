LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := \
	$(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := TestJNI

LOCAL_MODULE_TAGS := optional

include $(BUILD_PACKAGE)
