#!/usr/bin/python2 -B
# -*- coding: utf-8 -*-

################################################################################
# Name: Decorate_python_time_execution
################################################################################
import time

def timeit(f):

    def timed(*args, **kw):

        ts = time.time()
        result = f(*args, **kw)
        te = time.time()

        # print 'func:%r args:[%r, %r] took: %2.4f sec' % \
        #   (f.__name__, args, kw, te-ts)
        # print("func:{:s} args:[{:s}, {:s}] took: {:2.4f} sec".format(f.__name__, '{}'.format(args), '{}'.format(kw), te-ts))
        print("\033[32mfunc:{:s} args:[{:s}, {:s}] took: {:2.4f} sec\033[0m".format(f.__name__, '{}'.format(args), '{}'.format(kw), te-ts))

        return result

    return timed

# Example:
@timeit
def compute_magic(n):
     #function definition
     #....

################################################################################
# Name: 
################################################################################