#!/usr/bin/python
# -*- coding: utf-8 -*-

################################################################################
# Project      = MCAL Automatic Evaluation System                              #
# Module       = exe_concu.py                                                  #
# Version      = 1.00                                                          #
# Date         = 30/06/2020                                                    #
# AES Version  = 2.1.0(Official Version)                                       #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2020 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This file executes test case concurrently to save time and  #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  30/06/2020     : Initial Version                                     #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------


from __future__ import print_function

import datetime
import os
import re
import sys
import threading
import atexit
import time
import multiprocessing
import mmap
import math
import shlex
import subprocess

import argparse
import Queue
import signal
from threading import Lock

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------



# Get the started time of program
StartTime = time.time()

# Notification of compiling TC
CompilingNotification = '''
*************************************************************
COMPILING:
                     {}
*************************************************************
'''

JudgementNotification = '''
*************************************************************
JUDGEMENT:           {}
*************************************************************
'''

# Max number of theads can be used in execution time optimization depending on ExeOp flag
MAX_NO_CPU = multiprocessing.cpu_count()
CONFIG_NUM_OF_TOTAL_THREAD = MAX_NO_CPU
CONFIG_NUM_OF_THREAD_BUILD = CONFIG_NUM_OF_TOTAL_THREAD - 1

Target_Stream = ""
targetTCList = [] 
targetOptionList = []
DEBUG_FLAG = False
# DEBUG_FLAG = True
TargetExecutingList = []
FORCE_STOP = False
STAT_FLAG = 0
total_tc_run = 0
cur_tc_index = 0


# Register the exit handler calculate the execution time
def exit_handler():
    print(" [~] Execution time: " +
          str(time.time() - StartTime) + " seconds")
atexit.register(exit_handler)

def sigint_handler(sig, frame):
    global FORCE_STOP
    print("You pressed Ctrl+C!")
    FORCE_STOP = True
    # sys.exit(0)
signal.signal(signal.SIGINT, sigint_handler)



# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------


def print_status(last_message=False):
    """print a status at the completion of the program or print error and exit if failed
    """
    global STAT_FLAG
    if 2 == STAT_FLAG:
        # if GV.IGNO_FLAG and not last_message:
        if not last_message:
            print(" [!] Error: An error has occurred at recent command!")
        else:
            print(" [!] Test cases execution is not completed due to error(s) occur")
            os._exit(1)
    elif 1 == STAT_FLAG:
        print(" [~] Test cases execution complete with warning(s)")
    else:
        print(" [~] Test cases execution completed")
    return


def get_input_command():

    example = '''
    [PRE-CONDITION]
    init.py must be executed successfully.

    [USAGE]
        run_thread.py Target
    E.g: ./run_thread.py ALL
    E.g: ./run_thread.py all
    E.g: ./run_thread.py CAN_EXT020_CFG009_E2UH CAN_CRS348_CFG002_E2UH CAN_FIJ013_CFG002_E2UH CAN_FIJ013_CFG003_E2UH
    E.g: ./run_thread.py DIO_EXT020_CFG009_E2UH, DIO_CRS348_CFG002_E2UH, DIO_FIJ013_CFG002_E2UH, DIO_FIJ013_CFG003_E2UH
    '''
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('input_arg', type=str, nargs='+', help='target of make file')
    args = parser.parse_args()

    rawArgList = args.input_arg

    global targetTCList
    global targetOptionList
    targetTCList, targetOptionList = resolve_target_config(rawArgList)
    
def resolve_target_config(_make_args):
    global STAT_FLAG
    global Target_Stream
    global total_tc_run
    TargetList = []
    OptionList = []

    if not os.path.exists("./target_define.mak"):
        os.system("make list")
    with open("./target_define.mak", "r") as read_file:
        Target_Stream = mmap.mmap(read_file.fileno(), 0, access=mmap.ACCESS_READ)
    for make_arg in _make_args:
        target = str(make_arg).replace(',',' ').strip()
        groupCheck = Target_Stream.find(target.upper() + "_GRP_LIST = ")
        groupCheck_Build = Target_Stream.find(target.upper() + "_GRP_LIST_BUILD = ")
        groupCheck_Run = Target_Stream.find(target.upper() + "_GRP_LIST_RUN = ")
        singleCheck = Target_Stream.find(target + ":")
        if (groupCheck == -1) and (singleCheck == -1):
            STAT_FLAG = 1  # Warning
            print(" [~] Warning: Rule for {} doesn't exist".format(target))
            OptionList.append(target)
        else:
            if groupCheck != -1:
                # groupEndIndex = groupCheck + \
                #     Target_Stream[groupCheck:].index("\n")
                # groupString = Target_Stream[groupCheck:groupEndIndex]
                # TargetList.extend(
                #     re.sub(' +', ' ', groupString.split("=")[1].strip()).split(" "))
                tmp_arr = read_group(groupCheck, Target_Stream)
                TargetList.extend(tmp_arr)
            elif groupCheck_Build != -1 and groupCheck_Run != -1 :
                if groupCheck_Build != -1:
                    tmp_arr = read_group(groupCheck_Build, Target_Stream)
                    TargetList.extend(tmp_arr)
                if groupCheck_Run != -1:
                    tmp_arr = read_group(groupCheck_Run, Target_Stream)
                    TargetList.extend(tmp_arr)

                # additional check for RCAR:
                build_list = [data for data in TargetList if '_build' in data]
                run_list = [data for data in TargetList if '_run' in data]
                
                if len(build_list) != len(run_list):
                    print("parser error due to len is diff")
                    sys.exit(-1)

                TargetList = [str(data).replace('_build','') for data in TargetList if '_build' in data]

            elif singleCheck != -1:
                TargetList.append(target)

    if DEBUG_FLAG:
        print(" [~] Target List:")
        for i in range(0, len(TargetList), 4):
            print(TargetList[i:i+4])

    total_tc_run = len(TargetList)
    if 0 == total_tc_run:
        print("ERROR: Can not resolve target")
        sys.exit(-1)

    return TargetList, OptionList

def read_group(group_find_index, group_stream):
    groupEndIndex = group_find_index + group_stream[group_find_index:].index("\n")
    groupString = group_stream[group_find_index:groupEndIndex]
    return re.sub(' +', ' ', groupString.split("=")[1].strip()).split(" ")



def run_command(command, printCommand=False, printOutput=False, plainShell=True, asynch=False):
# def run_command(command, printCommand=False, printOutput=True, plainShell=True, asynch=False):
    """To run cmd command

    Arguments:
        command {string} -- the expected command line

    Returns:
        string -- the output or the error from cmd
    """
    if DEBUG_FLAG:
        printCommand = True
        printOutput = True
    if printCommand:
        print(command)
    command_list = command if plainShell else shlex.split(command)
    process = \
        subprocess.Popen(command_list, shell=plainShell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if asynch:
        return process
    else:
        while process.poll() is None:
            stdout, stderr = [None if not std else "".join(std) \
                for std in [process.stdout.readlines(),process.stderr.readlines()]]
            if stdout is not None and printOutput:
                print(stdout)
            if stderr is not None:
                if printOutput:
                    print(stderr)
                return False
    return True

def run_command_check(process, printOutput=False):
    stdout, stderr = [None if not std else "".join(std) \
        for std in process.communicate()[:2]]
    if stdout is not None and printOutput:
        print(stdout)
    if stderr is not None:
        if printOutput:
            print(stderr)
        return False
    return True

def build_target_object(thread_id, out_q, mutex):
    global targetTCList
    global cur_tc_index

    end_of_thread = False
    # print("START THREAD: ", thread_id)
    previousTime = time.time()
    while not FORCE_STOP:
        # if DEBUG_FLAG:
        #     currentTime = time.time()
        #     if (currentTime - previousTime) > 1:
        #         previousTime = currentTime
        #         print("IN-BUILD: Number of Target not build: " +
        #               str(len(targetTCList)))
        # with mutex:
        mutex.acquire()
        if total_tc_run <= cur_tc_index:
            end_of_thread = True
        else:
            target = targetTCList[cur_tc_index]
            cur_tc_index += 1
        mutex.release()

        if end_of_thread:
            break
        # buildCommand = get_build_cmd_tc(target)
        buildCommand = "make " + target + '_build'

        print(CompilingNotification.format(target))
        run_command(buildCommand)
        out_q.put(target)


def get_build_cmd_tc(tc_name, make_par=False):
    global Target_Stream

    buildCommand=""
    if make_par:
        buildCommand = "make -j {} ".format(MAX_NO_CPU)
    else:
        buildCommand = "make "
    if Target_Stream.find("EvalEnvelope/{}/log/$(MULTI_LOG)".format(tc_name)) != -1 :
        # This target is debugger log check and the make shall stop at build only
        buildCommand += "EvalEnvelope/{}/obj/target.out".format(tc_name)
    elif Target_Stream.find("EvalEnvelope/{}/log/$(BUILD_LOG)".format(tc_name)) != -1 :
        buildCommand += "EvalEnvelope/{}/log/Build.log".format(tc_name)
    else :
        buildCommand = 'echo "Can not determine test case"'
    return buildCommand

def judgement_tc(in_q, make_par=False):
    global targetTCList
    global TargetExecutingList
    global total_tc_run

    if DEBUG_FLAG:
        previousTime = time.time()
    while not FORCE_STOP:
        tc_target = in_q.get()
        # if DEBUG_FLAG:
        #     currentTime = time.time()
        #     if (currentTime - previousTime) > 1:
        #         previousTime = currentTime
        #         print("IN-EXECUTION: Number of Target need to be built: " +
        #               str(len(TargetExecutingList)))
        print(JudgementNotification.format(tc_target))
        # if make_par:
        #     run_command("make -j {}".format(tc_target))
        # else:
        #     run_command("make {}".format(tc_target))
        if make_par:
            run_command("make -j {}".format(tc_target + '_run'))
        else:
            run_command("make {}".format(tc_target + '_run'))
        TargetExecutingList.append(tc_target)
        if total_tc_run <= len(TargetExecutingList):
            print("End of judgement")
            break

def make_target(_num_of_thread_build):
    tc_thread_list = []

    tc_queue = Queue.Queue()
    mutex = Lock()

    # Build target object
    # t1 = threading.Thread(target=build_target_object, args = (0, tc_queue, mutex))
    # t1.start()
    for cnt in range(_num_of_thread_build):
        t = threading.Thread(target=build_target_object, args = (cnt, tc_queue, mutex))
        t.start()
        tc_thread_list.append(t)
        time.sleep(1)

    # Start new thread to check status of target list and execute TCs on board
    t2 = threading.Thread(target=judgement_tc, args = (tc_queue, ))
    t2.start()

    global targetTCList
    global TargetExecutingList
    global total_tc_run
    while total_tc_run > len(TargetExecutingList) and not FORCE_STOP:
        # loop in main thread
        pass

    # Confirm thread joint
    # t1.join()
    t2.join()
    for th in tc_thread_list:
        th.join()


def main():
    """main function of the program

    Returns:
        int -- status of the main function. 1 is successful
    """
    get_input_command()
    make_target(CONFIG_NUM_OF_THREAD_BUILD)
    print_status(True)


if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
