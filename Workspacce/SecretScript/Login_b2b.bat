@ECHO OFF
SET PULSESECUREPATH="C:\Program Files (x86)\Common Files\Pulse Secure\Integration"
SET USER_PATH=C:\Users\tra.nguyen-thanh.BANVIENCORP
rem SET PATH="C:\Users\tra.nguyen-thanh\Anaconda3";"C:\Users\tra.nguyen-thanh\Anaconda3\Scripts";%PATH%
rem SET PATH="C:\Users\tra.nguyen-thanh\Anaconda3\Library\bin";"C:\Users\tra.nguyen-thanh\Anaconda3\Library\usr\bin";%PATH%
rem SET PATH="C:\Users\tra.nguyen-thanh\Anaconda3\Library\mingw-w64\bin";"C:\Users\tra.nguyen-thanh\Anaconda3\bin";%PATH%
rem SET PATH="C:\Users\tra.nguyen-thanh\Anaconda3\condabin";%PATH%
SET PATH=%USER_PATH%\Anaconda3;%USER_PATH%\Anaconda3\Library\mingw-w64\bin;%USER_PATH%\Anaconda3\Library\usr\bin;%USER_PATH%\Anaconda3\Library\bin;%USER_PATH%\Anaconda3\Scripts;%USER_PATH%\Anaconda3\bin;%USER_PATH%\Anaconda3\condabin;%PATH%

SET SMX_LOGIN_APP="%USER_PATH%\Anaconda3\Scripts\smxlogin.exe"
SET USERNAME=a5124170
SET PATTERN=a1b1c1d1e2f2g2h2i4j4k4l4m3n3o3p3

Setlocal EnableDelayedExpansion
set LF=^


rem Two empty lines are required here
rem set greeting=###""###!LF!^
rem ###""###!LF!^
rem ###"                                                                                                                      "###!LF!^
rem ###"________            ________            ____         _   __________ __________ ____              ___  _  ____     ___ "###!LF!^
rem ###"`MMMMMMMb.          `MMMMMMMb.         6MMMMb/      dM.  MMMMMMMMMM `MMMMMMMMM `Mb(      db      )d' dM. `MM(     )M' "###!LF!^
rem ###" MM    `Mb           MM    `Mb        8P    YM     ,MMb  /   MM   \  MM      \  YM.     ,PM.     ,P ,MMb  `MM.    d'  "###!LF!^
rem ###" MM     MM   ____    MM     MM       6M      Y     d'YM.     MM      MM         `Mb     d'Mb     d' d'YM.  `MM.  d'   "###!LF!^
rem ###" MM    .M9  6MMMMb   MM    .M9       MM           ,P `Mb     MM      MM    ,     YM.   ,P YM.   ,P ,P `Mb   `MM d'    "###!LF!^
rem ###" MMMMMMM(  MM'  `Mb  MMMMMMM(        MM           d'  YM.    MM      MMMMMMM     `Mb   d' `Mb   d' d'  YM.   `MM'     "###!LF!^
rem ###" MM    `Mb      ,MM  MM    `Mb       MM     ___  ,P   `Mb    MM      MM    `      YM. ,P   YM. ,P ,P   `Mb    MM      "###!LF!^
rem ###" MM     MM     ,MM'  MM     MM       MM     `M'  d'    YM.   MM      MM           `Mb d'   `Mb d' d'    YM.   MM      "###!LF!^
rem ###" MM     MM   ,M'     MM     MM       YM      M  ,MMMMMMMMb   MM      MM            YM,P     YM,P ,MMMMMMMMb   MM      "###!LF!^
rem ###" MM    .M9 ,M'       MM    .M9        8b    d9  d'      YM.  MM      MM      /     `MM'     `MM' d'      YM.  MM      "###!LF!^
rem ###"_MMMMMMM9' MMMMMMMM _MMMMMMM9'         YMMMM9 _dM_     _dMM__MM_    _MMMMMMMMM      YP       YP_dM_     _dMM__MM_     "###!LF!^
rem ###"                                                                                                                      "###!LF!^
rem ###"                                                                                                                      "###!LF!^
rem ###""###!LF!^
rem ###""###

set greeting=###""###!LF!^
###""###!LF!^
###"o--o   --  o--o       o-o    O  o-O-o o--o o       o   O  o   o "###!LF!^
###"|   | o  o |   |     o      / \   |   |    |       |  / \  \ /  "###!LF!^
###"O--o    /  O--o      |  -o o---o  |   O-o  o   o   o o---o  O   "###!LF!^
###"|   |  /   |   |     o   | |   |  |   |     \ / \ /  |   |  |   "###!LF!^
###"o--o  o--o o--o       o-o  o   o  o   o--o   o   o   o   o  o   "###!LF!^
###""###!LF!^
###""###

set "greeting=!greeting:###"=!"
set "greeting=!greeting:"###=!"
echo !greeting!

rem pause

%SMX_LOGIN_APP% -v --password -u %USERNAME% -p %PATTERN% https://b2b-gateway.renesas.com/login/ssl > Output

rem pause

SET /p PASSWORD=<Output

%PULSESECUREPATH%\pulselauncher.exe -url b2b-portal.renesas.com -u %USERNAME% -p %PASSWORD% -r Users
del /f Output
