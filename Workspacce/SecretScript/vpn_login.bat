rem START /B vpn\vpn_login_script.bat
@echo off
Setlocal EnableDelayedExpansion
set LF=^


rem Two empty lines are required here
set greeting=###""###!LF!^
###"  ______                                                          __     __  _______   __    __ "###!LF!^
###" /      \                                                        /  |   /  |/       \ /  \  /  |"###!LF!^
###"/$$$$$$  |  _______   _______   ______    _______  _______       $$ |   $$ |$$$$$$$  |$$  \ $$ |"###!LF!^
###"$$ |__$$ | /       | /       | /      \  /       |/       |      $$ |   $$ |$$ |__$$ |$$$  \$$ |"###!LF!^
###"$$    $$ |/$$$$$$$/ /$$$$$$$/ /$$$$$$  |/$$$$$$$//$$$$$$$/       $$  \ /$$/ $$    $$/ $$$$  $$ |"###!LF!^
###"$$$$$$$$ |$$ |      $$ |      $$    $$ |$$      \$$      \        $$  /$$/  $$$$$$$/  $$ $$ $$ |"###!LF!^
###"$$ |  $$ |$$ \_____ $$ \_____ $$$$$$$$/  $$$$$$  |$$$$$$  |        $$ $$/   $$ |      $$ |$$$$ |"###!LF!^
###"$$ |  $$ |$$       |$$       |$$       |/     $$//     $$/          $$$/    $$ |      $$ | $$$ |"###!LF!^
###"$$/   $$/  $$$$$$$/  $$$$$$$/  $$$$$$$/ $$$$$$$/ $$$$$$$/            $/     $$/       $$/   $$/ "###!LF!^
###""###!LF!^
###""###


rem set greeting=###"This is a "###!LF!^
rem ###"multiline text\|/$$$ "###!LF!^
rem ###"line3"###
set "greeting=!greeting:###"=!"
set "greeting=!greeting:"###=!"
echo !greeting!






rem rem echo "a\|/$$$aaa"^

rem rem "bbb\|/$$$bbb"^

rem rem "ccc"

rem goto :eof

rem :dequote
rem setlocal
rem rem The tilde in the next line is the really important bit.
rem set thestring=%~1
rem endlocal&set ret=%thestring%
rem goto :eof



:eof
rem pause                                                                                               
CALL "CMD /C START vpn\vpn_login_script.bat"

sleep(5)
