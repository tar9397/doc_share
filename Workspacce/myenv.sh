#!/usr/bin/bash
MY_UTIL_SHELL_PATH=C:/Workspace/ShellScript
MY_UTIL_PYTHON_PATH=C:/Workspace/PythonScript
MY_PROGRAM_PATH=C:/Workspace/Program
# cd $MY_UTIL_PATH
# export PATH=$PATH:$PWD
# cd -
export PATH=$PATH:$(cygpath -u ${MY_UTIL_SHELL_PATH})
export PATH=$PATH:$(cygpath -u ${MY_UTIL_PYTHON_PATH})
export PATH=$PATH:$(cygpath -u ${MY_PROGRAM_PATH})
