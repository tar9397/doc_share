#!/bin/bash

GET_PID32() {
  BIN_PATH=$1
  ps -W | tr '\\' '/' | grep -i "$BIN_PATH" | gawk 'BEGIN {
    cnt=0;
  } {
    flg=1
    for(i = 0;i<cnt; i++){
      if(tmp_array[i]==$1){
        flg=0
      }
    }
    tmp_array[cnt]=$1
    if(flg == 1){
      array[cnt++]=$1
    }
} END {
    for( i=cnt-1; i>=0; --i ) {
      print array[i]
    }
  }'
}

for i; do
  echo "kill process: " $i
  GET_PID32 $i | while read i; do
    echo "PID:" $i
    /usr/bin/kill -f -2 "$i" >/dev/null 2>&1
  done
done

