#!/usr/bin/sh

# input 1: path to device 
# input 2: msn name

#list='"item 1" "item 2" "item 3"'
listMO="adc can dio eth fls fr gpt icu lin mcu port pwm spi wdg"

path=$(cygpath -w $1)
msn=$2
msn=${msn^^}

echo "Input path: " $path  

list_device=`ls $path`

if [[ -z $msn ]]; then
    echo "Update all is not permited"
    exit 1
fi

for device in $list_device; do
    echo "Found device: " $device  
    echo "********************************************************************************"
    echo "update: $device $msn"

    new_path="$path\\$device\\$msn" # because path is window path
    echo "update path : $new_path"
    svn update $new_path
done



# for msn in $listMO; do
#     if [[ $path =~ \\$msn\\ ]]; then
#         echo "Found module: " $msn  
#         cur_msn=$msn
#         break
#     fi
# done

# if [[ -z $cur_msn ]]; then
#     echo "Invalid Path "
#     exit 1
# fi

# for msn in $listMO; do
#     echo "********************************************************************************"
#     echo "update: $msn"
#     new_path=$(echo "$path" | sed "s/$cur_msn/$msn/")
#     # echo "update path : $new_path"
#     # svn update U:/internal/Module/$msn/08_IT/01_WorkProduct_D/integration_test/result
#     # svn update U:/internal/Module/$msn/08_IT/02_PeerReview
#     # svn update "$(cygpath -w $new_path)"
#     svn update $new_path
#     # ls -al $new_path/coverage_config
#     ls -al $new_path
#     # grep --color=always -rns "cpucore" $new_path
# done