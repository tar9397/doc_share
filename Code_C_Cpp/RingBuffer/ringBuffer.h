#ifndef __RINGBUFS_H    
#define __RINGBUFS_H    

#define RBUF_SIZE               256
#define CONF_OVERLAP_ENABLE     1

typedef struct  ringBufS    {      
    uint8_t buf[RBUF_SIZE];      
    uint8_t head;      
    uint8_t tail;      
    uint8_t count;    
} ringBufS;    

#ifdef  __cplusplus      
extern  "C" {    
#endif      

void ringBufS_init (ringBufS *_this);      
bool ringBufS_isEmpty (ringBufS *_this);      
bool ringBufS_isFull (ringBufS *_this);      
uint8_t ringBufS_get (ringBufS *_this);
uint8_t ringBufS_peek (ringBufS *_this);  
void ringBufS_put (ringBufS *_this, const uint8_t c);      
void ringBufS_flush (ringBufS *_this, const int clearBuffer);    
void ringBufS_peekSize (ringBufS *_this, const uint8_t size_peek, uint8_t * p_data);
void ringBufS_getSize (ringBufS *_this, const uint8_t size_get, uint8_t * p_data);


#ifdef  __cplusplus      
}    
#endif

#endif
