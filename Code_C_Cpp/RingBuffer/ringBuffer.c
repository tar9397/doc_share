#include  #include  "ringBuffer.h"

static uint8_t modulo_inc (uint8_t pos, uint8_t size) {
    if (++pos >= size)
        return 0;
    else
        return pos;
}



void ringBufS_init (ringBufS *_this) {    
    /*****      The following clears:        -> buf        -> head        -> tail        -> count      and sets head = tail    ***/    
    memset (_this, 0, sizeof (*_this));
}

bool ringBufS_isEmpty (ringBufS *_this) {    
    return (0 == _this->count);
}

bool ringBufS_isFull (ringBufS *_this) {
    return (RBUF_SIZE <= _this->count);
}   

uint8_t ringBufS_get (ringBufS *_this) {
    uint8_t c;    
    if (_this->count > 0) {
        c = _this->buf[_this->tail];
        _this->tail = modulo_inc (_this->tail, RBUF_SIZE);
        --_this->count;
    }

    return (c);
}

uint8_t ringBufS_peek (ringBufS *_this) {
    return _this->buf[_this->tail];
}

void ringBufS_put (ringBufS *_this, const uint8_t c) {
#if (1 == CONF_OVERLAP_ENABLE)
    // enable overlap 
    _this->buf[_this->head] = c;      
    _this->head = modulo_inc (_this->head, RBUF_SIZE);      
    ++_this->count;
#else
    if (_this->count < RBUF_SIZE) {      
        _this->buf[_this->head] = c;      
        _this->head = modulo_inc (_this->head, RBUF_SIZE);      
        ++_this->count;    
    }
#endif
}

void ringBufS_flush(ringBufS *_this, const int clearBuffer) {
    _this->count  = 0; 
    _this->head   = 0;  
    _this->tail   = 0;  
    if (clearBuffer) {   
        memset (_this->buf, 0, sizeof (_this->buf)); 
    }
}

void ringBufS_peekSize (ringBufS *_this, const uint8_t size_peek, uint8_t * p_data) {
    uint8_t tmp_tail = _this->tail;
    for (uint8_t i=0; i<size_peek; i++ ) {
        p_data[i] = _this->buf[tmp_tail];
        tmp_tail = modulo_inc (tmp_tail, RBUF_SIZE);
    }
}

void ringBufS_getSize (ringBufS *_this, const uint8_t size_get, uint8_t * p_data) {
    for (uint8_t i=0; i<size_peek; i++ ) {
        p_data[i] = ringBufS_get (_this);
    }
}
