/*============================================================================*/
/* Project      = R-CarGen3 AUTOSAR MCAL Development Project                  */
/* Module       = main.c                                                      */
/*============================================================================*/
/*                               COPYRIGHT                                    */
/*============================================================================*/
/* Copyright(c) 2018 Renesas Electronics Corporation                          */
/*============================================================================*/
/* Purpose:                                                                   */
/* This file is a sample file for preparation of testing                      */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Renesas Electronics Corporation the following shall apply!                 */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Renesas. Any        */
/* warranty is expressly disclaimed and excluded by Renesas, either expressed */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Renesas shall not have any obligation to maintain, service or provide bug  */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/* Each User is solely responsible for determining the appropriateness of     */
/* using the Product(s) and assumes all risks associated with its exercise    */
/* of rights under this Agreement, including, but not limited to the risks    */
/* and costs of program errors, compliance with applicable laws, damage to    */
/* or loss of data, programs or equipment, and unavailability or              */
/* interruption of operations.                                                */
/*                                                                            */
/* Limitation of Liability                                                    */
/*                                                                            */
/* In no event shall Renesas be liable to the User for any incidental,        */
/* consequential, indirect, or punitive damage (including but not limited     */
/* to lost profits) regardless of whether such liability is based on breach   */
/* of contract, tort, strict liability, breach of warranties, failure of      */
/* essential purpose or otherwise and even if advised of the possibility of   */
/* such damages. Renesas shall not be liable for any services or products     */
/* provided by third party vendors, developers or consultants identified or   */
/* referred to the User by Renesas in connection with the Product(s) and/or   */
/* the Application.                                                           */
/*                                                                            */
/*============================================================================*/
/* Environment:                                                               */
/*              Devices:        R-Car V3H                                     */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
 * V1.0.0:  25-Jun-2018  : Initial Version
 */
/******************************************************************************/

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
// #include "Dio.h"
// #include "DioTest_Specific.h"
// #include "Lib_Error_Check.h"

/*******************************************************************************
**                      Defines                                               **
*******************************************************************************/

/*******************************************************************************
**                      Global Data                                           **
*******************************************************************************/
// Dio_LevelType gChannel0Level1;
// Dio_LevelType gChannel0Level2;

/*******************************************************************************
**                      Function Definitions                                  **
*******************************************************************************/
int main(void)
{
  // /* Precondition */
  // Mcu_Init();

  // Port_Init();

  // /* Read channel  */
  // gChannel0Level1 = Dio_ReadChannel(DioConf_DioChannel_DioChannel);
  // if (STD_HIGH == gChannel0Level1) 
  // {
  //   Dio_WriteChannel(DioConf_DioChannel_DioChannel, STD_LOW);
  // }
  // else 
  // {
  //   Dio_WriteChannel(DioConf_DioChannel_DioChannel, STD_HIGH);
  // }

  // gChannel0Level2 = Dio_ReadChannel(DioConf_DioChannel_DioChannel);

  // AES_TEST_ASSERT(0u == GstDetBuffIndex && gChannel0Level2 == gChannel0Level1);
  // AES_Report_Result();
} /* End of main() function */

/*******************************************************************************
**                      End of File                                           **
*******************************************************************************/
