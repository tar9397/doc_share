#!/usr/bin/python3
# -*- coding: utf-8 -*-
################################################################################
#                              timer_test.py                                   #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
import concurrent.futures



################################################################################
#                                    Class                                     #
################################################################################




################################################################################
#                                 Function                                     #
################################################################################

# I/O intensive Task
def process_img(filename):
    """
    This method take the filename of the img file, performs a gray scale operation on the file, creates
    a thumbnail of the img and then saves the file in processed folder.
    :param filename: name of img file.
    :return: None
    """

    img = Image.open(f'.\\imgs\\{filename}')
    gray_img = img.convert('L')
    gray_img.thumbnail((200, 400))
    gray_img.save(f'.\\processed\\{filename}')

# Compute Intensive Task
def cpu_intensive_process(iteration):
    """
    This mehtod performs the the cpu extensive operation. It increments the count for each number in range
    of 9^9.
    :param iteration: 
    :return: None
    """

    # print(iteration)
    count = 0
    for i in range(9**9):
        count += i

# Mehtods that perform extensive I/O operations.
@calculate_exe_time
def io_operations_without_concurrency(file_names):
    """
    This method calls the process_img method for each file which performs simple image processing 
    on the img file and then moves the files to processed folder. This is done in a regular fashion 
    without any concurrency.
    :param file_names: list containing names of the img files to process
    :return: None
    """

    for filename in file_names:
        process_img(filename)


@calculate_exe_time
def io_operations_via_threading(file_names):
    """
    This method calls the process_img method for each file which performs simple image processing 
    on the img file and then moves the files to processed folder. This is done in a concurrent fashion
    using the multithreading feature in concurrent.futures module.
    :param file_names: list containing names of the img files to process
    :return: None
    """

    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(process_img, file_names)


@calculate_exe_time
def io_operations_via_multiprocessing(file_names):
    """
    This method calls the process_img method for each file which performs simple image processing 
    on the img file and then moves the files to processed folder. This is done in a concurrent fashion using the multiprocessing feature in 
    concurrent.futures module.
    :param file_names: list containing names of the img files to process
    :return: None
    """

    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.map(process_img, file_names)

################################################################################
#                                    main                                      #
################################################################################

def main():
    """main function of the program"""

    # data = get_port_name("PinConfig.xlsm", "CFG001")
    # # print("result: ",data)
    
    # text = create_function_str(data)
    # print('*'*80)
    # print(text)
    # with MyTimerFull(name="context manager"):
    #     print("Test")
    #     print('*'*80)
    pass

if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################