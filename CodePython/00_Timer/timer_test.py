#!/usr/bin/python3
# -*- coding: utf-8 -*-
################################################################################
#                              timer_test.py                                   #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
# from mytimer import MyTimer
from mytimer import *

################################################################################
#                                    Class                                     #
################################################################################




################################################################################
#                                    main                                      #
################################################################################

def main():
    """main function of the program"""

    # data = get_port_name("PinConfig.xlsm", "CFG001")
    # # print("result: ",data)
    
    # text = create_function_str(data)
    # print('*'*80)
    # print(text)
    with MyTimerFull(name="context manager"):
        print("Test")
        print('*'*80)


if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################