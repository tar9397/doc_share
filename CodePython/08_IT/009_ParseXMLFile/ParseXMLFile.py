#!/usr/bin/python3 -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = Stub                                                          #
# Module       = ParseXMLFile.py                                               #
# Version      = 1.01                                                          #
# Date         = 23-Sep-2020                                                   #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This script is used to parse xml file                                        #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  20-Jan-2019     : Initial Version                                    #
# V1.01:  15-Aug-2020     : Update template for U2A16 release                  #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------
from __future__ import print_function
from xlrd import open_workbook
from copy import copy
# import openpyxl
import subprocess
import shlex
import sys
import re
import os
import datetime
import json
import argparse
# import pandas as pd

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------


# -----------------------------------------------------------
# constant section
# -----------------------------------------------------------
# Load the data base stored in the file to the program


# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------




def main():
    """main function of the program

    Returns:
        None
    """
    
    print("*"*120)
    sum_data = []

    from bs4 import BeautifulSoup as bs
    content = []
    # Read the XML file
    with open("input.resx", "r", encoding="utf8") as reader:
        # Read each line in the file, readlines() returns a list of lines
        content = reader.readlines()
        # Combine the lines in the list into a string
        content = "".join(content)
        bs_content = bs(content, "lxml")
    result = bs_content.find_all("data") # get all tag 'data'
    for data in result:
        print("*"*80)
        # print(data)
        print(data.get("name")) # get attribute name in tag 'data'
        print(data.text)
        





    # with open('input.txt') as reader:
    #     data_read = reader.readlines()
    # for data in data_read:
    #     element_id_group = re.search(element_parttern, data)
    #     if element_id_group:
    #         element_id = element_id_group.group(1)
    #         # print(element_id)
    #         continue
    #     if found_child_value_in_next_line:
    #         # print('process: ',data)
    #         child_value_pattern_search = re.search(child_value_pattern, data)
    #         if child_value_pattern_search:
    #             # print(dir(child_value_pattern_search))
    #             # print(child_value_pattern_search.groups())
    #             # sys.exit()
    #             # child_value = child_value_pattern_search.group(1)
    #             child_value = child_value_pattern_search.groups()[-1]
    #             # result = parent_name + '[' + str(element_id) +'].' + found_child_value_in_next_line + ' == ' + child_value
    #             result = result_template.format(parent_name, str(element_id), found_child_value_in_next_line, child_value)
    #             sum_data.append(result)
    #             found_child_value_in_next_line = None
    #             continue
    #     for id_name, id_parttern in child_parttern_dict.items():  
    #         if re.search(id_parttern, data):
    #             found_child_value_in_next_line = id_name
    #             break


    # print('result')
    # for index, data in enumerate(sum_data):
    #     # print("Element {0} has : {1}".format(index, data))
    #     # print("Count element {0}".format(len(data)))
    #     print(data)
    
    


# main processing
if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
