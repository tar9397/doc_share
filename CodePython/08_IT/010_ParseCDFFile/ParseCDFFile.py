#!/usr/bin/python3 -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = Stub                                                          #
# Module       = ParseXMLFile.py                                               #
# Version      = 1.01                                                          #
# Date         = 23-Sep-2020                                                   #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This script is used to parse xml file                                        #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  20-Jan-2019     : Initial Version                                    #
# V1.01:  15-Aug-2020     : Update template for U2A16 release                  #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------
from __future__ import print_function
from xlrd import open_workbook
from copy import copy
# import openpyxl
import subprocess
import shlex
import sys
import re
import os
import datetime
import json
import argparse
# import pandas as pd

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------


# -----------------------------------------------------------
# constant section
# -----------------------------------------------------------
# Load the data base stored in the file to the program
tags_names = ['ECUC-CONTAINER-VALUE','DEFINITION-REF','ECUC-NUMERICAL-PARAM-VALUE']
end_nodes = ['ECUC-NUMERICAL-PARAM-VALUE','ECUC-TEXTUAL-PARAM-VALUE']
valid_child_node = ['DEFINITION-REF','VALUE']

# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------




def main():
    """main function of the program

    Returns:
        None
    """
    
    # print("*"*120)
    sum_data = []

    from bs4 import BeautifulSoup as bs
    from bs4 import NavigableString, Tag

    content = []
    # Read the XML file
    with open("cdf.arxml", "r", encoding="utf8") as reader:
        # # Read each line in the file, readlines() returns a list of lines
        # content = reader.readlines()
        # # Combine the lines in the list into a string
        # content = "".join(content)
        content = reader.read()
        bs_content = bs(content, "lxml")


    



#     containers = bs_content.find('CONTAINERS'.lower())
#     for child_node in list(containers.children):
#         if isinstance(child_node, Tag):
#             # print(child_node.name)
#             if (tags_names[0].lower() == child_node.name):
#                 tmp_data = parse_child_node(child_node)
#             print("*"*40)
#             print(tmp_data)
#             break
#     # for tag in bs_content.find('CONTAINERS'.lower()).contents:
#     #     print(tag.name)

# def parse_child_node(parent_node):
#     from bs4 import NavigableString, Tag
#     tag = {}
#     tag['data'] = []
#     tag['parent'] = []

#     # if 0 == len(list(parent_node.children)):
#     #     return

#     # print(list(parent_node.children))
#     # sys.exit()
    

#     for child_node in list(parent_node.children):
#         # if isinstance(child_node, NavigableString):
#         #     print(child_node)
#         #     print(len(child_node))
#         #     if 1 == len(child_node):
#         #         print(ord(child_node[0]))
#         # el
#         if '\n' == child_node:
#             continue

#         tmp = check_data_tag(child_node)
#         if tmp:
#             tag['data'].append(tmp)
#             continue 



#         if isinstance(child_node, Tag):
#             # print("*"*10)
#             # print(child_node)
#             # print("*"*10)
#             tmp_node = parse_child_node(child_node)
#             tag['parent'].append(tmp_node)
#             # print("*"*0)
#         else:
            

#             print("*"*20)
#             # if isinstance(child_node, NavigableString):
#             #     print(child_node)
#             # else:
#             #     print(child_node.name)
#             print(child_node)
#             print(type(child_node))
#             # sys.exit()
#     return tag

# def check_data_tag(tag_obj):
#     for valid_node in valid_child_node:
#         if valid_node.lower() == tag_obj.name:
#             # print(f"end node: {tag_obj.text}")
#             # print("*"*20)
#             return tag_obj.text
#     return None


    # for containers in bs_content.find_all(tags_names[0].lower()):
    #     print(containers)
    #     print("*"*40)








    for containers in bs_content.find_all(tags_names[0].lower()):
        # print(containers)
        # print("*"*40)

        container_name = containers.find_all(tags_names[1].lower(), recursive=False)
        # print(container_name)
        # print("*"*20)
        
        container_data = containers.find_all(tags_names[2].lower())
        # print(container_data)
        # print("*"*20)
        

        # Only 1in ECUC-CONTAINER-VALUE
        if 1 != len(container_name):
            print("container invalid !!! ", len(container_name))
            sys.exit(1)

        print("*"*80)
        print('Process: ',container_name[0].text)
        print("*"*80)
        for element_data in container_data:
            # print(element_data)
            for child_node in element_data.children:
                if isinstance(child_node, Tag): # ignore invalid tag (Ex: line ending ...)
                    print(child_node.text)











        # for contain_data in containers.find_all(tags_names[1].lower(), recursive=False):
        # # for contain_data in containers.find_all(tags_names[1].lower()):
        #     print(contain_data)
        #     print("*"*10)
        #     elements_data = contain_data.find_all(tags_names[2].lower())
        #     # print(elements_data)
        #     for element_data in elements_data:
        #         # print(element_data)
        #         for child_node in element_data.children:
        #             if isinstance(child_node, Tag): # ignore invalid tag (Ex: line ending ...)
        #                 print(child_node.text)









   
    


# main processing
if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
