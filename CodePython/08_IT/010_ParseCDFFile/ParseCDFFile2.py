#!/usr/bin/python3 -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = Stub                                                          #
# Module       = ParseXMLFile.py                                               #
# Version      = 1.01                                                          #
# Date         = 23-Sep-2020                                                   #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This script is used to parse xml file                                        #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  20-Jan-2019     : Initial Version                                    #
# V1.01:  15-Aug-2020     : Update template for U2A16 release                  #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------
from __future__ import print_function
from xlrd import open_workbook
from copy import copy
# import openpyxl
import subprocess
import shlex
import sys
import re
import os
import datetime
import json
import argparse
# import pandas as pd

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------
file_name = ''

# -----------------------------------------------------------
# constant section
# -----------------------------------------------------------
# Load the data base stored in the file to the program
# tags_names = ['ECUC-CONTAINER-VALUE','DEFINITION-REF','ECUC-NUMERICAL-PARAM-VALUE']
# end_nodes = ['ECUC-NUMERICAL-PARAM-VALUE','ECUC-TEXTUAL-PARAM-VALUE']
para_defs = ['DEFINITION-REF']
para_vals = ['VALUE','VALUE-REF']
type_items = ['PARAMETER-VALUES','REFERENCE-VALUES']

# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------
class CDFContainer():
    def __init__(self, shortName, daddy=None):
        self.ShortName = shortName
        self.Daddy = daddy

        self.Kids = []
        self.Items = []

    def get_path(self):
        path = self.ShortName
        daddy = self.Daddy
        while daddy != None:
            path = daddy.ShortName + "/" + path
            daddy = daddy.Daddy
        return path

    def print_indent(self, indent, message):
        print("\t"*indent + message)

    def print_container_tree(self, indent=1):
        self.print_indent(indent, self.ShortName)
        indent += 1
        for item in self.Items:
            self.print_indent(indent, "{}: {}".format(item.Name, item.Value))
        for kid in self.Kids:
            kid.print_container_tree(indent=indent)
        return    

class CDFItem():
    def __init__(self, name, defRef, value, daddy=None):
        self.Name = name
        self.DefRef = defRef
        self.Value = value
        self.Daddy = daddy

    def get_path(self):
        path = self.Name
        daddy = self.Daddy
        while daddy != None:
            path = daddy.Name + "/" + path
            daddy = daddy.Daddy
        return path



def entry():
    """Start application"""
    example = '''
    [Description]
        Read CDF Info structure
    [Start application]
    E.g: ./ParseCDFFile2.py file_name
    '''
    parser = argparse.ArgumentParser(description=example)
    # parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
    #                     help='valid target module: Adc, Can, Dio ...')
    parser.add_argument('file_name', type=str, help='file with extension', default=None )
    # parser.add_argument('sheet_name', type=str, nargs='?', help='sheet of file', default='Sheet1' )
    args = parser.parse_args()

    global file_name

    file_name = str(args.file_name) 
    # sheet_name = str(args.sheet_name) 

    # output_location = str(args.opl) + "output.xlsx"

    # return True

def main():
    """main function of the program

    Returns:
        None
    """

    entry()

    # print("*"*120)
    sum_data = []

    from bs4 import BeautifulSoup as bs
    from bs4 import NavigableString, Tag

    content = []
    # Read the XML file
    with open(file_name, "r", encoding="utf8") as reader:
    # with open("cdf.arxml", "r", encoding="utf8") as reader:
        # # Read each line in the file, readlines() returns a list of lines
        # content = reader.readlines()
        # # Combine the lines in the list into a string
        # content = "".join(content)
        content = reader.read()
        bs_content = bs(content, "lxml")



    root_container_xml = bs_content.find("ECUC-MODULE-CONFIGURATION-VALUES".lower())
    short_Name_Msn = root_container_xml.find("SHORT-NAME".lower()).text
    cdf_root_container = CDFContainer(short_Name_Msn)



    # containers = bs_content.find('CONTAINERS'.lower())
    for child_node in list(root_container_xml.find('CONTAINERS'.lower()).children):
        if isinstance(child_node, Tag):
            # print(child_node.name)
            # if (tags_names[0].lower() == child_node.name):
            child_container = parse_container_node(child_node)
            if child_container is not None:
                cdf_root_container.Kids.append(child_container)
        # print("*"*40)
            # print(child_container)
            # break

    print('*'*80)
    cdf_root_container.print_container_tree()
    print('*'*80)


def parse_container_node(parent_node):
    short_name = parent_node.find("SHORT-NAME".lower()).text
    cdf_container = CDFContainer(short_name)
    get_items_in_container(parent_node, cdf_container)

    # Parsing kid containers
    sub_containers = parent_node.find("SUB-CONTAINERS".lower(), recursive=False)
    if sub_containers is not None:
        for container_xml in sub_containers.find_all("ECUC-CONTAINER-VALUE".lower(), recursive=False):
            # print('='*60)
            # print(container_xml)
            # print('='*60)
            child_container = parse_container_node(container_xml)
            if child_container is not None:
                child_container.Daddy = cdf_container
                cdf_container.Kids.append(child_container)
    # else:
    #     get_items_in_container(parent_node, cdf_container)
    
    # get_items_in_container(parent_node, cdf_container)

    return cdf_container



def get_items_in_container(xml_node, cdf_container):
    for type_item in type_items:
        # print('-'*20)
        # print(xml_node)
        # print('-'*20)    

        # tmp = xml_node.find(type_item.lower())
        # if tmp is None :
        #     continue
        
        tmp = xml_node.find_all(type_item.lower(), recursive=False)
        if len(tmp) > 0:
            
            for para_node in xml_node.find(type_item.lower()).children:
                if '\n' == para_node:
                    continue
                def_para, val_para = get_para_node(para_node)
                name = def_para.split('/')[-1]
                cdf_item = CDFItem(name, def_para, val_para, daddy=cdf_container)
                cdf_container.Items.append(cdf_item)

def get_para_node(para_node):
    # print('='*40)
    # print(para_node)
    # print('='*40)
    def_para = para_node.find(para_defs[0].lower()).text
    for para_val in para_vals: # fix for SPI
        tmp = para_node.find(para_val.lower())
        if tmp:
            val_para = tmp.text
            return def_para, val_para


    # for containers in bs_content.find_all(tags_names[0].lower()):
    #     print(containers)
    #     print("*"*40)




   
    


# main processing
if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
