#!/usr/bin/python3 -B
# -*- coding: utf-8 -*-

import distutils.core
import Cython.Build
distutils.core.setup(
    ext_modules = Cython.Build.cythonize("test_cython.pyx", language_level = "3")
)
