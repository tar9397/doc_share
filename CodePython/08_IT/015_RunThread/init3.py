#!/usr/bin/python3 -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = MCAL Automatic Evaluation System                              #
# Module       = init.py                                                       #
# Version      = 2.00                                                          #
# Date         = 30-Oct-2020                                                   #
# AES Version  = 2.2.0(Official Version)                                       #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019-2020 Renesas Electronics Corporation. All rights reserved. #
################################################################################
# Purpose:                                                                     #
# This file does copy necessary files, directories for initializing workspace  #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  05-Nov-2019     : Initial Version                                    #
# V2.00:  30-Oct-2020     : Convert to python3 and openpyxl to xlrd, xlwt.     #
################################################################################

# ------------------------------------------------------------------------------
# include section
# ------------------------------------------------------------------------------

import argparse
import datetime
import json
import os
import re
import shlex
import shutil
import subprocess
import sys
import threading
import time
import multiprocessing
from pathlib import Path, PurePath

from openpyxl import load_workbook, Workbook

# ------------------------------------------------------------------------------
# version definition section
# ------------------------------------------------------------------------------

# Tool version
INITIALIZATION_VER = "2.00"

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------

# Msn = ""   # MCAL Msn
# msn = ""   # MCAL msn
# MSN = ""   # MCAL MSN
# TestType = ""  # DIT, TIT, VT
# TestDevice = ""  # 702300EBBG, 702300EBBB..
# ARVersion = ""  # 4_2_2, 4_3_1
# SHORT_ARVersion = ""  # R4.2.2, R4.3.1
# OSTest = ""  # OS, No_OS, None
# OSMultiTest = ""  # OS_Multi, No_OS_Multi
# TestDeviceName = ""  # U2A16, E2UH, E2H, U2B
# Target_cfg_list = [] # ["CFG001", "CFG002", RC001..]
# Valid_Cfg_Name = ["CFG", "RC", "DEF"]
# Spec_Cfg_Label = ["CFGxxx", "CFGnnn", "RCnnn", "DEFnnn", "xCFGnnn", "xRCnnn", "xDEFnnn"]
# Diff_HwIP_Msn = ["ADC", "ETH", "GPT", "ICU", "PWM", "SPI"]
# s0s1 = ""
# keep_tc = ""
# ignore_error = ""
# build_option = ""
# accepted_warning_list = []
# ExeOp = False
# RC_Cat_Map = {}
# Cat_OS_Map = {"No_OS": "CAT1", "OS": "CAT2", "None": ""}

start_time = time.time()

# # TRXML file as naming rule and fixed location
# trxml = "../03_stub/common/Test_Application_X2x.trxml"

# # switch generic gentool application name follwing used platform
# gentoo_exe = "MCALConfGen" if 'linux' == sys.platform else "MCALConfGen.exe"

# DEBUG_SCRIPT = False

# # Max number of column of Test Spec without categorization
# MAX_COL_SPEC = 45

# Cfg_dict = dict()
# Cfg_tc_dict = dict()

# # VT list module in 1 CFG
# Msn_list_module = []

# # Dictionary that store column index of DIT test spec
# COL_INDEX = {}

# # Dictionary that store input-destination location information
# TEST_LOC = {}

# # Dictionary that store input-evident location information
# EVIDENT_LOC = {}

# # Directory for evident folder
# EVIDENT_BASE_LOC = ""

# # ICE name that used to select Emulator in Jenkins Automation node.
# ICE_NAME = ""

# # Flag that indicate workspace initialize final status
# STAT_FLAG = 0  # OK

# Max number of threads can be used in execution time optimization depending on ExeOp flag
MAX_NO_CPU = multiprocessing.cpu_count()

# # Pattern to parse configuation
# CFG_NAME_PATTERN = "(CFG|RC|DEF)([\d]{3})(|_OS|_No_OS|_OS_Multi|_No_OS_Multi)"

# # Load the data base stored in the file to the program
# try:
#     with Path("./data.json").open() as read_file:
#         init_db = json.load(read_file)
# except:
#     print("An exception occurred: data.json is not existed")

# # VT json used only VT
# try:
#     with Path("./VT.json").open() as filee:
#         vt_init = json.load(filee)
# except:
#     print("An exception occurred: VT.json is not existed")

# Version and datetime output #
print('\n### Workspace initialization version ' + INITIALIZATION_VER + ' ###')
print('### ' + datetime.datetime.now().strftime(u'%Y/%m/%d') + ' ' +
    datetime.datetime.now().strftime(u'%H:%M:%S') + ' ###\n')
# ------------------------------------------------------------------------------
# Class section
# ------------------------------------------------------------------------------
class Config_IT:
    msn = ""
    test_type = ""  # DIT, TIT, VT
    device = ""  # 702300EBBG, 702300EBBB..
    ar_version = ""  # 4_2_2, 4_3_1
    skip_copy_workspace = False
    def __init__(self):
        pass

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------
    
# def print_debug(message, wb=None, var=None):
#     """Print question to guide developer in debugging
    
#     Arguments:
#         wb {workbook} -- the target saved workbook
#         var -- the target printed variable

#     Returns:
#         None -- It doesn't return any value
#     """
#     if DEBUG_SCRIPT:
#         if wb:
#             wb.save('Test_spec.xlsx')
#         if var:
#             print(var)
#         print("[!] " + message)
#         ans = input("[!] Continue?(y/n)")
#         if 'y' == ans:
#             pass
#         elif 'n' == ans:
#             print("[!] SCRIPT TERMINATED!!!")
#             sys.exit()
#         else:
#             print("[!] Invalid choise. only 'y' or 'n'")
#             print_debug("[!] Select y/n!")


def get_input_command(config):
    """Get all input for init process
    Print help if command is not in valid command

    Returns:
        None -- It doesn't return any value
    """
    global STAT_FLAG
    example = '''
    E.g: ./init.py Dio DIT V3U 4_2_2
    ./init.py Dio DIT 702300EBBG 4_3_1
    '''
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
                        help='valid target module: Adc, Can, Dio ...')
    parser.add_argument('TestType', type=str, help='type of test: DIT, TIT, VT')
    parser.add_argument('TestDevice', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
                        help='target device: H3, V3M, V3H , M3, M3N, V3U, V3Hv2')
    parser.add_argument('ARVersion', type=str, choices = ["4_2_2", "4.2.2"], \
                        help='autosar version: 4_2_2')
    parser.add_argument('--nc', action='store_true', \
                        help='(optional) skip copy item in WorkspaceSetup.sh', default=False, required=False)
    # parser.add_argument('OSTest', type=str,
    #                     help='OS Test option: OS, No_OS, None(TIT only)')
    # parser.add_argument('--s0s1', type=str, nargs='?',
    #                     help='(optional) run S0/S1 or normal TC: True, False (default=False)', \
    #                     default='False', required=False)
    # parser.add_argument('--target_cfg', type=str, nargs='?',
    #                     help='(optional) run specific cfg(s)/config type: ex. CFG001,CFG002,DEF001(split by ","), \
    #                     CFGnnn(all CFG), RCnnn(all RC), DEFnnn(all DEF), xCFGnnn(exclude all CFG), \
    #                     xRCnnn(exclude all RC), xDEFnnn(exclude all DEF)', default='CFGxxx', required=False)
    # parser.add_argument('--keep_tc', type=str, nargs='?',
    #                     help='(optional: True or False) when --target_cfg is used do you want to keep original \
    #                     TC or not (defalt=False)', default='False', required=False)
    # parser.add_argument('--ignore_error', type=str, nargs='?',
    #                     help='(optional: True or False) allow to continue progress when error occurs (defalt=false)', \
    #                     default='False', required=False)
    # parser.add_argument('--ice_number', type=str, nargs='?',
    #                     help='(optional) id number of E2 Emulator to select debugger connection (defalt=null)', \
    #                     required=False)
    # parser.add_argument('--build_option', type=str, nargs='?',
    #                     help='(optional: all/generate_forward/build_only) allow to choose selected actions only \
    #                     to reduce execution time (defalt=all)', default='all', required=False)
    # parser.add_argument('--exe_op', type=str, nargs='?',
    #                     help='(optional: True or False) execution time optimization, not recommended, this should \
    #                     only be used when everything is confirmed OK (default=False)', default='False', required=False)
    # parser.add_argument('--cat_map', type=str, nargs='?',
    #                     help='(optional) use interrupt category in data.json', default='False', required=False)
    # parser.add_argument('--debug', type=str, nargs='?',
    #                     help='(optional) turn on script debug mode', default='False', required=False)

    args = parser.parse_args()

    # msn = str(args.Msn)  # MCAL Msn
    # TestType = str(args.TestType)  # DIT, TIT
    # TestDevice = str(args.TestDevice)  # V3H, V3U..
    # ARVersion = str(args.ARVersion)  # 4_2_2, 4_3_1

    config.msn = str(args.Msn)  # MCAL Msn
    config.test_type = str(args.TestType).upper()  # DIT, TIT
    config.device = str(args.TestDevice)  # V3H, V3U..
    config.ar_version = str(args.ARVersion).replace('_','.')  # 4_2_2, 4_3_1
    config.skip_copy_workspace = args.nc



    # if (str(args.Msn) not in init_db["valid_input"]["valid_msn"]) or \
    #     (str(args.TestType) not in init_db["valid_input"]["valid_testtype"]) or \
    #     (str(args.TestDevice) not in init_db["valid_input"]["valid_testdevice"]) or \
    #     (str(args.ARVersion) not in init_db["valid_input"]["valid_arversion"]) or \
    #     (str(args.OSTest) not in init_db["valid_input"]["valid_ostest"]) or\
    #     (str(args.build_option) not in init_db["valid_input"]["valid_build_option"]) or\
    #         (str(args.TestType) == "DIT" and str(args.OSTest) == "None") or\
    #         (str(args.TestType) != "VT" and str(args.Msn) == "Com") or\
    #         (str(args.TestType) == "VT" and str(args.Msn) != "Com"):
    #     parser.print_help()
    #     STAT_FLAG = 2  # Error
    #     print_status()
    # else:
    #     global Msn
    #     global msn
    #     global MSN
    #     global TestType
    #     global TestDevice
    #     global ARVersion
    #     global SHORT_ARVersion
    #     global OSTest
    #     global OSMultiTest
    #     global EVIDENT_BASE_LOC
    #     global TestDeviceName
    #     global Target_cfg_list
    #     global s0s1
    #     global keep_tc
    #     global ignore_error
    #     global build_option
    #     global ExeOp
    #     global Valid_Cfg_Name
    #     global RC_Cat_Map
    #     global DEBUG_SCRIPT

    #     Msn = str(args.Msn)  # MCAL Msn
    #     msn = Msn.lower()  # MCAL msn
    #     MSN = Msn.upper()  # MCAL MSN
    #     TestType = str(args.TestType)  # DIT, TIT
    #     TestDevice = str(args.TestDevice)  # 702300EBBG, 702300EBBB..
    #     ARVersion = str(args.ARVersion)  # 4_2_2, 4_3_1
        
    #     Target_cfg_list = get_unique_list(str(args.target_cfg).split(",")) # [CFG001, CFG002, RC001..]
    #     keep_tc = True if (str(args.keep_tc) == "True") and (Target_cfg_list[0] != "CFGxxx") else False
    #     ignore_error = True if (str(args.ignore_error) == "True") else False
    #     build_option = str(args.build_option) # all/build_only/generate_forward
    #     ExeOp = True if(str(args.exe_op) == "True") else False
    #     DEBUG_SCRIPT = True if(str(args.debug) == "True") else False

    #     s0s1 = str(args.s0s1) # True, False
    #     SHORT_ARVersion = init_db[ARVersion] # R4.2.2, R4.3.1
    #     OSTest = str(args.OSTest)  # OS, No_OS, None
    #     OSMultiTest = OSTest + "_Multi"
    #     TestDeviceName = init_db["device_map"][TestDevice] # U2A16, E2UH, E2H, U2B
    #     if TestType != "VT" and "True" == str(args.cat_map):
    #         RC_Cat_Map = init_db["rc_map"][TestDevice][Msn]
    #     if Target_cfg_list[0] == "CFGnnn":
    #         Valid_Cfg_Name = ["CFG"]
    #     elif Target_cfg_list[0] == "RCnnn":
    #         Valid_Cfg_Name = ["RC"]
    #     elif Target_cfg_list[0] == "DEFnnn":
    #         Valid_Cfg_Name = ["DEF"]
    #     elif Target_cfg_list[0] == "xCFGnnn":
    #         Valid_Cfg_Name.remove("CFG")
    #     elif Target_cfg_list[0] == "xRCnnn":
    #         Valid_Cfg_Name.remove("RC")
    #     elif Target_cfg_list[0] == "xDEFnnn":
    #         Valid_Cfg_Name.remove("DEF")
    #     else:
    #         pass
    #     if args.ice_number != None:
    #         global ICE_NAME
    #         ICE_NAME = str(args.ice_number)
    #     if TestType == "DIT" and build_option == "all":
    #         EVIDENT_BASE_LOC = "./RH850_" + TestDevice + "_" + MSN + "_DIT_Test_Input_" + \
    #                            datetime.datetime.today().strftime("%Y%m%d")
    #         TEST_LOC.update({
    #             "test_app": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/app/src".format(msn), \
    #                 "./TestProgram"],
    #             "test_header": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/app/include".format(msn), \
    #                 "./TestProgram"],
    #             "test_spec": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/plan/RH850_X2x_{}_{}_TS.xlsx". \
    #                 format(msn, MSN, TestType), \
    #                 "./Test_spec_original.xlsx"],
    #             "cdf": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/cfg/{}".format(msn, TestDevice), \
    #                 "./DescriptionFile"],
    #             "script": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/script".format(msn), \
    #                 "./TestScript"]
    #         })
    #         EVIDENT_LOC.update({
    #             "TEST_SPEC": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/plan/RH850_X2x_{}_DIT_TS.xlsx". \
    #                 format(msn, MSN), EVIDENT_BASE_LOC + "/DIT"],
    #             "TEST_APP": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/app".format(msn), \
    #                 EVIDENT_BASE_LOC + "/DIT/app"],
    #             "TEST_CDF": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/cfg/{}".format(msn, TestDevice), \
    #                 EVIDENT_BASE_LOC + "/DIT/cdf"],
    #             "LINKERS_COMMON": [
    #                 init_db[TestDeviceName]["run_cwd"] + "App_Common_Sample.ld", \
    #                 EVIDENT_BASE_LOC + "/LINKER/App_Common_Sample.ld"],
    #             "LINKERS_DEVICE": [
    #                 init_db[TestDeviceName]["run_cwd"] + "App_{}_Sample.ld".format(TestDeviceName), \
    #                 EVIDENT_BASE_LOC + "/LINKER/App_{}_Sample.ld".format(TestDeviceName)],
    #             "ECODE_SRC": [
    #                 init_db["external_dir"] + "/X2x/modules/" + msn + "/src", \
    #                 EVIDENT_BASE_LOC + "/ECODE/src"],
    #             "ECODE_INC": [
    #                 init_db["external_dir"] + "/X2x/modules/" + msn + "/include", \
    #                 EVIDENT_BASE_LOC + "/ECODE/include"]
    #         })

    #         # If S0/S1 is running, copy coverage option to workspace and linker for specific device
    #         if s0s1 == "True":
    #             if MSN not in Diff_HwIP_Msn:
    #                 TEST_LOC.update({
    #                     "s0s1_config": [
    #                         "../../../../{}/08_IT/01_WorkProduct_D/function_call_coverage/coverage_config". \
    #                         format(msn), \
    #                         "./Coverage_Workspace/config"]
    #                 })
    #             else:
    #                 if "U2A8" != TestDeviceName and "U2A16" != TestDeviceName:
    #                     TEST_LOC.update({
    #                         "s0s1_config": [
    #                         "../../../../{}/08_IT/01_WorkProduct_D/function_call_coverage/coverage_config/E2x". \
    #                         format(msn), \
    #                         "./Coverage_Workspace/config"]
    #                     })
    #                 else:
    #                     TEST_LOC.update({
    #                         "s0s1_config": [
    #                         "../../../../{}/08_IT/01_WorkProduct_D/function_call_coverage/coverage_config/U2Ax".\
    #                         format(msn), \
    #                         "./Coverage_Workspace/config"]
    #                     })
    #             os.system("rm -rfv ./Coverage_Workspace/BaseProject/*.ld")
    #             shutil.copy("./../03_stub/common/App_{0}_Sample.ld".format(TestDeviceName), \
    #                         "./Coverage_Workspace/BaseProject/App_{0}_Sample.ld".format(TestDeviceName))
    #             if "U2A8" != TestDeviceName and "U2A16" != TestDeviceName:
    #                 shutil.copy("./../03_stub/common/App_Common_E2x_S0S1.ld", \
    #                 "./Coverage_Workspace/BaseProject/App_ZCommon_Sample.ld")
    #             else:
    #                 shutil.copy("./../03_stub/common/App_Common_U2x_S0S1.ld", \
    #                 "./Coverage_Workspace/BaseProject/App_ZCommon_Sample.ld")

    #     #*****************Copy all CDF, excel file,....... in link for VT *********************************#
    #     elif TestType == "VT":
    #         EVIDENT_BASE_LOC = "./RH850_" + TestDevice + "_COM_VT_Test_Input_" + \
    #                            datetime.datetime.today().strftime("%Y%m%d")
    #         TEST_LOC.update({
    #             "test_app": [
    #                 "../../../../generic/09_VT/01_Workproduct/app/src", "./TestProgram"],
    #             "test_header": [
    #                 "../../../../generic/09_VT/01_Workproduct/app/include", "./TestProgram"],
    #             "test_spec": [
    #                 "../../../../generic/09_VT/01_Workproduct/plan/RH850_X2x_VT_TS.xlsx", \
    #                 "./Test_spec_original.xlsx"],
    #             "cdf": [
    #                 "../../../../generic/09_VT/01_Workproduct/cfg/{}".format(TestDevice), \
    #                 "./DescriptionFile"],
    #         })

    #         EVIDENT_LOC.update({
    #             "TEST_SPEC": [
    #                 "../../../../generic/09_VT/01_Workproduct/plan/RH850_X2x_VT_TS.xlsx", \
    #                 EVIDENT_BASE_LOC + "/VT"],
    #             "TEST_APP": ["../../../../generic/09_VT/01_Workproduct/app/", EVIDENT_BASE_LOC + "/VT/app"],
    #             "TEST_CDF": [
    #                 "../../../../generic/09_VT/01_Workproduct/cfg/{}".format(TestDevice), \
    #                 EVIDENT_BASE_LOC + "/VT/cdf"],
    #             "LINKERS_COMMON": [
    #                 init_db[TestDeviceName]["run_cwd"] + "App_Common_Sample.ld", \
    #                 EVIDENT_BASE_LOC + "/LINKER/App_Common_Sample.ld"],
    #             "LINKERS_DEVICE": [
    #                 init_db[TestDeviceName]["run_cwd"] + "App_{}_Sample.ld".format(TestDeviceName), \
    #                 EVIDENT_BASE_LOC + "/LINKER/App_{}_Sample.ld".format(TestDeviceName)],
    #             "ECODE_SRC_PORT": [
    #                 init_db["external_dir"] + "/X2x/modules/port/src", EVIDENT_BASE_LOC + "/ECODE/src/port"],
    #             "ECODE_INC_PORT": [
    #                 init_db["external_dir"] + "/X2x/modules/port/include", EVIDENT_BASE_LOC + "/ECODE/include/port"],
    #             "ECODE_SRC_MCU": [
    #                 init_db["external_dir"] + "/X2x/modules/mcu/src", EVIDENT_BASE_LOC + "/ECODE/src/mcu"],
    #             "ECODE_INC_MCU": [
    #                 init_db["external_dir"] + "/X2x/modules/mcu/include", EVIDENT_BASE_LOC + "/ECODE/include/mcu"],
    #             "ECODE_SRC_CAN": [
    #                 init_db["external_dir"] + "/X2x/modules/can/src", EVIDENT_BASE_LOC + "/ECODE/src/can"],
    #             "ECODE_INC_CAN": [
    #                 init_db["external_dir"] + "/X2x/modules/can/include", EVIDENT_BASE_LOC + "/ECODE/include/can"],
    #             "ECODE_SRC_LIN": [
    #                 init_db["external_dir"] + "/X2x/modules/lin/src", EVIDENT_BASE_LOC + "/ECODE/src/lin"],
    #             "ECODE_INC_LIN": [
    #                 init_db["external_dir"] + "/X2x/modules/lin/include", EVIDENT_BASE_LOC + "/ECODE/include/lin"],
    #             "ECODE_SRC_GPT": [
    #                 init_db["external_dir"] + "/X2x/modules/gpt/src", EVIDENT_BASE_LOC + "/ECODE/src/gpt"],
    #             "ECODE_INC_GPT": [
    #                 init_db["external_dir"] + "/X2x/modules/gpt/include", EVIDENT_BASE_LOC + "/ECODE/include/gpt"],
    #             "ECODE_SRC_ICU": [
    #                 init_db["external_dir"] + "/X2x/modules/icu/src", EVIDENT_BASE_LOC + "/ECODE/src/icu"],
    #             "ECODE_INC_ICU": [
    #                 init_db["external_dir"] + "/X2x/modules/icu/include", EVIDENT_BASE_LOC + "/ECODE/include/icu"]
    #         })

    #     elif TestType == "TIT":
    #         EVIDENT_BASE_LOC = "./RH850_" + TestDevice + "_" + MSN + "_TIT_Test_Input_" + \
    #                            datetime.datetime.today().strftime("%Y%m%d")
    #         TEST_LOC.update({
    #             "test_app": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/app/src".format(msn), \
    #                 "./TestProgram"],
    #             "test_header": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/app/include".format(msn), \
    #                 "./TestProgram"],
    #             "test_stubs": ["../../../../{}/08_IT/01_WorkProduct_T/stubs".format(msn), "./DescriptionFile"],
    #             "test_spec": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/plan/RH850_X2x_{}_{}_TS.xlsx". \
    #                 format(msn, MSN, TestType), \
    #                 "./Test_spec_original.xlsx"],
    #             "cdf": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/cfg/{}".format(msn, TestDevice), \
    #                 "./DescriptionFile"],
    #             "cdf_append_DIT": [
    #                 "../../../../{}/08_IT/01_WorkProduct_D/integration_test/cfg/{}".format(msn, TestDevice), \
    #                 "./DescriptionFile"],
    #             "script": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/script".format(msn), \
    #                 "./TestScript"]
    #         })
    #         EVIDENT_LOC.update({
    #             "TEST_SPEC": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/plan".format(msn), \
    #                 EVIDENT_BASE_LOC + "/TITP"],
    #             "TEST_APP": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/app".format(msn), \
    #                 EVIDENT_BASE_LOC + "/TITP/app"],
    #             "TEST_CDF": [
    #                 "../../../../{}/08_IT/01_WorkProduct_T/cfg/{}".format(msn, TestDevice), \
    #                 EVIDENT_BASE_LOC + "/TITP/cdf"],
    #             "GENTOOL": [
    #                 "../../../../{}/06_CD/01_WorkProduct/generator_cs".format(msn), \
    #                 EVIDENT_BASE_LOC + "/GENTOOL"]
    #         })
    #     if TestType != "VT":
    #         EVIDENT_LOC.update({
    #             "PDF": [init_db["external_dir"] + "/X2x/modules/" + msn + "/definition/", EVIDENT_BASE_LOC + "/PDF/"],
    #             "EXE": [init_db["gentoo_exe"] + gentoo_exe, EVIDENT_BASE_LOC + "/EXE/MCALConfGen.exe"],
    #             "DLL": [
    #                 init_db[TestDeviceName]["dll"] + Msn + init_db[TestDeviceName]["device_family"][1] + ".dll", \
    #                 EVIDENT_BASE_LOC + "/DLL/" + Msn + init_db[TestDeviceName]["device_family"][1] + ".dll"]
    #         })
    #     else:
    #         EVIDENT_LOC.update({
    #             "PDF_PORT": [init_db["external_dir"] + "/X2x/modules/port/definition/", EVIDENT_BASE_LOC + "/PDF/"],
    #             "PDF_CAN": [init_db["external_dir"] + "/X2x/modules/can/definition/", EVIDENT_BASE_LOC + "/PDF/"],
    #             "PDF_LIN": [init_db["external_dir"] + "/X2x/modules/lin/definition/", EVIDENT_BASE_LOC + "/PDF/"],
    #             "PDF_GPT": [init_db["external_dir"] + "/X2x/modules/gpt/definition/", EVIDENT_BASE_LOC + "/PDF/"],
    #             "PDF_ICU": [init_db["external_dir"] + "/X2x/modules/icu/definition/", EVIDENT_BASE_LOC + "/PDF/"],
    #             "EXE": [init_db["gentoo_exe"] + gentoo_exe, EVIDENT_BASE_LOC + "/EXE/MCALConfGen.exe"],
    #             "DLL_PORT": [
    #                 init_db[TestDeviceName]["dll"] + "Port" + init_db[TestDeviceName]["device_family"][1] + ".dll", \
    #                 EVIDENT_BASE_LOC + "/DLL/" +"Port"+ init_db[TestDeviceName]["device_family"][1] + ".dll"],
    #             "DLL_CAN": [
    #                 init_db[TestDeviceName]["dll"] + "Can" + init_db[TestDeviceName]["device_family"][1] + ".dll", \
    #                 EVIDENT_BASE_LOC + "/DLL/" +"Can"+ init_db[TestDeviceName]["device_family"][1] + ".dll"],
    #             "DLL_LIN": [
    #                 init_db[TestDeviceName]["dll"] + "Lin" + init_db[TestDeviceName]["device_family"][1] + ".dll", \
    #                 EVIDENT_BASE_LOC + "/DLL/" +"Lin"+ init_db[TestDeviceName]["device_family"][1] + ".dll"],
    #             "DLL_GPT": [
    #                 init_db[TestDeviceName]["dll"] + "Gpt" + init_db[TestDeviceName]["device_family"][1] + ".dll", \
    #                 EVIDENT_BASE_LOC + "/DLL/" +"Gpt"+ init_db[TestDeviceName]["device_family"][1] + ".dll"],
    #             "DLL_ICU": [
    #                 init_db[TestDeviceName]["dll"] + "Icu" + init_db[TestDeviceName]["device_family"][1] + ".dll", \
    #                 EVIDENT_BASE_LOC + "/DLL/" +"Icu"+ init_db[TestDeviceName]["device_family"][1] + ".dll"]
    #         })

    #     if DEBUG_SCRIPT == True:
    #         print("Input command: {} {} {} {} {} {} {} {}".format(args.Msn, args.TestType, args.TestDevice, \
    #             args.ARVersion, args.OSTest, args.s0s1, args.target_cfg, args.ice_number))
    return True


# def to_aaa(num):
#     """Change the format number from 1 --> 001, 2 --> 002 and so on

#     Arguments:
#         num {int} -- integer number

#     Returns:
#         str -- string in 3-digit format of input number
#     """
#     return "{0:03}".format(num)


# def get_unique_list(input_list, isSorted=True):
#     """
#     Get list contains unique element from input list

#     Arguments:
#         input_list -- input list

#     Returns:
#         sorted list contains unique element form input list
#     """
#     output_list = []
#     for element in input_list:
#         if not element in output_list:
#             output_list.append(element)
#     if TestType != "VT" and isSorted:
#         return sorted(output_list)
#     else:
#         return output_list


# def record_test_evident():
#     """Collect and record all revision of test target

#     Returns:
#         None -- It does a copy and not return any value
#     """
#     if build_option == "all":
#         text = ""
#         print("*"*80)
#         print("Recording test evidents")
#         for target in sorted(EVIDENT_LOC.keys()):
#             rescursive_copy(EVIDENT_LOC[target][0], EVIDENT_LOC[target][1])
#             info = get_svn_revision(EVIDENT_LOC[target][0])
#             text += target + ":\r\n" + str(info) + "\r\n"
#         print("*"*80)
#         file = Path(EVIDENT_BASE_LOC + "/svn_info_{}.txt".format(
#             datetime.datetime.today().strftime("%Y%m%d")))
#         file.write_text(text)
#     return True


# def clean_make_file():
#     """Remove all old makefile in workspace."""
#     import os
#     import sys
#     stt = True
#     if build_option == "all":
#         print("Step Prepare: Clean up make file of previous run if any")
	    
#         try:
#             if os.path.exists("./target_define.mak"):
#                 os.remove("./target_define.mak")
#             if os.path.exists("./AES_Config.mak"):
#                 os.remove("./AES_Config.mak")
#             if os.path.exists("./path_define.mak"):
#                 os.remove("./path_define.mak")
#             stt = True
#         except:
#             print("Unexpected error: ", sys.exc_info()[0])
#             stt = False
#     return stt


# def copy_input_for_IT_Test():
#     """Copy test env from expected location to workspace.

#     Returns:
#         None -- It does a copy and not return any value
#     """
#     if build_option == "all":
#         for target in sorted(TEST_LOC.keys()):
#             print("*"*80)
#             print("Copying: {}".format(target))
#             rescursive_copy(TEST_LOC[target][0], TEST_LOC[target][1], TestDevice)
#             if 'test_spec' == target:
#                 refresh_workbook('Test_spec_original.xlsx', 'Test_spec.xlsx')
#             print("*"*80)
#     return True

def prepare_env_for_IT_Test(config):
    workspace_setup_script_loc = "U:/internal/RCar/common_platform"
    workspace_loc = "U:/internal/RCar/{0}/modules/{1}/workspace".format(config.device, config.msn.lower())
    
    cmd = "workspace\\n{0}\\n{1}\\n{2}".format(config.msn.lower(), config.device, config.ar_version)
    clean_cmd_script = cmd + "\\nY\\nY"
    create_cmd_script = cmd + "\\narm6.6.3\\n\\n\\n2"

    # print("workspace_loc: ",workspace_loc)
    # path_loc = Path(workspace_loc)
    
    # print((Path("U:")))
    # if not Path.exists(Path("U:")):
    #     print("Map U First")
    #     sys.exit(-1)
        
    command = "C:/Workspace/Cygwin/bin/bash.exe -lc \"cd " + workspace_setup_script_loc + "; "
    if Path.exists(Path(workspace_loc)):
        print("Clean old workspace.")
        clean_command = command + 'echo -e \'' + clean_cmd_script + '\' | ./WorkspaceSetup.sh '
        clean_command += '\"'
        # run_command('echo -e "' + clean_cmd + '" | ./WorkspaceSetup.sh ', cwd=workspace_setup_script_loc)
        print(clean_command)
        run_command(clean_command, cwd=workspace_setup_script_loc)
    print("Create new workspace.")
    create_command = command + 'echo -e \'' + create_cmd_script + '\' | ./WorkspaceSetup.sh ' 
    create_command += '\"'
    # run_command('echo -e "' + create_cmd + '" | ./WorkspaceSetup.sh ', cwd=workspace_setup_script_loc)
    print(create_command)
    run_command(create_command, cwd=workspace_setup_script_loc)

    return True

# def refresh_workbook(old_wb_path, new_wb_path):
#     """Copy all data of old workbook into new workbook.

#     Arguments:
#         old_wb_path {path} -- source workbook
#         new_wb_path {path} -- destination workbook
#     Returns:
#         None -- It does a copy and not return any value
#     """
#     new_wb = Workbook()
#     new_wb.remove(new_wb.active)
#     old_wb = load_workbook(old_wb_path, read_only=True, data_only=True, keep_links=False, keep_vba=False)
#     for sheetname in old_wb.sheetnames:
#         new_sheet = new_wb.create_sheet(sheetname)
#         curr_sheet = old_wb[sheetname]
#         for row in curr_sheet.iter_rows():
#             for cell in row:
#                 if cell.value is not None:
#                     new_sheet.cell(cell.row, cell.column).value = cell.value
#     new_wb.save(new_wb_path)
#     new_wb.save(old_wb_path)
#     new_wb.close()
#     old_wb.close()
#     return True


# def rescursive_copy(src_loc, des_loc, extracted_device=""):
#     """Copy recursively folder into folder.

#     Arguments:
#         src_loc {path} -- source location
#         des_loc {path} -- destination location
#         extracted_device {string} -- extract the folder of device into destination location
#     Returns:
#         None -- It does a copy and not return any value
#     """
#     des_loc = Path(des_loc)
#     src_loc = Path(src_loc)
#     if not Path.exists(des_loc):
#         if not Path.is_file(src_loc):
#             Path.mkdir(des_loc, parents=True)
#         else:
#             file_folder = Path(des_loc.parent)
#             if not Path.exists(file_folder):
#                 Path.mkdir(file_folder, parents=True)
#     if not Path.is_file(src_loc):
#         if "" != extracted_device:
#             for item in Path.iterdir(src_loc):
#                 if item.name not in init_db["device_map"].keys() and \
#                         item.name not in init_db["device_map"].values():
#                     src = PurePath.joinpath(src_loc, item.name)
#                     des = PurePath.joinpath(des_loc, item.name)
#                     rescursive_copy(src, des)
#                 elif item.name == extracted_device:
#                     src = PurePath.joinpath(src_loc, item.name)
#                     rescursive_copy(src, des_loc)
#                 elif item.name == init_db["device_map"][extracted_device]:
#                     src = PurePath.joinpath(src_loc, item.name)
#                     rescursive_copy(src, des_loc)
#                 else:
#                     pass
#         else:
#             for item in Path.iterdir(src_loc):
#                 src = PurePath.joinpath(src_loc, item.name)
#                 des = PurePath.joinpath(des_loc, item.name)
#                 rescursive_copy(src, des)
#     else:
#         shutil.copy(str(src_loc), str(des_loc))
#     return None


def get_svn_revision(input_item):
    """Get URL and revision of input file/folder

    Arguments:
        input_item {path} -- input file/folder

    Returns:
        string -- return string of URL and revision of input
    """
    p = subprocess.Popen(
        "svn info " + input_item, stdout=subprocess.PIPE, shell=True)
    info, err = p.communicate()
    return info.decode()


# def get_column_index(ws):
#     """Get number of configuration in test spec

#     Arguments:
#         ws -- the input test spec sheet
#     Returns:
#         None -- It doesn't return a value but the global dict to store config
#     """
#     if TestType == "DIT" or TestType == "VT":
#         global COL_INDEX
#         # getting how many config are there
#         # Update column index dictionary and count how many config are there in the test spec
#         for col in ws.iter_cols():
#             COL_INDEX.update({col[0].value: col[0].column})
#     return None

def get_column_index(ws):
    """Get number of configuration in test spec

    Arguments:
        ws -- the input test spec sheet
    Returns:
        None -- It doesn't return a value but the global dict to store config
    """
    COL_INDEX = {}
    # getting how many config are there
    # Update column index dictionary and count how many config are there in the test spec
    for col in ws.iter_cols():
        COL_INDEX.update({col[0].value: col[0].column})
    return COL_INDEX

# def get_config_number(ws):
#     """Get number of configuration in test spec

#     Returns:
#         None -- It doesn't return a value but the global dict to store config
#     """
#     if TestType == "DIT" or TestType == "VT":
#         global STAT_FLAG
#         # getting how many config are there
#         global Cfg_dict
#         global Cfg_tc_dict

#         # list of all CDF and DEF
#         spec_index = COL_INDEX["Spec Number"]
#         device_index = COL_INDEX[TestDevice]
#         data = {row[spec_index-1].value: row[device_index-1].value for row in ws.iter_rows(min_row=2) \
#                 if (row[device_index-1].value != '-') and (row[device_index-1].value != '') and \
#                    (row[spec_index-1].value is not None)}
#         Out_Format_list = []
#         for tc in sorted(data.keys()):
#             if tc not in Cfg_tc_dict:
#                 Cfg_tc_dict.update({tc: []})
#             for cdf in str(data[tc]).split(","):
#                 mod_cdf = cdf.strip()
#                 try:
#                     cfg_type = re.search(CFG_NAME_PATTERN, mod_cdf).group(1)
#                     cfg_name = \
#                         re.search(CFG_NAME_PATTERN, mod_cdf).group(1)+re.search(CFG_NAME_PATTERN, mod_cdf).group(2)
#                     isMulti = False if "_Multi" not in mod_cdf else True
#                     # check if config label is valid for the current OS use.
#                     isOSUsageAffort = (False if "_No_OS" in mod_cdf else True) if "OS" == OSTest else \
#                         (False if ("_OS" in mod_cdf and "_No_OS" not in mod_cdf) else True)
#                     if cfg_name in RC_Cat_Map and RC_Cat_Map[cfg_name] != Cat_OS_Map[OSTest] and TestType != "VT":
#                         if cfg_name not in Out_Format_list:
#                             Out_Format_list.append(cfg_name)
#                             print(" [~] Info: {0} is not ran in {1}.".format(cfg_name, OSTest))
#                     if cfg_type in Valid_Cfg_Name and isOSUsageAffort:
#                         if cfg_name not in Cfg_dict:
#                             Cfg_dict.update({cfg_name: isMulti})
#                         else:
#                             if isMulti != Cfg_dict[cfg_name]:
#                                 print(" [~] Error: Config {} is using for both single and multi instances. \
#                                         It is not allowed.".format(cfg_name))
#                                 STAT_FLAG = 2  # Error
#                                 print_status()
#                             else:
#                                 pass
#                         if cfg_name not in Cfg_tc_dict[tc]:
#                             Cfg_tc_dict[tc].append(cfg_name)
#                         else:
#                             pass
#                     else:
#                         pass
#                 except:
#                     if mod_cdf not in Out_Format_list:
#                         Out_Format_list.append(mod_cdf)
#                         print(" [~] Warning: {} is a unsupported config label.".format(mod_cdf))
#                         STAT_FLAG = 1  # Warning
#                     else:
#                         pass
#         # Error out if no config specified
#         if 0 == len(Cfg_dict):
#             print(" [~] Error: No configuration is specified.")
#             STAT_FLAG = 2  # Error
#             print_status()
#     return None

def is_valid_row(cell_str):
    if (cell_str != '-') and \
                (cell_str != '') and \
                (cell_str.lower() != 'x'):
        return True
    else:
        return False



def modify_testspec(config, wb):
    # global TestDevice
    """Filter out TC which is not belong to input module, device, os test, count = ''
    Change the test spec to get a multiple execution of TCs

    Arguments:
        wb -- the input test spec book

    Returns:
        None -- It doesn't return any value but the updated of test_spec.xls at same location
    """
    ws_sp = wb["Test_Spec"]
    col_index = get_column_index(ws_sp)
    # Check if Test spec uses Device id or device name
    test_device = config.device 
    ar_version = config.ar_version
    if test_device in col_index:
        # Device Id is used
        # for row in range(ws_sp.max_row + 1, 2, -1):
        #     if (ws_sp.cell(row, col_index[test_device]).value != '-') and \
        #         (ws_sp.cell(row, col_index[test_device]).value != '') and \
        #         (ws_sp.cell(row, col_index[test_device]).value != 'x') and \
        #         (ws_sp.cell(row, col_index[ar_version]).value != '') and \
        #         (ws_sp.cell(row, col_index[ar_version]).value != '-') and \
        #         (ws_sp.cell(row, col_index[ar_version]).value != 'x'):
        #         pass
        #     else:
        #         ws_sp.delete_rows(row)
        for row in range(ws_sp.max_row + 1, 1, -1):
            if (is_valid_row(str(ws_sp.cell(row, col_index[test_device]).value))):
                # print(ws_sp.cell(row, col_index[test_device]).value)
                # print(row)
                pass
            else:
                ws_sp.delete_rows(row)


    # if TestType == "DIT"  and build_option == "all" or TestType == "VT":
    #     global STAT_FLAG
    #     global Target_cfg_list
    #     # Step Prepare: Update sheet AES_Config depend on OS argument [OS/No_OS]
    #     print("Step Prepare: Update sheet AES_Config")

    #     # prepare sheet 'AES_Config'
    #     try:
    #         ws_aes = wb["AES_Config"]
    #     except:
    #         print(" [~] The sheet name 'AES_Config' does not exists.")
    #         STAT_FLAG = 2  # Error
    #         print_status()

    #     # Update sheet 'AES_Config' for the current test execution
    #     modify_aes_config(ws_aes)

    #     # Step 0: Remove Categorization if it is existed
    #     print("Step Prepare: Remove Test categorization")
    #     ws_sp = wb["Test_Spec"]
    #     if ws_sp.max_column > MAX_COL_SPEC:
    #         # Ignore none-count TCs
    #         for col in range(ws_sp.max_column + 1, 1, -1):
    #             if col == 1 or col == 14 or  col == 15 or col == 16 or col > 21:
    #                 pass
    #             else:
    #                 ws_sp.delete_cols(col)
    #         for row in range(ws_sp.max_row + 1, 2, -1):
    #             if (ws_sp.cell(row, 1).value is not None) and (ws_sp.cell(row, 1).value != '') and \
    #                 (ws_sp.cell(row, 1).value != '-') and (ws_sp.cell(row, 16).value is not None) and \
    #                 (ws_sp.cell(row, 16).value != '-') and (ws_sp.cell(row, 16).value != ''):
    #                 pass
    #             else:
    #                 ws_sp.delete_rows(row)
    #     else:
    #         if DEBUG_SCRIPT == True:
    #             print("No categorization found!!! Nothing need to be removed")

    #     # Step 1: Filter out all TC not belong to target_device, ARVersion, OS
    #     print("Step 1: Filtering out all TCs not belong to target_device, ARVersion, OS/No_OS")

    #     # Get column index after removing unused columns
    #     get_column_index(ws_sp)
    #     print_debug("Category was removed!", wb, COL_INDEX)

    #     # Check if Test spec uses Device id or device name
    #     if TestDevice in COL_INDEX:
    #         # Device Id is used
    #         for row in range(ws_sp.max_row + 1, 2, -1):
    #             if (ws_sp.cell(row, COL_INDEX[TestDevice]).value != '-') and \
    #                (ws_sp.cell(row, COL_INDEX[TestDevice]).value != '') and \
    #                (ws_sp.cell(row, COL_INDEX[TestDevice]).value != 'x') and \
    #                (ws_sp.cell(row, COL_INDEX[SHORT_ARVersion]).value != '') and \
    #                (ws_sp.cell(row, COL_INDEX[SHORT_ARVersion]).value != '-') and \
    #                (ws_sp.cell(row, COL_INDEX[SHORT_ARVersion]).value != 'x'):
    #                pass
    #             else:
    #                 ws_sp.delete_rows(row)
    #     else:
    #         print(" [~] Error: Target device is not specified in test spec.")
    #         STAT_FLAG = 2  # Error
    #         print_status()

    #     # continue to filter test cases that applied for S0S1 if running S0S1
    #     if s0s1 == "True" and COL_INDEX["Coverage Swap File"] != None:
    #         for row in range(ws_sp.max_row + 1, 2, -1):
    #             if ws_sp.cell(row, COL_INDEX["Coverage Swap File"]-1).value != '' and \
    #                 ws_sp.cell(row, COL_INDEX["Coverage Swap File"]-1).value != '-':
    #                 pass
    #             else:
    #                 ws_sp.delete_rows(row)
    #     else:
    #         pass
    #     print_debug("Test cases were filtered!", wb)
    #     # Step 2: Multi TCs and Configuration --> Total of TCs
    #     print("Step 2: Multi TCs and Configuration to get final total of TCs")

    #     # Get number of config after removing untested TCs
    #     get_config_number(ws_sp)
    #     print_debug("List config parsing done!", wb, Cfg_dict)

    #     # Handling if the is only specific config want to be run
    #     global keep_tc, Msn_list_module

    #     if Target_cfg_list[0] in Spec_Cfg_Label:
    #         Target_cfg_list = sorted(Cfg_dict.keys())
    #     # list of all CDF and DEF
    #     if False == keep_tc:
    #         data = [[cell.value for cell in row] for row in ws_sp.iter_rows() if row[0].row == 1 or \
    #             any(cfg.strip().split('_')[0].strip() in Target_cfg_list \
    #             for cfg in str(row[COL_INDEX[TestDevice]-1].value).split(","))]
    #     else:
    #         data = [[cell.value for cell in row] for row in ws_sp.iter_rows() \
    #             if row[COL_INDEX[TestDevice]-1].value is not None and row[COL_INDEX[TestDevice]-1].value != '' and \
    #             row[COL_INDEX[TestDevice]-1].value != '-']
    #     cdf_device_list = [[test_cdf.strip() for test_cdf in str(row[COL_INDEX[TestDevice]-1]).split(',')] 
    #                                             for row in data[1:]]
    #     # new data is created to store multiply data of test case
    #     new_data = []
    #     # tc_cfg: relationship between test case and config mapped with it
    #     tc_cfg = []
    #     for row in range(1, len(data)):
    #         for cfg in cdf_device_list[row-1]:
    #             try:
    #                 cfg_name = re.search(CFG_NAME_PATTERN, cfg).group(1) + \
    #                     re.search(CFG_NAME_PATTERN, cfg).group(2)
    #             except:
    #                 pass
    #             if cfg_name in Target_cfg_list and cfg_name in Cfg_tc_dict[data[row][COL_INDEX['Spec Number'] - 1]]:
    #                 tc_cfg.append([to_aaa(int(data[row][2])), cfg_name])
    #                     # for each "x" the row will be increase 1
    #                 new_data.append(data[row])
        
    #     # overwrite new data to the old one
    #     # new data can be smaller or larger than original data, clean sheet first
    #     for row in range(ws_sp.max_row + 1, 1, -1):
    #         ws_sp.delete_rows(row)
    #     # then write new data
    #     for row in range(len(new_data)):
    #         for col in range(len(new_data[0])):
    #             if TestType != "VT":
    #                 # Set all "Category" to "ETC"
    #                 # Old test case number (in format aaa): to_aaa(tc_cfg[row].keys()[0])
    #                 # Configuration number it was mapped: str(tc_cfg[row].values()[0])
    #                 category = re.search(
    #                     MSN + "_DIT_([A-Z]{3})", new_data[row][11]).group(1)
    #                 sub_category = ""
    #                 if re.search(MSN + "_DIT_([A-Z]{3})_([A-Z0-9]{2})_", new_data[row][11]):
    #                     sub_category = re.search(
    #                         MSN + "_DIT_([A-Z]{3})_([A-Z0-9]{2})_", new_data[row][11]).group(2)
    #                 modified_category = category + sub_category
    #                 if col == COL_INDEX["Category"]-1:
    #                     ws_sp.cell(row+2, col+1).value = modified_category
    #                 # Re-fill "Item number"
    #                 elif col == COL_INDEX["Item number"]-1:
    #                     ws_sp.cell(row+2, col+1).value = str(tc_cfg[row][0]) + "_" + str(tc_cfg[row][1])
    #                 # Re-fill "Spec Number"
    #                 elif col == COL_INDEX["Spec Number"]-1:
    #                     ws_sp.cell(row+2, col+1).value = Msn + "_" + category + sub_category + \
    #                                                      str(tc_cfg[row][0]) + "_" + str(tc_cfg[row][1])
    #                 # Re-fill Test Device column with x
    #                 elif col == COL_INDEX[TestDevice]-1:
    #                     ws_sp.cell(row+2, col+1).value = str(tc_cfg[row][1])
    #                 # Remaining is kept the same.
    #                 else:
    #                     ws_sp.cell(row+2, col+1).value = new_data[row][col]
    #            #********************VT read CFG name *******************************************#  
    #             else:
    #                 category = re.search(
    #                     MSN + "_VT_([A-Z]{3})", new_data[row][11]).group(1)
    #                 sub_category = ""
    #                 if re.search(MSN + "_VT_([A-Z]{3})_([A-Z0-9]{2})_", new_data[row][11]):
    #                     sub_category = re.search(
    #                         MSN + "_VT_([A-Z]{3})_([A-Z0-9]{2})_", new_data[row][11]).group(2)
    #                 modified_category = category + sub_category
    #                 if col == COL_INDEX["Category"]-1:
    #                     ws_sp.cell(row+2, col+1).value = modified_category
    #                 # Re-fill "Item number"
    #                 elif col == COL_INDEX["Item number"]-1:
    #                     ws_sp.cell(row+2, col+1).value = str(tc_cfg[row][0]) + "_" + str(tc_cfg[row][1])
    #                 # Re-fill "Spec Number"
    #                 elif col == COL_INDEX["Spec Number"]-1:
    #                     ws_sp.cell(row+2, col+1).value = Msn + "_" + category + sub_category + \
    #                                                      str(tc_cfg[row][0]) + "_" + str(tc_cfg[row][1])
    #                 # Re-fill Test Device column with x
    #                 elif col == COL_INDEX[TestDevice]-1:
    #                     ws_sp.cell(row+2, col+1).value = "x"
    #                 # Remaining is kept the same.
    #                 else:
    #                     ws_sp.cell(row+2, col+1).value = new_data[row][col]
    #     print_debug("Cloning test cases done!", wb)

    #     # Fill the configured Driver for each test-case in Appendix column
    #     if s0s1 == "False":
    #         if TestType == "DIT":
    #             for row in range(len(new_data)):
    #                 # the format is Msn_cgfxxx.a
    #                 original_info = new_data[row][10] if new_data[row][10] is not None else ''
    #                 appendix = "\nLINKERN+=-l./../../ConfiguredDriver/" + tc_cfg[row][1] + "/obj/" + Msn + "_" + \
    #                     tc_cfg[row][1] + ".a" + "\n" + \
    #                     "CC_INCLUDE_PATH+=./../../ConfiguredDriver/" + \
    #                     tc_cfg[row][1] + "/include" + "\n"+ \
    #                     "CFLAGS += -D" + init_db["device_map_pin"][TestDevice] + "\n" + \
    #                     "CFLAGS += -D" +(ws_sp.cell(row+2,4)).value.upper() + "_" + init_db["device_map"][TestDevice]+"\n"
    #                 if "Fls" in Msn:
    #                     appendix = appendix + "CFLAGS += -DFLS_" +str((ws_sp.cell(row+2,2)).value) +"\n"
    #                 ws_sp.cell(row+2, 11).value = original_info + appendix
    #     #*************Check Column Module in Test_Spec and Fill in column Make Appendix********************************#
    #         else:                 
    #             for row in range(len(new_data)):
    #                 # the format is Msn_cgfxxx.a
    #                 original_info = new_data[row][10] if new_data[row][10] is not None else ''
    #                 Msn_list = []
    #                 Msn_list_module.append(new_data[row][34])
    #                 Msn_list.append(new_data[row][34])
    #                 for item in Msn_list:
    #                     appendix = ""
    #                     for Modulename in str(item).split(","):
    #                         Modulename = Modulename.strip()
    #                         appendix = appendix + "\nLINKERN+=-l./../../ConfiguredDriver/" + tc_cfg[row][1] +\
    #                                    "/obj/" + Modulename + "_" + tc_cfg[row][1] + ".a" 
    #                 nextappendix = "\n" + "CC_INCLUDE_PATH+=./../../ConfiguredDriver/" + \
    #                                tc_cfg[row][1] + "/include" + "\n"
    #                 ws_sp.cell(row+2, 11).value = original_info + appendix + nextappendix
    #     else:
    #         # In case S0/S1 Test, build all source alone with Test app and Canata Lib
    #         appendix = "\n"
    #         for row in range(len(new_data)):
    #             appendix = "\n"
    #             # the format is Msn_cgfxxx.a
    #             original_info = new_data[row][10] if new_data[row][10] is not None else ''
    #             # Add generated file from GenTool
    #             appendix += "CC_INCLUDE_PATH+=./../../ConfiguredDriver/" + tc_cfg[row][1] + "/include" + "\n"
    #             appendix += "include s0s1.mak" + "\n"
    #             ws_sp.cell(row+2, 11).value = original_info + appendix
    #             ws_sp.cell(row+2, 14).value = "Build Log Check"
    #             ws_sp.cell(row+2, 16).value = "+Exit status of Target Build: TRUE"
    #     print_debug("Modifying test case make appendix done!", wb)

    #     # open and copy to new ws then replaced the old one
    #     # Sort the Spec number column again
    #     data = [[cell.value for cell in row] for row in ws_sp.iter_rows() \
    #                                          if row[3].value is not None and row[3].value != '']
    #     target_filter_col = [row[3] for row in data]
    #     sorted_target_filter_col = target_filter_col[1:]
    #     sorted_target_filter_col.sort()
    #     filtered_index = list()
    #     for i in sorted_target_filter_col:
    #         filtered_index.append(target_filter_col.index(i))
    #     # Write the first line - header first
    #     for col in ws_sp.iter_cols():
    #         col[0].value = data[0][col[0].column-1]
    #     # Write the rest of content which already sorted
    #     for row in ws_sp.iter_rows(min_row=2):
    #         for cell in row:
    #             if cell.row-1 < len(data) and cell.column-1 < len(data[0]):
    #                 cell.value = data[filtered_index[cell.row-2]][cell.column-1]
    #             else:
    #                 cell.value = ''
    #     print_debug("Sorting test cases done!", wb)
    # elif TestType == "TIT":
    #     # Change the device name in config sheet same as the input command
    #     try:
    #         ws_aes = wb["AES_Config"]
    #     except:
    #         print(" [~] Error: The sheet name 'AES_Config' does not exists.")
    #         STAT_FLAG = 2  # Error
    #         print_status()
    #     modify_aes_config(ws_aes)
        
    #     # Filter test case not belong to target device
    #     Tested_Device_column_index = 20
    #     ws_sp = wb["Test_Spec"]
    #     for row in range(ws_sp.max_row + 1, 2, -1):
    #         if TestDeviceName in str(ws_sp.cell(row,Tested_Device_column_index).value) and \
    #             (ws_sp.cell(row, 1).value is not None) and (ws_sp.cell(row, 1).value != '') and \
    #             (ws_sp.cell(row, 1).value != '-'):
    #             pass
    #         else:
    #             ws_sp.delete_rows(row)
    # elif build_option != "all":
    #     # prepare sheet 'AES_Config'
    #     ws_sp = wb["Test_Spec"]
    #     get_column_index(ws_sp)
    #     get_config_number(ws_sp)

    return None


# def modify_aes_config(ws):
#     """To update sheet 'AES_Config' for specific test execution

#     Arguments:
#         ws -- the output excel wooksheet
#     Returns:
#         None
#     """
#     if TestType == "DIT":
#         aes_header_name = ("No_OS" if 'No_OS' == OSTest else "OS") if 'False' == s0s1 else \
#             ("S0S1_No_OS" if 'No_OS' == OSTest else "S0S1_OS")
#         header_list = [col[1].value for col in ws.iter_cols()]
#         aes_header_index = header_list.index(aes_header_name) if aes_header_name in header_list else 2
#         if 2 == aes_header_index:
#             print("[~] Warning: Sheet 'AES_Config' does not contain configuration for " + aes_header_name)
#             print("[~] Used default AES configuration")
#             STAT_FLAG = 1  # Warning
#         else:
#             for row in ws.iter_rows(min_row=3):
#                 row[2].value = row[aes_header_index].value
#         if "" != ICE_NAME:
#             ws.cell(13, 3).value = init_db[TestDeviceName]["debugger_option"] + " -ice " + ICE_NAME
#         else:
#             ws.cell(13, 3).value =  init_db[TestDeviceName]["debugger_option"]
#     # Iterate each row of C column and update HWIP, AR version and device specific information by tags
#     hwip_dict = init_db["hw_ip"][TestDevice][Msn]
#     for row in ws.iter_rows():
#         old_val = row[2].value
#         if old_val and (row[1].value != "MultiCore Connect"):
#             new_val = old_val.replace("{ARV}", ARVersion).replace("{DVN}", TestDeviceName).replace("{DVF}", \
#                 init_db[TestDeviceName]["device_family"][0]).replace("{ARBSW}", \
#                 ("R431" if ARVersion == "4_3_1" else "R422"))
#             for ip in hwip_dict.keys():
#                 new_val = new_val.replace(ip, hwip_dict[ip])
#             if s0s1 == "True" or OSTest == "OS":
#                 new_val = remove_redundant_hwip(new_val)
#             row[2].value = new_val
#     # change gentool generic path name if script is run on Linux
#     if 'linux' == sys.platform:
#         ws.cell(18, 3).value = str(ws.cell(18, 3).value).replace("MCALConfGen.exe", "MCALConfGen")
#     # Update CPU core and debugger option in AES_Config
#     ws.cell(11, 3).value = init_db[TestDeviceName]["cpu_core"]
#     # Update device name
#     ws.cell(9, 3).value = TestDeviceName
#     ws.cell(10, 3).value =  TestDevice
#     return None


# def remove_redundant_hwip(lines):
#     """ To remove all redundant HWIP in sheet AES_Config """
#     pattern = "(\{HWIP\d{1}\})"
#     if re.search(pattern, lines):
#         redunt_ip = re.search(pattern, lines).group(1)
#         tmp_new_val_list = []
#         for file_path in lines.split():
#             if redunt_ip not in file_path:
#                 tmp_new_val_list.append(file_path)
#         lines = ' '.join(tmp_new_val_list)
#         return remove_redundant_hwip(lines)
#     else:
#         return ' '.join(get_unique_list(lines.split(), False))


# def get_acceptable_warning(wb):
#     """ To get list of acceptable warning during Generation and Compilation progress 
    
#     Arguments:
#         wb {workbook} -- the input workbook
#     """
#     global accepted_warning_list
#     try:
#         ol_ws = wb['Outline']
#     except:
#         print("[~] Error: Sheet 'Outline' does not exist in test specification.")
#         STAT_FLAG = 2  # Error
#         print_status()
#     try:
#         first_col = [row[1].value for row in ol_ws.iter_rows()]
#         start_row = first_col.index("1.9     LIMITATIONS") + 1
#         end_row = first_col.index("2.0     HOW TO EXECUTE DRIVER IT TEST CASES")
#         limitation_info = [[cell.value for cell in row[1: len(row)]] \
#                         for row in ol_ws.iter_rows(min_row=start_row, max_row=end_row)]
#         table_header_index = first_col[start_row: end_row].index("Item") + 1
#         target_dev_index = limitation_info[table_header_index].index( \
#                         [col for col in limitation_info[table_header_index] if TestDeviceName in col][0])
#         for infor_row_index in range(table_header_index + 1,len(limitation_info)):
#             tmp_list = \
#                 limitation_info[infor_row_index][target_dev_index].replace("  ",",").replace("\n",",").split(",") \
#                     if limitation_info[infor_row_index][target_dev_index] is not None else []
#             for item in tmp_list:
#                 if "warning" in item or "WRN" in item:
#                     accepted_warning_list.append(item.strip())
#     except:
#         accepted_warning_list.append("") #To avoid re-execute in the next command
#     return True


def run_command(command, cwd):
    """To run cmd command on specific working dir

    Arguments:
        command {string} -- the expected command line
        cwd {path} -- the expected working dir

    Returns:
        string -- the output or the error from cmd
    """
    print("debug cmd: ",command)
    # process = subprocess.Popen(
    #     shlex.split(str(command)), stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=str(cwd), shell=True)
    process = subprocess.Popen(
        shlex.split(str(command)), stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=str(cwd))
    # while True:
    #     try:
    #         output, errout = process.communicate()
    #     except:
    #         break
    #     if output:
    #         # if not ExeOp:
    #         #     print(str(output.decode()).strip())
    #         # check_output = re.search("WRN[0-9]+", str(output.decode()))
    #         # if check_output and check_output.group(0) not in accepted_warning_list:
    #         #     print(
    #         #         " [~] Error: Making configured driver failed due to warning occurs in tool generation.")
    #         #     STAT_FLAG = 2  # Error
    #         #     print_status()
    #         print(str(output.decode()).strip())
    #     if errout:
    #         # if not ExeOp:
    #         #     print(str(errout.decode()).strip())
    #         # check_err = re.search("warning #[0-9]+", str(errout.decode()))
    #         # if check_err and check_err.group(0) not in accepted_warning_list:
    #         #     print(
    #         #         " [~] Error: Making configured driver failed due to warning occurs in source compilation.")
    #         #     STAT_FLAG = 2  # Error
    #         #     print_status()
    #         print(str(errout.decode()).strip())
    #     if (output or errout) and process.poll() is not None:
    #         break
    # if 0 != process.poll():
    #     print(" [~] Error: Got an error when running the current command")
    #     # STAT_FLAG = 2  # Error
    #     # print_status()

    # while True:
    #     output = process.stdout.readline()
    #     if output == '' and process.poll() is not None:
    #         break
    #     if output:
    #         print(str(output.decode()).strip())


    output, errout = process.communicate()
    process.wait()
    if output:
        print(str(output.decode()).strip())
    if errout:
        print(str(errout.decode()).strip())


    # return None


# def make_config_gentool(cfg):
#     """Execute Gentool for all configs to generate output files (Msn_Cfg.h, Msn_PBcfg.c,...)

#     Arguments:
#         cfg {string} -- the config name (CFG001, DEF002,...)

#     Returns:
#         None -- it doesn't return a value but the output files of Gentool shall be in expected location
#     """
#     cfg_num = re.search(CFG_NAME_PATTERN, cfg).group(2)
#     cfg_name = re.search(CFG_NAME_PATTERN, cfg).group(1)

#     # Ignore DEF test case if OS_Multi running or this is for s0s1 coverage
#     if cfg_name == "DEF" and (Cfg_dict[cfg] == True or s0s1 == "True"):
#         return True

#     # BSWMDT file as naming rule and fixed location
#     bswmdt = init_db["external_dir"] + "/X2x/modules/{0}/generator/{1}/{2}_{3}_{1}_BSWMDT.arxml".format( \
#         msn, TestDeviceName, SHORT_ARVersion.replace(".", ""), MSN)

#     if ExeOp: print(" [+] Gentool execution for {}".format(cfg))
#     # Prepare command line for gentool on main module
#     # output path to "configured Driver" folder
#     output_path = "./ConfiguredDriver/" + cfg
#     # main CDF as naming rule
#     main_cdf = "./DescriptionFile/RH850_" + Msn + "_"
#     main_cdf += cfg_num if (cfg_name == "CFG") else ("DEF_" if (cfg_name == "DEF") else "RC_") + cfg_num
#     main_cdf += ".arxml"
#     stub_arxml_list = []
#     # dependence module
#     for depend_module in init_db["stub_depend_for_gentool"][Msn]:
#         if depend_module == "Mcu" and cfg_name != "DEF":
#             stub_arxml_list.append("./DescriptionFile/RH850_" + \
#                 "Mcu" + "_" + Msn + ("_" if cfg_name != "RC" else "_RC_") + cfg_num + ".arxml")
#         elif depend_module == "FrIf" and cfg_name != "DEF":
#             stub_arxml_list.append("./DescriptionFile/RH850_" + \
#                 Msn + ("IF_" if cfg_name != "RC" else "IF_RC_") + cfg_num + ".arxml")
#         elif depend_module in ["Ecum", "Os", "Dem"]:
#             stub_arxml_list.append(init_db["external_dir"] + "/X2x/common/generic/stubs/" + \
#                 ARVersion + "/{0}/xml/{0}_{1}.arxml".format(depend_module, Msn))
#         else:
#             pass
    
#     # gentool command
#     cmd = init_db["gentoo_exe"] + gentoo_exe + " -m " + Msn + " -o " + output_path + " " + \
#         main_cdf + " " + bswmdt + " " + trxml
#     for stub_arxml in stub_arxml_list:
#         cmd += (" " + stub_arxml)

#     # execution of module GenTool
#     run_command(cmd, cwd=init_db["workspace"])

#     # Run OS Generation Tool if OSTest=OS
#     if OSTest == "OS":
#         single_or_multi = "_Multi" if Cfg_dict[cfg] == True else ""
#         Os_Config = "./TestProgram/AutosarOSCfg{}/Os_Config_Default.oil".format(single_or_multi)
#         # OS gentool command
#         Os_cmd = init_db["os_exe_command"].format(ARVersion, cfg) + " " + Os_Config
#         if DEBUG_SCRIPT == True:
#             print(Os_cmd + "is running in ")
#             print(str("./ConfiguredDriver/" + cfg))
#         run_command(Os_cmd, cwd=init_db["workspace"])

#     # the dependence module: MCU, PORT ...
#     if cfg_name != "DEF":
#         for sub_module in (init_db["module_depend_multi"][Msn] if OSTest == "OS" and Cfg_dict[cfg] == True else \
#                             init_db["module_depend"][Msn]):
#             # similar to the main one ...
#             sub_cdf = "./DescriptionFile/RH850_" + \
#                 sub_module + "_" + Msn + ("_" if cfg_name == "CFG" else "_RC_") + cfg_num + ".arxml"
#             sub_bswmdt = init_db["external_dir"] + "/X2x/modules/{0}/generator/{1}/{2}_{3}_{1}_BSWMDT.arxml".format( \
#                 sub_module.lower(), TestDeviceName, SHORT_ARVersion.replace(".", ""), sub_module.upper())
#             if sub_module in ["Mcu", "Port", "Gpt","Adc"]:
#                 dem_sub_arxml = init_db["external_dir"] + "/X2x/common/generic/stubs/" + \
#                     ARVersion + "/Dem/xml/Dem_"+ sub_module +".arxml"
#                 cmd = init_db["gentoo_exe"] + gentoo_exe + " -m " + sub_module + " -o " + \
#                     output_path + " " + sub_cdf + " " + sub_bswmdt + " " + \
#                     trxml + " " + dem_sub_arxml
#                 run_command(cmd, cwd=init_db["workspace"])
#             else:
#                 pass
#     return True


# def make_config_source(cfg):
#     """Execute Gentool for all configs to generate output files (Msn_Cfg.h, Msn_PBcfg.c,... )
#     and Make Msn_configxxx.a for all configs

#     Arguments:
#         cfg {string} -- the config name (CFG001, DEF002,...)

#     Returns:
#         None -- it doesn't return a value but the config driver in expected location
#     """

#     # Ignore compiling Driver source if this is:
#     # DEF test case
#     # and
#     # OS_Multi running or s0s1 execution
#     cfg_name = re.search(CFG_NAME_PATTERN, cfg).group(1)
#     if cfg_name == "DEF" and (Cfg_dict[cfg] == True or s0s1 == "True"):
#         return True

#     if ExeOp: print(" [+] Compiling configured driver source for {}".format(cfg))
#     # make the configured Driver, make file must be prepared in advanced.
#     run_command("make -f ./ConfiguredDriver/make/makefile" + " " + Msn + " -j MSN=" + Msn + \
#                 " DEVICE=" + init_db[TestDeviceName]["device_family"][1] + \
#                 " DEVICE_ID=" + TestDevice + " ARV=" + ARVersion + " CFG=" + cfg + " WS=./ " + \
#                 " OSTest=" + (OSMultiTest if Cfg_dict[cfg] == True else OSTest),
#                 init_db["workspace"])
#     return True


# def make_configured_driver(wb):
#     """Make Msn_configxxx.a for all configs
    
#     Arguments:
#         wb {workbook} -- the input workbook

#     Returns:
#         None -- it doesn't return a value but the config driver in expected location
#     """
#     global STAT_FLAG
#     global Cfg_dict
#     global Target_cfg_list
#     if TestType == "DIT":
#         global accepted_warning_list
#         if 0 == len(accepted_warning_list):
#             get_acceptable_warning(wb)

#         if Target_cfg_list[0] in Spec_Cfg_Label:
#             Target_cfg_list = sorted(Cfg_dict.keys())

#         # cleanup old output before run make for each configured driver
#         for cfg in Target_cfg_list:
#             if build_option != "build_only":
#                 # clean both Gentool output and old objects
#                 run_command("make -f ./ConfiguredDriver/make/makefile clean -j MSN=" + \
#                             Msn + " DEVICE=" + init_db[TestDeviceName]["device_family"][1] + \
#                             " ARV=" + ARVersion + " CFG=" + cfg + " WS=./" + " OSTest=" + \
#                             (OSMultiTest if Cfg_dict[cfg] == True else OSTest), cwd=init_db["workspace"])
#             else:
#                 # cleanup old objects only
#                 run_command("make -f ./ConfiguredDriver/make/makefile clean_obj -j MSN=" + \
#                             Msn + " DEVICE=" + init_db[TestDeviceName]["device_family"][1] + \
#                             " ARV=" + ARVersion + " CFG=" + cfg + " WS=./ " + " OSTest=" + OSTest, \
#                             cwd=init_db["workspace"])

#         # Loop all CFG and DEF to build configured Driver
#         if ExeOp:
#             # To achieve best optimization, all Gentool output should be done first by multi-thread.
#             for cfg in Target_cfg_list:
#                 while threading.activeCount() > MAX_NO_CPU:
#                     pass
#                 threading.Thread(target=make_config_gentool, args=[cfg]).start()
#             while threading.active_count() > 1:
#                 pass
#             # After Gentool executed completely, Driver source code will be compiled by make with "-j" option.
#             for cfg in Target_cfg_list:
#                 # Because "make -j" optimizes most of the performance, 
#                 # compiling Driver should be handled on 2 threads only
#                 while threading.activeCount() > MAX_NO_CPU:
#                     pass
#                 threading.Thread(target=make_config_source, args=[cfg]).start()
#             while threading.active_count() > 1:
#                 pass
#         else:
#             for cfg in Target_cfg_list:
#                 if build_option != "build_only":
#                     make_config_gentool(cfg)
#                 make_config_source(cfg)
        
#         # Prepare object files path for s0s1
#         for cfg_path in os.listdir(init_db["workspace"] + "/ConfiguredDriver"):
#             if re.search(CFG_NAME_PATTERN, cfg_path) != None:
#                 s0s1_mak_path = os.path.join(init_db["workspace"] + "/ConfiguredDriver", cfg_path, "s0s1.mak")
#                 if os.path.exists(s0s1_mak_path):
#                     if s0s1 == "True":
#                         try:
#                             file = open(s0s1_mak_path, "r+")
#                         except:
#                             print(" [!] Error: Cannot open s0s1.mak file to prepare for s0s1")
#                             STAT_FLAG = 2 #Error
#                             print_status()
#                         new_content = ""
#                         for line in file:
#                             if "OBJS_S0S1" in line:
#                                 obj_path_list = line.split("=")[1].rstrip().strip().split(" ")
#                                 obj_path_list = [path[path.find("obj/"):] for path in obj_path_list]
#                                 new_content = "OBJS_S0S1 = " + " ".join(obj_path_list)
#                                 break
#                         file.seek(0)
#                         file.truncate()
#                         file.write(new_content)
#                         file.close()
#                     else:
#                         os.remove(s0s1_mak_path)

#     elif TestType == "VT":
#         if Target_cfg_list[0] in Spec_Cfg_Label:
#             Target_cfg_list = sorted(Cfg_dict.keys())

#         # Loop all CFG and DEF to build configured Driver
#         num  = 0
        
#         for cfg in Target_cfg_list:
#             clean= 0
#             ccInclude = ""
#             TS_Compiler = ""
#             Common_Test_Program = ""
#             for Modulename in str(Msn_list_module[num]).split(","):
#                 clean = clean + 1
#                 Modulename = Modulename.strip()
#                 MODULENAME = Modulename.upper()
#                 modulename = Modulename.lower() 
#                 ccInclude = ccInclude + " " + vt_init[Modulename]["CC_Include_Path"]
#                 if MODULENAME != "MCU" and MODULENAME != "PORT":
#                     TS_Compiler = vt_init[Modulename]["TS_Compiler"]
#                     Common_Test_Program = Common_Test_Program + vt_init[Modulename]["Common_Test_Program"]
#                 # BSWMDT file as naming rule and fixed location
#                 bswmdt = init_db["external_dir"] + "/X2x/modules/{0}/generator/{1}/{2}_{3}_{1}_BSWMDT.arxml".format( \
#                          modulename, TestDeviceName, SHORT_ARVersion.replace(".", ""), MODULENAME)

#                 cfg_num = re.search(CFG_NAME_PATTERN, cfg).group(2)
#                 cfg_name = re.search(CFG_NAME_PATTERN, cfg).group(1)

#                 # Prepare command line for gentool on main module
#                 # output path to "configured Driver" folder
#                 output_path = init_db["workspace"] + "/ConfiguredDriver/" + cfg
#                 # main CDF as naming rule
#                 if cfg_name == "CFG" or cfg_name == "RC":
#                     if OSTest == "OS" and Cfg_dict[cfg_name] == True:
#                         main_cdf = init_db["workspace"] + "/DescriptionFile/RH850_" + Modulename + \
#                         ("_" if cfg_name == "CFG" else "_RC_") + cfg_num + "_Multi.arxml"
#                     else:
#                         main_cdf = init_db["workspace"] + \
#                             "/DescriptionFile/RH850_Com_" + cfg_num + ".arxml"
                            
#                 mcu_arxml = ""
#                 ecum_arxml = ""
#                 os_arxml = ""
#                 dem_arxml = ""
#                 frif_arxml = ""
#                 dem_mcu_arxml = ""
#                 # dependence module
#                 for depend_module in init_db["stub_depend_for_gentool"][Modulename]:
#                     if depend_module == "Mcu":
#                         if cfg_name == "CFG" or cfg_name == "RC":
#                             1
#                             # mcu_arxml = init_db["workspace"] + "/DescriptionFile/RH850_" + \
#                             #     "Com_" + cfg_num + ".arxml"
#                     elif depend_module == "Ecum":
#                         ecum_arxml = init_db["external_dir"] + "/X2x/common/generic/stubs/" + \
#                             ARVersion + "/EcuM/xml/EcuM_" + Modulename + ".arxml"
#                     elif depend_module == "Os":
#                         os_arxml = init_db["external_dir"] + "/X2x/common/generic/stubs/" + \
#                             ARVersion + "/Os/xml/Os_" + Modulename + ".arxml"
#                     elif depend_module == "Dem":
#                         dem_arxml = init_db["external_dir"] + "/X2x/common/generic/stubs/" + \
#                             ARVersion + "/Dem/xml/Dem_" + Modulename + ".arxml"
#                     elif depend_module == "FrIf":
#                         if cfg_name == "CFG" or cfg_name == "RC":
#                             frif_arxml = init_db["workspace"] + "/DescriptionFile/RH850_" + \
#                                 Modulename + ("If_" if cfg_name == "CFG" else "If_RC_") + cfg_num + ".arxml"
#                         else:
#                             frif_arxml = ""
#                     else:
#                         pass

#                 # gentool command
#                 cmd = init_db["gentoo_exe"]  + gentoo_exe + " -m " + Modulename + " -o " + output_path + " " + \
#                     main_cdf + " " + bswmdt + " " + trxml + " " + mcu_arxml + " " + ecum_arxml + " " + \
#                     os_arxml + " " + dem_arxml + " " + frif_arxml

#                 # execution of GenTool
#                 if build_option == "all" or  build_option == "generate_forward":
#                     # cleanup old output before run gentool
#                     if clean == 1:
#                         run_command("make -f ./ConfiguredDriver/make/makefile clean -j MSN=" + Modulename + \
#                                     " DEVICE=" + init_db[TestDeviceName]["device_family"][1] + " ARV=" + ARVersion + \
#                                     " CFG=" + cfg + " WS=./ " + " OSTest=" + OSTest, \
#                                     cwd=init_db["workspace"] + "/ConfiguredDriver/make")
#                     run_command(cmd, cwd=init_db["workspace"])

#                 if s0s1 == "False":
#                     # make the configured Driver, make file must be prepared in advanced.
#                     run_command("make -f ./ConfiguredDriver/make/makefile " + Modulename + " -j MSN=" + Modulename + \
#                                 " DEVICE=" + init_db[TestDeviceName]["device_family"][1] + " DEVICE_ID=" + \
#                                 TestDevice + " ARV=" + ARVersion + " CFG=" + cfg + " WS=./ " + " OSTest=" + OSTest, \
#                                 init_db["workspace"])
#                 else:
#                     pass
#             num = num + 1

#         with load_workbook('Test_spec.xlsx') as wb:
#             ws = wb["AES_Config"]
#             ws.cell(3, 3).value = TS_Compiler
#             ws.cell(23, 3).value = ccInclude
#             ws.cell(30, 3).value = Common_Test_Program 
#             for cel in ws.iter_rows()[0]:
#                 old_val = cel.value
#                 new_val = old_val.replace("{ARV}", ARVersion).replace("{DVN}", TestDeviceName).replace("{DVF}", \
#                     init_db[TestDeviceName]["device_family"][0]).replace("{ARBSW}", \
#                     ("R431" if ARVersion == "4_3_1" else "R422"))
#                 cel.value = new_val
#             wb.save('Test_spec.xlsx')
#     return True


# def prepare_test_app(wb):
#     """Change test app name in test spec and generate/update .c file

#     Arguments:
#         wb -- the input test spec book

#     Returns:
#         none -- it doesn't return a value
#     """
#     if build_option == "all":
#         global STAT_FLAG
#         # dictionary to store key as test app name and value is sequence name.
#         test_app_dict = {}
#         tool_cp_dict = {}
#         ws_sp = wb["Test_Spec"]

#         for row in [r for r in ws_sp.iter_rows(min_row=2) if r[2].value is not None and r[2].value != '']:
#             if TestType != "TIT":
#                 # Update test app column first
#                 mod_app_name = ''
#                 for raw_app_name in str(row[11].value).strip().strip('\n').strip().split('\n'):
#                     try:
#                         seq_numb = re.search("_([0-9]{4}).c", raw_app_name).group(1)
#                         testapp_name = raw_app_name.replace(seq_numb, row[2].value)
#                         test_app_dict.update({testapp_name: row[13].value})
#                         mod_app_name = testapp_name
#                     except:
#                         try:
#                             seq_numb = re.search("_([0-9]{4})_\d.c", raw_app_name).group(1)
#                             testapp_name = raw_app_name.replace(seq_numb, row[2].value)
#                             test_app_dict.update({testapp_name: row[13].value})
#                             mod_app_name += testapp_name + '\n'
#                         except:
#                             print(" [~] Error: {} is a wrong format name".format(raw_app_name))
#                             STAT_FLAG = 2  # Error
#                             print_status()
#                             sys.exit()
#                 row[11].value = mod_app_name.strip('\n')
#             else:
#                 if row[12].value is not None and row[12].value != '' and row[12].value != '-':
#                     testapp_name = str(row[12].value)
#                     if testapp_name in test_app_dict:
#                         print(" [~] Warning: Test app name {} should be unique in test suite.".format(testapp_name))
#                         STAT_FLAG = 1  # Warning
#                     else:
#                         test_app_dict.update({testapp_name: row[14].value})
#         if DEBUG_SCRIPT == True:
#             print("List of Test Application: {}".format(test_app_dict))

#         # Prepare all gentool checkpoints and insert into test app, except S0S1
#         if s0s1 != "True" and TestType != "VT":
#             # Step 3: Add Gentool checkpoint into test app belong to target_device
#             print("Step {}: Add Gentool checkpoint into test app file ".format(3 if TestType != "TIT" else 'Prepare'))
#             if TestType == "DIT":
#                 try:
#                     ws_pm = wb['PictMaster']
#                 except:
#                     print(" [~] Error: Sheet 'PictMaster' does not exist in test specification.")
#                     STAT_FLAG = 2  # Error
#                     print_status()
#                 tool_cp_dict = get_GenTool_checkpoint(ws_pm)
#             else:
#                 tool_cp_dict = get_GenTool_checkpoint(None)
#             print_debug("Parsing gentool checkpoint done", None, tool_cp_dict)
            
#             # loop for each test app then add gentool checkpoint
#             for test_app in sorted(test_app_dict.keys()):
#                 if test_app_dict[test_app] != "Build Log Check":
#                     update_test_app(test_app, tool_cp_dict)
            
#             # check and warn if any remain checkpoint in dictionary
#             if 0 != len(tool_cp_dict):
#                 print(" [~] Warning: Gentool checkpoint for {} are not inserted into any test app file.".format( \
#                     ", ".join(tool_cp_dict.keys())))
#                 STAT_FLAG = 1  # Warning

#             print_debug("Updating test app done", None)
#     return None


# def parse_checkpoint_info(ws):
#     """Get dictionary of checkpoint data from input sheet
    
#     Argument:
#         ws -- the input worksheet

#     Returns:
#         [dict] -- dictionaly of checkpoint infor of config key
#     """
#     cp_info_dict = {}
    
#     # get colum index of config
#     cfg_cp_dict = {}
#     for i in [0, 1]:
#         cfg_cp_dict.update({col[i].column: col[i].value for col in ws.iter_cols() if col[i].value is not None and \
#             ((col[i].value in Cfg_dict) or (('TIT' == TestType) and \
#             (re.search(CFG_NAME_PATTERN, col[i].value))))})
#     if 0 == len(cfg_cp_dict):
#         print(" [~] Warning: No config checkpoint is described in sheet " + ws.title)
#         STAT_FLAG = 1 # warning
#         return cp_info_dict
#     low_range_idx = min(cfg_cp_dict.keys())
#     idx_list = sorted(cfg_cp_dict.keys())[1:]
#     idx_list.append(ws.max_column)
    
#     for cur_idx in idx_list:
#         try:
#             cfg_idx = [col[2].value \
#                 for col in ws.iter_cols(min_col=low_range_idx, max_col=cur_idx)].index("Expected gentool output")
#         except:
#             print(" [~] Warning: Gentool checkpoint is not described in cfgSpec for " + cfg_cp_dict[low_range_idx])
#             STAT_FLAG = 1 # warning
#             break # skip this loop
#         cfg_idx = cfg_idx + low_range_idx
#         cfg_cp_info_list = [row[cfg_idx - 1].value for row in ws.iter_rows(min_row=4)]
#         if cfg_cp_dict[low_range_idx] not in cp_info_dict:
#             cp_info_dict.update({cfg_cp_dict[low_range_idx]: cfg_cp_info_list})
#         else:
#             print(" [~] Warning: Gentool checkpoint is dupplicated in cfgSpec for " + cfg_cp_dict[low_range_idx])
#             STAT_FLAG = 1 # warning
#         low_range_idx = cur_idx
#     return cp_info_dict


# def get_GenTool_checkpoint(ws_pm):
#     """Extract Gentool checkpoints from Renesas Configuration

#     Arguments:
#         ws_pm -- the input worksheet 'Pictmaster'

#     Returns:
#         [dict] -- dictionary of Gentool checkpoint code block of config key
#     """
#     global STAT_FLAG
#     cp_dict = {}

#     print("Step Prepare: Get Gentool checkpoint from 'Renesas_config' and 'Pict_master'")
#     # update checkpoint for CFG and RC
#     # if not exist in test spec, look for checkpoint in device CfgSpec
#     cfg_spec = "./DescriptionFile/RH850_{0}_DIT_CfgSpec_{1}.xlsx".format(MSN, TestDevice)
#     cfg_tool_cp = []
#     rc_tool_cp = []
#     try:
#         wb_cs = load_workbook(cfg_spec, data_only=True)
#         cfg_ws = wb_cs['CFG']
#         rc_ws = wb_cs['RC']
#     except:
#         print(" [~] Warning: Could not get sheet 'RC' or 'CFG' in file {0}.".format(cfg_spec))
#         STAT_FLAG = 1  # Warning
#         return cp_dict
    
#     # after getting sheet, parse data from cfgSpec of CFG and RC
#     try:
#         cfg_cp_dict = parse_checkpoint_info(cfg_ws)
#         cfg_cp_dict.update(parse_checkpoint_info(rc_ws))
#     except:
#         print(" [~] Warning: Gentool checkpoint is dupplicated in cfgSpec sheets")
#         STAT_FLAG = 1 # warning
#     wb_cs.close()
#     # loop for each Renesas config
#     for cfg in cfg_cp_dict.keys():
#         code_block = "  #if ({0}_TIT_TEST == STD_ON)\r\n{1}  #endif\r\n"
#         null_cp_str = "  #ifndef {0}\r\n  AES_TEST_ASSERT(1);\r\n  #else\r\n  AES_TEST_ASSERT(0);\r\n  #endif\r\n"
#         code_text_ins0 = ""
#         code_text_ins1 = ""
#         for row in cfg_cp_dict[cfg]:
#             cell_value = str(row).replace("\r\n","\n").strip().strip("\n").split("\n")
#             row_idx = cfg_cp_dict[cfg].index(row) + 4
#             for cp in (x for x in cell_value if x != "" and x != "-" and x != " "):
#                 if "==" in cp:
#                     # if checkpoint is for instance 0
#                     if cp.split("==")[0].endswith("(0)"):
#                         code_text_ins0 += "  AES_TEST_ASSERT(" + cp.replace("(0)", "_0") + ");\r\n"
#                     # if checkpoint is for instance 1
#                     if cp.split("==")[0].endswith("(1)"):
#                         code_text_ins1 += "  AES_TEST_ASSERT(" + cp.replace("(1)", "_1") + ");\r\n"
#                     else:
#                         code_text_ins0 += "  AES_TEST_ASSERT(" + cp + ");\r\n"
#                 elif re.search("NOT\(([\x00-\x7F]+)\)", cp):
#                     code_text_ins0 += null_cp_str.format(
#                         re.search("NOT\(([\x00-\x7F]+)\)", cp).group(1))
#                 else:
#                     print(" [~] Warning: Wrong format cell in row {0} of {1}".format(row_idx, cfg))
#                     STAT_FLAG = 1  # Warning
#         # update the inserted code block into dictionary
#         cp_dict.update({cfg: [code_block.format(MSN, code_text_ins0), code_block.format(MSN, code_text_ins1)]})

#     if ws_pm is not None:
#         # update checkpoint for DEF
#         # Read the Pictmaster sheet then prepare the DIT checkpoint
#         try:
#             first_col = [row[0].value for row in ws_pm.iter_rows()]
#             if TestDeviceName in first_col:
#                 start_row = first_col.index(TestDeviceName) + 1 # TestDeviceName = U2A16,U2A8(373), U2A8(292)
#             else: 
#                 start_row = first_col.index(TestDevice) + 1 # TestDeviceName = 702300EBBG,702301EBBA, 702301EABG
#             try:
#                 end_row = first_col.index([i for i in first_col[start_row+1:] \
#                           if i in init_db["device_map"].values()][0]) +1
#             except:
#                 end_row = ws_pm.max_row + 1
#         except:
#             print(" Info section for device is not specified, parse whole sheet.")
#             start_row = 0
#             end_row = ws_pm.max_row + 1
#         data = [[cell.value for cell in row if cell.column > 1 and cell.value != "" and cell.value != None] \
#                 for row in ws_pm.iter_rows(min_row=start_row, max_row=end_row) \
#                 if re.search('.*_'+Msn+'_DEF_[0-9]{3}', str(row[2].value)) != None or row[2].value == "Configuration"]
#         if len(data) < 2 or data[0][0] != "No. ID" or data[0][1] != "Configuration":
#             print(" [~] Error: Content of sheet PictMaster is wrong format !!!")
#             STAT_FLAG = 2  # Error
#             print_status()
#         else:
#             # format of checkpoint for each test app
#             code_block = "  #if ({0}_TIT_TEST == STD_ON)\r\n{1}  #endif\r\n"
#             # loop all def cdf
#             for def_config in range(1,len(data)):
#                 # reset the content of checkpoint to None
#                 code_text = ""
#                 # at each cdf loop all macros
#                 for macro, index in zip(data[0][2:], range(2, len(data[0]))):
#                     # get the value of macro at the same column
#                     try:
#                         macro_value = data[def_config][index]
#                     except:
#                         print("[~] Error: Pictmaster Test: one of values of macro is empty")
#                         STAT_FLAG = 2  # Error
#                         print_status()
#                     # combine macro and value to be a checkpoint
#                     # check if the macro is not "-", because "-" presented for not generated.
#                     if macro_value != "-":
#                         code_text += "  AES_TEST_ASSERT({} == {});\r\n".format(macro, macro_value)
#                 cp_dict.update({"DEF"+str(def_config).zfill(3): [code_block.format(MSN, code_text), ""]})
#     return cp_dict


# def update_test_app(app_name, tool_cp):
#     """Update Gentool checkpoint test app .c file

#     Arguments:
#         app_name {string} -- name of the test app to write
#         tool_cp {string} -- config match with test app

#     Returns:
#         none -- it doesn't return a value
#     """
#     global STAT_FLAG
#     try:
#         cfg_name = re.search(CFG_NAME_PATTERN, app_name).group()
#     except:
#         print(" [~] Warning: Couldn't parse config name from test app name {}".format(app_name))
#         cfg_name = ""
#     if DEBUG_SCRIPT == True:
#         print("  Start updating for file: {}".format(app_name))
#     output = []
#     isMain = False    # Use to identify the index of Main function if it existed
#     # if cfg_name not in checkpoint dict, return function
#     if cfg_name not in tool_cp.keys():
#         return None
#     # open file
#     try:
#         file = open("./TestProgram/" + app_name, "r+")
#     except:
#         if TestType != "TIT":
#             print(" [~] Warning: File {} doesn't exist in test app folder".format(app_name))
#             return None
#         else:
#             try:
#                 file = open("./TestProgram/" + MSN + "_TIT_Template_App.c", "r+")
#             except:
#                 print(" [~] Error: Can not find test app template file.")
#                 STAT_FLAG = 2  # Error
#                 print_status()
#     # use readline() to read the first line 
#     line = file.readline()
#     isFirstMain = True
#     while line:
#         output.append(line)
#         if re.search("(main.*\()", line) and isFirstMain:
#             isMain = True
#             isFirstMain = False
#         if "{" in line and isMain == True and ("REQ" in app_name and ("CFG"in cfg_name or "RC" in cfg_name)):
#             isMain = False
#             if app_name.endswith('_0.c'):
#                 if not tool_cp[cfg_name][0].endswith('_0.c'):
#                     if not tool_cp[cfg_name][1].endswith('_1.c'):
#                         output.append(tool_cp[cfg_name][0])
#                         tool_cp[cfg_name][0] = app_name
#                     else:
#                         if app_name == tool_cp[cfg_name][1].replace('_1.c', '_0.c'):
#                             output.append(tool_cp[cfg_name][0])
#                             tool_cp.pop(cfg_name)
#                         else:
#                             pass
#                 else:
#                     pass
#             elif app_name.endswith('_1.c'):
#                 if not tool_cp[cfg_name][1].endswith('_1.c'):
#                     if not tool_cp[cfg_name][0].endswith('_0.c'):
#                         output.append(tool_cp[cfg_name][1])
#                         tool_cp[cfg_name][1] = app_name
#                     else:
#                         if app_name == tool_cp[cfg_name][0].replace('_0.c', '_1.c'):
#                             output.append(tool_cp[cfg_name][1])
#                             tool_cp.pop(cfg_name)
#                         else:
#                             pass
#                 else:
#                     pass
#             else:
#                 output.append(tool_cp[cfg_name][0])
#                 tool_cp.pop(cfg_name)
#             print(" [~] Info: Gentool checkpoint of config {0} was inserted into file {1}".format(
#                 cfg_name, app_name))
#         elif "{" in line and isMain == True and ("DEF" in app_name and "DEF" in cfg_name):
#             isMain = False
#             output.append(tool_cp[cfg_name][0])
#             tool_cp.pop(cfg_name)
#             print(" [~] Info: Gentool checkpoint of config {0} was inserted into file {1}".format(
#                 cfg_name, app_name))
#         elif "{" in line and isMain == True and (TestType == "TIT" and cfg_name in app_name):
#             isMain = False
#             output.append(tool_cp[cfg_name][0]+tool_cp[cfg_name][1])
#             tool_cp.pop(cfg_name)
#             print(" [~] Info: Gentool checkpoint of config {0} was inserted into file {1}".format(
#                 cfg_name, app_name))
#         else:
#             pass
#         line = file.readline() # use realine() to read next line
#     file.close()
#     file = open("./TestProgram/"+app_name, "w")
#     file.writelines(output)
#     file.close()
#     if DEBUG_SCRIPT == True:
#         print("  Done for file '{}'".format(app_name))
#     return None


# def print_status(last_message=False):
#     """print a status at the completion of the program or print error and exit if failed
#     """
#     if 2 == STAT_FLAG:
#         if False != ignore_error and False == last_message:
#             print(" [~] Error: An error has occurred at recent command!")
#         else:
#             print(
#                 " [~] Exit: Workspace initialization for {0} {1} is not completed due to error(s) occur".format( \
#                 Msn, TestType))
#         if not ExeOp:
#             sys.exit()
#         else:
#             os._exit(1)
#     elif 1 == STAT_FLAG:
#         print(" [~] Exit: Workspace initialization for {0} {1} is completed with warning(s)".format( \
#             Msn, TestType))
#     else:
#         print(" [~] Exit: Workspace initialization for {0} {1} is completed!".format(
#             Msn, TestType))
#     print('\r### ' + datetime.datetime.now().strftime(u'%Y/%m/%d') + ' ' +
#         datetime.datetime.now().strftime(u'%H:%M:%S') + ' ###')
#     print('### Duration: ' + str(time.time() - start_time) + 's ###')


def main():
    """main function of the program

    Returns:
        int -- status of the main function. 1 is successful
    """
    test_spec_path = 'U:/internal/RCar/V3U/modules/dio/workspace/4.2.2/Test_spec.xlsx'
    test_cfg = Config_IT()

    if get_input_command(test_cfg):
        if not test_cfg.skip_copy_workspace:
            if not prepare_env_for_IT_Test(test_cfg):
                print("Fail when prepare env")
                sys.exit(1)
    
        # wb = load_workbook('Test_spec.xlsx', data_only=True)
        wb = load_workbook(test_spec_path, data_only=True)
        modify_testspec(test_cfg, wb)
        # if record_test_evident():
        #     clean_make_file()
        #     modify_testspec(wb)
        #     prepare_test_app(wb)
        #     wb.save('Test_spec.xlsx')
        #     make_configured_driver(wb)
        #     # print_status(True)
        wb.save(test_spec_path)
        wb.close()

if __name__ == '__main__':
    main()

# ------------------------------------------------------------------------------
# end of script                                              
# ------------------------------------------------------------------------------
