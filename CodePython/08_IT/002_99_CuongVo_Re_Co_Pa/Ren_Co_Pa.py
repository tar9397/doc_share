#!/usr/bin/python3
# -*- coding: utf-8 -*-

################################################################################
# Project      = MCAL Automatic Evaluation System                              #
# Module       = Ren_Co_Pa.py                                                  #
# Version      = 1.00                                                          #
# Date         = 13/07/2020                                                    #
# AES Version  = 2.1.0(Official Version)                                       #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2020 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This file supports in generating Renesas Configuration file expressed by     #
# Microsoft Excel file.                                                        #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  13/07/2020     : Initial Version                                     #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------


from __future__ import print_function

import os
import re
import sys
import math
from copy import deepcopy, copy
from openpyxl import load_workbook
from openpyxl.utils.cell import get_column_letter
from openpyxl.styles import Alignment, Border, Side, Font, PatternFill
from openpyxl.formatting.formatting import ConditionalFormattingList
import xml.etree.ElementTree as ET

# -----------------------------------------------------------
# Class definition
# -----------------------------------------------------------
class Logger(object):
    def __init__(self):
        self.terminal = sys.stdout
        self.log = open("Debug.log", "w")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        # this flush method is needed for python 3 compatibility.
        # this handles the flush command by doing nothing.
        # you might want to specify some extra behavior here.
        pass

class PDFContainer():
    def __init__(self, name, rowIndex, daddy=None, range="NA"):
        self.Name = name
        self.RowIndex = rowIndex
        self.Daddy = daddy
        self.Range = range
        if range == "NA" or range == "N/A":
            self.Range = "NA"
            if rowIndex not in GV.PDFNAList: GV.PDFNAList.append(rowIndex)

        self.Kids = []
        self.Items = []
        self.CDFContainerList = []
        self.Length = 0

    def get_path(self):
        path = self.Name
        daddy = self.Daddy
        while daddy != None:
            path = daddy.Name + "/" + path
            daddy = daddy.Daddy
        return path

    def print_indent(self, indent, message):
        print("\t"*indent + message)

    def print_container_tree(self, printCDF=False, indent=1):
        message = "{:<5}{}".format(str(self.RowIndex) + ".", self.Name)
        if printCDF:
            message += ":"
            for cdfContainer in self.CDFContainerList:
                message += ("\t" + cdfContainer.ShortName)
        self.print_indent(indent, message)
        indent += 1
        for item in self.Items:
            message = "{:<5}{}".format(str(item.RowIndex) + ".", item.Name)
            if printCDF:
                message += ":"
                for cdfItem in item.CDFItemList:
                    message += ("\t" + cdfItem.Value)
            self.print_indent(indent, message)
        for kid in self.Kids:
            kid.print_container_tree(indent=indent, printCDF=printCDF)
        return

    def find_container_by_path(self, path):
        pathList = path.split("/")
        while "" in pathList:
            pathList.remove("")
        if len(pathList) == 0:
            return None
        if pathList[0] != self.Name:
            return None
        if len(pathList) == 1 and pathList[0] == self.Name:
            return self
        for kid in self.Kids:
            tempFind = kid.find_container_by_path("/".join(pathList[1:]))
            if tempFind is not None:
                return tempFind
        return None

class PDFItem():
    def __init__(self, name, rowIndex, daddy=None, range="NA"):
        self.Name = name
        self.RowIndex = rowIndex
        self.Daddy = daddy
        self.Range = range
        if range == "NA" or range == "N/A":
            self.Range = "NA"
            if rowIndex not in GV.PDFNAList: GV.PDFNAList.append(rowIndex)

        self.Length = 0
        self.CDFItemList = []

    def get_path(self):
        path = self.Name
        daddy = self.Daddy
        while daddy != None:
            path = daddy.Name + "/" + path
            daddy = daddy.Daddy
        return path
        
        
class CDFContainer():
    def __init__(self, shortName, defRef, pdfContainer, daddy=None):
        self.ShortName = shortName
        self.DefRef = defRef
        self.Daddy = daddy
        self.PDFContainer = pdfContainer

        self.Kids = []
        self.Items = []
        self.Coord = [None, None]

    def get_path(self):
        path = self.ShortName
        daddy = self.Daddy
        while daddy != None:
            path = daddy.ShortName + "/" + path
            daddy = daddy.Daddy
        return path

    def print_indent(self, indent, message):
        print("\t"*indent + message)

    def print_container_tree(self, indent=1):
        self.print_indent(indent, self.ShortName)
        indent += 1
        for item in self.Items:
            self.print_indent(indent, "{}: {}".format(item.Name, item.Value))
        for kid in self.Kids:
            kid.print_container_tree(indent=indent)
        return

class CDFItem():
    def __init__(self, name, defRef, value, pdfItem, daddy=None):
        self.Name = name
        self.DefRef = defRef
        self.Value = value
        self.Daddy = daddy
        self.PDFItem = pdfItem

        self.Coord = [None, None]

    def get_path(self):
        path = self.Name
        daddy = self.Daddy
        while daddy != None:
            path = daddy.Name + "/" + path
            daddy = daddy.Daddy
        return path

class PDFVer():
    def __init__(self, daddy=None):
        self.PDFContainer = None
        self.Daddy = daddy
        self.Kids = []
        self.CDFContainerArr = [None] * GV.CDFListLen

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------

class GlobalVariables(object):
    DEBUG_FLAG = None
    DEBUG_CONTENT = None
    VERTICAL_FORMAT = None
    MERGE_CELL = None
    HelpDescription = None
    MSN = None
    MSNList = None
    CDFList = None
    CDFListLen = None
    NsXml = None
    PrefixDef = None
    WorkBook = None
    WorkSheet = None
    MaxColumn = None
    MaxRow = None
    PdfSheetName = None
    PdfColName = None
    PdfColIndex = None
    MultiColIndex = None    
    PDFRootContainerOriginal = None
    PDFRootContainer = None
    PDFRootContainerDict = None
    PDFRootVerList = None
    PDFNAList = None
    CONST_TITLE_ROW_INDEX = None
    CONST_RANGE_ROW_INDEX = None
    CONST_ROOT_ROW_INDEX = None
    CONST_ROOT_COLUMN_INDEX = None
    CONST_OUTPUT_FILE_NAME = None
    CONST_PDF_FILE_NAME = None

# GLOBAL VARIABLES
GV = GlobalVariables()

# Flag for debugging
GV.DEBUG_FLAG = False

# Debugging content signal
GV.DEBUG_CONTENT = ['*****************START_DEBUG_SECTION*****************', '*****************END_DEBUG_SECTION*****************']

# Flag for vertical format
GV.VERTICAL_FORMAT = False

# Flag for merging cells in horizontal format
GV.MERGE_CELL = True

# Help description for command line input
GV.HelpDescription = '''
[INPUT]
PDF design: RH850_X2x_<MSN>_ParameterDefinition.xlsx
CDF files: *.arxml
  must be placed at the same directory as this script file.

[OUTPUT]
RenesasConfiguration.xlsx

[EXAMPLE]:
E.g:
(1)  Ren_Co_Pa.py ADC E2UH_468pin RH850_ADC_001.arxml RH850_ADC_002.arxml
(2)  Ren_Co_Pa.exe MCU U2A16 RH850_MCU_001.arxml RH850_MCU_002.arxml --pdf_sheet 'Parameter Definition (U2x)' 

[USAGE]
Syntax:
  Ren_Co_Pa.py/.exe MSN COL_NAME CDF [CDFs[]] [--pdf_sheet PDF_SHEET_NAME] [--vertical] [--unmerge] [-h] [--help] [--debug]
Positional arguments:
  MSN               Module Short Name (ADC, CAN, DIO,...)
  COL_NAME          Column name under the 'Range' column in PDF design sheet (E2M, U2A16, E2UH_468pin, 702300EBBG)
  CDF               At least one cdf must be provided
Optional arguments:
  CDFs[]            Other CDFs to be inputted.
  --pdf_sheet PDF_SHEET_NAME
                    Sheet to be used in PDF design, sheet name must be inside the quotes
                    (Default: 'Parameter Definition')
  --vertical        Distribute CDF data in vertical format.
                    (Default: horizontal format)
  --unmerge         Unmerged cell when horizontal format is used.
                    (Deafult: Disable - Cells will be merged)
  -h --help         Print help description
  --debug           Flag supports in debugging, this will print out processing data at some sections.
                    All log data will be recorded at Debug.log file
                    (Default: Disable)

[NOTE]
There are 3 types of message:
  [+]: Information
  [!]: Warning
  [x]: Error
Searching in the log may help you to resolve the issue.
'''

# Module Short Name
GV.MSN = ''

# Module Short Name List
GV.MSNList = ["ADC", "CAN", "DIO", "ETH", "FLS", "FR", \
              "GPT", "ICU", "LIN", "MCU", "PORT", "PWM", "SPI", "WDG"]

# CDFList
GV.CDFList = []

# Length of CDFList
GV.CDFListLen = -1

# CDF xml namespace ("http://autosar.org/schema/r4.0",...)
GV.NsXml = ''

# Prefix of defition ref of CDF file ("/Renesas/EcucDefs_Adc",...)
GV.PrefixDef = ''

# Input WorkBook
GV.WorkBook = None

# Input WorkSheet
GV.WorkSheet = None

# WorkSheet Max Column, to be the first column index of Renesas Config data
GV.MaxColumn = -1

# WorkSheet Max Row, to be the first row index of Renesas Config data
GV.MaxRow = -1

# Sheet name to be used in PDF design
GV.PdfSheetName = 'Parameter Definition'

# Column name to determine the device of CDFs in PDF design
GV.PdfColName = ''

# Column index to determine the device of CDFs in PDF design
GV.PdfColIndex = -1

# Column index of Multiplicity column in PDF design
GV.MultiColIndex = -1

# Root Container of data from PDF
GV.PDFRootContainerOriginal = None

# Root Container for walking thru
GV.PDFRootContainer = None

# Root Container list of data from both CDF and PDF
GV.PDFRootContainerDict = {}

# Root Container list to combine CDF for vertical format 
GV.PDFRootVerList = []

# List of PDF rows (Containers or Items) that NA for device inputted
GV.PDFNAList = []

# Constant definition
# Row and column index
GV.CONST_TITLE_ROW_INDEX = 3
GV.CONST_RANGE_ROW_INDEX = 5
GV.CONST_ROOT_ROW_INDEX = 6
GV.CONST_ROOT_COLUMN_INDEX = 4

# File Name
GV.CONST_OUTPUT_FILE_NAME = "RenesasConfiguration.xlsx"
GV.CONST_PDF_FILE_NAME = "RH850_X2x_{}_ParameterDefinition.xlsx"
# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------


def print_inf(message, level=1):
    print("  "*level + "[+] " + format(message))
    return

def print_war(message, level=1):
    print("  "*level + "[!] " + format(message))
    return

def print_err(message, level=1):
    print("  "*level + "[x] " + format(message))
    sys.exit()
    return

def print_deb(message, method=None, args=[], kargs={}):
    if GV.DEBUG_FLAG:
        print(GV.DEBUG_CONTENT[0])
        if message != None and message != "":
            print(message)
        if method != None:
            method(*args, **kargs)
        print(GV.DEBUG_CONTENT[1])
    return


def get_input_command():
    global GV

    # Validate and process input
    rawArgList = sys.argv

    # Check raw arguments
    returnHelpFlag, printHelpFlag = [True, True] if (
        len(rawArgList) < 4) else [False, False]
    while ("--help" in rawArgList):
        rawArgList.remove("--help")
        printHelpFlag = True
    while ("-h" in rawArgList):
        rawArgList.remove("-h")
        printHelpFlag = True
    while ("--pdf_sheet" in rawArgList):
        pdfSheetNameIndex = rawArgList.index("--pdf_sheet") + 1
        if pdfSheetNameIndex == len(rawArgList):
            print(GV.HelpDescription)
            print_err("Cannot find PDF_SHEET_NAME")
        GV.PdfSheetName = rawArgList[pdfSheetNameIndex]
        del rawArgList[pdfSheetNameIndex]
        del rawArgList[pdfSheetNameIndex-1]
    if printHelpFlag:
        print(GV.HelpDescription)
        if returnHelpFlag:
            print_err("Two few arguments in command input")
    # DEBUG_FLAG
    while ("--debug" in rawArgList):
        rawArgList.remove("--debug")
        GV.DEBUG_FLAG = True
    if GV.DEBUG_FLAG:
        sys.stdout = Logger()
    # MERGE_CELL
    while ("--unmerge" in rawArgList):
        rawArgList.remove("--unmerge")
        GV.MERGE_CELL = False
    # VERTICAL_FORMAT
    while ("--vertical" in rawArgList):
        rawArgList.remove("--vertical")
        GV.VERTICAL_FORMAT = True
    # MSN
    if rawArgList[1].upper() not in GV.MSNList:
        print(GV.HelpDescription)
        print_err("MSN must be in " + ", ".join(GV.MSNList))
    GV.MSN = rawArgList[1].upper()
    GV.CONST_PDF_FILE_NAME = GV.CONST_PDF_FILE_NAME.format(GV.MSN)
    # COL_NAME
    GV.PdfColName = rawArgList[2]
    # CDF files at last with all remaining arguments
    for i in range(3, len(rawArgList)):
        if not rawArgList[i].endswith(".arxml"):
            print_err("Cannot determine {} argument ".format(rawArgList[i]))
        else:
            GV.CDFList.append(rawArgList[i])
    if not GV.CDFList:
        print(GV.HelpDescription)
        print_err("Cannot find any CDF files in command input")
    return


def cell_str(row, col, ws=None):
    global GV
    if ws is None:
        ws = GV.WorkSheet
    retStr = ws.cell(row=row, column=col).value
    return "" if retStr is None else str(retStr)


def container_walk(daddy, rowRange, col):
    global GV
    if col == GV.MultiColIndex:
        return
    startRow = None
    startName = None
    for rowId in range(rowRange[0], rowRange[1] + 1):
        preCell = cell_str(rowId-1, col)
        curCell = cell_str(rowId, col)
        if curCell == "" and startRow == None:
            # If no item or container was created but theo cell is empty
            print_err("The cell at row[{}] & col[{}] must not be empty".format(rowId, col), level=2)
        else:
            if curCell == "":
                # Currently in a container
                continue
            else:
                # New item or container
                print_inf("Proceeding item " + curCell, level=2)
                if preCell != "":
                    # Previous is an item, update items has gone
                    item = PDFItem(preCell, startRow, daddy=daddy, range=cell_str(startRow, GV.PdfColIndex))
                    daddy.Items.append(item)
                else:
                    # If not first, previous is container
                    if startName != None: # update the container has gone 
                        daddy.Kids.append(PDFContainer(startName, startRow, daddy=daddy, range=cell_str(startRow, GV.PdfColIndex)))
                        container_walk(daddy.Kids[-1], [startRow+1, rowId-1], col+1)
                # Initialize new item or container
                startRow = rowId
                startName = curCell
    
    # Process the last
    if startRow == rowRange[1]:
        # The last is item, update it
        item = PDFItem(startName, startRow, daddy=daddy, range=cell_str(startRow, GV.PdfColIndex))
        daddy.Items.append(item)
    else:
        # The last is container
        daddy.Kids.append(PDFContainer(startName, startRow, daddy=daddy, range=cell_str(startRow, GV.PdfColIndex)))
        container_walk(daddy.Kids[-1], [startRow+1, rowRange[1]], col+1)
    return


def create_container_tree():
    global GV

    print_inf("Getting data tree from PDF Design")
    firstCellIndex = [GV.CONST_ROOT_ROW_INDEX, GV.CONST_ROOT_COLUMN_INDEX]
    GV.PDFRootContainerOriginal = PDFContainer(cell_str(firstCellIndex[0], firstCellIndex[1]), firstCellIndex[0], range=cell_str(firstCellIndex[0], GV.PdfColIndex))
    if GV.PDFRootContainerOriginal.Name.upper() not in GV.MSN:
        print_err("Cannot find the root container of '{}'".format(GV.MSN), level=2)
    
    # Find row range Container Structure
    rowRange = GV.MaxRow
    while rowRange > 0:
        if cell_str(rowRange, GV.MultiColIndex) != "":
            break
        rowRange -= 1
    container_walk(GV.PDFRootContainerOriginal, [firstCellIndex[0]+1, rowRange], firstCellIndex[1]+1)
    print_inf("Create data tree completely from PDF design")
    return


def proceed_pdf_design():
    global GV

    # Open Workbook
    try:
        print_inf("Opening PDF Design '{}'".format(GV.CONST_PDF_FILE_NAME))
        GV.WorkBook = load_workbook(GV.CONST_PDF_FILE_NAME)
        print_inf("Opened PDF Design '{}'".format(GV.CONST_PDF_FILE_NAME))
    except:
        print_err("Cannot open '{}'".format(GV.CONST_PDF_FILE_NAME))

    # Open Worksheet
    try:
        GV.WorkSheet = GV.WorkBook[GV.PdfSheetName]
    except:
        print_err("Cannot parse '{}' sheet".format(GV.PdfSheetName))
    
    # None-used: Unmerge cells before manupilation
    # for mergedCellRanges in GV.WorkSheet.merged_cells.ranges:
    #     GV.WorkSheet.unmerge_cells(range_string=mergedCellRanges.coord)
    GV.MaxColumn = GV.WorkSheet.max_column
    GV.MaxRow = GV.WorkSheet.max_row

    # Check for the column name by go through all columns in reverse way
    i = GV.MaxColumn
    while i > 0:
        cellValue = cell_str(GV.CONST_RANGE_ROW_INDEX, i)
        if GV.PdfColName in cellValue:
            GV.PdfColIndex = i
            break
        else:
            # Hide column that not used
            GV.WorkSheet.column_dimensions.group(start=get_column_letter(i), hidden=True)
            i -= 1
    if GV.PdfColIndex == -1:
        print_err("Cannot find '{}' column in '{}' sheet".format(GV.PdfColName, GV.PdfSheetName))
    
    # Hide first unused column (ID, Attribute, Ref,...)
    if GV.CONST_ROOT_COLUMN_INDEX > 1:
        GV.WorkSheet.column_dimensions.group(start="A", end=get_column_letter(GV.CONST_ROOT_COLUMN_INDEX-1), hidden=True)

    i = 1
    while i < GV.PdfColIndex:
        cellValue = cell_str(GV.CONST_TITLE_ROW_INDEX, i)
        if "Multiplicity" == cellValue:
            GV.MultiColIndex = i
            # Increase index to ignore the next column
            i += 1
        else:
            # Hide the remaining columns that not used
            if GV.MultiColIndex != -1:
                GV.WorkSheet.column_dimensions.group(start=get_column_letter(i), hidden=True)
        i += 1
    if GV.MultiColIndex == -1:
        print_err("Cannot find 'Multiplicity' column in '{}' sheet".format(GV.PdfSheetName))

    # Adjust all row size
    for i in range(GV.CONST_ROOT_ROW_INDEX, GV.MaxRow+1):
        GV.WorkSheet.row_dimensions[i].height = 16
    # Freeze suitable panes
    GV.WorkSheet.freeze_panes = GV.WorkSheet[get_column_letter(GV.MaxColumn+1)+str(GV.CONST_ROOT_ROW_INDEX)]
    # Delete redundant worksheets
    for sheetName in GV.WorkBook.sheetnames:
        if sheetName != GV.PdfSheetName:
            GV.WorkBook.remove(GV.WorkBook[sheetName])

    # Get data from PDF Design
    create_container_tree()

    # Delete conditional format in worksheet
    GV.WorkSheet.conditional_formatting = ConditionalFormattingList()
    return

def print_xml_tag(root):
    for child in root:
        print(child.tag)
        print_xml_tag(child)

def namespace_parser(element):
    m = re.match(r'\{.*\}', element.tag)
    return m.group(0) if m else ''

def find_child_xml(element, match):
    global GV
    return element.find(GV.NsXml + match)

def find_child_all_xml(element, match):
    global GV
    return element.findall(GV.NsXml + match)

def find_xml(element, match):
    global GV
    for child in element:
        if child.tag == GV.NsXml + match:
            return child
        tempFind = find_xml(child, match)
        if tempFind is not None:
            return tempFind
        continue
    return None

def find_all_xml(element, match):
    global GV
    ret = []
    for child in element:
        if child.tag == GV.NsXml + match:
            ret.append(child)
        tempFind = find_all_xml(child, match)
        if len(tempFind) > 0:
            ret.extend(tempFind)
    return ret

def parse_item_from_xml(cdfContainer, element, typeItem="PARAM"):
    if typeItem == "PARAM":
        typeItem = "PARAMETER-VALUES"
        valueTag = "VALUE"
    else:
        typeItem = "REFERENCE-VALUES"
        valueTag = "VALUE-REF"
        
    paramListXml = find_child_xml(element, typeItem)
    if paramListXml is not None:
        for paramXml in  paramListXml:
            defRef = find_child_xml(paramXml, "DEFINITION-REF").text.replace(GV.PrefixDef, "")
            name = defRef.split("/")[-1]
            notFound = True
            for pdfItem in cdfContainer.PDFContainer.Items:
                if name != pdfItem.Name: 
                    continue
                value = find_child_xml(paramXml, valueTag)
                if value is None:
                    print_war("{} value is not set".format(defRef), level=2)
                    continue
                value = value.text
                cdfItem = CDFItem(name, defRef, value, pdfItem, daddy=cdfContainer)
                cdfContainer.Items.append(cdfItem)
                pdfItem.CDFItemList.append(cdfItem)
                notFound = False
                break
            if notFound:
                print_war("Cannot find '{}' {} in PDF design".format(name, typeItem), level=2)
    return


def parse_container_from_xml(element):
    shortName = find_child_xml(element, "SHORT-NAME").text
    defRef = find_child_xml(element, "DEFINITION-REF").text.replace(GV.PrefixDef, "")
    pdfContainer = GV.PDFRootContainer.find_container_by_path(defRef)
    if pdfContainer is None:
        print_war("Cannot find path '{}' of container in PDF design".format(defRef), level=2)
        return None
    cdfContainer = CDFContainer(shortName, defRef, pdfContainer)
    pdfContainer.CDFContainerList.append(cdfContainer)
    # Parsing parameter and reference values
    parse_item_from_xml(cdfContainer, element, "PARAM")
    parse_item_from_xml(cdfContainer, element, "REFER")
    # Parsing kid containers
    containerListXml = find_child_xml(element, "SUB-CONTAINERS")
    if containerListXml is not None:
        for containerXml in find_child_all_xml(containerListXml, "ECUC-CONTAINER-VALUE"):
            childContainer = parse_container_from_xml(containerXml)
            if childContainer is not None:
                childContainer.Daddy = cdfContainer
                cdfContainer.Kids.append(childContainer)
    return cdfContainer

def parse_cdf_data(rootElement):
    global GV
    GV.NsXml = namespace_parser(rootElement)
    cdfRootContainerList = []
    # Get root container
    rootContainerXmlList = find_all_xml(rootElement, "ECUC-MODULE-CONFIGURATION-VALUES")
    for rootContainerXml in rootContainerXmlList:
        if rootContainerXml is None:
            print_war("Cannot find root conainer in this CDF file", level=2)
            continue
        shortName = find_child_xml(rootContainerXml, "SHORT-NAME").text
        defRef = find_child_xml(rootContainerXml, "DEFINITION-REF").text
        prefixDef = defRef[:defRef.rfind("/")]
        defRef = defRef.replace(prefixDef, "")
        GV.PrefixDef = prefixDef
        if GV.PDFRootContainer.find_container_by_path(defRef) is None:
            print_war("Cannot find root path '{}' of container in PDF design".format(defRef), level=2)
            continue
        cdfRootContainer = CDFContainer(shortName, defRef, GV.PDFRootContainer)
        GV.PDFRootContainer.CDFContainerList.append(cdfRootContainer)
        # Get kid containers
        for childXml in find_child_all_xml( \
            find_child_xml(rootContainerXml, "CONTAINERS"), \
            "ECUC-CONTAINER-VALUE"):
            childContainer = parse_container_from_xml(childXml)
            if childContainer is not None:
                childContainer.Daddy = cdfRootContainer
                cdfRootContainer.Kids.append(childContainer)
        cdfRootContainerList.append(cdfRootContainer)
    return cdfRootContainerList


def proceed_cdf_files():
    global GV
    for cdfFileName in GV.CDFList.copy():
        try:
            tree = ET.parse(cdfFileName)
        except:
            print_war("Cannot parse CDF arxml file '{}'".format(cdfFileName))
            GV.CDFList.remove(cdfFileName)
            continue
        print_inf("Parsing CDF file '{}'".format(cdfFileName))
        GV.PDFRootContainer = deepcopy(GV.PDFRootContainerOriginal)
        cdfContainerList = parse_cdf_data(tree.getroot())
        if len(cdfContainerList) == 0:
            print_war("Parse error for CDF file '{}'".format(cdfFileName))
            GV.CDFList.remove(cdfFileName)
            continue
        else:
            print_inf("Parse complete CDF file '{}'".format(cdfFileName))
            GV.PDFRootContainerDict[cdfFileName] = GV.PDFRootContainer
            for cdfContainer in cdfContainerList:
                print_deb("DATA PARSED FROM: {} WITH ROOT CONTAINER SHORT NAME: {}".format(cdfFileName, cdfContainer.ShortName),
                    method=cdfContainer.print_container_tree)
    return

def print_pdf_containers(cdfContainerList):
    cdfContainerList.sort(key=lambda con: con.Coord[0])
    if len(cdfContainerList) > 0:
        content = ("\t" * (cdfContainerList[0].Coord[0] - 1))
        for cdfContainer in cdfContainerList:
            content += (cdfContainer.ShortName + ("\t" * cdfContainer.Coord[1]))
        print(content)
    else:
        print("")
    return

def print_pdf_items(cdfItemList):
    cdfItemList.sort(key=lambda item: item.Coord[0])
    if len(cdfItemList) > 0:
        previousCoord = cdfItemList[0].Coord
        content = ("\t" * (cdfItemList[0].Coord[0] - 1) + cdfItemList[0].Value)
        first = True
        for cdfItem in cdfItemList:
            if previousCoord[0] == cdfItem.Coord[0]:
                if first:
                    first = False
                else:
                    content += "||" + cdfItem.Value
            else:
                content += ("\t" * previousCoord[1]) + cdfItem.Value
            previousCoord = cdfItem.Coord
        content += ("\t" * previousCoord[1])
        print(content)
    else:
        print("")
    return

def print_to_debug_excel_hor(pdfRootContainer):
    print_pdf_containers(pdfRootContainer.CDFContainerList)
    combineList = []
    for pdfContainer in pdfRootContainer.Kids:
        combineList.append(pdfContainer)
    for pdfItem in pdfRootContainer.Items:
        combineList.append(pdfItem)
    combineList.sort(key=lambda pdf:pdf.RowIndex)
    for pdf in combineList:
        if type(pdf) == PDFContainer:
            print_to_debug_excel_hor(pdf)
        else:
            print_pdf_items(pdf.CDFItemList)
    return

def print_to_debug_excel_ver(pdfVer):
    if pdfVer.PDFContainer != None:
        content = "[{}]".format(pdfVer.PDFContainer.Name)
        for x in range(GV.CDFListLen):
            if pdfVer.CDFContainerArr[x] == None:
                content += "\t" + "-"
            else:
                content += "\t" + pdfVer.CDFContainerArr[x].ShortName
        print(content)
        for pdfItem in pdfVer.PDFContainer.Items:
            content = "[{}]".format(pdfItem.Name)
            for x in range(GV.CDFListLen):
                if pdfVer.CDFContainerArr[x] == None:
                    content += "\t" + "-"
                else:
                    cdfContainer = pdfVer.CDFContainerArr[x]
                    flag = False
                    for cdfItem in cdfContainer.Items:
                        if cdfItem.Name == pdfItem.Name:
                            content += "\t" + cdfItem.Value
                            flag = True
                            break
                    if not flag: content += "\t" + "-"
            print(content)
        for pdfVerKid in pdfVer.Kids:
            print_to_debug_excel_ver(pdfVerKid)      
    return

def increase_pdf_daddy_length(pdfContainer):
    if pdfContainer == None or pdfContainer.Daddy == None \
        or pdfContainer.Length <= pdfContainer.Daddy.Length:
        return
    pdfContainer.Daddy.Length += 1
    for item in pdfContainer.Daddy.Items:
        item.Length += 1
    increase_pdf_daddy_length(pdfContainer.Daddy)
    return

def increase_cdf_daddy_coord(cdfContainer):
    if cdfContainer == None or cdfContainer.Daddy == None \
        or sum(cdfContainer.Coord) - 1 <= cdfContainer.Daddy.PDFContainer.Length:
        return
    cdfContainer.Daddy.Coord[1] += 1
    for item in cdfContainer.Daddy.Items:
        item.Coord[1] += 1
    increase_cdf_daddy_coord(cdfContainer.Daddy)
    return

def apply_new_pdf_len(maxLength, pdfContainer):
    pdfContainer.Length = maxLength
    for pdfItem in pdfContainer.Items:
        pdfItem.Length = maxLength
    for pdfKidCon in pdfContainer.Kids:
        apply_new_pdf_len(maxLength, pdfKidCon)
    return

def insert_empty_cdf_item(pdfItem, cdfDaddy, newCoord):
    cdfItem = CDFItem(pdfItem.Name, "-", "-", pdfItem, daddy=cdfDaddy)
    cdfItem.Coord = newCoord
    cdfDaddy.Items.append(cdfItem)
    pdfItem.CDFItemList.append(cdfItem)
    return

def insert_empty_cdf_container(pdfContainer, cdfDaddy, newCoord):
    cdfContainer = CDFContainer("-", "-", pdfContainer, daddy=cdfDaddy)
    cdfContainer.Coord = newCoord
    for pdfItem in pdfContainer.Items:
        insert_empty_cdf_item(pdfItem, cdfContainer, newCoord)
    cdfDaddy.Kids.append(cdfContainer)
    pdfContainer.CDFContainerList.append(cdfContainer)
    for pdfKidCon in pdfContainer.Kids:
        insert_empty_cdf_container(pdfKidCon, cdfContainer, newCoord)
    return

def apply_new_cdf_coord(newCoord, cdfContainer):
    # Apply new coord for CDF container and its items
    for cdfItem in cdfContainer.Items:
        cdfItem.Coord = newCoord
    cdfContainer.Coord = newCoord

    # Classify kid CDF containers based on PDF container
    cdfKidConClass = {}
    for cdfKidCon in cdfContainer.Kids:
        pdfConName = cdfKidCon.PDFContainer.Name
        if pdfConName in cdfKidConClass:
            cdfKidConClass[pdfConName].append(cdfKidCon)
        else:
            cdfKidConClass[pdfConName] = [cdfKidCon]

    # Classify CDF items based on PDF item
    cdfItemClass = {}
    for cdfItem in cdfContainer.Items:
        pdfItemName = cdfItem.PDFItem.Name
        if pdfItemName in cdfItemClass:
            cdfItemClass[pdfItemName].append(cdfItem)
        else:
            cdfItemClass[pdfItemName] = [cdfItem]
        
    # Apply new coord for the empty kid PDF containers and their items (no CDF container config) with "-" value
    for pdfKidCon in cdfContainer.PDFContainer.Kids:
        if pdfKidCon.Name not in cdfKidConClass:
            insert_empty_cdf_container(pdfKidCon, cdfContainer, newCoord)
    for pdfItem in cdfContainer.PDFContainer.Items:
        if pdfItem.Name not in cdfItemClass:
            insert_empty_cdf_item(pdfItem, cdfContainer, newCoord)

    # Distribute coord for all kid CDF containers
    for pdfConName in cdfKidConClass:
        cdfKidConList = cdfKidConClass[pdfConName]
        cdfKidConList.sort(key=lambda con: con.Coord[0])
        coord1Total = sum([con.Coord[1] for con in cdfKidConList])
        coord1Remain = newCoord[1] - coord1Total
        coord1AddedAverage = int(math.floor(coord1Remain/len(cdfKidConList)))
        coord1AddedRemain = coord1Remain - coord1AddedAverage*len(cdfKidConList)
        coord0Index = newCoord[0]
        for cdfKidCon in cdfKidConList:
            newCoordKid = [coord0Index, cdfKidCon.Coord[1] + coord1AddedAverage]
            if coord1AddedRemain > 0:
                coord1AddedRemain -= 1
                newCoordKid[1] += 1
            coord0Index += newCoordKid[1]
            # Apply new coord for kid CDF container
            apply_new_cdf_coord(newCoordKid, cdfKidCon)
    
    return

def process_hor_container(cdfContainerList):
    for cdfContainer in cdfContainerList:
        pdfContainer = cdfContainer.PDFContainer
        # Increase PDF Container length for itself
        pdfContainer.Length += 1
        # Increase length for PDF Container's Items
        for pdfItem in pdfContainer.Items:
            pdfItem.Length += 1
        # Update CDF Items coord
        for cdfItem in cdfContainer.Items:
            cdfItem.Coord = [cdfItem.PDFItem.Length, 1]
        # Update CDF Container coord
        cdfContainer.Coord = [pdfContainer.Length, 1]

        # Increase PDF length, then CDF coord of all upper containers and items if their
        # PDF container length less than current PDF container length
        increase_pdf_daddy_length(pdfContainer)
        increase_cdf_daddy_coord(cdfContainer)
        
        # Proceed kid CDF containers
        process_hor_container(cdfContainer.Kids)
        
        # The current PDF container length has been changed due to the processing of kids
        # Get the new max length
        maxLength = pdfContainer.Length
        # Apply new length to all current PDF Items and kid PDF containers
        apply_new_pdf_len(maxLength, pdfContainer)
        # Distribute new coord based on max length to all current CDF Items and kid CDF containers
        newCoord1 = maxLength - cdfContainer.Coord[0] + 1
        apply_new_cdf_coord([cdfContainer.Coord[0], newCoord1], cdfContainer)
    return

def process_ver_pdf(pdfVerDaddy):
    global GV
    # Classify all kid containers based on PDF and put them in to array
    classKidConDicArr = [{} for x in range(GV.CDFListLen)]
    for pdfKidCon in pdfVerDaddy.PDFContainer.Kids:
        for x in range(GV.CDFListLen):
            classKidConDicArr[x][pdfKidCon.Name] = []
    for x in range(GV.CDFListLen):
        classKidConDic = classKidConDicArr[x]
        cdfContainer = pdfVerDaddy.CDFContainerArr[x]
        if cdfContainer == None:
            continue
        for cdfKidContainer in cdfContainer.Kids:
            if cdfKidContainer.PDFContainer.Name in classKidConDic:
                classKidConDic[cdfKidContainer.PDFContainer.Name].append(cdfKidContainer)
    
    # Process kid containers
    for pdfKidCon in pdfVerDaddy.PDFContainer.Kids:
        # Number of kid PDFVer is the max number of kid containers based on previous dictionary
        pdfVerLen = max([len(classKidConDic[pdfKidCon.Name]) for classKidConDic in classKidConDicArr])
        for y in range(pdfVerLen):
            pdfVer = PDFVer(pdfVerDaddy)
            for x in range(GV.CDFListLen):
                cdfConList = classKidConDicArr[x][pdfKidCon.Name]
                if y < len(cdfConList):
                    pdfVer.CDFContainerArr[x] = cdfConList[y]
                    if pdfVer.PDFContainer == None:
                        pdfVer.PDFContainer = cdfConList[y].PDFContainer
            process_ver_pdf(pdfVer)
            pdfVerDaddy.Kids.append(pdfVer)
    return

def proceed_excel_file():
    global GV
    if GV.VERTICAL_FORMAT:
        print_inf("Start processing CDF data in vertical format")
        # Use PDFVer class to express one row of container in vertical format ouput
        GV.CDFListLen = len(GV.CDFList)
        pdfRootContainerArray = []
        for x in range(GV.CDFListLen):
            pdfRootContainerArray.append(GV.PDFRootContainerDict[GV.CDFList[x]])
        pdfVerLen = max([len(pdfRootContainer.CDFContainerList) for pdfRootContainer in pdfRootContainerArray])
        for y in range(pdfVerLen):
            pdfVer = PDFVer()
            for x in range(GV.CDFListLen):
                cdfConList = pdfRootContainerArray[x].CDFContainerList
                if y < len(cdfConList):
                    pdfVer.CDFContainerArr[x] = cdfConList[y]
                    if pdfVer.PDFContainer == None:
                        pdfVer.PDFContainer = cdfConList[y].PDFContainer
            process_ver_pdf(pdfVer)
            GV.PDFRootVerList.append(pdfVer)
        print_inf("Complete processing CDF data in vertical format")
        for pdfRootVer in GV.PDFRootVerList:
            print_deb( \
                "THIS IS RAW DATA FROM CDF FILES\nYOU CAN COPY BELOW DATA AND PASTE INTO THE LAST COLUMN OF PDF XLSX FILE TO SEE THE MAGIC", \
                method=print_to_debug_excel_ver, \
                args=[pdfRootVer])
    else:
        print_inf("Start processing CDF data in horizontal format")
        # Process coordinate for CDF and Length for PDF
        for cdfFileName in GV.PDFRootContainerDict:
            print_inf("Start processing CDF data for " + cdfFileName, level=2)
            GV.PDFRootContainer = GV.PDFRootContainerDict[cdfFileName]
            process_hor_container(GV.PDFRootContainer.CDFContainerList)
            print_deb( \
                "THIS IS RAW DATA FROM {}\nYOU CAN COPY BELOW DATA AND PASTE INTO THE LAST COLUMN OF PDF XLSX FILE TO SEE THE MAGIC".format(cdfFileName), \
                method=print_to_debug_excel_hor, \
                args=[GV.PDFRootContainerDict[cdfFileName]])
            print_inf("Process CDF data complete for " + cdfFileName, level=2)
        print_inf("Complete processing CDF data in horizontal format")
    return

def writing_ver_excel(preRow, pdfVer):
    if pdfVer.PDFContainer != None:
        # Writing for containers
        preRow += 1
        sourceRowId = pdfVer.PDFContainer.RowIndex
        for colIndex in range(GV.CONST_ROOT_COLUMN_INDEX, GV.MaxColumn+GV.CDFListLen+1):
            GV.WorkSheet.cell(preRow, colIndex).value = GV.WorkSheet.cell(sourceRowId, colIndex).value
            GV.WorkSheet.cell(preRow, colIndex).font = copy(GV.WorkSheet.cell(sourceRowId, colIndex).font)
            GV.WorkSheet.cell(preRow, colIndex).border = copy(GV.WorkSheet.cell(sourceRowId, colIndex).border)
        content = "Writing to excel for container: " + pdfVer.PDFContainer.Name
        for x in range(GV.CDFListLen):
            if pdfVer.CDFContainerArr[x] == None:
                GV.WorkSheet.cell(preRow, GV.MaxColumn+1+x).value = "-"
                content += "[-]"
            else:
                GV.WorkSheet.cell(preRow, GV.MaxColumn+1+x).value = pdfVer.CDFContainerArr[x].ShortName
                content += "[{}]".format(pdfVer.CDFContainerArr[x].ShortName)
        print_inf(content, level=2)
        # Writing for items
        for pdfItem in pdfVer.PDFContainer.Items:
            sourceRowId = pdfItem.RowIndex
            # If this item is not valid for the device inputted, ignore it
            if "NA" == pdfItem.Range:
                continue
            preRow += 1
            for colIndex in range(GV.CONST_ROOT_COLUMN_INDEX, GV.MaxColumn+GV.CDFListLen+1):
                GV.WorkSheet.cell(preRow, colIndex).value = GV.WorkSheet.cell(sourceRowId, colIndex).value
                GV.WorkSheet.cell(preRow, colIndex).font = copy(GV.WorkSheet.cell(sourceRowId, colIndex).font)
                GV.WorkSheet.cell(preRow, colIndex).border = copy(GV.WorkSheet.cell(sourceRowId, colIndex).border)
            for x in range(GV.CDFListLen):
                # If container is None or Item is not configured, set "-" value
                if pdfVer.CDFContainerArr[x] == None:
                    GV.WorkSheet.cell(preRow, GV.MaxColumn+1+x).value = "-"
                else:
                    cdfContainer = pdfVer.CDFContainerArr[x]
                    flag = False
                    for cdfItem in cdfContainer.Items:
                        if cdfItem.Name == pdfItem.Name:
                            GV.WorkSheet.cell(preRow, GV.MaxColumn+1+x).value = cdfItem.Value
                            flag = True
                            break
                    if not flag: GV.WorkSheet.cell(preRow, GV.MaxColumn+1+x).value = "-"
        for pdfVerKid in pdfVer.Kids:
            preRow = writing_ver_excel(preRow, pdfVerKid)
    return preRow

def writing_hor_excel(prevColumn, pdfContainer):
    global GV
    borderStyleThin = Side(border_style="thin", color="000000")
    borderStyleThick = Side(border_style="thick", color="000000")
    if GV.MERGE_CELL:
        borderObject = Border(left=borderStyleThin, right=borderStyleThin, top=borderStyleThin, bottom=borderStyleThin)
        borderObjectLeftThick = Border(left=borderStyleThick, right=borderStyleThin, top=borderStyleThin, bottom=borderStyleThin)
        borderObjectRightThick = Border(left=borderStyleThin, right=borderStyleThick, top=borderStyleThin, bottom=borderStyleThin)
    else:
        borderObject = Border(top=borderStyleThin, bottom=borderStyleThin)
        borderObjectLeft = Border(left=borderStyleThin, top=borderStyleThin, bottom=borderStyleThin)
        borderObjectLeftThick = Border(left=borderStyleThick, top=borderStyleThin, bottom=borderStyleThin)
        borderObjectRightThick = Border(left=borderStyleThin, right=borderStyleThick, top=borderStyleThin, bottom=borderStyleThin)
    borderObjectLeftRight = Border(left=borderStyleThin, right=borderStyleThin, top=borderStyleThin, bottom=borderStyleThin)
    # Writing for containers
    print_inf("Writing to excel for container: " + pdfContainer.Name, level=2)
    for cdfContainer in pdfContainer.CDFContainerList:
        for colId in range(prevColumn+cdfContainer.Coord[0], prevColumn+sum(cdfContainer.Coord)):
            GV.WorkSheet.cell(pdfContainer.RowIndex, colId).border = borderObject
        if GV.MERGE_CELL:
            cdfContainerCell = GV.WorkSheet.cell(row=pdfContainer.RowIndex, column=(prevColumn + cdfContainer.Coord[0]))
            GV.WorkSheet.merge_cells(start_row=pdfContainer.RowIndex, start_column=prevColumn+cdfContainer.Coord[0], \
                end_row=pdfContainer.RowIndex, end_column=prevColumn+sum(cdfContainer.Coord)-1)
        else:
            cdfContainerCell = GV.WorkSheet.cell(\
                row=pdfContainer.RowIndex, column=(prevColumn+cdfContainer.Coord[0]+math.floor((cdfContainer.Coord[1]-1)/2)))
            GV.WorkSheet.cell(pdfContainer.RowIndex, prevColumn + cdfContainer.Coord[0]).border = borderObjectLeft
        cdfContainerCell.value = cdfContainer.ShortName
        cdfContainerCell.alignment = Alignment(horizontal="center", vertical="center")
    GV.WorkSheet.cell(row=pdfContainer.RowIndex, column=prevColumn+1).border = borderObjectLeftThick
    GV.WorkSheet.cell(row=pdfContainer.RowIndex, column=prevColumn+pdfContainer.Length+1).border = borderObjectLeftRight
    GV.WorkSheet.cell(row=pdfContainer.RowIndex, column=prevColumn+pdfContainer.Length+2).border = borderObjectRightThick
    # Writing for items
    for pdfItem in pdfContainer.Items:
        print_inf("Writing to excel for item: " + pdfItem.Name, level=3)
        existedCdfItemCoord0List = []
        for cdfItem in pdfItem.CDFItemList:
            if GV.MERGE_CELL:
                cdfItemCell = GV.WorkSheet.cell(row=pdfItem.RowIndex, column=(prevColumn + cdfItem.Coord[0]))
            else:
                cdfItemCell = GV.WorkSheet.cell(\
                    row=pdfItem.RowIndex, column=(prevColumn+cdfItem.Coord[0]+math.floor((cdfItem.Coord[1]-1)/2)))
            if cdfItem.Coord[0] not in existedCdfItemCoord0List:
                cdfItemCell.value = cdfItem.Value
                existedCdfItemCoord0List.append(cdfItem.Coord[0])
                cdfItemCell.alignment = Alignment(horizontal="center", vertical="center")
                for colId in range(prevColumn+cdfItem.Coord[0], prevColumn+sum(cdfItem.Coord)):
                    GV.WorkSheet.cell(pdfItem.RowIndex, colId).border = borderObject
            else:
                cdfItemCell.value += " | " + cdfItem.Value
            if GV.MERGE_CELL:
                GV.WorkSheet.merge_cells(start_row=pdfItem.RowIndex, start_column=prevColumn+cdfItem.Coord[0], \
                    end_row=pdfItem.RowIndex, end_column=prevColumn+sum(cdfItem.Coord)-1)
            else:
                GV.WorkSheet.cell(pdfItem.RowIndex, prevColumn + cdfItem.Coord[0]).border = borderObjectLeft
        GV.WorkSheet.cell(row=pdfItem.RowIndex, column=prevColumn+1).border = borderObjectLeftThick
        GV.WorkSheet.cell(row=pdfItem.RowIndex, column=prevColumn+pdfItem.Length+1).border = borderObjectLeftRight
        GV.WorkSheet.cell(row=pdfItem.RowIndex, column=prevColumn+pdfItem.Length+2).border = borderObjectRightThick

    # Writing for kid containers
    for pdfKidCon in pdfContainer.Kids:
        writing_hor_excel(prevColumn, pdfKidCon)
    return

def writing_excel_file():
    global GV
    if GV.VERTICAL_FORMAT:
        print_inf("Start writing CDF data to excel file in vertical format")
        prevColumn = GV.MaxColumn
        # Write CDF file name and format setting
        for cdfFileName in GV.CDFList:
            GV.WorkSheet.merge_cells(start_row=GV.CONST_TITLE_ROW_INDEX, start_column=prevColumn+1, \
                end_row=GV.CONST_RANGE_ROW_INDEX, end_column=prevColumn+1)
            cdfFileNameCell = GV.WorkSheet.cell(row=GV.CONST_TITLE_ROW_INDEX, column=prevColumn+1)
            cdfFileNameCell.value = cdfFileName
            cdfFileNameCell.alignment = Alignment(horizontal="center", vertical="center")
            cdfFileNameCell.fill = PatternFill("solid", fgColor="c5d9f1")
            cdfFileNameCell.font = Font(b=True, size=16)
            borderStyle = Side(border_style="thick", color="000000")
            borderObject = Border(left=borderStyle, right=borderStyle, top=borderStyle, bottom=borderStyle)
            for rowId in range(GV.CONST_TITLE_ROW_INDEX, GV.CONST_RANGE_ROW_INDEX+1):
                GV.WorkSheet.cell(rowId, prevColumn+1).border = borderObject
            borderStyle = Side(border_style="thin", color="000000")
            borderObject = Border(left=borderStyle, right=borderStyle, top=borderStyle, bottom=borderStyle)
            for rowId in range(GV.CONST_ROOT_ROW_INDEX, GV.MaxRow+1):
                GV.WorkSheet.cell(rowId, prevColumn+1).border = borderObject
            GV.WorkSheet.column_dimensions[get_column_letter(prevColumn+1)].width = 45
            prevColumn += 1
        preRow = GV.MaxRow
        for pdfRootVer in GV.PDFRootVerList:
            # Write data to excel
            preRow = writing_ver_excel(preRow, pdfRootVer)
        # Delete orginal pdf rows
        GV.WorkSheet.delete_rows(GV.CONST_ROOT_ROW_INDEX, GV.MaxRow-GV.CONST_ROOT_ROW_INDEX+1)
        print_inf("Complete writing CDF data to excel file in vertical format")
    else:
        print_inf("Start writing CDF data to excel file in horizontal format")
        prevColumn = GV.MaxColumn
        for cdfFileName in GV.PDFRootContainerDict:
            GV.PDFRootContainer = GV.PDFRootContainerDict[cdfFileName]
            # Fill the CDF file name to upper row
            GV.WorkSheet.merge_cells(start_row=GV.CONST_TITLE_ROW_INDEX, start_column=prevColumn+1, \
                 end_row=GV.CONST_RANGE_ROW_INDEX, end_column=prevColumn+GV.PDFRootContainer.Length)
            GV.WorkSheet.merge_cells(start_row=GV.CONST_TITLE_ROW_INDEX, start_column=prevColumn+GV.PDFRootContainer.Length+1, \
                 end_row=GV.CONST_RANGE_ROW_INDEX, end_column=prevColumn+GV.PDFRootContainer.Length+1)
            GV.WorkSheet.merge_cells(start_row=GV.CONST_TITLE_ROW_INDEX, start_column=prevColumn+GV.PDFRootContainer.Length+2, \
                 end_row=GV.CONST_RANGE_ROW_INDEX, end_column=prevColumn+GV.PDFRootContainer.Length+2)
            GV.WorkSheet.column_dimensions[get_column_letter(prevColumn+GV.PDFRootContainer.Length+1)].width = 43
            GV.WorkSheet.column_dimensions[get_column_letter(prevColumn+GV.PDFRootContainer.Length+2)].width = 43
            borderStyle = Side(border_style="thick", color="000000")
            borderObject = Border(left=borderStyle, right=borderStyle, top=borderStyle, bottom=borderStyle)
            cdfFileNameCell = GV.WorkSheet.cell(row=GV.CONST_TITLE_ROW_INDEX, column=prevColumn+1)
            cdfFileNameCell.value = cdfFileName
            cdfFileNameCell.alignment = Alignment(horizontal="center", vertical="center")
            cdfFileNameCell.fill = PatternFill("solid", fgColor="c5d9f1")
            cdfFileNameCell.font = Font(b=True, size=16)
            expectGentoolCell = GV.WorkSheet.cell(row=GV.CONST_TITLE_ROW_INDEX, column=prevColumn+GV.PDFRootContainer.Length+1)
            expectGentoolCell.value = "Expected gentool output"
            expectGentoolCell.alignment = Alignment(horizontal="center", vertical="center")
            expectGentoolCell.fill = PatternFill("solid", fgColor="ffff00")
            expectGentoolCell.font = Font(b=True, size=16)
            expectGentoolCell.border = borderObject
            designIdCell = GV.WorkSheet.cell(row=GV.CONST_TITLE_ROW_INDEX, column=prevColumn+GV.PDFRootContainer.Length+2)
            designIdCell.value = "Design ID"
            designIdCell.alignment = Alignment(horizontal="center", vertical="center")
            designIdCell.fill = PatternFill("solid", fgColor="ffff00")
            designIdCell.font = Font(b=True, size=16)
            designIdCell.border = borderObject
            for rowId in range(GV.CONST_TITLE_ROW_INDEX, GV.CONST_RANGE_ROW_INDEX+1):
                for colId in range(prevColumn+1, prevColumn+GV.PDFRootContainer.Length+3):
                    GV.WorkSheet.cell(rowId, colId).border = borderObject
            # Write data to excel
            writing_hor_excel(prevColumn, GV.PDFRootContainer)
            prevColumn += (GV.PDFRootContainer.Length + 2)
        # Hide NA row in output excel files
        GV.PDFNAList.sort(reverse=True)
        for rowId in GV.PDFNAList:
            GV.WorkSheet.row_dimensions.group(start=rowId, hidden=True)
        print_inf("Complete writing CDF data to excel file in horizontal format")
    try:
        GV.WorkSheet.title = "Renesas_config_" + GV.PdfColName
        GV.WorkBook.save(filename=GV.CONST_OUTPUT_FILE_NAME)
    except:
        print_err("Cannot save the output '{}' file.".format(GV.CONST_OUTPUT_FILE_NAME))
    try:
        print_inf("Openning '{}' file...".format(GV.CONST_OUTPUT_FILE_NAME))
        os.startfile(GV.CONST_OUTPUT_FILE_NAME)
    except:
        print_err("Cannot open the output '{}' file.".format(GV.CONST_OUTPUT_FILE_NAME))

    return


def main():
    """main function of the program

    Returns:
        int -- status of the main function. 1 is successful
    """
    get_input_command()

    proceed_pdf_design()
    print_deb("TREE OF PDF DATA: " + GV.PDFRootContainerOriginal.Name , \
        method=GV.PDFRootContainerOriginal.print_container_tree)
    
    proceed_cdf_files()
    for cdfFileName in GV.PDFRootContainerDict:
        print_deb("CDF DATA FROM: " + cdfFileName, \
            method=GV.PDFRootContainerDict[cdfFileName].print_container_tree, kargs={"printCDF":True})

    proceed_excel_file()

    writing_excel_file()

if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------