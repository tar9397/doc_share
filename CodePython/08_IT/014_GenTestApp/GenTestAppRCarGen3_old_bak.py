#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                           GenTestAppRCar.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
import openpyxl
import json

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------
line_ending = '\r\n'
output_loc = './Output'


with open("./config.json", "r+") as read_file:
    init_db = json.load(read_file)

with open("./template.c", "r+") as read_file:
    template_content = read_file.readlines()

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------
def write_to_file(file_name, content_array):
    if not os.path.exists(output_loc):
        os.makedirs(output_loc)

    with open(os.path.join(output_loc, file_name), "w+") as write_file:
        # write_file.writelines(content_array)
        write_file.write(line_ending.join(content_array))
        write_file.writelines(line_ending)

def update_file_content(temp_file, file_name, content_array):
    new_file = []

    data_pattern = {
        'file_name' : "<TEMPLATE_FILENAME>",
        'marco_name' : "<MARCO_FILE>",
        'func_code' : "<FUNCTION_CODE>"
    }

    for data in temp_file:
        if data_pattern['file_name'] in data:
            tmp_data = data.replace(data_pattern['file_name'], file_name.ljust(len(data_pattern['file_name'])))
            new_file.append(tmp_data.strip())
            # print(tmp_data)
        elif data_pattern['marco_name'] in data:
            msn_test = str(file_name.split('_')[0]).upper() + 'TEST'
            testcase_name =  '_'.join(str(file_name.split('.')[0]).split('_')[1:]).upper()
            tmp_data = data.replace(data_pattern['marco_name'], '_'.join([msn_test, testcase_name, 'C']))
            new_file.append(tmp_data.strip())
            # print(tmp_data)
        elif data_pattern['func_code'] in data:
            new_file.extend(content_array)
        else:
            new_file.append(data.strip())

    return new_file

# ------------------------------------------------------------------------------
# Handle parser create function
# ------------------------------------------------------------------------------
def create_function_str(param, config_name, port_id = None):
    tmp_header = [
        "void Port_Init(void)",
        "{{",
        "  /* Port config: {0} */",
        "{1}",
        "}}"
    ]

    tmp_cfg = []

    for data in param:
        port_num = data['PortName'].strip().split('_')[1]
        # print('process port: ',port_num)
        # print(port_id)
        if not port_id or len(port_id) == 0:
            tmp_cfg.append(create_cfg_for_single_port(port_num, data['Config']))
        elif int(port_num) in port_id:
            tmp_cfg.append(create_cfg_for_single_port(port_num, data['Config']))

        # return template_header.format(line_ending, (line_ending*2).join(tmp_cfg))
    return line_ending.join(tmp_header).format(config_name, (line_ending*2).join(tmp_cfg))

def create_cfg_for_single_port(port_num, port_config):
    template_common = [
        "  /* Port {0} */",
        "  /* Set the polarity positive of port {0} pin*/",
        "  POSNEG{0} &= (uint32) ~{1};",
        "  /* Configuration port {0} as IO mode */",
        "  IOINTSEL{0} &= (uint32) ~{1};",
        "  /* Configuration direction for port {0} pin are output */",
        "  INOUTSEL{0} |= (uint32) {2};"
    ]

    tmp_output_general = [
        "  /* Select output data is output by OUTDT register */",
        "  OUTDTSEL{0} &= (uint32) ~{1};",
        "  /* Init level for pin */",
        "  OUTDT{0} = (uint32) {3};"
    ]
    
    tmp_output_high_low = [
        "  /* Select output data is output by OUTDTH/OUTDTL register */",
        "  OUTDTSEL{0} |= (uint32) {1};",
        "  /* Init level for pin */",
        "  OUTDTH{0} = (uint32) {3};"
    ]

    port_mask = port_config[1]
    output_mask = port_config[2]
    port_init_val = port_config[3]
    output_str = None
    if 'General' in port_config[0]:
        output_str = template_common + tmp_output_general
    else:
        output_str = template_common + tmp_output_high_low
        
    return line_ending.join(output_str).format(port_num, port_mask, output_mask, port_init_val)

# def update_arr_content(arr):
#     pass

def padded_hex(dev_value):
    return "{0:#0{1}X}".format(dev_value,10).replace('0X','0x')

# ------------------------------------------------------------------------------
# Read dat form excel
# ------------------------------------------------------------------------------
'''data struct content: [{"PortName": 'PORTGROUP_0_BITS_0_TO_27', "Config": [data]}, {}, ...]'''
def get_port_name(file_name, sheet_name):
    ret = []
    cur_portname = None
    out_data = None
    try:
        wbook = openpyxl.load_workbook(file_name, data_only=True)
        ws = wbook.get_sheet_by_name(sheet_name)
        for row in ws.iter_rows():
            if "PortName" == str(row[0].value):
                cur_portname = str(row[1].value)
                port_mask = 0
                output_mask = 0
                continue
            if cur_portname:
                if "OutSelect" in str(row[0].value):
                    if "1" in str(row[1].value):
                        out_data = 'HighLow' 
                    else:
                        out_data = 'General' 
                elif "Mode" in str(row[0].value):
                    bit_pos = 31
                    for cell in row[1:]:
                        cell_content = str(cell.value).upper()
                        if 'I' in cell_content or 'O' in cell_content:
                            valid_pin = 1
                            port_mask += int(pow(2, bit_pos))
                            if 'O' in cell_content:
                                output_mask += int(pow(2, bit_pos))
                        if bit_pos >= 0:
                            bit_pos -= 1
                        else:
                            break
                elif "State" in str(row[0].value):
                    port_init_val = 0
                    bit_pos = 31
                    for cell in row[1:]:
                        cell_content = str(cell.value)
                        if '1' in cell_content and (output_mask & int(pow(2, bit_pos)) != 0):
                            port_init_val += int(pow(2, bit_pos))
                        if bit_pos >= 0:
                            bit_pos -= 1
                        else:
                            break
                    
                    temp = {}
                    temp['PortName'] = cur_portname
                    temp['Config'] = [out_data, padded_hex(port_mask), padded_hex(output_mask), padded_hex(port_init_val)]
                    ret.append(temp)
                    cur_portname = None
                    out_data = None
                    
        wbook.close()
    except Exception as e:
        print("[E]Issue occurs when opening xlsx file to read")
        print(e)
        
    return ret



def main():
    """main function of the program"""

    config_name = "CFG001"
    pin_config_file = "PinConfig.xlsm"

    data = get_port_name(pin_config_file, config_name)
    print("Collect {0} in {1} ".format(config_name, pin_config_file))
    # print("result: ",data) 
    # ('result: ', [{'PortName': 'PORTGROUP_0_BITS_0_TO_27', 'Config': ['General', '0x0FFFFFFF', '0x003FFFFF', '0x0000000F']}, {'PortName': 'PORTGROUP_1_BITS_0_TO_30', 'Config': ['General', '0x7FFFFFFF', '0x0000FFFF', '0x00000000']}, {'PortName': 'PORTGROUP_2_BITS_0_TO_24', 'Config': ['General', '0x01FFFFFF', '0x0000000F', '0x00000000']}, {'PortName': 'PORTGROUP_3_BITS_0_TO_16', 'Config': ['General', '0x0001FFFF', '0x00000FFF', '0x00000000']}, {'PortName': 'PORTGROUP_4_BITS_0_TO_26', 'Config': ['General', '0x07FFFFFF', '0x003FFFFF', '0x00000000']}, {'PortName': 'PORTGROUP_5_BITS_0_TO_20', 'Config': ['General', '0x001FFFFF', '0x001FFFFF', '0x00000000']}, {'PortName': 'PORTGROUP_6_BITS_0_TO_20', 'Config': ['General', '0x001FFFFF', '0x001FE00F', '0x00000000']}, {'PortName': 'PORTGROUP_7_BITS_0_TO_20', 'Config': ['General', '0x001FFFFF', '0x0000FFFF', '0x00000000']}, {'PortName': 'PORTGROUP_8_BITS_0_TO_20', 'Config': ['HighLow', '0x001FFFFF', '0x001FFF00', '0x00000000']}, {'PortName': 'PORTGROUP_9_BITS_0_TO_20', 'Config': ['HighLow', '0x001FFFFF', '0x0010000F', '0x0000000C']}])
    
    # text = create_function_str(data, config_name)
    # print('*'*80)
    # print(text)
    # print('*'*80)
    msn = str(init_db['msn'])
    device = str(init_db['device']).upper()
    for testcase, port_num_arr in init_db[config_name].items():
        text = create_function_str(data, config_name, port_num_arr)
        # print('*'*80)
        # print(text)
        # print('*'*80)
        tmp_file_name_ele = [msn.capitalize()]
        file_name = '_'.join([msn.capitalize(), ''.join(testcase.split('_')), device]) + '.c'
        new_file = update_file_content(template_content, file_name, text.split(line_ending))
        # print('*'*80)
        # print(line_ending.join(new_file))
        # print('*'*80)
        write_to_file(file_name, new_file)
        print("Create file {} success !".format(file_name))

# def write_to_file(file_name, content_array):
#     with open(os.path.join(output_loc, file_name), "w+") as write_file:
#         # write_file.writelines(content_array)
#         write_file.write(line_ending.join(content_array))



if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################