#!/usr/bin/python
import os
import re
import sys

HELP = '''
########################### Tool check macro defined but unused in TCODE! ############################################
#                                                                                                                    #
# Command: Run_File SourcePath MapNamePath ResourcePath                                                              #
#                                                                                                                    #
# Example:                                                                                                           #
#./Find_Macro_UnUsed.py ./ "generator_cs\SpiCommon\MapName\MapName.cs" "generator_cs\SpiResources\SpiResources.resx" #
#                                                                                                                    #
######################################################################################################################
'''

def showHelp():
    print(HELP)

def main_finding(array_macro, source_path):
    couter_unused = 0
    for macro in array_macro:
        flag_existed = False
        for root, dirs, files in os.walk(source_path):
            for file in files:
                if file.endswith(".cs") and file != 'MapName.cs':
                    with open(os.path.join(root, file), 'r+') as file_cs:
                        for line in file_cs.read().splitlines():
                            if macro in line:
                                flag_existed = True
                                break
                if flag_existed:
                    break
            if flag_existed:
                break
        if flag_existed == False:
            print '[!]', macro
            couter_unused = couter_unused + 1
    print '\n', 'Number macro unused:', couter_unused
    print '===================================================================', '\n\n'

def main_handle_finding_MapName(source_path, input_MapName_path):
    print '\n', '[-] Checking macro in MapName.cs'
    # Get marco by class: Input.SpiMap.SpiSeqEndNotification
    class_key = ''
    with open(input_MapName_path) as f_MapName:
        for line in f_MapName.read().splitlines(): # Remove \r\n
            if 'public class' in line:
                line = re.sub(r'^ *', '', line)
                class_key = class_key + '.' + line.split(' ')[2]
            elif '=' in line and '/' not in line:
                line = line.split('=')[0]
                line = line.strip()
                line = line.rsplit(' ', 1)[1]
                arr_marco_map_name.append((class_key + '.' + line)[1:])
            elif '}' in line:
                class_key = class_key.rsplit('.', 1)[0]
                if class_key == '.':
                    class_key = ''
            else:
                continue
    #print arr_marco_map_name
    f_MapName.close()
    
    if len(arr_marco_map_name) == 0:
        print '[x] ERROR read MapName.cs'
    else:
        print 'Total macro:', len(arr_marco_map_name), '\n'
        main_finding(arr_marco_map_name, source_path)

def main_handle_finding_Resource(source_path, input_Resource_path):
    print '[-] Checking macro in Resources.resx'
    # Get marco by string: <data name="WRN083006"
    key_name_leading = '<data name='
    key_name_trailing = 'xml:space="preserve">'
    with open(input_Resource_path) as f_Resource:
        for line in f_Resource.read().splitlines(): # Remove \r\n
            if key_name_leading in line and key_name_trailing in line:
                line = line.split(key_name_leading)[1]
                line = line.split(key_name_trailing)[0]
                line = line.strip()
                line = line.replace('\"', '')
                arr_marco_resource.append(resource_name + '.' + line)
            else:
                continue
    #print arr_marco_resource
    f_Resource.close()
    
    if len(arr_marco_resource) == 0:
        print '[x] ERROR read' + resource_name + '.resx'
    else:
        print 'Total macro:', len(arr_marco_resource), '\n'
        main_finding(arr_marco_resource, source_path)

def get_file_name(input_Resource_path):
    file_name = ''
    if '\\' in input_Resource_path:
        file_name = input_Resource_path.rsplit('\\', 1)[1]
    elif '/' in input_Resource_path:
        file_name = input_Resource_path.rsplit('/', 1)[1]
    else:
        print '[x] Resource path incorrect.!!!'
    #print file_name.split('.resx')[0]
    return file_name.split('.resx')[0]

############################################

def main():
    global arr_marco_map_name, arr_marco_resource, resource_name
    arr_marco_map_name = []
    arr_marco_resource = []
    resource_name = ''
    
    if len(sys.argv) != 4:
        showHelp()
    else:
        main_handle_finding_MapName(sys.argv[1], sys.argv[2])
        resource_name = get_file_name(sys.argv[3])
        main_handle_finding_Resource(sys.argv[1], sys.argv[3])

if __name__ == '__main__':
    main()