@echo off
setlocal EnableDelayedExpansion
set Filename=%~n0

echo --------------------------------------------------------------------
echo Run %Filename% ...
echo --------------------------------------------------------------------
echo.

IF "%GNUMAKE%" == "" (GOTO :noGnuMake) ELSE (GOTO :GnuMake)

:noGnuMake
echo If you have make.exe located in a different directory
echo then your standard path, please add a local
echo user variable named 'GNUMAKE' to your Windows
echo enviroment variables and specified the location
echo of make.exe by using this variable.
echo.
echo Your current path variable is
echo.
echo %path%
echo.
echo --------------------------------------------------------------------
GOTO :continue

:GnuMake
set path=%GNUMAKE%
echo Found user variable GNUMAKE ...
echo --------------------------------------------------------------------
echo Temporary modify path variable to be sure to use correct make, shell...
echo path = %path%
echo --------------------------------------------------------------------
GOTO :continue

:continue

SETLOCAL

:: set root folder
SET ROOT_FOLDER=..\..\..\..

::Specify the Family and Micro Variant
SET MICRO_FAMILY=RCar

IF /i "%1"=="DisplEnv" (
GOTO :Display_Env
)

IF /i "%1"=="" (
GOTO :Help
)

SET TARGET=%1

IF %TARGET% == Global (
SET FOLDER=Global_Sample_Application
SET MAKEFILE=global.mak
) ELSE (
CALL :CONV_VAR_to_MAJ TARGET
SET FOLDER=modules\%TARGET%\sample_application
SET MAKEFILE=common.mak
)

SET AUTOSAR_VERSION=%2

SET BULID_OPTION=%5

IF "%2" NEQ "4.2.2" (
echo ERROR: AUTOSAR version '%2' is not supported.
GOTO :Help
)

:: Currently, R-Car only has H3 and V3M
IF "%3" NEQ "H3" IF "%3" NEQ "V3M" IF "%3" NEQ "V3H" IF "%3" NEQ "M3" IF "%3" NEQ "M3N" IF "%3" NEQ "V3U" (
echo ERROR: Device variant '%3' is not supported.
GOTO :Help
)

:: Currently, V3M only supports ARM compiler
IF "%3"=="V3M" IF "%4" NEQ "arm6.6.1" (
  IF "%4" NEQ "arm6.6.3" (
  echo ERROR: Compiler '%4' is not supported for 'V3M'.
  GOTO :Help)
)
:: Currently, V3H only supports ARM compiler
IF "%3"=="V3H" IF "%4" NEQ "arm6.6.1" (
  IF "%4" NEQ "arm6.6.3" (
  echo ERROR: Compiler '%4' is not supported for 'V3H'.
  GOTO :Help)
)
:: Currently, H3 only supports GHS compiler
IF "%3"=="H3" IF "%4" NEQ "ghs" (
echo ERROR: Compiler '%4' is not supported for 'H3'.
GOTO :Help
)
:: Currently, M3 only supports ARM compiler
IF "%3"=="M3" IF "%4" NEQ "arm6.6.1" (
echo ERROR: Compiler '%4' is not supported for 'M3'.
GOTO :Help
)
:: Currently, M3N only supports ARM compiler
IF "%3"=="M3N" IF "%4" NEQ "arm6.6.1" IF "%4" NEQ "ghs" (
  IF "%4" NEQ "arm6.6.3" (
  echo ERROR: Compiler '%4' is not supported for 'M3N'.
  GOTO :Help)
)

rem :: Set compiler directory
rem IF "%4"=="arm6.6.1" (
rem SET COMPILER_INSTALL_DIR=C:\ARMCompiler6.6.1\bin
rem )

rem IF "%4"=="arm6.6.3" (
rem SET COMPILER_INSTALL_DIR=C:\ARMCompiler6.6.3\bin
rem )

rem IF "%4"=="ghs" (
rem   IF "%3"=="M3N" (
rem      SET COMPILER_INSTALL_DIR=C:\ghs\comp_201714
rem   ) ELSE (
rem      SET COMPILER_INSTALL_DIR=C:\ghs\comp_201516
rem   )
rem )

SET MICRO_VARIANT=%3

SET COMPILER_NAME=%4

SET CONFIG_FOLDER=%ROOT_FOLDER%\%MICRO_FAMILY%\%MICRO_VARIANT%\%FOLDER%\%AUTOSAR_VERSION%

IF "%4" == "arm6.6.1" (
SET OBJ_FOLDER=%ROOT_FOLDER%\%MICRO_FAMILY%\%MICRO_VARIANT%\%FOLDER%\obj\arm
) ELSE IF "%4"=="arm6.6.3" (
SET OBJ_FOLDER=%ROOT_FOLDER%\%MICRO_FAMILY%\%MICRO_VARIANT%\%FOLDER%\obj\arm
) ELSE (
SET OBJ_FOLDER=%ROOT_FOLDER%\%MICRO_FAMILY%\%MICRO_VARIANT%\%FOLDER%\obj\%COMPILER_NAME%
)

if not exist "%ROOT_FOLDER%" (
echo ERROR: Root Folder %ROOT_FOLDER% doesn't exist.
GOTO :End
)

rem if not exist "%COMPILER_INSTALL_DIR%" (
rem echo ERROR: Compiler installed directory %COMPILER_INSTALL_DIR% doesn't exist.
rem GOTO :End
rem )

if not exist "%CONFIG_FOLDER%" (
echo ERROR: Folder %CONFIG_FOLDER% doesn't exist.
GOTO :End
)

:: Check and create required folder
if not exist "%OBJ_FOLDER%" (
mkdir "%OBJ_FOLDER%"
)

SET DEBUG=""
IF /i "%5"=="debug" IF "%TARGET%" NEQ "Global" (
echo ERROR: Debug option is only supported for 'H3' Global Sample app.
GOTO :Help
)

IF /i "%5"=="debug" IF "%TARGET%"=="Global" IF "%MICRO_VARIANT%"=="H3" (
SET DEBUG=OK
SET BULID_OPTION=%6
)

IF /i "%5"=="debug" IF "%TARGET%"=="Global" IF "%MICRO_VARIANT%"=="V3M" (
SET DEBUG=OK
SET BULID_OPTION=%6
)

IF /i "%5"=="debug" IF "%TARGET%"=="Global" IF "%MICRO_VARIANT%"=="V3H" (
SET DEBUG=OK
SET BULID_OPTION=%6
)

IF /i "%5"=="debug" IF "%TARGET%"=="Global" IF "%MICRO_VARIANT%"=="M3" (
SET DEBUG=OK
SET BULID_OPTION=%6
)

IF /i "%5"=="debug" IF "%TARGET%"=="Global" IF "%MICRO_VARIANT%"=="M3N" (
SET DEBUG=OK
SET BULID_OPTION=%6
)

IF /i "%5" NEQ "debug" (
SET BULID_OPTION=%5
)

SET OPTION=no

echo.
echo --------------------------------------------------------------------
echo                      BUILDING SAMPLE APPLICATION: %TARGET%
echo --------------------------------------------------------------------

IF "%BULID_OPTION%"=="clean" (
SET OPTION=yes
echo --------------------------------------------------------------------
echo                               CLEAN ONLY
echo --------------------------------------------------------------------
make -f %MAKEFILE% clean
make -f %MAKEFILE% clean_all
rmdir "%OBJ_FOLDER%"
rmdir "%OBJ_FOLDER%\.."
)

IF "%BULID_OPTION%"=="generate" (
SET OPTION=yes
echo --------------------------------------------------------------------
echo                           CLEAN AND RE-GENERATE
echo --------------------------------------------------------------------
make -f %MAKEFILE% clean
make -f %MAKEFILE% clean_all
make -f %MAKEFILE% generate_%TARGET%_config
)

IF "%BULID_OPTION%"=="make" (
SET OPTION=yes
echo --------------------------------------------------------------------
echo                           CLEAN AND RE-MAKE
echo --------------------------------------------------------------------
make -f %MAKEFILE% clean_all
IF "%4"=="arm" (
IF NOT x%TARGET:CDD=%==x%TARGET% (
make -f %MAKEFILE% App_CDD_%TARGET:~3%_Sample.axf
) ELSE (
make -f %MAKEFILE% App_%TARGET%_Sample.axf
)
)
IF "%4"=="ghs" (
IF NOT x%TARGET:CDD=%==x%TARGET% (
make -f %MAKEFILE% App_CDD_%TARGET:~3%_Sample.out
) ELSE (
make -f %MAKEFILE% App_%TARGET%_Sample.out
)
)
)

IF "%BULID_OPTION%"=="list" (
SET OPTION=yes
echo --------------------------------------------------------------------
echo                           LIST BUILD VARIABLES
echo --------------------------------------------------------------------
make -f %MAKEFILE% debug_base_make
)

IF "%OPTION%"=="no" (
echo --------------------------------------------------------------------
echo                           CLEAN AND RE-BUILD
echo --------------------------------------------------------------------
make -f %MAKEFILE% clean
make -f %MAKEFILE% clean_all
make -f %MAKEFILE% generate_%TARGET%_config
IF "%4"=="arm6.6.1" (
IF NOT x%TARGET:CDD=%==x%TARGET% (
make -f %MAKEFILE% App_CDD_%TARGET:~3%_Sample.axf
) ELSE (
make -f %MAKEFILE% App_%TARGET%_Sample.axf
)
)

IF "%4"=="arm6.6.3" (
IF NOT x%TARGET:CDD=%==x%TARGET% (
make -f %MAKEFILE% App_CDD_%TARGET:~3%_Sample.axf
) ELSE (
make -f %MAKEFILE% App_%TARGET%_Sample.axf
)
)

IF "%4"=="ghs" (
IF NOT x%TARGET:CDD=%==x%TARGET% (
make -f %MAKEFILE% App_CDD_%TARGET:~3%_Sample.out
) ELSE (
make -f %MAKEFILE% App_%TARGET%_Sample.out
)
)
)

echo.
echo --------------------------------------------------------------------
echo                           BUILDING COMPLETED
echo --------------------------------------------------------------------
GOTO :eof


:CONV_VAR_to_MAJ
  FOR %%z IN (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO CALL set %~1=%%%~1:%%z=%%z%%
GOTO :eof


:Help
echo.
echo --------------------------------------------------------------------
echo                        HELP TO BUILD SAMPLE APPLICATION
echo --------------------------------------------------------------------
echo Usage:
echo.
echo SampleApp.bat TARGET AUTOSAR_VERSION VARIANT COMPILER (debug) (clean/generate/make)
echo.
echo TARGET           -   Module Short Name to be generated e.g. 'Port', 'Can', 'Adc' .. etc or 'Global' to run Global Sample app
echo.
echo.AUTOSAR_VERSION  -   AutoSAR version to be compiled which is available 4.2.2
echo.
echo.VARIANT          -   Variant of R-Car Gen 3 Soc - currently only H3, V3M, V3H and M3 and M3N is available
echo.
echo.COMPILER_NAME    -   Compiler name - currently only ghs(H3) and arm6.6.1(V3M, V3H, M3, M3N) and arm6.6.3(V3M, V3H, M3N) is available
echo.
echo.debug            -   Enable debug mode - currently only H3 Global Sample app is supported
echo.
echo DisplEnv         -   Display Useful Variable for debug
echo --------------------------------------------------------------------
goto :END

:Display_Env
echo Display Env Variables
echo --------------------------------------------------------------------
echo AUTOSAR_VERSION=%AUTOSAR_VERSION%
echo TARGET=%TARGET%

echo --------------------------------------------------------------------


:End

ENDLOCAL

@echo on
