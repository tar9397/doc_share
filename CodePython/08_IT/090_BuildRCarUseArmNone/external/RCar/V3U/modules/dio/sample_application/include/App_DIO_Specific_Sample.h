/*============================================================================*/
/* Project      = R-CarGen3 AUTOSAR MCAL Development Project                  */
/* Module       = App_DIO_Specific_Sample.h                                   */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* Copyright(c) 2020 Renesas Electronics Corporation                          */
/*============================================================================*/
/* Purpose:                                                                   */
/* This file contains macros/function prototypes/variables required for       */
/* source application file.                                                   */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Renesas Electronics Corporation the following shall apply!                 */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Renesas. Any        */
/* warranty is expressly disclaimed and excluded by Renesas, either expressed */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Renesas shall not have any obligation to maintain, service or provide bug  */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/* Each User is solely responsible for determining the appropriateness of     */
/* using the Product(s) and assumes all risks associated with its exercise    */
/* of rights under this Agreement, including, but not limited to the risks    */
/* and costs of program errors, compliance with applicable laws, damage to    */
/* or loss of data, programs or equipment, and unavailability or              */
/* interruption of operations.                                                */
/*                                                                            */
/* Limitation of Liability                                                    */
/*                                                                            */
/* In no event shall Renesas be liable to the User for any incidental,        */
/* consequential, indirect, or punitive damage (including but not limited     */
/* to lost profits) regardless of whether such liability is based on breach   */
/* of contract, tort, strict liability, breach of warranties, failure of      */
/* essential purpose or otherwise and even if advised of the possibility of   */
/* such damages. Renesas shall not be liable for any services or products     */
/* provided by third party vendors, developers or consultants identified or   */
/* referred to the User by Renesas in connection with the Product(s) and/or   */
/* the Application.                                                           */
/*                                                                            */
/*============================================================================*/
/* Environment:                                                               */
/*              Devices:        R-Car V3U                                     */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
* V1.0.0:   04-Nov-2020  : Initial Version
*/
/******************************************************************************/
#ifndef APP_DIO_SPECIFIC_SAMPLE_H
#define APP_DIO_SPECIFIC_SAMPLE_H

/*******************************************************************************
**                     Include Section                                        **
*******************************************************************************/
#include "Dio_Cfg.h"
#include "Det.h"
#include "Dio.h"

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
#define DIO_FAIL                    0
#define DIO_PASS                    1
/* Define address of registers by macros*/
#define GPIO0_BASE_ADDR      (uint32)0xE6058180UL
#define GPIO1_BASE_ADDR      (uint32)0xE6050180UL
#define GPIO2_BASE_ADDR      (uint32)0xE6050980UL
#define GPIO3_BASE_ADDR      (uint32)0xE6058980UL
#define GPIO4_BASE_ADDR      (uint32)0xE6060180UL
#define GPIO5_BASE_ADDR      (uint32)0xE6060980UL
#define GPIO6_BASE_ADDR      (uint32)0xE6068180UL
#define GPIO7_BASE_ADDR      (uint32)0xE6068980UL
#define GPIO8_BASE_ADDR      (uint32)0xE6069180UL
#define GPIO9_BASE_ADDR      (uint32)0xE6069980UL

#define IOINTSEL0            *((volatile uint32 *)(GPIO0_BASE_ADDR + 0x00))
#define IOINTSEL1            *((volatile uint32 *)(GPIO1_BASE_ADDR + 0x00))
#define IOINTSEL2            *((volatile uint32 *)(GPIO2_BASE_ADDR + 0x00))
#define IOINTSEL3            *((volatile uint32 *)(GPIO3_BASE_ADDR + 0x00))
#define IOINTSEL4            *((volatile uint32 *)(GPIO4_BASE_ADDR + 0x00))
#define IOINTSEL5            *((volatile uint32 *)(GPIO5_BASE_ADDR + 0x00))
#define IOINTSEL6            *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x00))
#define IOINTSEL7            *((volatile uint32 *)(GPIO7_BASE_ADDR + 0x00))
#define IOINTSEL8            *((volatile uint32 *)(GPIO8_BASE_ADDR + 0x00))
#define IOINTSEL9            *((volatile uint32 *)(GPIO9_BASE_ADDR + 0x00))

#define INOUTSEL0            *((volatile uint32 *)(GPIO0_BASE_ADDR + 0x04))
#define INOUTSEL1            *((volatile uint32 *)(GPIO1_BASE_ADDR + 0x04))
#define INOUTSEL2            *((volatile uint32 *)(GPIO2_BASE_ADDR + 0x04))
#define INOUTSEL3            *((volatile uint32 *)(GPIO3_BASE_ADDR + 0x04))
#define INOUTSEL4            *((volatile uint32 *)(GPIO4_BASE_ADDR + 0x04))
#define INOUTSEL5            *((volatile uint32 *)(GPIO5_BASE_ADDR + 0x04))
#define INOUTSEL6            *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x04))
#define INOUTSEL7            *((volatile uint32 *)(GPIO7_BASE_ADDR + 0x04))
#define INOUTSEL8            *((volatile uint32 *)(GPIO8_BASE_ADDR + 0x04))
#define INOUTSEL9            *((volatile uint32 *)(GPIO9_BASE_ADDR + 0x04))

#define OUTDT0               *((volatile uint32 *)(GPIO0_BASE_ADDR + 0x08))
#define OUTDT1               *((volatile uint32 *)(GPIO1_BASE_ADDR + 0x08))
#define OUTDT2               *((volatile uint32 *)(GPIO2_BASE_ADDR + 0x08))
#define OUTDT3               *((volatile uint32 *)(GPIO3_BASE_ADDR + 0x08))
#define OUTDT4               *((volatile uint32 *)(GPIO4_BASE_ADDR + 0x08))
#define OUTDT5               *((volatile uint32 *)(GPIO5_BASE_ADDR + 0x08))
#define OUTDT6               *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x08))
#define OUTDT7               *((volatile uint32 *)(GPIO7_BASE_ADDR + 0x08))
#define OUTDT8               *((volatile uint32 *)(GPIO8_BASE_ADDR + 0x08))
#define OUTDT9               *((volatile uint32 *)(GPIO9_BASE_ADDR + 0x08))

#define OUTDTH0               *((volatile uint32 *)(GPIO0_BASE_ADDR + 0x44))
#define OUTDTH1               *((volatile uint32 *)(GPIO1_BASE_ADDR + 0x44))
#define OUTDTH2               *((volatile uint32 *)(GPIO2_BASE_ADDR + 0x44))
#define OUTDTH3               *((volatile uint32 *)(GPIO3_BASE_ADDR + 0x44))
#define OUTDTH4               *((volatile uint32 *)(GPIO4_BASE_ADDR + 0x44))
#define OUTDTH5               *((volatile uint32 *)(GPIO5_BASE_ADDR + 0x44))
#define OUTDTH6               *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x44))
#define OUTDTH7               *((volatile uint32 *)(GPIO7_BASE_ADDR + 0x44))
#define OUTDTH8               *((volatile uint32 *)(GPIO8_BASE_ADDR + 0x44))
#define OUTDTH9               *((volatile uint32 *)(GPIO9_BASE_ADDR + 0x44))

#define OUTDTL0               *((volatile uint32 *)(GPIO0_BASE_ADDR + 0x48))
#define OUTDTL1               *((volatile uint32 *)(GPIO1_BASE_ADDR + 0x48))
#define OUTDTL2               *((volatile uint32 *)(GPIO2_BASE_ADDR + 0x48))
#define OUTDTL3               *((volatile uint32 *)(GPIO3_BASE_ADDR + 0x48))
#define OUTDTL4               *((volatile uint32 *)(GPIO4_BASE_ADDR + 0x48))
#define OUTDTL5               *((volatile uint32 *)(GPIO5_BASE_ADDR + 0x48))
#define OUTDTL6               *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x48))
#define OUTDTL7               *((volatile uint32 *)(GPIO7_BASE_ADDR + 0x48))
#define OUTDTL8               *((volatile uint32 *)(GPIO8_BASE_ADDR + 0x48))
#define OUTDTL9               *((volatile uint32 *)(GPIO9_BASE_ADDR + 0x48))

#define POSNEG0              *((volatile uint32 *)(GPIO0_BASE_ADDR + 0x20))
#define POSNEG1              *((volatile uint32 *)(GPIO1_BASE_ADDR + 0x20))
#define POSNEG2              *((volatile uint32 *)(GPIO2_BASE_ADDR + 0x20))
#define POSNEG3              *((volatile uint32 *)(GPIO3_BASE_ADDR + 0x20))
#define POSNEG4              *((volatile uint32 *)(GPIO4_BASE_ADDR + 0x20))
#define POSNEG5              *((volatile uint32 *)(GPIO5_BASE_ADDR + 0x20))
#define POSNEG6              *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x20))
#define POSNEG7              *((volatile uint32 *)(GPIO7_BASE_ADDR + 0x20))
#define POSNEG8              *((volatile uint32 *)(GPIO8_BASE_ADDR + 0x20))
#define POSNEG9              *((volatile uint32 *)(GPIO9_BASE_ADDR + 0x20))

#define OUTDTSEL0            *((volatile uint32 *)(GPIO0_BASE_ADDR + 0x40))
#define OUTDTSEL1            *((volatile uint32 *)(GPIO1_BASE_ADDR + 0x40))
#define OUTDTSEL2            *((volatile uint32 *)(GPIO2_BASE_ADDR + 0x40))
#define OUTDTSEL3            *((volatile uint32 *)(GPIO3_BASE_ADDR + 0x40))
#define OUTDTSEL4            *((volatile uint32 *)(GPIO4_BASE_ADDR + 0x40))
#define OUTDTSEL5            *((volatile uint32 *)(GPIO5_BASE_ADDR + 0x40))
#define OUTDTSEL6            *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x40))
#define OUTDTSEL7            *((volatile uint32 *)(GPIO7_BASE_ADDR + 0x40))
#define OUTDTSEL8            *((volatile uint32 *)(GPIO8_BASE_ADDR + 0x40))
#define OUTDTSEL9            *((volatile uint32 *)(GPIO9_BASE_ADDR + 0x40))

#define GPSR0                *((volatile uint32 *)(GPIO0_BASE_ADDR - 0x140))
#define GPSR1                *((volatile uint32 *)(GPIO1_BASE_ADDR - 0x140))
#define GPSR2                *((volatile uint32 *)(GPIO2_BASE_ADDR - 0x140))
#define GPSR3                *((volatile uint32 *)(GPIO3_BASE_ADDR - 0x140))
#define GPSR4                *((volatile uint32 *)(GPIO4_BASE_ADDR - 0x140))
#define GPSR5                *((volatile uint32 *)(GPIO5_BASE_ADDR - 0x140))
#define GPSR6                *((volatile uint32 *)(GPIO6_BASE_ADDR - 0x140))
#define GPSR7                *((volatile uint32 *)(GPIO7_BASE_ADDR - 0x140))
#define GPSR8                *((volatile uint32 *)(GPIO8_BASE_ADDR - 0x140))
#define GPSR9                *((volatile uint32 *)(GPIO9_BASE_ADDR - 0x140))

#define PMMR0                *((volatile uint32 *)(GPIO0_BASE_ADDR - 0x180))
#define PMMR1                *((volatile uint32 *)(GPIO1_BASE_ADDR - 0x180))
#define PMMR2                *((volatile uint32 *)(GPIO2_BASE_ADDR - 0x180))
#define PMMR3                *((volatile uint32 *)(GPIO3_BASE_ADDR - 0x180))
#define PMMR4                *((volatile uint32 *)(GPIO4_BASE_ADDR - 0x180))
#define PMMR5                *((volatile uint32 *)(GPIO5_BASE_ADDR - 0x180))
#define PMMR6                *((volatile uint32 *)(GPIO6_BASE_ADDR - 0x180))
#define PMMR7                *((volatile uint32 *)(GPIO7_BASE_ADDR - 0x180))
#define PMMR8                *((volatile uint32 *)(GPIO8_BASE_ADDR - 0x180))
#define PMMR9                *((volatile uint32 *)(GPIO9_BASE_ADDR - 0x180))

#define INEN6                *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x50))
#define INDT6                *((volatile uint32 *)(GPIO6_BASE_ADDR + 0x0C))

/* Address  Module Stop Control Register */
#define REG_RMSTPCR9_ADDR             0xE6152D24UL
#define REG_RMSTPCR9                  *((volatile uint32 *)(REG_RMSTPCR9_ADDR))

/* Module Stop Status Register */
#define REG_MSTPSR9_ADDR              0xE6152E24UL
#define REG_MSTPSR9                   *((volatile uint32 *)(REG_MSTPSR9_ADDR))


/* CPG Write Protect Register */
#define REG_CPGWPR                    *((volatile uint32 *)(0xE6150000UL))
#define REG_CPGWPCR                   *((volatile uint32 *)(0xE6150004UL))
#define CPG_CPGWPCR_KEYCODE           (uint32)(0xA5A50000UL)
#define CPG_DISABLE_PROTECTION        (uint32)(CPG_CPGWPCR_KEYCODE | 0x0UL)
#define CPG_ENABLE_PROTECTION         (uint32)(CPG_CPGWPCR_KEYCODE | 0x1UL)

/*******************************************************************************
**                      Function Prototypes                                   **
*******************************************************************************/

#endif /* APP_DIO_SPECIFIC_SAMPLE_H */

/*******************************************************************************
**                      End of File                                           **
*******************************************************************************/
