/*============================================================================*/
/* Project      = R-CarGen3 AUTOSAR MCAL Development Project                  */
/* Module       = App_DIO_Specific_Sample.c                                   */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* Copyright(c) 2020 Renesas Electronics Corporation                          */
/*============================================================================*/
/* Purpose:                                                                   */
/* This file contains sample application for DIO Driver Component             */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Renesas Electronics Corporation the following shall apply!                 */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Renesas. Any        */
/* warranty is expressly disclaimed and excluded by Renesas, either expressed */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Renesas shall not have any obligation to maintain, service or provide bug  */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/* Each User is solely responsible for determining the appropriateness of     */
/* using the Product(s) and assumes all risks associated with its exercise    */
/* of rights under this Agreement, including, but not limited to the risks    */
/* and costs of program errors, compliance with applicable laws, damage to    */
/* or loss of data, programs or equipment, and unavailability or              */
/* interruption of operations.                                                */
/*                                                                            */
/* Limitation of Liability                                                    */
/*                                                                            */
/* In no event shall Renesas be liable to the User for any incidental,        */
/* consequential, indirect, or punitive damage (including but not limited     */
/* to lost profits) regardless of whether such liability is based on breach   */
/* of contract, tort, strict liability, breach of warranties, failure of      */
/* essential purpose or otherwise and even if advised of the possibility of   */
/* such damages. Renesas shall not be liable for any services or products     */
/* provided by third party vendors, developers or consultants identified or   */
/* referred to the User by Renesas in connection with the Product(s) and/or   */
/* the Application.                                                           */
/*                                                                            */
/*============================================================================*/
/* Environment:                                                               */
/*              Devices:        R-Car V3U                                     */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
* V1.0.0:   26-Nov-20  : Initial Version
*/
/******************************************************************************/

/*******************************************************************************
**                     Include Section                                        **
*******************************************************************************/
#include "App_DIO_Specific_Sample.h"
#include "Dio.h"
#include "scif.h"
#include "log.h"
/*******************************************************************************
**                      Defines                                               **
*******************************************************************************/

/*******************************************************************************
**                      Global variables                                      **
*******************************************************************************/
/* Local variable to store the version information of the DIO Driver
   Component */
Std_VersionInfoType VersionInfo;

/* Local variable to store the return value of the Dio_ReadPort() and
   Dio_ReadChannelGroup() */
volatile Dio_PortLevelType LddPortLevel;

/* Local variable to store the return value of the Dio_ReadChannel() */
volatile Dio_LevelType LddLevel;

/*******************************************************************************
**                      User Function Prototypes                              **
*******************************************************************************/
void Wdg_Init(void);
void Mcu_Init(void);
void Port_Init(void);
void Disable_CPG_Protection(void);
void Write_CPGRegister(uint32 address, uint32 value);
void GPIO_Start_Module(void);

/*******************************************************************************
**                      Main Function Definitions                             **
*******************************************************************************/
int main(void)
{
  /* Initialize Watchdog */
  Wdg_Init();
  /* Initialize MCU */
  Mcu_Init();
  /* Initialize PORT */
  Port_Init();
  /* Initialize SCIF module */
  Scif_Init();
  /* Start program */
  Console_Print("PROGRAM START\r\n");

  /* To Get Version Information of the DIO Driver component */
  Dio_GetVersionInfo(&VersionInfo);
  if ((VersionInfo.vendorID == DIO_VENDOR_ID) &&
     (VersionInfo.moduleID == DIO_MODULE_ID) &&
     (VersionInfo.sw_major_version == DIO_SW_MAJOR_VERSION) &&
     (VersionInfo.sw_minor_version == DIO_SW_MINOR_VERSION) &&
     (VersionInfo.sw_patch_version == DIO_SW_PATCH_VERSION))
  {
     Console_Print("VersionInfor is correct\r\n");
  }
  else
  {
    return -1;
  }
  /* To Set the Channel value */
  Dio_WriteChannel(DioConf_DioChannel_DioChannel, STD_HIGH);
  if (0u == GstDetBuffIndex && (OUTDT0 & (0x1u))==0x1u)
  {
     Console_Print("Dio_WriteChannel is correct\r\n");
  }
  else
  {
    return -1;
  }
  /* To Read the Channel value */
  LddLevel = STD_LOW;
  LddLevel = Dio_ReadChannel(DioConf_DioChannel_DioChannel);
  if (0u == GstDetBuffIndex && STD_HIGH == LddLevel)
  {
     Console_Print("Dio_ReadChannel is correct\r\n");
  }
  else
  {
    return -1;
  }
  /* To Set the Port value */
  Dio_WritePort(DioConf_DioPort_DioPort, 0xAAAAA);
  if ((0u == GstDetBuffIndex) && (OUTDT0 & (0x0FFFFFFFu))==0xAAAAAu)
  {
     Console_Print("Dio_WritePort is correct\r\n");
  }
  else
  {
    return -1;
  }
  /* To Read the Port value */
  LddPortLevel = Dio_ReadPort(DioConf_DioPort_DioPort_006);
  if ((0u == GstDetBuffIndex)  && (0x1C0000ul == (LddPortLevel&0x1C0000ul)))
  {
     Console_Print("Dio_ReadPort is correct\r\n");
  }
  else
  {
    return -1;
  }
  /* To Set the ChannelGroup value */
  Dio_WriteChannelGroup(DioConf_DioChannelGroup_DioChannelGroup, 0xA);
  if (0u == GstDetBuffIndex && (OUTDT0 & (0xF0u))==0xA0u)
  {
     Console_Print("Dio_WriteChannelGroup is correct\r\n");
  }
  else
  {
    return -1;
  }
  /* To Read the ChannelGroup value  */
  LddPortLevel = Dio_ReadChannelGroup( \
    DioConf_DioChannelGroup_DioChannelGroup_006);
  if ((0u == GstDetBuffIndex) && (0x7ul == LddPortLevel))
  {
     Console_Print("Dio_ReadChannelGroup is correct\r\n");
  }
  else
  {
    return -1;
  }
  /*
  * Service to flip (change from 1 to 0 or from 0 to 1) the level of
  * a channel and return the level of the channel after flip
  */
  LddLevel = Dio_FlipChannel(DioConf_DioChannel_DioChannel);
  if (0u == GstDetBuffIndex && (OUTDT0 & (0x1u))==0x1u)
  {
     Console_Print("Dio_FlipChannel is correct\r\n");
  }
  else
  {
    return -1;
  }
  /* Stop program */
  Console_Print("PROGRAM END\r\n");
 
  return 0;
}
/* End of main() function */

/*******************************************************************************
**                      Watchdog Initialization                               **
*******************************************************************************/
void Wdg_Init(void)
{
  /* No Action Required*/
}

/*******************************************************************************
**                      Mcu Initialization                                    **
*******************************************************************************/
void Disable_CPG_Protection(void)
{
  /* Set inverted writing value of CPGWPCR to CPGWPR */
  REG_CPGWPR = (uint32)(~CPG_DISABLE_PROTECTION);
  /* Disable CPG protection */
  REG_CPGWPCR = CPG_DISABLE_PROTECTION;
  return;
}

void Write_CPGRegister(uint32 address, uint32 value)
{
  volatile uint32 *reg = (volatile uint32 *)address;
  REG_CPGWPR = ~(value);
  *reg = value;
  return;
}

void GPIO_Start_Module(void)
{
  Write_CPGRegister(REG_RMSTPCR9_ADDR, REG_RMSTPCR9 & (uint32)(~(0xF << 15)));
  Write_CPGRegister(REG_MSTPSR9_ADDR, REG_MSTPSR9 & (uint32)(~(0xF << 15)));
  return;
}

void Mcu_Init(void)
{
  Disable_CPG_Protection();
  GPIO_Start_Module();
  return;
}

/*******************************************************************************
**                      Port Initialization                                   **
*******************************************************************************/
void Port_Init(void)
{
  /* Configuration port 0 as IO mode */
  IOINTSEL0 &= (uint32)~0x0FFFFFFF;
  /* Configuration direction for port 0 pin are output */
  INOUTSEL0 |= (uint32)0x0FFFFFFF;
  /* Set the polarity positive of port 0 pin*/
  POSNEG0 &= (uint32)~0x0FFFFFFF;
  /* Select output data is output by OUTDT register */
  OUTDTSEL0 &= (uint32)~0x0FFFFFFF;
  /* Init level for pin */
  OUTDT0 &= (uint32)~0x0FFFFFFF;

  /* Configuration port 6 as IO mode, input_enable */
  PMMR6 = ~(uint32)(0xFFE3FFFF);
  GPSR6 = (uint32)(0xFFE3FFFF);
  INEN6 = (uint32)(0xFFFFFFFF);
  /* Configuration port 6 as IO mode */
  IOINTSEL6 &= (uint32)~0x1FFFFF;
  /* Configuration direction for port 6 pin are input */
  INOUTSEL6 &= (uint32)~0x1FFFFF;
  /* Set the polarity positive of port 6 pin*/
  POSNEG6 &= (uint32)~0x1FFFFF;

  return;
}

/*******************************************************************************
**                      End of File                                           **
*******************************************************************************/
