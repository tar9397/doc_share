/*============================================================================*/
/* Project      = R-CarGen3 AUTOSAR MCAL Development Project                  */
/* Module       = Dio_Lcfg.c                                                  */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* Copyright(c) 2015-2020 Renesas Electronics Corporation                     */
/*============================================================================*/
/* Purpose:                                                                   */
/* This file contains link time parameters.                                   */
/* AUTOMATICALLY GENERATED FILE - DO NOT EDIT                                 */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Renesas Electronics Corporation the following shall apply!                 */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Renesas. Any        */
/* warranty is expressly disclaimed and excluded by Renesas, either expressed */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Renesas shall not have any obligation to maintain, service or provide bug  */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/* Each User is solely responsible for determining the appropriateness of     */
/* using the Product(s) and assumes all risks associated with its exercise    */
/* of rights under this Agreement, including, but not limited to the risks    */
/* and costs of program errors, compliance with applicable laws, damage to    */
/* or loss of data, programs or equipment, and unavailability or              */
/* interruption of operations.                                                */
/*                                                                            */
/* Limitation of Liability                                                    */
/*                                                                            */
/* In no event shall Renesas be liable to the User for any incidental,        */
/* consequential, indirect, or punitive damage (including but not limited     */
/* to lost profits) regardless of whether such liability is based on breach   */
/* of contract, tort, strict liability, breach of warranties, failure of      */
/* essential purpose or otherwise and even if advised of the possibility of   */
/* such damages. Renesas shall not be liable for any services or products     */
/* provided by third party vendors, developers or consultants identified or   */
/* referred to the User by Renesas in connection with the Product(s) and/or   */
/* the Application.                                                           */
/*                                                                            */
/*============================================================================*/
/* Environment:                                                               */
/*              Devices:        R-Car V3U                                    */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
* V1.0.0:  23-Dec-2015  : Initial Version
* V1.0.1:  05-Jul-2016  : Add MISRA-C Violation
* V1.0.2:  30-Sep-2016  : Support Compiler Abstraction, Memory Mapping
*                         and Platform types for AUTOSAR 4.2.2
* V1.0.3:  26-Oct-2018  : - Update MISRA C rule #0306
*                         - Update DIO memory section based on Compliance of
*                           INIT POLICY with AUTOSAR 4.2.2 Memory Mapping
* V1.0.4:  13-Nov-2020  : Update copyright version
*/
/******************************************************************************/

/*******************************************************************************
**                   Generation Tool Version                                  **
*******************************************************************************/
/*
* TOOL VERSION:  1.1.11
*/

/*******************************************************************************
**                         Input File                                         **
*******************************************************************************/
/*
 * INPUT FILE:     D:\Workspace\repo\RCAR_GIT\RCar_V3U_DIO_Develop\external\
 *                  RCar\common_platform\generic\stubs\4.2.2\Port\xml\
 *                  Port_Dio_V3U.arxml
 *                 D:\Workspace\repo\RCAR_GIT\
 *                  RCar_V3U_DIO_Develop\external\RCar\V3U\modules\dio\
 *                  sample_application\4.2.2\config\App_DIO_Sample.arxml
 *                 D:\Workspace\repo\RCAR_GIT\
 *                  RCar_V3U_DIO_Develop\external\RCar\common_platform\generic
 *                  \stubs\4.2.2\Dem\xml\Dem_Dio.arxml
 *                 D:\Workspace\repo\RCAR_GIT\
 *                  RCar_V3U_DIO_Develop\external\RCar\V3U\modules\dio\
 *                  generator\R422_DIO_V3U_BSWMDT.arxml
 * GENERATED ON:    2 Mar 2021 - 19:14:03
 */

/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/
#include "Dio.h"
#include "Dio_LTTypes.h"

/*******************************************************************************
**                      Version Information                                   **
*******************************************************************************/

/* AUTOSAR release version information */
#define DIO_LCFG_C_AR_RELEASE_MAJOR_VERSION  4U
#define DIO_LCFG_C_AR_RELEASE_MINOR_VERSION  2U
#define DIO_LCFG_C_AR_RELEASE_REVISION_VERSION  2U

/* File version information */
#define DIO_LCFG_C_SW_MAJOR_VERSION   1U
#define DIO_LCFG_C_SW_MINOR_VERSION   1U


/*******************************************************************************
 **                      MISRA C Rule Violations                              **
 ******************************************************************************/
/* 1. MISRA C RULE VIOLATION:                                                 */
/* Message       : (2:0306) [I] Cast between a pointer to object and          */
/*                 an integral type                                           */
/* Rule          : MISRA-C:2004 Rule 11.3                                     */
/* Justification : Casting between a pointer and an integer type may be       */
/*                 unavoidable when addressing memory mapped registers.       */
/* Verification  : However, part of the code is verified manually and it is   */
/*                 not having any impact.                                     */
/* Reference     : Look for START Msg(2:0306)-1 and                           */
/*                 END Msg(2:0306)-1 tags in the code.                        */

/*******************************************************************************
**                      Version Check                                         **
*******************************************************************************/

#if (DIO_LCFG_C_AR_RELEASE_MAJOR_VERSION != DIO_LCFG_AR_RELEASE_MAJOR_VERSION)
  #error "Dio_Lcfg.c : Mismatch in Major Version"
#endif

#if (DIO_LCFG_C_AR_RELEASE_MINOR_VERSION != DIO_LCFG_AR_RELEASE_MINOR_VERSION)
  #error "Dio_Lcfg.c : Mismatch in Minor Version"
#endif

#if (DIO_LCFG_C_AR_RELEASE_REVISION_VERSION != \
  DIO_LCFG_AR_RELEASE_REVISION_VERSION)
  #error "Dio_Lcfg.c : Mismatch in Patch Version"
#endif

#if (DIO_LCFG_C_SW_MAJOR_VERSION != DIO_LCFG_SW_MAJOR_VERSION)
  #error "Dio_Lcfg.c : Mismatch in Software Major Version"
#endif

#if (DIO_LCFG_C_SW_MINOR_VERSION != DIO_LCFG_SW_MINOR_VERSION)
  #error "Dio_Lcfg.c : Mismatch in Software Minor Version"
#endif


/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/
/* TRACE [SWS_BSW_00014] */
#define DIO_START_SEC_CONST_32
#include "Dio_MemMap.h"

/* Structure of DIO Port Group Configuration */
CONST (Dio_PortGroup, DIO_CONST) Dio_GstPortGroup[] =
{
  /* Index: 0 - DioPort */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6050000UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x0FFFFFFFUL
  },

  /* Index: 1 - DioPort_001 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6051000UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x7FFFFFFFUL
  },

  /* Index: 2 - DioPort_002 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6052000UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x01FFFFFFUL
  },

  /* Index: 3 - DioPort_003 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6053000UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x0001FFFFUL
  },

  /* Index: 4 - DioPort_004 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6054000UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x07FFFFFFUL
  },

  /* Index: 5 - DioPort_005 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6055000UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x001FFFFFUL
  },

  /* Index: 6 - DioPort_006 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6055400UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x001FFFFFUL
  },

  /* Index: 7 - DioPort_007 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6055800UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x001FFFFFUL
  },

  /* Index: 8 - DioPort_008 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6055800UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x001FFFFFUL
  },

  /* Index: 9 - DioPort_009 */
  {
    /* pPortAddress */
    /* MISRA Violation: START Msg(2:0306)-1 */
    (P2VAR(uint32, TYPEDEF, DIO_CONFIG_DATA)) 0xE6055800UL,
    /* START Msg(2:0306)-1 */

    /* ulModeMask */
    0x001FFFFFUL
  }
};


/* Data Structure of DIO Port Channel Configuration */
CONST(Dio_PortChannel, DIO_CONST) Dio_GstPortChannel[] =
{
  /* Index: 0 - DioPort/DioChannel */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 1 - DioPort/DioChannel_001 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 2 - DioPort/DioChannel_002 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 3 - DioPort/DioChannel_003 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 4 - DioPort/DioChannel_004 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 5 - DioPort/DioChannel_005 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 6 - DioPort/DioChannel_006 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 7 - DioPort/DioChannel_007 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 8 - DioPort/DioChannel_008 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 9 - DioPort/DioChannel_009 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 10 - DioPort/DioChannel_010 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 11 - DioPort/DioChannel_011 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 12 - DioPort/DioChannel_012 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 13 - DioPort/DioChannel_013 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 14 - DioPort/DioChannel_014 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 15 - DioPort/DioChannel_015 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 16 - DioPort/DioChannel_016 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 17 - DioPort/DioChannel_017 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 18 - DioPort/DioChannel_018 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 19 - DioPort/DioChannel_019 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 20 - DioPort/DioChannel_020 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 21 - DioPort/DioChannel_021 */
  {
    /* ulMask */
    0x00200000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 22 - DioPort/DioChannel_022 */
  {
    /* ulMask */
    0x00400000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 23 - DioPort/DioChannel_023 */
  {
    /* ulMask */
    0x00800000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 24 - DioPort/DioChannel_024 */
  {
    /* ulMask */
    0x01000000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 25 - DioPort/DioChannel_025 */
  {
    /* ulMask */
    0x02000000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 26 - DioPort/DioChannel_026 */
  {
    /* ulMask */
    0x04000000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 27 - DioPort/DioChannel_027 */
  {
    /* ulMask */
    0x08000000UL,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 28 - DioPort_001/DioChannel_028 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 29 - DioPort_001/DioChannel_029 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 30 - DioPort_001/DioChannel_030 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 31 - DioPort_001/DioChannel_031 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 32 - DioPort_001/DioChannel_032 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 33 - DioPort_001/DioChannel_033 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 34 - DioPort_001/DioChannel_034 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 35 - DioPort_001/DioChannel_035 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 36 - DioPort_001/DioChannel_036 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 37 - DioPort_001/DioChannel_037 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 38 - DioPort_001/DioChannel_038 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 39 - DioPort_001/DioChannel_039 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 40 - DioPort_001/DioChannel_040 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 41 - DioPort_001/DioChannel_041 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 42 - DioPort_001/DioChannel_042 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 43 - DioPort_001/DioChannel_043 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 44 - DioPort_001/DioChannel_044 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 45 - DioPort_001/DioChannel_045 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 46 - DioPort_001/DioChannel_046 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 47 - DioPort_001/DioChannel_047 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 48 - DioPort_001/DioChannel_048 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 49 - DioPort_001/DioChannel_049 */
  {
    /* ulMask */
    0x00200000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 50 - DioPort_001/DioChannel_050 */
  {
    /* ulMask */
    0x00400000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 51 - DioPort_001/DioChannel_051 */
  {
    /* ulMask */
    0x00800000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 52 - DioPort_001/DioChannel_052 */
  {
    /* ulMask */
    0x01000000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 53 - DioPort_001/DioChannel_053 */
  {
    /* ulMask */
    0x02000000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 54 - DioPort_001/DioChannel_054 */
  {
    /* ulMask */
    0x04000000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 55 - DioPort_001/DioChannel_055 */
  {
    /* ulMask */
    0x08000000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 56 - DioPort_001/DioChannel_056 */
  {
    /* ulMask */
    0x10000000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 57 - DioPort_001/DioChannel_057 */
  {
    /* ulMask */
    0x20000000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 58 - DioPort_001/DioChannel_058 */
  {
    /* ulMask */
    0x40000000UL,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 59 - DioPort_002/DioChannel_059 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 60 - DioPort_002/DioChannel_060 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 61 - DioPort_002/DioChannel_061 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 62 - DioPort_002/DioChannel_062 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 63 - DioPort_002/DioChannel_063 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 64 - DioPort_002/DioChannel_064 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 65 - DioPort_002/DioChannel_065 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 66 - DioPort_002/DioChannel_066 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 67 - DioPort_002/DioChannel_067 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 68 - DioPort_002/DioChannel_068 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 69 - DioPort_002/DioChannel_069 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 70 - DioPort_002/DioChannel_070 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 71 - DioPort_002/DioChannel_071 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 72 - DioPort_002/DioChannel_072 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 73 - DioPort_002/DioChannel_073 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 74 - DioPort_002/DioChannel_074 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 75 - DioPort_002/DioChannel_075 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 76 - DioPort_002/DioChannel_076 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 77 - DioPort_002/DioChannel_077 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 78 - DioPort_002/DioChannel_078 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 79 - DioPort_002/DioChannel_079 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 80 - DioPort_002/DioChannel_080 */
  {
    /* ulMask */
    0x00200000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 81 - DioPort_002/DioChannel_081 */
  {
    /* ulMask */
    0x00400000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 82 - DioPort_002/DioChannel_082 */
  {
    /* ulMask */
    0x00800000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 83 - DioPort_002/DioChannel_083 */
  {
    /* ulMask */
    0x01000000UL,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 84 - DioPort_003/DioChannel_084 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 85 - DioPort_003/DioChannel_085 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 86 - DioPort_003/DioChannel_086 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 87 - DioPort_003/DioChannel_087 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 88 - DioPort_003/DioChannel_088 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 89 - DioPort_003/DioChannel_089 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 90 - DioPort_003/DioChannel_090 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 91 - DioPort_003/DioChannel_091 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 92 - DioPort_003/DioChannel_092 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 93 - DioPort_003/DioChannel_093 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 94 - DioPort_003/DioChannel_094 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 95 - DioPort_003/DioChannel_095 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 96 - DioPort_003/DioChannel_096 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 97 - DioPort_003/DioChannel_097 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 98 - DioPort_003/DioChannel_098 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 99 - DioPort_003/DioChannel_099 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 100 - DioPort_003/DioChannel_100 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 101 - DioPort_004/DioChannel_101 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 102 - DioPort_004/DioChannel_102 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 103 - DioPort_004/DioChannel_103 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 104 - DioPort_004/DioChannel_104 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 105 - DioPort_004/DioChannel_105 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 106 - DioPort_004/DioChannel_106 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 107 - DioPort_004/DioChannel_107 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 108 - DioPort_004/DioChannel_108 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 109 - DioPort_004/DioChannel_109 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 110 - DioPort_004/DioChannel_110 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 111 - DioPort_004/DioChannel_111 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 112 - DioPort_004/DioChannel_112 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 113 - DioPort_004/DioChannel_113 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 114 - DioPort_004/DioChannel_114 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 115 - DioPort_004/DioChannel_115 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 116 - DioPort_004/DioChannel_116 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 117 - DioPort_004/DioChannel_117 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 118 - DioPort_004/DioChannel_118 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 119 - DioPort_004/DioChannel_119 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 120 - DioPort_004/DioChannel_120 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 121 - DioPort_004/DioChannel_121 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 122 - DioPort_004/DioChannel_122 */
  {
    /* ulMask */
    0x00200000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 123 - DioPort_004/DioChannel_123 */
  {
    /* ulMask */
    0x00400000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 124 - DioPort_004/DioChannel_124 */
  {
    /* ulMask */
    0x00800000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 125 - DioPort_004/DioChannel_125 */
  {
    /* ulMask */
    0x01000000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 126 - DioPort_004/DioChannel_126 */
  {
    /* ulMask */
    0x02000000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 127 - DioPort_004/DioChannel_127 */
  {
    /* ulMask */
    0x04000000UL,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 128 - DioPort_005/DioChannel_128 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 129 - DioPort_005/DioChannel_129 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 130 - DioPort_005/DioChannel_130 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 131 - DioPort_005/DioChannel_131 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 132 - DioPort_005/DioChannel_132 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 133 - DioPort_005/DioChannel_133 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 134 - DioPort_005/DioChannel_134 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 135 - DioPort_005/DioChannel_135 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 136 - DioPort_005/DioChannel_136 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 137 - DioPort_005/DioChannel_137 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 138 - DioPort_005/DioChannel_138 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 139 - DioPort_005/DioChannel_139 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 140 - DioPort_005/DioChannel_140 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 141 - DioPort_005/DioChannel_141 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 142 - DioPort_005/DioChannel_142 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 143 - DioPort_005/DioChannel_143 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 144 - DioPort_005/DioChannel_144 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 145 - DioPort_005/DioChannel_145 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 146 - DioPort_005/DioChannel_146 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 147 - DioPort_005/DioChannel_147 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 148 - DioPort_005/DioChannel_148 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 149 - DioPort_006/DioChannel_149 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 150 - DioPort_006/DioChannel_150 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 151 - DioPort_006/DioChannel_151 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 152 - DioPort_006/DioChannel_152 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 153 - DioPort_006/DioChannel_153 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 154 - DioPort_006/DioChannel_154 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 155 - DioPort_006/DioChannel_155 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 156 - DioPort_006/DioChannel_156 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 157 - DioPort_006/DioChannel_157 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 158 - DioPort_006/DioChannel_158 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 159 - DioPort_006/DioChannel_159 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 160 - DioPort_006/DioChannel_160 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 161 - DioPort_006/DioChannel_161 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 162 - DioPort_006/DioChannel_162 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 163 - DioPort_006/DioChannel_163 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 164 - DioPort_006/DioChannel_164 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 165 - DioPort_006/DioChannel_165 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 166 - DioPort_006/DioChannel_166 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 167 - DioPort_006/DioChannel_167 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 168 - DioPort_006/DioChannel_168 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 169 - DioPort_006/DioChannel_169 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 170 - DioPort_007/DioChannel_170 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 171 - DioPort_007/DioChannel_171 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 172 - DioPort_007/DioChannel_172 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 173 - DioPort_007/DioChannel_173 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 174 - DioPort_007/DioChannel_174 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 175 - DioPort_007/DioChannel_175 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 176 - DioPort_007/DioChannel_176 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 177 - DioPort_007/DioChannel_177 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 178 - DioPort_007/DioChannel_178 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 179 - DioPort_007/DioChannel_179 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 180 - DioPort_007/DioChannel_180 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 181 - DioPort_007/DioChannel_181 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 182 - DioPort_007/DioChannel_182 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 183 - DioPort_007/DioChannel_183 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 184 - DioPort_007/DioChannel_184 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 185 - DioPort_007/DioChannel_185 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 186 - DioPort_007/DioChannel_186 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 187 - DioPort_007/DioChannel_187 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 188 - DioPort_007/DioChannel_188 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 189 - DioPort_007/DioChannel_189 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 190 - DioPort_007/DioChannel_190 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 191 - DioPort_008/DioChannel_191 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 192 - DioPort_008/DioChannel_192 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 193 - DioPort_008/DioChannel_193 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 194 - DioPort_008/DioChannel_194 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 195 - DioPort_008/DioChannel_195 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 196 - DioPort_008/DioChannel_196 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 197 - DioPort_008/DioChannel_197 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 198 - DioPort_008/DioChannel_198 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 199 - DioPort_008/DioChannel_199 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 200 - DioPort_008/DioChannel_200 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 201 - DioPort_008/DioChannel_201 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 202 - DioPort_008/DioChannel_202 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 203 - DioPort_008/DioChannel_203 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 204 - DioPort_008/DioChannel_204 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 205 - DioPort_008/DioChannel_205 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 206 - DioPort_008/DioChannel_206 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 207 - DioPort_008/DioChannel_207 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 208 - DioPort_008/DioChannel_208 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 209 - DioPort_008/DioChannel_209 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 210 - DioPort_008/DioChannel_210 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 211 - DioPort_008/DioChannel_211 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 212 - DioPort_009/DioChannel_212 */
  {
    /* ulMask */
    0x00000001UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 213 - DioPort_009/DioChannel_213 */
  {
    /* ulMask */
    0x00000002UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 214 - DioPort_009/DioChannel_214 */
  {
    /* ulMask */
    0x00000004UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 215 - DioPort_009/DioChannel_215 */
  {
    /* ulMask */
    0x00000008UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 216 - DioPort_009/DioChannel_216 */
  {
    /* ulMask */
    0x00000010UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 217 - DioPort_009/DioChannel_217 */
  {
    /* ulMask */
    0x00000020UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 218 - DioPort_009/DioChannel_218 */
  {
    /* ulMask */
    0x00000040UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 219 - DioPort_009/DioChannel_219 */
  {
    /* ulMask */
    0x00000080UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 220 - DioPort_009/DioChannel_220 */
  {
    /* ulMask */
    0x00000100UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 221 - DioPort_009/DioChannel_221 */
  {
    /* ulMask */
    0x00000200UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 222 - DioPort_009/DioChannel_222 */
  {
    /* ulMask */
    0x00000400UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 223 - DioPort_009/DioChannel_223 */
  {
    /* ulMask */
    0x00000800UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 224 - DioPort_009/DioChannel_224 */
  {
    /* ulMask */
    0x00001000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 225 - DioPort_009/DioChannel_225 */
  {
    /* ulMask */
    0x00002000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 226 - DioPort_009/DioChannel_226 */
  {
    /* ulMask */
    0x00004000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 227 - DioPort_009/DioChannel_227 */
  {
    /* ulMask */
    0x00008000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 228 - DioPort_009/DioChannel_228 */
  {
    /* ulMask */
    0x00010000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 229 - DioPort_009/DioChannel_229 */
  {
    /* ulMask */
    0x00020000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 230 - DioPort_009/DioChannel_230 */
  {
    /* ulMask */
    0x00040000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 231 - DioPort_009/DioChannel_231 */
  {
    /* ulMask */
    0x00080000UL,

    /* ucPortIndex */
    0x09U
  },

  /* Index: 232 - DioPort_009/DioChannel_232 */
  {
    /* ulMask */
    0x00100000UL,

    /* ucPortIndex */
    0x09U
  }
};


/* Data Structure of DIO Port Channel Group Configuration */
CONST(Dio_ChannelGroupType, DIO_CONST) Dio_GstChannelGroupData[] =
{
  /* Index: 0 - DioPort/DioChannelGroup */
  {
    /* ulMask */
    0x000000F0UL,

    /* ucOffset */
    0x04U,

    /* ucPortIndex */
    0x00U
  },

  /* Index: 1 - DioPort_001/DioChannelGroup_001 */
  {
    /* ulMask */
    0x0000000FUL,

    /* ucOffset */
    0x00U,

    /* ucPortIndex */
    0x01U
  },

  /* Index: 2 - DioPort_002/DioChannelGroup_002 */
  {
    /* ulMask */
    0x0000000FUL,

    /* ucOffset */
    0x00U,

    /* ucPortIndex */
    0x02U
  },

  /* Index: 3 - DioPort_003/DioChannelGroup_003 */
  {
    /* ulMask */
    0x0000000FUL,

    /* ucOffset */
    0x00U,

    /* ucPortIndex */
    0x03U
  },

  /* Index: 4 - DioPort_004/DioChannelGroup_004 */
  {
    /* ulMask */
    0x0000000FUL,

    /* ucOffset */
    0x00U,

    /* ucPortIndex */
    0x04U
  },

  /* Index: 5 - DioPort_005/DioChannelGroup_005 */
  {
    /* ulMask */
    0x0000000FUL,

    /* ucOffset */
    0x00U,

    /* ucPortIndex */
    0x05U
  },

  /* Index: 6 - DioPort_006/DioChannelGroup_006 */
  {
    /* ulMask */
    0x001C0000UL,

    /* ucOffset */
    0x12U,

    /* ucPortIndex */
    0x06U
  },

  /* Index: 7 - DioPort_007/DioChannelGroup_007 */
  {
    /* ulMask */
    0x0000000FUL,

    /* ucOffset */
    0x00U,

    /* ucPortIndex */
    0x07U
  },

  /* Index: 8 - DioPort_008/DioChannelGroup_008 */
  {
    /* ulMask */
    0x0000000FUL,

    /* ucOffset */
    0x00U,

    /* ucPortIndex */
    0x08U
  },

  /* Index: 9 - DioPort_009/DioChannelGroup_009 */
  {
    /* ulMask */
    0x00007000UL,

    /* ucOffset */
    0x0CU,

    /* ucPortIndex */
    0x09U
  }
};


#define DIO_STOP_SEC_CONST_32
#include "Dio_MemMap.h"

/*******************************************************************************
**                      Function Definitions                                  **
*******************************************************************************/

/*******************************************************************************
**                          End of File                                       **
*******************************************************************************/
