/*============================================================================*/
/* Project      = R-CarGen3 AUTOSAR MCAL Development Project                  */
/* Module       = Dio_Cfg.h                                                   */
/*============================================================================*/
/*                                  COPYRIGHT                                 */
/*============================================================================*/
/* Copyright(c) 2015-2020 Renesas Electronics Corporation                     */
/*============================================================================*/
/* Purpose:                                                                   */
/* This file contains pre-compile time parameters.                            */
/* AUTOMATICALLY GENERATED FILE - DO NOT EDIT                                 */
/*                                                                            */
/*============================================================================*/
/*                                                                            */
/* Unless otherwise agreed upon in writing between your company and           */
/* Renesas Electronics Corporation the following shall apply!                 */
/*                                                                            */
/* Warranty Disclaimer                                                        */
/*                                                                            */
/* There is no warranty of any kind whatsoever granted by Renesas. Any        */
/* warranty is expressly disclaimed and excluded by Renesas, either expressed */
/* or implied, including but not limited to those for non-infringement of     */
/* intellectual property, merchantability and/or fitness for the particular   */
/* purpose.                                                                   */
/*                                                                            */
/* Renesas shall not have any obligation to maintain, service or provide bug  */
/* fixes for the supplied Product(s) and/or the Application.                  */
/*                                                                            */
/* Each User is solely responsible for determining the appropriateness of     */
/* using the Product(s) and assumes all risks associated with its exercise    */
/* of rights under this Agreement, including, but not limited to the risks    */
/* and costs of program errors, compliance with applicable laws, damage to    */
/* or loss of data, programs or equipment, and unavailability or              */
/* interruption of operations.                                                */
/*                                                                            */
/* Limitation of Liability                                                    */
/*                                                                            */
/* In no event shall Renesas be liable to the User for any incidental,        */
/* consequential, indirect, or punitive damage (including but not limited     */
/* to lost profits) regardless of whether such liability is based on breach   */
/* of contract, tort, strict liability, breach of warranties, failure of      */
/* essential purpose or otherwise and even if advised of the possibility of   */
/* such damages. Renesas shall not be liable for any services or products     */
/* provided by third party vendors, developers or consultants identified or   */
/* referred to the User by Renesas in connection with the Product(s) and/or   */
/* the Application.                                                           */
/*                                                                            */
/*============================================================================*/
/* Environment:                                                               */
/*              Devices:        R-Car V3U                                    */
/*============================================================================*/

/*******************************************************************************
**                      Revision Control History                              **
*******************************************************************************/
/*
* V1.0.0:  22-Dec-2015  : Initial Version.
* V1.1.0:  30-Sep-2016  : - Change format of Channel, Port and ChannelGroup
*                         handler.
*                         - Define new macro DIO_PORT_CONFIGURED.
*                         - Add GIC address to support disable/enable interrupt
*                         when get/release exclusive control.
* V1.1.1:  28-Oct-2016  : Define new macro DIO_EXCLUSIVE_CONTROL_TIMEOUT
* V1.1.2:  03-Mar-2017  : Remove macro DIO_PORT_CONFIGURED, 
*                         DIO_CHANNEL_CONFIGURED, DIO_CHANNELGROUP_CONFIGURED.
* V1.1.3:  13-Nov-2020  : Update copyright version.
*/
/******************************************************************************/

/*******************************************************************************
**                   Generation Tool Version                                  **
*******************************************************************************/
/*
* TOOL VERSION:  1.1.11
*/

/*******************************************************************************
**                          Input File                                        **
*******************************************************************************/

/*
 * INPUT FILE:     D:\Workspace\repo\RCAR_GIT\RCar_V3U_DIO_Develop\external\
 *                  RCar\common_platform\generic\stubs\4.2.2\Port\xml\
 *                  Port_Dio_V3U.arxml
 *                 D:\Workspace\repo\RCAR_GIT\
 *                  RCar_V3U_DIO_Develop\external\RCar\V3U\modules\dio\
 *                  sample_application\4.2.2\config\App_DIO_Sample.arxml
 *                 D:\Workspace\repo\RCAR_GIT\
 *                  RCar_V3U_DIO_Develop\external\RCar\common_platform\generic
 *                  \stubs\4.2.2\Dem\xml\Dem_Dio.arxml
 *                 D:\Workspace\repo\RCAR_GIT\
 *                  RCar_V3U_DIO_Develop\external\RCar\V3U\modules\dio\
 *                  generator\R422_DIO_V3U_BSWMDT.arxml
 * GENERATED ON:    2 Mar 2021 - 19:14:03
 */

#ifndef DIO_CFG_H 
#define DIO_CFG_H  
/*******************************************************************************
**                      Include Section                                       **
*******************************************************************************/

/*******************************************************************************
**                      Version Information                                   **
*******************************************************************************/

/* AUTOSAR release version information */
#define DIO_CFG_AR_RELEASE_MAJOR_VERSION  4U
#define DIO_CFG_AR_RELEASE_MINOR_VERSION  2U
#define DIO_CFG_AR_RELEASE_REVISION_VERSION  2U

/* File version information */
#define DIO_CFG_SW_MAJOR_VERSION   1U
#define DIO_CFG_SW_MINOR_VERSION   1U


/*******************************************************************************
**                       Common Published Information                         **
*******************************************************************************/

#define DIO_AR_RELEASE_MAJOR_VERSION_VALUE  4U
#define DIO_AR_RELEASE_MINOR_VERSION_VALUE  2U
#define DIO_AR_RELEASE_REVISION_VERSION_VALUE  2U

#define DIO_SW_MAJOR_VERSION_VALUE  1U
#define DIO_SW_MINOR_VERSION_VALUE  1U
#define DIO_SW_PATCH_VERSION_VALUE  12U

#define DIO_VENDOR_ID_VALUE  59U
#define DIO_MODULE_ID_VALUE  120U

/*******************************************************************************
**                      Global Symbols                                        **
*******************************************************************************/
/* TRACE [SWS_Dio_00026] */
/* TRACE [SWS_BSW_00031] */
/* Instance ID of the DIO Driver */
#define DIO_INSTANCE_ID_VALUE               0U

/* TRACE [R4, ECUC_Dio_00142] */ 
/* Pre-compile option for Development Error Detect */
#define DIO_DEV_ERROR_DETECT                STD_OFF

/* TRACE [R4, ECUC_Dio_00143] */
/* Pre-compile option for Version Info API */
#define DIO_VERSION_INFO_API                STD_ON

/* TRACE [R4, ECUC_Dio_00153] */
/* Pre-compile option for presence of Dio_FlipChannel API */
#define DIO_FLIP_CHANNEL_API                STD_ON

/* Pre-compile option for enable or disable version check of inter-module
   dependencies */
#define DIO_VERSION_CHECK_EXT_MODULES       STD_ON

/* Pre-compile option for critical section protection */
#define DIO_CRITICAL_SECTION_PROTECTION     STD_ON

/* Pre-compile option for Exclusive control functionality */
#define DIO_EXCLUSIVE_CONTROL               STD_OFF

/* Offset for getting IOINTSEL register address. */
#define DIO_IOINTSEL_OFFSET                 (uint32)0x00000000

/* Offset for getting INOUTSEL register address. */
#define DIO_INOUTSEL_OFFSET                 (uint32)0x00000001

/* Offset for getting OUTDT register address. */
#define DIO_OUTDT_OFFSET                    (uint32)0x00000002

/* Offset for getting INDT register address. */
#define DIO_INDT_OFFSET                     (uint32)0x00000003

/* Offset for getting OUTDTSEL register address. */
#define DIO_OUTDTSEL_OFFSET                 (uint32)0x00000010

/* Offset for getting OUTDTH register address. */
#define DIO_OUTDTH_OFFSET                   (uint32)0x00000011

/* Offset for getting OUTDTL register address. */
#define DIO_OUTDTL_OFFSET                   (uint32)0x00000012

/* DEM for Exclusive Control */
#define DIO_E_GET_CONTROL_FAILURE           \
  DemConf_DemEventParameter_DIO_E_GET_CONTROL_FAILURE


/*******************************************************************************
**                        Related Registers                                   **
*******************************************************************************/

/* MFIS Lock Register */
#define DIO_MFISLCKR                       (*((volatile uint32  *)0xE62600C4UL))
#define DIO_MFISLCKR_ADDRESS               ((uint32)0xE62600C4UL)


/*******************************************************************************
**                        Exclusive Control Timeout                           **
*******************************************************************************/

/* Timeout Value for exclusive control */
#define DIO_EXCLUSIVE_CONTROL_TIMEOUT       0x64UL

/*******************************************************************************
**                      Global Data Types                                     **
*******************************************************************************/
/* TRACE [SWS_Dio_00063] */

/* Total number of configured ports */
#define DIO_MAXNOOFPORT                     10U

/* Total number of configured channel groups */
#define DIO_MAXNOOFCHANNELGRP               10U

/* Total number of configured channels */
#define DIO_MAXNOOFCHANNEL                  233U

/* Configuration Set Handles */

/* DIO Port Configuration Handles */
#define DioConf_DioPort_DioPort             (Dio_PortType)0
#define DioConf_DioPort_DioPort_001         (Dio_PortType)1
#define DioConf_DioPort_DioPort_002         (Dio_PortType)2
#define DioConf_DioPort_DioPort_003         (Dio_PortType)3
#define DioConf_DioPort_DioPort_004         (Dio_PortType)4
#define DioConf_DioPort_DioPort_005         (Dio_PortType)5
#define DioConf_DioPort_DioPort_006         (Dio_PortType)6
#define DioConf_DioPort_DioPort_007         (Dio_PortType)7
#define DioConf_DioPort_DioPort_008         (Dio_PortType)8
#define DioConf_DioPort_DioPort_009         (Dio_PortType)9

/* The Pointer to Port Group name */
#define Dio_GpPortGroup                     Dio_GstPortGroup

/* DIO Channel Configuration Handles */
#define DioConf_DioChannel_DioChannel       (Dio_ChannelType)0
#define DioConf_DioChannel_DioChannel_001   (Dio_ChannelType)1
#define DioConf_DioChannel_DioChannel_002   (Dio_ChannelType)2
#define DioConf_DioChannel_DioChannel_003   (Dio_ChannelType)3
#define DioConf_DioChannel_DioChannel_004   (Dio_ChannelType)4
#define DioConf_DioChannel_DioChannel_005   (Dio_ChannelType)5
#define DioConf_DioChannel_DioChannel_006   (Dio_ChannelType)6
#define DioConf_DioChannel_DioChannel_007   (Dio_ChannelType)7
#define DioConf_DioChannel_DioChannel_008   (Dio_ChannelType)8
#define DioConf_DioChannel_DioChannel_009   (Dio_ChannelType)9
#define DioConf_DioChannel_DioChannel_010   (Dio_ChannelType)10
#define DioConf_DioChannel_DioChannel_011   (Dio_ChannelType)11
#define DioConf_DioChannel_DioChannel_012   (Dio_ChannelType)12
#define DioConf_DioChannel_DioChannel_013   (Dio_ChannelType)13
#define DioConf_DioChannel_DioChannel_014   (Dio_ChannelType)14
#define DioConf_DioChannel_DioChannel_015   (Dio_ChannelType)15
#define DioConf_DioChannel_DioChannel_016   (Dio_ChannelType)16
#define DioConf_DioChannel_DioChannel_017   (Dio_ChannelType)17
#define DioConf_DioChannel_DioChannel_018   (Dio_ChannelType)18
#define DioConf_DioChannel_DioChannel_019   (Dio_ChannelType)19
#define DioConf_DioChannel_DioChannel_020   (Dio_ChannelType)20
#define DioConf_DioChannel_DioChannel_021   (Dio_ChannelType)21
#define DioConf_DioChannel_DioChannel_022   (Dio_ChannelType)22
#define DioConf_DioChannel_DioChannel_023   (Dio_ChannelType)23
#define DioConf_DioChannel_DioChannel_024   (Dio_ChannelType)24
#define DioConf_DioChannel_DioChannel_025   (Dio_ChannelType)25
#define DioConf_DioChannel_DioChannel_026   (Dio_ChannelType)26
#define DioConf_DioChannel_DioChannel_027   (Dio_ChannelType)27
#define DioConf_DioChannel_DioChannel_028   (Dio_ChannelType)28
#define DioConf_DioChannel_DioChannel_029   (Dio_ChannelType)29
#define DioConf_DioChannel_DioChannel_030   (Dio_ChannelType)30
#define DioConf_DioChannel_DioChannel_031   (Dio_ChannelType)31
#define DioConf_DioChannel_DioChannel_032   (Dio_ChannelType)32
#define DioConf_DioChannel_DioChannel_033   (Dio_ChannelType)33
#define DioConf_DioChannel_DioChannel_034   (Dio_ChannelType)34
#define DioConf_DioChannel_DioChannel_035   (Dio_ChannelType)35
#define DioConf_DioChannel_DioChannel_036   (Dio_ChannelType)36
#define DioConf_DioChannel_DioChannel_037   (Dio_ChannelType)37
#define DioConf_DioChannel_DioChannel_038   (Dio_ChannelType)38
#define DioConf_DioChannel_DioChannel_039   (Dio_ChannelType)39
#define DioConf_DioChannel_DioChannel_040   (Dio_ChannelType)40
#define DioConf_DioChannel_DioChannel_041   (Dio_ChannelType)41
#define DioConf_DioChannel_DioChannel_042   (Dio_ChannelType)42
#define DioConf_DioChannel_DioChannel_043   (Dio_ChannelType)43
#define DioConf_DioChannel_DioChannel_044   (Dio_ChannelType)44
#define DioConf_DioChannel_DioChannel_045   (Dio_ChannelType)45
#define DioConf_DioChannel_DioChannel_046   (Dio_ChannelType)46
#define DioConf_DioChannel_DioChannel_047   (Dio_ChannelType)47
#define DioConf_DioChannel_DioChannel_048   (Dio_ChannelType)48
#define DioConf_DioChannel_DioChannel_049   (Dio_ChannelType)49
#define DioConf_DioChannel_DioChannel_050   (Dio_ChannelType)50
#define DioConf_DioChannel_DioChannel_051   (Dio_ChannelType)51
#define DioConf_DioChannel_DioChannel_052   (Dio_ChannelType)52
#define DioConf_DioChannel_DioChannel_053   (Dio_ChannelType)53
#define DioConf_DioChannel_DioChannel_054   (Dio_ChannelType)54
#define DioConf_DioChannel_DioChannel_055   (Dio_ChannelType)55
#define DioConf_DioChannel_DioChannel_056   (Dio_ChannelType)56
#define DioConf_DioChannel_DioChannel_057   (Dio_ChannelType)57
#define DioConf_DioChannel_DioChannel_058   (Dio_ChannelType)58
#define DioConf_DioChannel_DioChannel_059   (Dio_ChannelType)59
#define DioConf_DioChannel_DioChannel_060   (Dio_ChannelType)60
#define DioConf_DioChannel_DioChannel_061   (Dio_ChannelType)61
#define DioConf_DioChannel_DioChannel_062   (Dio_ChannelType)62
#define DioConf_DioChannel_DioChannel_063   (Dio_ChannelType)63
#define DioConf_DioChannel_DioChannel_064   (Dio_ChannelType)64
#define DioConf_DioChannel_DioChannel_065   (Dio_ChannelType)65
#define DioConf_DioChannel_DioChannel_066   (Dio_ChannelType)66
#define DioConf_DioChannel_DioChannel_067   (Dio_ChannelType)67
#define DioConf_DioChannel_DioChannel_068   (Dio_ChannelType)68
#define DioConf_DioChannel_DioChannel_069   (Dio_ChannelType)69
#define DioConf_DioChannel_DioChannel_070   (Dio_ChannelType)70
#define DioConf_DioChannel_DioChannel_071   (Dio_ChannelType)71
#define DioConf_DioChannel_DioChannel_072   (Dio_ChannelType)72
#define DioConf_DioChannel_DioChannel_073   (Dio_ChannelType)73
#define DioConf_DioChannel_DioChannel_074   (Dio_ChannelType)74
#define DioConf_DioChannel_DioChannel_075   (Dio_ChannelType)75
#define DioConf_DioChannel_DioChannel_076   (Dio_ChannelType)76
#define DioConf_DioChannel_DioChannel_077   (Dio_ChannelType)77
#define DioConf_DioChannel_DioChannel_078   (Dio_ChannelType)78
#define DioConf_DioChannel_DioChannel_079   (Dio_ChannelType)79
#define DioConf_DioChannel_DioChannel_080   (Dio_ChannelType)80
#define DioConf_DioChannel_DioChannel_081   (Dio_ChannelType)81
#define DioConf_DioChannel_DioChannel_082   (Dio_ChannelType)82
#define DioConf_DioChannel_DioChannel_083   (Dio_ChannelType)83
#define DioConf_DioChannel_DioChannel_084   (Dio_ChannelType)84
#define DioConf_DioChannel_DioChannel_085   (Dio_ChannelType)85
#define DioConf_DioChannel_DioChannel_086   (Dio_ChannelType)86
#define DioConf_DioChannel_DioChannel_087   (Dio_ChannelType)87
#define DioConf_DioChannel_DioChannel_088   (Dio_ChannelType)88
#define DioConf_DioChannel_DioChannel_089   (Dio_ChannelType)89
#define DioConf_DioChannel_DioChannel_090   (Dio_ChannelType)90
#define DioConf_DioChannel_DioChannel_091   (Dio_ChannelType)91
#define DioConf_DioChannel_DioChannel_092   (Dio_ChannelType)92
#define DioConf_DioChannel_DioChannel_093   (Dio_ChannelType)93
#define DioConf_DioChannel_DioChannel_094   (Dio_ChannelType)94
#define DioConf_DioChannel_DioChannel_095   (Dio_ChannelType)95
#define DioConf_DioChannel_DioChannel_096   (Dio_ChannelType)96
#define DioConf_DioChannel_DioChannel_097   (Dio_ChannelType)97
#define DioConf_DioChannel_DioChannel_098   (Dio_ChannelType)98
#define DioConf_DioChannel_DioChannel_099   (Dio_ChannelType)99
#define DioConf_DioChannel_DioChannel_100   (Dio_ChannelType)100
#define DioConf_DioChannel_DioChannel_101   (Dio_ChannelType)101
#define DioConf_DioChannel_DioChannel_102   (Dio_ChannelType)102
#define DioConf_DioChannel_DioChannel_103   (Dio_ChannelType)103
#define DioConf_DioChannel_DioChannel_104   (Dio_ChannelType)104
#define DioConf_DioChannel_DioChannel_105   (Dio_ChannelType)105
#define DioConf_DioChannel_DioChannel_106   (Dio_ChannelType)106
#define DioConf_DioChannel_DioChannel_107   (Dio_ChannelType)107
#define DioConf_DioChannel_DioChannel_108   (Dio_ChannelType)108
#define DioConf_DioChannel_DioChannel_109   (Dio_ChannelType)109
#define DioConf_DioChannel_DioChannel_110   (Dio_ChannelType)110
#define DioConf_DioChannel_DioChannel_111   (Dio_ChannelType)111
#define DioConf_DioChannel_DioChannel_112   (Dio_ChannelType)112
#define DioConf_DioChannel_DioChannel_113   (Dio_ChannelType)113
#define DioConf_DioChannel_DioChannel_114   (Dio_ChannelType)114
#define DioConf_DioChannel_DioChannel_115   (Dio_ChannelType)115
#define DioConf_DioChannel_DioChannel_116   (Dio_ChannelType)116
#define DioConf_DioChannel_DioChannel_117   (Dio_ChannelType)117
#define DioConf_DioChannel_DioChannel_118   (Dio_ChannelType)118
#define DioConf_DioChannel_DioChannel_119   (Dio_ChannelType)119
#define DioConf_DioChannel_DioChannel_120   (Dio_ChannelType)120
#define DioConf_DioChannel_DioChannel_121   (Dio_ChannelType)121
#define DioConf_DioChannel_DioChannel_122   (Dio_ChannelType)122
#define DioConf_DioChannel_DioChannel_123   (Dio_ChannelType)123
#define DioConf_DioChannel_DioChannel_124   (Dio_ChannelType)124
#define DioConf_DioChannel_DioChannel_125   (Dio_ChannelType)125
#define DioConf_DioChannel_DioChannel_126   (Dio_ChannelType)126
#define DioConf_DioChannel_DioChannel_127   (Dio_ChannelType)127
#define DioConf_DioChannel_DioChannel_128   (Dio_ChannelType)128
#define DioConf_DioChannel_DioChannel_129   (Dio_ChannelType)129
#define DioConf_DioChannel_DioChannel_130   (Dio_ChannelType)130
#define DioConf_DioChannel_DioChannel_131   (Dio_ChannelType)131
#define DioConf_DioChannel_DioChannel_132   (Dio_ChannelType)132
#define DioConf_DioChannel_DioChannel_133   (Dio_ChannelType)133
#define DioConf_DioChannel_DioChannel_134   (Dio_ChannelType)134
#define DioConf_DioChannel_DioChannel_135   (Dio_ChannelType)135
#define DioConf_DioChannel_DioChannel_136   (Dio_ChannelType)136
#define DioConf_DioChannel_DioChannel_137   (Dio_ChannelType)137
#define DioConf_DioChannel_DioChannel_138   (Dio_ChannelType)138
#define DioConf_DioChannel_DioChannel_139   (Dio_ChannelType)139
#define DioConf_DioChannel_DioChannel_140   (Dio_ChannelType)140
#define DioConf_DioChannel_DioChannel_141   (Dio_ChannelType)141
#define DioConf_DioChannel_DioChannel_142   (Dio_ChannelType)142
#define DioConf_DioChannel_DioChannel_143   (Dio_ChannelType)143
#define DioConf_DioChannel_DioChannel_144   (Dio_ChannelType)144
#define DioConf_DioChannel_DioChannel_145   (Dio_ChannelType)145
#define DioConf_DioChannel_DioChannel_146   (Dio_ChannelType)146
#define DioConf_DioChannel_DioChannel_147   (Dio_ChannelType)147
#define DioConf_DioChannel_DioChannel_148   (Dio_ChannelType)148
#define DioConf_DioChannel_DioChannel_149   (Dio_ChannelType)149
#define DioConf_DioChannel_DioChannel_150   (Dio_ChannelType)150
#define DioConf_DioChannel_DioChannel_151   (Dio_ChannelType)151
#define DioConf_DioChannel_DioChannel_152   (Dio_ChannelType)152
#define DioConf_DioChannel_DioChannel_153   (Dio_ChannelType)153
#define DioConf_DioChannel_DioChannel_154   (Dio_ChannelType)154
#define DioConf_DioChannel_DioChannel_155   (Dio_ChannelType)155
#define DioConf_DioChannel_DioChannel_156   (Dio_ChannelType)156
#define DioConf_DioChannel_DioChannel_157   (Dio_ChannelType)157
#define DioConf_DioChannel_DioChannel_158   (Dio_ChannelType)158
#define DioConf_DioChannel_DioChannel_159   (Dio_ChannelType)159
#define DioConf_DioChannel_DioChannel_160   (Dio_ChannelType)160
#define DioConf_DioChannel_DioChannel_161   (Dio_ChannelType)161
#define DioConf_DioChannel_DioChannel_162   (Dio_ChannelType)162
#define DioConf_DioChannel_DioChannel_163   (Dio_ChannelType)163
#define DioConf_DioChannel_DioChannel_164   (Dio_ChannelType)164
#define DioConf_DioChannel_DioChannel_165   (Dio_ChannelType)165
#define DioConf_DioChannel_DioChannel_166   (Dio_ChannelType)166
#define DioConf_DioChannel_DioChannel_167   (Dio_ChannelType)167
#define DioConf_DioChannel_DioChannel_168   (Dio_ChannelType)168
#define DioConf_DioChannel_DioChannel_169   (Dio_ChannelType)169
#define DioConf_DioChannel_DioChannel_170   (Dio_ChannelType)170
#define DioConf_DioChannel_DioChannel_171   (Dio_ChannelType)171
#define DioConf_DioChannel_DioChannel_172   (Dio_ChannelType)172
#define DioConf_DioChannel_DioChannel_173   (Dio_ChannelType)173
#define DioConf_DioChannel_DioChannel_174   (Dio_ChannelType)174
#define DioConf_DioChannel_DioChannel_175   (Dio_ChannelType)175
#define DioConf_DioChannel_DioChannel_176   (Dio_ChannelType)176
#define DioConf_DioChannel_DioChannel_177   (Dio_ChannelType)177
#define DioConf_DioChannel_DioChannel_178   (Dio_ChannelType)178
#define DioConf_DioChannel_DioChannel_179   (Dio_ChannelType)179
#define DioConf_DioChannel_DioChannel_180   (Dio_ChannelType)180
#define DioConf_DioChannel_DioChannel_181   (Dio_ChannelType)181
#define DioConf_DioChannel_DioChannel_182   (Dio_ChannelType)182
#define DioConf_DioChannel_DioChannel_183   (Dio_ChannelType)183
#define DioConf_DioChannel_DioChannel_184   (Dio_ChannelType)184
#define DioConf_DioChannel_DioChannel_185   (Dio_ChannelType)185
#define DioConf_DioChannel_DioChannel_186   (Dio_ChannelType)186
#define DioConf_DioChannel_DioChannel_187   (Dio_ChannelType)187
#define DioConf_DioChannel_DioChannel_188   (Dio_ChannelType)188
#define DioConf_DioChannel_DioChannel_189   (Dio_ChannelType)189
#define DioConf_DioChannel_DioChannel_190   (Dio_ChannelType)190
#define DioConf_DioChannel_DioChannel_191   (Dio_ChannelType)191
#define DioConf_DioChannel_DioChannel_192   (Dio_ChannelType)192
#define DioConf_DioChannel_DioChannel_193   (Dio_ChannelType)193
#define DioConf_DioChannel_DioChannel_194   (Dio_ChannelType)194
#define DioConf_DioChannel_DioChannel_195   (Dio_ChannelType)195
#define DioConf_DioChannel_DioChannel_196   (Dio_ChannelType)196
#define DioConf_DioChannel_DioChannel_197   (Dio_ChannelType)197
#define DioConf_DioChannel_DioChannel_198   (Dio_ChannelType)198
#define DioConf_DioChannel_DioChannel_199   (Dio_ChannelType)199
#define DioConf_DioChannel_DioChannel_200   (Dio_ChannelType)200
#define DioConf_DioChannel_DioChannel_201   (Dio_ChannelType)201
#define DioConf_DioChannel_DioChannel_202   (Dio_ChannelType)202
#define DioConf_DioChannel_DioChannel_203   (Dio_ChannelType)203
#define DioConf_DioChannel_DioChannel_204   (Dio_ChannelType)204
#define DioConf_DioChannel_DioChannel_205   (Dio_ChannelType)205
#define DioConf_DioChannel_DioChannel_206   (Dio_ChannelType)206
#define DioConf_DioChannel_DioChannel_207   (Dio_ChannelType)207
#define DioConf_DioChannel_DioChannel_208   (Dio_ChannelType)208
#define DioConf_DioChannel_DioChannel_209   (Dio_ChannelType)209
#define DioConf_DioChannel_DioChannel_210   (Dio_ChannelType)210
#define DioConf_DioChannel_DioChannel_211   (Dio_ChannelType)211
#define DioConf_DioChannel_DioChannel_212   (Dio_ChannelType)212
#define DioConf_DioChannel_DioChannel_213   (Dio_ChannelType)213
#define DioConf_DioChannel_DioChannel_214   (Dio_ChannelType)214
#define DioConf_DioChannel_DioChannel_215   (Dio_ChannelType)215
#define DioConf_DioChannel_DioChannel_216   (Dio_ChannelType)216
#define DioConf_DioChannel_DioChannel_217   (Dio_ChannelType)217
#define DioConf_DioChannel_DioChannel_218   (Dio_ChannelType)218
#define DioConf_DioChannel_DioChannel_219   (Dio_ChannelType)219
#define DioConf_DioChannel_DioChannel_220   (Dio_ChannelType)220
#define DioConf_DioChannel_DioChannel_221   (Dio_ChannelType)221
#define DioConf_DioChannel_DioChannel_222   (Dio_ChannelType)222
#define DioConf_DioChannel_DioChannel_223   (Dio_ChannelType)223
#define DioConf_DioChannel_DioChannel_224   (Dio_ChannelType)224
#define DioConf_DioChannel_DioChannel_225   (Dio_ChannelType)225
#define DioConf_DioChannel_DioChannel_226   (Dio_ChannelType)226
#define DioConf_DioChannel_DioChannel_227   (Dio_ChannelType)227
#define DioConf_DioChannel_DioChannel_228   (Dio_ChannelType)228
#define DioConf_DioChannel_DioChannel_229   (Dio_ChannelType)229
#define DioConf_DioChannel_DioChannel_230   (Dio_ChannelType)230
#define DioConf_DioChannel_DioChannel_231   (Dio_ChannelType)231
#define DioConf_DioChannel_DioChannel_232   (Dio_ChannelType)232

/* TRACE [RRS_GET_FR_0012] */
/* The Pointer to Port Channel name */
#define Dio_GpPortChannel                   Dio_GstPortChannel

/* DIO Channel Group Configuration Handles */
#define DioConf_DioChannelGroup_DioChannelGroup        \
  (&Dio_GstChannelGroupData[0])
#define DioConf_DioChannelGroup_DioChannelGroup_001    \
  (&Dio_GstChannelGroupData[1])
#define DioConf_DioChannelGroup_DioChannelGroup_002    \
  (&Dio_GstChannelGroupData[2])
#define DioConf_DioChannelGroup_DioChannelGroup_003    \
  (&Dio_GstChannelGroupData[3])
#define DioConf_DioChannelGroup_DioChannelGroup_004    \
  (&Dio_GstChannelGroupData[4])
#define DioConf_DioChannelGroup_DioChannelGroup_005    \
  (&Dio_GstChannelGroupData[5])
#define DioConf_DioChannelGroup_DioChannelGroup_006    \
  (&Dio_GstChannelGroupData[6])
#define DioConf_DioChannelGroup_DioChannelGroup_007    \
  (&Dio_GstChannelGroupData[7])
#define DioConf_DioChannelGroup_DioChannelGroup_008    \
  (&Dio_GstChannelGroupData[8])
#define DioConf_DioChannelGroup_DioChannelGroup_009    \
  (&Dio_GstChannelGroupData[9])


/*******************************************************************************
**                      Function Prototypes                                   **
*******************************************************************************/

#endif /* DIO_CFG_H  */

/*******************************************************************************
**                      End of File                                           **
*******************************************************************************/
