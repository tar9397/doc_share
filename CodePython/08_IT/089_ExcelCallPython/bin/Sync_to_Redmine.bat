@echo off

SET PATH=C:\Users\tra.nguyen-thanh\Anaconda3\Library\mingw-w64\bin;%PATH%
SET PATH=C:\Users\tra.nguyen-thanh\Anaconda3\Library\bin;%PATH%

echo %~dp0
cd /d %~dp0

>.\..\log\log.txt (
@echo %date%:%time%
C:\Users\tra.nguyen-thanh\Anaconda3\python PeriodAutoRedmine.py %1
)

del ".\..\locked.txt"