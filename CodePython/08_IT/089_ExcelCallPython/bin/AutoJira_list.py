#!/usr/bin/python

# Automaton logtime to Jira by period

import sys, glob
from jira import JIRA
from datetime import datetime
from openpyxl import load_workbook

# 'Jira_TimeLog.xlsx'
wb = load_workbook(filename = glob.glob("./../*.xlsm")[0], data_only=True)
# 'Jira_04'
ex_sheet = wb[sys.argv[1]]

# info
server_jira=ex_sheet.cell(row=1, column=1).value
username=ex_sheet.cell(row=1, column=2).value
password=ex_sheet.cell(row=1, column=3).value

jira = JIRA(server=server_jira, auth=(username, password))

for tic in jira.search_issues("assignee = currentUser()  order by status ASC"):
     print("[Jira];{};{};{}".format(tic.key, tic.fields.summary, tic.fields.status))