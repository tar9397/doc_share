#!/usr/bin/python

import sys, glob
from redminelib import Redmine
from datetime import datetime, date
from openpyxl import load_workbook

# 'Jira_TimeLog.xlsx'
wb = load_workbook(filename = glob.glob("./../*.xlsm")[0], data_only=True)
# 'Jira_04'
ex_sheet = wb[sys.argv[1]]

# info
server_redmine=ex_sheet.cell(row=2, column=1).value
s_username=ex_sheet.cell(row=2, column=2).value
s_password=ex_sheet.cell(row=2, column=3).value
issue_id=ex_sheet.cell(row=3, column=8).value

# print (server_redmine)
# print(s_username)
# print(s_password)
# issue_id = 245952
try:
    redmine = Redmine(server_redmine, username=s_username, password=s_password, requests={'verify': False})
    user_redmine = redmine.auth()

    def get_worklog(s_user_id):
        global redmine
        existed_worklog=[]
        for en_time in redmine.time_entry.filter(user_id = s_user_id):
            # print(list(en_time))
            existed_worklog.append(en_time.spent_on)
        # print(existed_worklog)
        return existed_worklog

    # print out number of timelog
    x=0
    aaworklog = get_worklog(user_redmine.id)

    for i_row in range(6 ,ex_sheet.max_row + 1):
        #Check if need add worklog
        if ex_sheet.cell(row=i_row, column=3).value != '':

            # spend_time=8
            started_date=ex_sheet.cell(row=i_row, column=1).value
            spend_time=ex_sheet.cell(row=i_row, column=3).value
            spend_time_arr = spend_time.split(" ")
            time_sum=0
            for e in spend_time_arr:
                if e[-1] == 'd':
                    spend_time = int(e[0 : -1])*8
                elif e[-1] == 'h':
                    spend_time = int(e[0 : -1])
                time_sum = time_sum + spend_time

            # if spend_time[-1] == 'd':
                # spend_time = int(spend_time[0 : -1])*8
            # else
                # spend_time = int(spend_time[0 : -1])
            user_comment=ex_sheet.cell(row=i_row, column=4).value
            started_date_temp=date(started_date.year, started_date.month, started_date.day)

            if started_date_temp not in aaworklog:
                # print(started_date_temp)
                print("Logtime Redmine on {0} for ticket {1}: {2}.".format(started_date_temp, issue_id, str(time_sum)+"h"))
                # Add logtime to Redmine
                # ('activity', {'id': 44, 'name': 'Preparing test'})
                # ('activity', {'id': 12, 'name': 'Spec creation'})
                # ('activity', {'id': 36, 'name': 'Spec review'})
                redmine.time_entry.create(issue_id=issue_id, spent_on=started_date_temp, hours=time_sum, activity_id=44, comments=user_comment)
                x=x+1
    print("Done Log work on Redmine. Num_of_ticket: "+str(x))
    # break
except:
    print("Can not access Redmine. Please check your account and connection.")
