#!/usr/bin/python

# Automaton logtime to Jira by period

import sys, glob
# from jira import JIRA
from datetime import datetime, timedelta
from openpyxl import load_workbook

# 'Jira_TimeLog.xlsx'
wb = load_workbook(filename = glob.glob("./../*.xlsm")[0], data_only=True)
# 'Jira_04'
ex_sheet = wb[sys.argv[1]]

# info
input = ex_sheet.cell(row=1, column=1).value
# server_jira=ex_sheet.cell(row=1, column=1).value
# username=ex_sheet.cell(row=1, column=2).value
# password=ex_sheet.cell(row=1, column=3).value

# jira = JIRA(server=server_jira, auth=(username, password))

# # 'author', 'comment', 'created', 'delete', 'find', 'id', 'issueId', 'raw', 'self',
# # 'started', 'timeSpent', 'timeSpentSeconds', 'update', 'updateAuthor', 'updated'
# def get_worklog():
#      global jira
#      existed_worklog=[]
#      for _ticket_num in jira.search_issues("worklogAuthor = currentUser()"): 
#           for wlog in jira.worklogs(_ticket_num):
#                date_temp=datetime.strptime(wlog.started, '%Y-%m-%dT%H:%M:%S.%f%z')
#                existed_worklog.append(datetime(date_temp.year, date_temp.month, date_temp.day))
#                # print("{}:{}:Created({}):Started({})".format(_ticket_num, wlog.author, wlog.created, wlog.started))
#                # wlog.delete()
#      return existed_worklog

# aaworklog = get_worklog()
# # print(aaworklog)

# x=0
# for i_row in range(4 ,ex_sheet.max_row + 1):
#      #Check if need add worklog
#      if ex_sheet.cell(row=i_row, column=4).value == 1:
#           # print("in row {}: {}".format(i_row, ex_sheet.cell(row=i_row, column=2).value))
#           started_date=ex_sheet.cell(row=i_row, column=2).value
#           user_comment=ex_sheet.cell(row=i_row, column=3).value
#           spend_time="8"
#           ticket_number=ex_sheet.cell(row=i_row, column=5).value
#           started_date_temp=datetime(started_date.year, started_date.month, started_date.day)

#           if started_date_temp not in aaworklog:
#             started_date=started_date + timedelta(hours=10.5)
#             print("Logtime Jira on {0} for ticket {1}: {2}.".format(str(started_date+ timedelta(hours=7)),ticket_number, str(spend_time)+"h"))
#             #Add worklog to Jira
#             jira.add_worklog(ticket_number, timeSpent=str(spend_time)+"h", started=started_date, comment=user_comment)
#             x=x+1
# print("Done Log work on Jira. Num_of_ticket: "+str(x))
# for wlog in jira.worklogs(ticket_number):
#      print(wlog.started + ": " + wlog.timeSpent)
# print(jira.issue(ticket_number).fields.timetracking.timeSpent)

print("input: ",input)
output = input + "1234"
print("output: ",output)

print("[data];{};{}".format("output", output))
