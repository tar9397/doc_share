#!/usr/bin/python

# Automaton logtime to Jira by period

import sys, glob
from jira import JIRA
from datetime import datetime, timedelta
from openpyxl import load_workbook

# 'Jira_TimeLog.xlsx'
wb = load_workbook(filename = glob.glob("./../*.xlsm")[0], data_only=True)
# 'Jira_04'
ex_sheet = wb[sys.argv[1]]

# info
server_jira=ex_sheet.cell(row=1, column=1).value
username=ex_sheet.cell(row=1, column=2).value
password=ex_sheet.cell(row=1, column=3).value

jira = JIRA(server=server_jira, auth=(username, password))

# 'author', 'comment', 'created', 'delete', 'find', 'id', 'issueId', 'raw', 'self',
# 'started', 'timeSpent', 'timeSpentSeconds', 'update', 'updateAuthor', 'updated'

for yy in range(2, ex_sheet.max_column+1, 2):
    user_jira = ex_sheet.cell(row=4, column=yy).value
    for _ticket_num in jira.search_issues("worklogAuthor = {}".format(user_jira)): 
        for wlog_jira in jira.worklogs(_ticket_num):
            if wlog_jira.author.name == user_jira:
                date_temp=datetime.strptime(wlog_jira.started, '%Y-%m-%dT%H:%M:%S.%f%z')

                #Example: lam.tran;20/06/2020;2006;20
                print("{};{}/{}/{};{};{};{}".format(wlog_jira.author.name,date_temp.day, date_temp.month, date_temp.year, _ticket_num, wlog_jira.timeSpent, wlog_jira.comment.replace('\r',' ').replace('\n',' ')))

