#!/usr/bin/python

# Automaton logtime to Jira

import sys, glob
from jira import JIRA
from datetime import datetime, timedelta
from openpyxl import load_workbook

wb = load_workbook(filename = glob.glob("./../*.xlsm")[0], data_only=True)
ex_sheet = wb[sys.argv[1]]

# info
server_jira=ex_sheet.cell(row=1, column=1).value
username=ex_sheet.cell(row=1, column=2).value
password=ex_sheet.cell(row=1, column=3).value

jira = JIRA(server=server_jira, auth=(username, password))


def get_worklog():
     global jira
     existed_worklog=[]
     for _ticket_num in jira.search_issues("worklogAuthor = currentUser()"): 
          for wlog in jira.worklogs(_ticket_num):
               date_temp=datetime.strptime(wlog.started, '%Y-%m-%dT%H:%M:%S.%f%z')
               existed_worklog.append(datetime(date_temp.year, date_temp.month, date_temp.day))
               # print("{}:{}:Created({}):Started({})".format(_ticket_num, wlog.author, wlog.created, wlog.started))
     return existed_worklog

#print(existed_worklog)
aaworklog = get_worklog()

started_date=ex_sheet.cell(row=4, column=2).value
user_comment=ex_sheet.cell(row=4, column=3).value
spend_time=ex_sheet.cell(row=4, column=4).value
ticket_number=ex_sheet.cell(row=4, column=5).value
started_date_temp=datetime(started_date.year, started_date.month, started_date.day)

if started_date_temp not in aaworklog:
    started_date=started_date + timedelta(hours=10.5)
    print("Log {0} on Jira {1}, date {2}.".format(str(spend_time)+"h", ticket_number, str(started_date.year)+'-'+str(started_date.month)+'-'+str(started_date.day)))
    #Add worklog to Jira '%Y-%m-%dT%H:%M:%S.%f%z'
    jira.add_worklog(ticket_number, timeSpent=str(spend_time)+"h", started=started_date, comment=user_comment)
    
else:
    print("Timelog Jira on {0} was existed!".format(str(started_date.year)+'-'+str(started_date.month)+'-'+str(started_date.day)))
    for _ticket_num in jira.search_issues("worklogAuthor = currentUser()"): 
        for wlog_jira in jira.worklogs(_ticket_num):
            date_temp=datetime.strptime(wlog_jira.started, '%Y-%m-%dT%H:%M:%S.%f%z')
            if datetime(date_temp.year, date_temp.month, date_temp.day) == started_date_temp and user_comment != wlog_jira.comment:
                print("Jira {}: {} Change comment ({}) to ({})".format(_ticket_num, wlog_jira.started, wlog_jira.comment, user_comment))
                wlog_jira.update(comment=user_comment)

# update time and comment
# else:
# print("Timelog Jira on {0} was existed!".format(str(started_date.year)+'-'+str(started_date.month)+'-'+str(started_date.day)))

# for wlog in jira.worklogs(ticket_number):
#      print(wlog.started + ": " + wlog.timeSpent)
# print(jira.issue(ticket_number).fields.timetracking.timeSpent)
