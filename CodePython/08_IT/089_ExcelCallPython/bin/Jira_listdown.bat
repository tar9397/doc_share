@echo off
echo %~dp0
cd /d %~dp0

IF EXIST .\..\log\Issues_list.txt  (del .\..\log\Issues_list.txt)

>.\..\log\Issues_list.txt (
    python AutoJira_list.py %1
)
del ".\..\locked.txt"