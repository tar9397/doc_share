@echo off
echo %~dp0
cd /d %~dp0

>.\..\log\log.txt (
@echo %date%:%time%
python PeriodAutoJira.py %1
)

del ".\..\locked.txt"