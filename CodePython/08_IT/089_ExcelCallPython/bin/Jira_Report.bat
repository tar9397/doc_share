@echo off

SET PATH=C:\Users\tra.nguyen-thanh\Anaconda3\Library\mingw-w64\bin;%PATH%
SET PATH=C:\Users\tra.nguyen-thanh\Anaconda3\Library\bin;%PATH%

echo %~dp0
cd /d %~dp0

IF EXIST .\..\log\report.txt  (del .\..\log\report.txt)

>.\..\log\report.txt (
    C:\Users\tra.nguyen-thanh\Anaconda3\python PeriodJiraReport.py %1
)
del ".\..\locked.txt"