#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                             GetTITCheckPoint                                 #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
import openpyxl
import argparse


DEBUG = False
# CASE_BY_CASE = False
# SelectTc = ""
# Specific_Tc_List = []
# Except_Tc_List = []
TIT_Checkpoint = {}
file_name = ''
sheet_name = ''
output_location = ''
support_device = ['U2Ax', 'U2x', 'E2x']

start_hearder = 'Object' 
header_template_rows = [[start_hearder], ['Parameter', 'Description'], ['Input'], ['Checkpoint','Check point']]


def entry():
    """Start application"""
    example = '''
    [Description]
        Read info in TIT append task
    [Start application]
    E.g: ./GetTITCheckPoint.py file_name sheet_name
    '''
    parser = argparse.ArgumentParser(description=example)
    # parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
    #                     help='valid target module: Adc, Can, Dio ...')
    parser.add_argument('file_name', type=str, help='file with extension', default=None )
    parser.add_argument('sheet_name', type=str, nargs='?', help='sheet of file', default='Sheet1' )
    parser.add_argument('--opl', type=str, nargs='?',
                        help='(optional) specify desired output file name and location (default=./)', 
                        default='./', required=False)
    # parser.add_argument('--select_tc', type=str, nargs='?',
    #                     help='(optional) all/cbc/<Specific TC(s) name> choose the way TC will be updated (default=all)', 
    #                     default='all', required=False)
    # parser.add_argument('-d','--debug', action='store_true',
    #                     help='(optional) true/false Enable debug data (default=false)', required=False)
    # # parser.add_argument('-c','--collect', action='store_true',
    # #                     help='(optional) true/false Collect data only (default=false)', required=False)
    # parser.add_argument('--filter_req_id', type=str, nargs='?',
    #                     help='(optional) list of partterns to filter id when write to file (default=None)', default=None, required=False)
    # parser.add_argument('--all_except', type=str, nargs='?',
    #                     help='(optional) Run for all test case except (default=None)', default='None', required=False)
    args = parser.parse_args()

    global file_name
    global sheet_name
    global output_location

    file_name = str(args.file_name) 
    sheet_name = str(args.sheet_name) 

    output_location = str(args.opl) + "output.xlsx"

    return True

# def get_unique_dict(input_list):
#     """Get dict contains unique element from input dict"""
#     output_list = {}
#     for element in sorted(input_list.keys()):
#         if not element in output_list.keys():
#             output_list.update({element: input_list[element]})
#     return output_list

def Get_TIT_checkpoint():
    check_point = {}
    # check_point = []
    wbook = openpyxl.load_workbook(file_name, data_only=True)
    wsheet = wbook.get_sheet_by_name(sheet_name)


    # check_point = [[row_idx, str(wsheet.cell(row_idx, 23).value), str(wsheet.cell(row_idx, 18).value)] \ 
    #                 for row_idx in range(2, wsheet.max_row+1) for col_idx in range(1, wsheet.max_col+1) \
    #                 if wsheet.cell(row_idx, 16).value and wsheet.cell(row_idx, 16).value != "" and \
    #                 wsheet.cell(row_idx, 16).value != "-" and \
    # #                 wsheet.cell(row_idx, 1).value != "" and wsheet.cell(row_idx, 1).value ]
    # data = [[wsheet.cell(row_idx)[col_idx] for col_idx in range(1, wsheet.max_col+1)] for row_idx in range(2, wsheet.max_row+1) \
    #             if wsheet.cell(row_idx, col_idx).value and wsheet.cell(row_idx, 16).value != "" and \ ]

    start_row = start_col = None

    for row_idx in range(1, wsheet.max_row+1):
        for col_idx in range(1, wsheet.max_column+1):
            if wsheet.cell(row_idx, col_idx).value and wsheet.cell(row_idx, 16).value != "" and start_hearder in str(wsheet.cell(row_idx, col_idx).value):
                start_row = row_idx
                start_col = col_idx
                break


    if start_row and start_col:
        # check header is valid:
        header_row_data = [str(wsheet.cell(start_row, col_idx).value) for col_idx in range(start_col, wsheet.max_column+1)]
        for idx, header_temp_row in enumerate(header_template_rows):
            valid_header = None
            for cmp_header in header_temp_row:
                for header_idx, header_data in enumerate(header_row_data):
                    if cmp_header in header_data:
                        valid_header = cmp_header
                        # check_point.update({valid_header: [wsheet.cell(row_idx, start_col + header_idx).value for row_idx in range(start_row + 1, wsheet.max_row+1)]})
                        check_point.update({header_temp_row[0]: [wsheet.cell(row_idx, start_col + header_idx).value  \
                                                                if wsheet.cell(row_idx, start_col + header_idx).value and wsheet.cell(row_idx, start_col + header_idx).value != '-' else '' \
                                                                for row_idx in range(start_row + 1, wsheet.max_row+1) ]})
                        # check_point.append([valid_header, [wsheet.cell(row_idx, start_col + header_idx).value for row_idx in range(start_row + 1, wsheet.max_row+1)]])
                        break
            if not valid_header:
                print(header_temp_row)
                print("Header rows missing")
                sys.exit(1)
            
    else:
        print("Cannot allocate header rows")
        sys.exit(1)

    return check_point

def Convert_data(data_dict, device_type=None):
    header = []
    body = []
    result_template = {}
    # cnt = 0
    # result_template.update({header_template_rows[0][0]: "{0}"})
    result_template.update({header_template_rows[1][0]: "[Test functionality] {0}"})
    result_template.update({header_template_rows[2][0]: "[Test condition] {0}"})
    result_template.update({header_template_rows[3][0]: "[Expected] {0}"})

    for keys, values in data_dict.items():
        if keys in result_template.keys():
            header.append(keys)
            for data_idx, data in enumerate(values):
                tmp = data
                if device_type:
                    tmp = Filter_device(device_type, str(data))
                    # print(tmp)
                    # cnt += 1
                    # if (5 == cnt): sys.exit(0)

                #     content = result_template[keys].format(tmp) if tmp != '' else ''
                # else:
                #     content = result_template[keys].format(data) if data != '' else ''
                content = result_template[keys].format(tmp) if tmp != '' else ''

                if data_idx >= len(body):
                    body.append([])
                body[data_idx].append(content)

    # print(header)
    # print(body)
    # print('*'*40)
    # return body

    result = [header]
    for data_body in body:
        result.append(data_body)

    # result = [header].extend(body)

    return result

def Filter_device(dev_type, data_cell):
    temp = data_cell.replace('U2Ax','U2x').replace('\r','').split("\n")
    result = []
    common = []
    ignore_list = []

    for dev in support_device :
        if dev not in dev_type:
            ignore_list.append(dev)

    # filter
    start = None
    ignore_flag = None
    for idx, data in enumerate(temp):

        # if dev_type in data:
        #     start = idx
        #     # result.append(data.replace(dev_type,""))
        #     result.append(data)
        # elif not start:
        #     result.append(data)
        # elif '' is data:
        #     result.extend(temp[start, idx+1])
        #     start = None

        
        for dev in ignore_list:
            if dev in data:
                ignore_flag = data
                # result.append(data.replace(dev_type,""))
                continue
        if not ignore_flag:
            # result.append(data.replace(dev_type,""))
            for dev in dev_type:
                data = data.replace(dev,"").strip()
                result.append(data)
        elif '' is data:
            ignore_flag = None
        else:
            for dev in dev_type:
                if dev in data:
                    data = data.replace(dev,"").strip()
                    result.append(data)
                    ignore_flag = None

    return '\n'.join(result)

def Gen_report(check_point):
    wbook = openpyxl.Workbook()



    data_list = Convert_data(check_point, ['U2x:'])

    wsheet = wbook.create_sheet(title="Report_U2x")
    for row_idx, data_row in enumerate(data_list):
        for col_idx, data_idx in enumerate(data_row):
            wsheet.cell(row = 2+row_idx, column = 2+col_idx).value = str(data_idx)



    data_list = Convert_data(check_point, ['E2x:'])

    wsheet = wbook.create_sheet(title="Report_E 2x")
    for row_idx, data_row in enumerate(data_list):
        for col_idx, data_idx in enumerate(data_row):
            wsheet.cell(row = 2+row_idx, column = 2+col_idx).value = str(data_idx)



    wbook.save(output_location)



def main():
    """main function of the program"""
    if entry():
        check_point = Get_TIT_checkpoint()
        # print('*'*40)
        # print(check_point)
        # print('*'*40)
        # for key, value in data_dict.items():
        #     print('*'*40)
        #     print(key)
        #     print('*'*40)
        #     print(value)
        #     print('*'*40)

        # data_list = Convert_data(check_point, ['U2x:'])
        # print(data_list)
        Gen_report(check_point)



if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################