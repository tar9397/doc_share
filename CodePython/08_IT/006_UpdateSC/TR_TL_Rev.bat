
::REM DO NOT EDIT SECTION_______________________________________________________
@ECHO OFF
cls

REM echo "RH850_X2x_DIO_DIT_TS"
REM svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\plan\RH850_X2x_DIO_DIT_TS.xls"

echo "E2M"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\plan\RH850_X2x_DIO_DIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\plan\RH850_X2x_DIO_TIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\archive_test_log\E2M\Final\RH850_E2M_DIO_DIT_TL_Final_CS.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\archive_test_log\E2M\Final\RH850_E2M_DIO_IT_TL_Final.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\result\E2M\Final\RH850_E2M_DIO_DIT_TR_Final_CS.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2M\Final\RH850_E2M_DIO_COV_TR_Final_CS.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2M\Final\RH850_E2M_DIO_COV_TR_Final_CS_RC001.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\result\E2M\Final\RH850_E2M_DIO_TIT_TR_Final.xlsx"
echo ""
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_DIT_PRM.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_TIT_PRM.xlsx"



echo "E2H"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\plan\RH850_X2x_DIO_DIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\plan\RH850_X2x_DIO_TIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\archive_test_log\E2H\Beta\RH850_E2H_DIO_DIT_TL_Beta_WS.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\archive_test_log\E2H\Final\RH850_E2H_DIO_DIT_TL_Final_WS.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\archive_test_log\E2H\Final\RH850_E2H_DIO_IT_TL_Final.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\result\E2H\Beta\RH850_E2H_DIO_DIT_TR_Beta_WS.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\result\E2H\Final\RH850_E2H_DIO_DIT_TR_Final_WS.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2H\Beta\RH850_E2H_DIO_COV_TR_Beta_WS.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2H\Final\RH850_E2H_DIO_COV_TR_Final_WS.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2H\Final\RH850_E2H_DIO_COV_TR_Final_WS_RC001.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\result\E2H\Final\RH850_E2H_DIO_TIT_TR_Final.xlsx"
echo ""
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_DIT_PRM.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_TIT_PRM.xlsx"




echo "E2UH"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\plan\RH850_X2x_DIO_DIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\plan\RH850_X2x_DIO_TIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\archive_test_log\E2UH\Beta\RH850_E2UH_DIO_DIT_TL_Beta_ES.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\archive_test_log\E2UH\Final\RH850_E2UH_DIO_DIT_TL_Final_ES.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\archive_test_log\E2UH\Final\RH850_E2UH_DIO_IT_TL_Final.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\result\E2UH\Beta\RH850_E2UH_DIO_DIT_TR_Beta_ES.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\result\E2UH\Final\RH850_E2UH_DIO_DIT_TR_Final_ES.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2UH\Beta\RH850_E2UH_DIO_COV_TR_Beta_ES.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2UH\Final\RH850_E2UH_DIO_COV_TR_Final_ES.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\E2UH\Final\RH850_E2UH_DIO_COV_TR_Final_ES_RC001.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\result\E2UH\Final\RH850_E2UH_DIO_TIT_TR_Final.xlsx"
echo ""
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_DIT_PRM.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_TIT_PRM.xlsx"



echo "U2A16"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\plan\RH850_X2x_DIO_DIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\plan\RH850_X2x_DIO_TIT_TS.xls"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\archive_test_log\U2A16\Beta\RH850_U2A16_DIO_DIT_TL_Beta_ES.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\archive_test_log\U2A16\Final\RH850_U2A16_DIO_DIT_TL_Final_ES.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\archive_test_log\U2A16\Final\RH850_U2A16_DIO_IT_TL_Final.zip"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\result\U2A16\Beta\RH850_U2A16_DIO_DIT_TR_Beta_ES.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\integration_test\result\U2A16\Final\RH850_U2A16_DIO_DIT_TR_Final_ES.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\U2A16\Beta\RH850_U2A16_DIO_COV_TR_Beta_ES.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\U2A16\Final\RH850_U2A16_DIO_COV_TR_Final_ES.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_D\function_call_coverage\analysis\U2A16\Final\RH850_U2A16_DIO_COV_TR_Final_ES_RC001.xlsm"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\01_WorkProduct_T\result\U2A16\Final\RH850_U2A16_DIO_TIT_TR_Final.xlsx"
echo ""
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_DIT_PRM.xlsx"
svnversion -c "D:\Workspace\repo\RH850_X2x\internal\Module\dio\08_IT\02_PeerReview\RH850_X2x_DIO_TIT_PRM.xlsx"




::REM END_______________________________________________________________________