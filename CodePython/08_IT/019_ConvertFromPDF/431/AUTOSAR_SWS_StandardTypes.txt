********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
Document Title
Document Owner
Document Responsibility
Part of AUTOSAR Standard
Part of Standard Release
Date
2017-12-08
2016-11-30
2015-07-31
2014-10-31
2013-10-31
2013-03-15
2011-12-22
2010-02-02
Specification of Standard
Types
AUTOSAR
AUTOSAR
Document Identification No 049
Document Status
Final
Classic Platform
4.3.1
Document Change History
Release Changed by Change Description
4.3.1 AUTOSAR
Release
Management
4.3.0 AUTOSAR
Release
Management
4.2.2 AUTOSAR
Release
Management
4.2.1 AUTOSAR
Release
Management
4.1.2 AUTOSAR
Release
Management
4.1.1 AUTOSAR
Administration
4.0.3 AUTOSAR
Administration
3.1.4 AUTOSAR
Administration
 Updated OSEK reference (editorial)
 Corrected editorial traceability
issues
 Harmonized traceability
 Editorial changes
 Editorial changes
 Removed chapter(s) on change
documentation
 Harmonized requirements according
to SWS_General
 Update of SWS documents for new
traceability mechanism
 Removed instanceID from StdVersionType

Concretized the published
parameters to have the prefix
STD_TYPES
2008-08-13
3.1.1 AUTOSAR
Administration
 Legal disclaimer revised
 Legal disclaimer revised
1 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
Date
2007-12-21
Document Change History
Release Changed by Change Description
3.0.1 AUTOSAR
Administration
2007-01-24 2.1.15 AUTOSAR
Administration
2006-11-28
2.1.1 AUTOSAR
Administration
 Add Module ID for Complex Drivers
 Document meta information
extended
 Small layout adaptations made
 “Advice for users” revised
 “Revision Information” added
 Changed definition of
Standard_ReturnType to match the
RTE definition.
 A complete overview of definitions
and values has been performed to
match the requirements in the SRS
General.
2006-05-16
2.0 AUTOSAR
Administration
 Legal disclaimer revised
 Initial Release
2 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
Disclaimer
This work (specification and/or software implementation) and the material contained
in it, as released by AUTOSAR, is for the purpose of information only. AUTOSAR
and the companies that have contributed to it shall not be liable for any use of the
work.
The material contained in this work is protected by copyright and other types of
intellectual property rights. The commercial exploitation of the material contained in
this work requires a license to such intellectual property rights.
This work may be utilized or reproduced without any modification, in any form or by
any means, for informational purposes only. For any other purpose, no part of the
work may be utilized or reproduced, in any form or by any means, without permission
in writing from the publisher.
The work has been developed for automotive applications only. It has neither been
developed, nor tested for non-automotive applications.
The word AUTOSAR and the AUTOSAR logo are registered trademarks.
3 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
Table of Contents
1 Introduction and functional overview ................................................................... 5
2 Acronyms and abbreviations ............................................................................... 6
3 Related documentation........................................................................................ 7
3.1
3.2
3.3
Input documents ........................................................................................... 7
Related standards and norms ...................................................................... 7
Related specification .................................................................................... 7
4 Constraints and assumptions .............................................................................. 8
4.1 Limitations .................................................................................................... 8
4.2 Applicability to car domains .......................................................................... 8
5 Software Architecture .......................................................................................... 9
5.1 Dependencies to other modules ................................................................... 9
5.2
File structure ................................................................................................ 9
5.2.1 Communication related BSW modules .................................................. 9
5.2.2
Hierarchy in Standard Types ................................................................. 9
6 Requirements traceability .................................................................................. 11
7 Functional specification ..................................................................................... 17
7.1 General issues ........................................................................................... 17
8 API specification ................................................................................................ 18
8.1 Type definitions .......................................................................................... 18
8.1.1 Std_ReturnType .................................................................................. 18
8.1.2 Std_VersionInfoType ........................................................................... 19
8.2 Symbol definitions ...................................................................................... 19
8.2.1 E_OK, E_NOT_OK ............................................................................. 19
8.2.2 STD_HIGH, STD_LOW ....................................................................... 19
8.2.3 STD_ACTIVE, STD_IDLE ................................................................... 20
8.2.4 STD_ON, STD_OFF ........................................................................... 20
8.3 Function definitions .................................................................................... 20
9 Sequence diagrams .......................................................................................... 21
10
11
Configuration specification ............................................................................. 22
Not applicable requirements .......................................................................... 23
4 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
1 Introduction and functional overview
This document specifies the AUTOSAR standard types header file. It contains all
types that are used across several modules of the basic software and that are
platform and compiler independent.
It is strongly recommended that those standard types files are unique within the
AUTOSAR community to guarantee unique types and to avoid types changes when
changing from supplier A to B.
5 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
2 Acronyms and abbreviations
Acronyms and abbreviations that have a local scope are not contained in the
AUTOSAR glossary. These must appear in a local glossary.
Acronym:
Description:
API
OSEK/VDX
Application Programming Interface
Offene Systeme und deren Schnittstellen für die Elektronik im Kraftfahrzeug
Abreviation: Description:
STD
Standard
6 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
3 Related documentation
3.1 Input documents
[1] General Requirements on Basic Software Modules
AUTOSAR_SRS_BSWGeneral.pdf
[2] General Requirements on SPAL
AUTOSAR_SRS_SPALGeneral.pdf
[3] Specification of RTE Software
AUTOSAR_SWS_RTE.pdf
[4] Basic Software Module Description Template,
AUTOSAR_TPS_BSWModuleDescriptionTemplate.pdf
[5] List of Basic Software Modules
AUTOSAR_TR_BSWModuleList
[6] General Specification of Basic Software Modules
AUTOSAR_SWS_BSWGeneral.pdf
3.2 Related standards and norms
[7] OSEK/VDX Operating System, ISO 17356-3: OS
[8] ISO/IEC 9899:1990 Programming Language – C
3.3 Related specification
AUTOSAR provides a General Specification on Basic Software modules [6] (SWS
BSW General), which is also valid for Standard Types.
Thus, the specification SWS BSW General shall be considered as additional and
required specification for Standard Types.
7 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
4 Constraints and assumptions
4.1 Limitations
No limitations.
4.2 Applicability to car domains
Many symbols defined in this specification (like OK, NOT_OK, ON, OFF) are already
defined and used within legacy software. These conflicts (‘redefinition of existing
symbol’) are expected, but neglected, because of the following reasons:
1. AUTOSAR has to maintain network compatibility with legacy ECUs, but no
software architecture compatibility with legacy software Many types are defined
and used exactly in the same way that legacy software does. Legacy software
can keep on using the symbols, only the definitions have to be removed and
taken from this file instead.
8 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
5 Software Architecture
5.1 Dependencies to other modules
.
5.2 File structure
The include structures differ between BSW modules which are part of the COM-stack
and other modules. BSW modules which is considered part of the COM stack shall
include the ComStack_Types.h other modules shall include Std_Types.h
5.2.1 Communication related BSW modules
[SWS_Std_00016] ⌈The include file structure shall be as follows:
Communication related
BSW modules
include
ComStack_Types.h
Std_Types.h
include
 ComStack_Types.h shall include Std_Types.h
 Communication related basic software modules shall include
ComStack_Types.h
⌋ (SRS_BSW_00024)
5.2.2 Hierarchy in Standard Types
The headers are structured as follows:
[SWS_Std_00019]⌈
9 of 23
- AUTOSAR confidential -
Document ID 049:AUTOSAR_SWS_StandardTypes
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
(SRS_BSW_00024)
The standard types implement the following interface:
[SWS_Std_00020]⌈
«module»
Std_Types
«realize»
Std_Types
(SRS_BSW_00024)
)
10 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
6 Requirements traceability
Requirement
Description
Satisfied by
SRS_BSW_00004 All Basic SW Modules shall perform a pre-processor
check of the versions of all imported include files
SRS_BSW_00005 Modules of the µC Abstraction Layer (MCAL) may not
have hard coded horizontal interfaces
SRS_BSW_00006 The source code of software modules above the µC
Abstraction Layer (MCAL) shall not be processor and
compiler dependent.
SRS_BSW_00007 All Basic SW Modules written in C language shall
conform to the MISRA C 2012 Standard.
SRS_BSW_00009 All Basic SW Modules shall be documented according
to a common standard.
SRS_BSW_00010 The memory consumption of all Basic SW Modules
shall be documented for a defined configuration for all
supported platforms.
SRS_BSW_00024 -
SRS_BSW_00059 -
SRS_BSW_00101 The Basic Software Module shall be able to initialize
variables and hardware in a separate initialization
function
SRS_BSW_00158 All modules of the AUTOSAR Basic Software shall
strictly separate configuration from implementation
SRS_BSW_00159 All modules of the AUTOSAR Basic Software shall
support a tool based configuration
SRS_BSW_00160 Configuration files of AUTOSAR Basic SW module
shall be readable for human beings
SRS_BSW_00161 The AUTOSAR Basic Software shall provide a
microcontroller abstraction layer which provides a
standardized interface to higher software layers
SRS_BSW_00162 The AUTOSAR Basic Software shall provide a
hardware abstraction layer
SRS_BSW_00164 The Implementation of interrupt service routines shall
be done by the Operating System, complex drivers or
modules
SRS_BSW_00167 All AUTOSAR Basic Software Modules shall provide
configuration rules and constraints to enable
plausibility checks
SRS_BSW_00168 SW components shall be tested by a function defined
in a common API in the Basis-SW
SRS_BSW_00170 The AUTOSAR SW Components shall provide
information about their dependency from faults, signal
qualities, driver demands
SRS_BSW_00171 Optional functionality of a Basic-SW component that
is not required in the ECU shall be configurable at
pre-compile-time
11 of 23
- AUTOSAR confidential -
SWS_Std_00015
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00016
SWS_Std_00014
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00004,
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
Document ID 049:AUTOSAR_SWS_StandardTypes
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
SRS_BSW_00172 The scheduling strategy that is built inside the Basic
Software Modules shall be compatible with the
strategy used in the system
SRS_BSW_00300 All AUTOSAR Basic Software Modules shall be
identified by an unambiguous name
SRS_BSW_00301 All AUTOSAR Basic Software Modules shall only
import the necessary information
SRS_BSW_00302 All AUTOSAR Basic Software Modules shall only
export information needed by other modules
SRS_BSW_00304 All AUTOSAR Basic Software Modules shall use the
following data types instead of native C data types
SRS_BSW_00305 Data types naming convention
SRS_BSW_00306 AUTOSAR Basic Software Modules shall be compiler
and platform independent
SRS_BSW_00307 Global variables naming convention
SRS_BSW_00308 AUTOSAR Basic Software Modules shall not define
global data in their header files, but in the C file
SRS_BSW_00309 All AUTOSAR Basic Software Modules shall indicate
all global data with read-only purposes by explicitly
assigning the const keyword
SRS_BSW_00310 API naming convention
SRS_BSW_00312 Shared code shall be reentrant
SRS_BSW_00314 All internal driver modules shall separate the interrupt
frame definition from the service routine
SRS_BSW_00321 The version numbers of AUTOSAR Basic Software
Modules shall be enumerated according specific rules
SRS_BSW_00323 All AUTOSAR Basic Software Modules shall check
passed API parameters for validity
SRS_BSW_00325 The runtime of interrupt service routines and functions
that are running in interrupt context shall be kept short
SRS_BSW_00327 Error values naming convention
SRS_BSW_00330 It shall be allowed to use macros instead of functions
where source code is used and runtime is critical
SRS_BSW_00331 All Basic Software Modules shall strictly separate
error and status information
SRS_BSW_00333 For each callback function it shall be specified if it is
called from interrupt context or not
SRS_BSW_00334 All Basic Software Modules shall provide an XML file
that contains the meta data
SRS_BSW_00335 Status values naming convention
SRS_BSW_00336 Basic SW module shall be able to shutdown
SRS_BSW_00337 Classification of development errors
SRS_BSW_00339 Reporting of production relevant error status
SRS_BSW_00341 Module documentation shall contains all needed
informations
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SRS_BSW_00342 It shall be possible to create an AUTOSAR ECU out SWS_Std_00999
12 of 23
- AUTOSAR confidential -
Document ID 049:AUTOSAR_SWS_StandardTypes
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
of modules provided as source code and modules
provided as object code, even mixed
SRS_BSW_00343 The unit of time for specification and configuration of
Basic SW modules shall be preferably in physical
time unit
SRS_BSW_00344 BSW Modules shall support link-time configuration
SRS_BSW_00346 All AUTOSAR Basic Software Modules shall provide
at least a basic set of module files
SRS_BSW_00347 A Naming seperation of different instances of BSW
drivers shall be in place
SRS_BSW_00348 All AUTOSAR standard types and constants shall be
placed and organized in a standard type header file
SRS_BSW_00350 All AUTOSAR Basic Software Modules shall allow the
enabling/disabling of detection and reporting of
development errors.
SRS_BSW_00353 All integer type definitions of target and compiler
specific scope shall be placed and organized in a
single type header
SRS_BSW_00357 For success/failure of an API call a standard return
type shall be defined
SRS_BSW_00358 The return type of init() functions implemented by
AUTOSAR Basic Software Modules shall be void
SRS_BSW_00359 All AUTOSAR Basic Software Modules callback
functions shall avoid return types other than void if
possible
SRS_BSW_00360 AUTOSAR Basic Software Modules callback
functions are allowed to have parameters
SRS_BSW_00361 All mappings of not standardized keywords of
compiler specific scope shall be placed and organized
in a compiler specific type and keyword header
SRS_BSW_00369 All AUTOSAR Basic Software Modules shall not
return specific development error codes via the API
SRS_BSW_00371 The passing of function pointers as API parameter is
forbidden for all AUTOSAR Basic Software Modules
SRS_BSW_00373 The main processing function of each AUTOSAR
Basic Software Module shall be named according the
defined convention
SRS_BSW_00374 All Basic Software Modules shall provide a readable
module vendor identification
SRS_BSW_00377 A Basic Software Module can return a module
specific types
SRS_BSW_00378 AUTOSAR shall provide a boolean type
SRS_BSW_00379 All software modules shall provide a module identifier
in the header file and in the module XML description
file.
13 of 23
- AUTOSAR confidential -
SWS_Std_00999
SWS_Std_00999
SRS_BSW_00345 BSW Modules shall support pre-compile configuration SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00007,
SWS_Std_00010,
SWS_Std_00013
SWS_Std_00999
SWS_Std_00999
SWS_Std_00005,
SWS_Std_00006,
SWS_Std_00011
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SRS_BSW_00375 Basic Software Modules shall report wake-up reasons SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
Document ID 049:AUTOSAR_SWS_StandardTypes
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
SRS_BSW_00380 Configuration parameters being stored in memory
shall be placed into separate c-files
SRS_BSW_00381 The pre-compile time parameters shall be placed into
a separate configuration header file
SRS_BSW_00383 The Basic Software Module specifications shall
specify which other configuration files from other
modules they use at least in the description
SRS_BSW_00385 List possible error notifications
SRS_BSW_00386 The BSW shall specify the configuration for detecting
an error
SRS_BSW_00388 Containers shall be used to group configuration
parameters that are defined for the same object
SRS_BSW_00389 Containers shall have names
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SRS_BSW_00390 Parameter content shall be unique within the module SWS_Std_00999
SRS_BSW_00392 Parameters shall have a type
SRS_BSW_00393 Parameters shall have a range
SRS_BSW_00394 The Basic Software Module specifications shall
specify the scope of the configuration parameters
SRS_BSW_00395 The Basic Software Module specifications shall list all
configuration parameter dependencies
SRS_BSW_00396 The Basic Software Module specifications shall
specify the supported configuration classes for
changing values and multiplicities for each
parameter/container
SRS_BSW_00397 The configuration parameters in pre-compile time are
fixed before compilation starts
SRS_BSW_00398 The link-time configuration is achieved on object code
basis in the stage after compiling and before linking
SRS_BSW_00399 Parameter-sets shall be located in a separate
segment and shall be loaded after the code
SRS_BSW_00400 Parameter shall be selected from multiple sets of
parameters after code has been loaded and started
SRS_BSW_00401 Documentation of multiple instances of configuration
parameters shall be available
SRS_BSW_00405 BSW Modules shall support multiple configuration
sets
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SRS_BSW_00404 BSW Modules shall support post-build configuration SWS_Std_00999
SWS_Std_00999
SRS_BSW_00406 A static status variable denoting if a BSW module is
initialized shall be initialized with value 0 before any
APIs of the BSW module is called
SRS_BSW_00407 Each BSW module shall provide a function to read
out the version information of a dedicated module
implementation
SRS_BSW_00408 All AUTOSAR Basic Software Modules configuration
parameters shall be named according to a specific
naming rule
SRS_BSW_00409 All production code error ID symbols are defined by
the Dem module and shall be retrieved by the other
14 of 23
- AUTOSAR confidential -
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
Document ID 049:AUTOSAR_SWS_StandardTypes
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
BSW modules from Dem configuration
SRS_BSW_00410 Compiler switches shall have defined values
SRS_BSW_00411 All AUTOSAR Basic Software Modules shall apply a
naming rule for enabling/disabling the existence of the
API
SRS_BSW_00412 References to c-configuration parameters shall be
placed into a separate h-file
SRS_BSW_00413 An index-based accessing of the instances of BSW
modules shall be done
SRS_BSW_00414 Init functions shall have a pointer to a configuration
structure as single parameter
SRS_BSW_00415 Interfaces which are provided exclusively for one
module shall be separated into a dedicated header
file
SRS_BSW_00416 The sequence of modules to be initialized shall be
configurable
SRS_BSW_00417 Software which is not part of the SW-C shall report
error events only after the DEM is fully operational.
SRS_BSW_00419 If a pre-compile time configuration parameter is
implemented as "const" it should be placed into a
separate c-file
SRS_BSW_00422 Pre-de-bouncing of error status information is done
within the DEM
SRS_BSW_00423 BSW modules with AUTOSAR interfaces shall be
describable with the means of the SW-C Template
SRS_BSW_00424 BSW module main processing functions shall not be
allowed to enter a wait state
SRS_BSW_00425 The BSW module description template shall provide
means to model the defined trigger conditions of
schedulable objects
SRS_BSW_00426 BSW Modules shall ensure data consistency of data
which is shared between BSW modules
SRS_BSW_00427 ISR functions shall be defined and documented in the
BSW module description template
SRS_BSW_00428 A BSW module shall state if its main processing
function(s) has to be executed in a specific order or
sequence
SRS_BSW_00429 Access to OS is restricted
SRS_BSW_00432 Modules should have separate main processing
functions for read/receive and write/transmit data path
SRS_BSW_00433 Main processing functions are only allowed to be
called from task bodies provided by the BSW
Scheduler
SRS_BSW_00441 Naming convention for type, macro and function
SRS_BSW_00452 Classification of runtime errors
SRS_BSW_00458 Classification of production errors
SRS_BSW_00466 Classification of extended production errors
15 of 23
- AUTOSAR confidential -
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
SWS_Std_00011
SWS_Std_00999
SWS_Std_00999
SWS_Std_00999
Document ID 049:AUTOSAR_SWS_StandardTypes
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
SRS_BSW_00473 Classification of transient faults
SWS_Std_00999
16 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
7 Functional specification
7.1 General issues
[SWS_Std_00004] ⌈It is not allowed to add any project or supplier specific extension
to this file. Any extension invalidates the AUTOSAR conformity. ⌋ (SRS_BSW_00161)
[SWS_Std_00014] ⌈The standard types header file shall be protected against
multiple inclusion:
#ifndef STD_TYPES_H
#define STD_TYPES_H
..
/*
* Contents of file
*/
..
#endif /* STD_TYPES_H */
⌋ (SRS_BSW_00059)
17 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
8 API specification
8.1 Type definitions
8.1.1 Std_ReturnType
[SWS_Std_00005] ⌈
Name
Kind
Derived
from
Description
uint8
This type can be used as standard API return type which is shared between the RTE
and the BSW modules. It shall be defined as follows:
typedef uint8 Std_ReturnType;
E_OK
0
Range
Variation
E_NOT_OK
0x02-0x3F
-⌋
(SRS_BSW_00357)
[SWS_Std_00011] ⌈The Std_ReturnType shall normally be used with value E_OK or E_NOT_OK. If
those return values are not sufficient user specific values can be defined by using the 6 least specific
bits.
For the naming of the user defined values the module prefix shall be used as requested in
SRS_BSW_00441
Layout of the Std_ReturnType shall be as stated in the RTE specification. Bit 7 and Bit 8 are
reserved and defined by the RTE specification.
1
2
see 8.2.1, SWS_Std_00006
see 8.2.1, SWS_Std_00006
Available to user specific errors
Std_ReturnType
Type
⌋ (SRS_BSW_00357,SRS_BSW_00441)
18 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
8.1.2 Std_VersionInfoType
[SWS_Std_00015] ⌈
Name:
Type:
Element:
Std_VersionInfoType
Structure
uint16
uint16
uint8
uint8
uint8
Description:
⌋ (SRS_BSW_00004)
8.2 Symbol definitions
8.2.1 E_OK, E_NOT_OK
[SWS_Std_00006] ⌈
Name:
Type:
Range:
Description:
E_OK, E_NOT_OK
Enumeration
E_OK
E_NOT_OK
0x00u
0x01u
--Because
E_OK is already defined within OSEK, the symbol E_OK has to be
shared. To avoid name clashes and redefinition problems, the symbols have to be
defined in the following way (approved within implementation):
#ifndef STATUSTYPEDEFINED
#define STATUSTYPEDEFINED
#define E_OK 0x00u
typedef unsigned char StatusType; /* OSEK compliance */
#endif
#define E_NOT_OK 0x01u
⌋ (SRS_BSW_00357)
vendorID
moduleID
--sw_major_version
-sw_minor_version
-sw_patch_version
-This
type shall be used to request the version of a BSW module using the <Module
name>_GetVersionInfo() function.
8.2.2 STD_HIGH, STD_LOW
[SWS_Std_00007] ⌈
Name:
Type:
Range:
Description:
⌋ (SRS_BSW_00348)
19 of 23
- AUTOSAR confidential -
Document ID 049:AUTOSAR_SWS_StandardTypes
STD_HIGH, STD_LOW
Enumeration
STD_LOW
STD_HIGH
0x00u
0x01u
The symbols STD_HIGH and STD_LOW shall be defined as follows:
#define STD_HIGH 0x01u /* Physical state 5V or 3.3V */
#define STD_LOW 0x00u /* Physical state 0V */
--********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
8.2.3 STD_ACTIVE, STD_IDLE
[SWS_Std_00013] ⌈
Name:
Type:
Range:
Description:
STD_ACTIVE, STD_IDLE
Enumeration
STD_IDLE
STD_ACTIVE
0x00u
0x01u
--The
symbols STD_ACTIVE and STD_IDLE shall be defined as follows:
#define STD_ACTIVE 0x01u /* Logical state active */
#define STD_IDLE 0x00u /* Logical state idle */
⌋ (SRS_BSW_00348)
8.2.4 STD_ON, STD_OFF
[SWS_Std_00010] ⌈
Name:
Type:
Range:
Description:
STD_ON, STD_OFF
Enumeration
STD_OFF
STD_ON
0x00u
0x01u
The symbols STD_ON and STD_OFF shall be defined as follows:
#define STD_ON 0x01u
#define STD_OFF 0x00u
⌋ (SRS_BSW_00348)
8.3 Function definitions
Not applicable.
--20
of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
9 Sequence diagrams
Not applicable.
21 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
10 Configuration specification
Not applicable.
22 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
********************************************************************************
Specification of Standard Types
AUTOSAR CP Release 4.3.1
11 Not applicable requirements
[SWS_Std_00999] ⌈These requirements are not applicable to this specification.⌋
(SRS_BSW_00300, SRS_BSW_00301, SRS_BSW_00302, SRS_BSW_00304, SRS_BSW_00305,
SRS_BSW_00306, SRS_BSW_00307, SRS_BSW_00308, SRS_BSW_00309, SRS_BSW_00310,
SRS_BSW_00312, SRS_BSW_00314, SRS_BSW_00321, SRS_BSW_00325, SRS_BSW_00327,
SRS_BSW_00330, SRS_BSW_00331, SRS_BSW_00333, SRS_BSW_00334, SRS_BSW_00335,
SRS_BSW_00342, SRS_BSW_00343, SRS_BSW_00341, SRS_BSW_00346, SRS_BSW_00347,
SRS_BSW_00350, SRS_BSW_00353, SRS_BSW_00358, SRS_BSW_00359, SRS_BSW_00360,
SRS_BSW_00361, SRS_BSW_00371, SRS_BSW_00373, SRS_BSW_00374, SRS_BSW_00377,
SRS_BSW_00378, SRS_BSW_00379, SRS_BSW_00401, SRS_BSW_00408, SRS_BSW_00410,
SRS_BSW_00411, SRS_BSW_00413, SRS_BSW_00414, SRS_BSW_00415, SRS_BSW_00005,
SRS_BSW_00006, SRS_BSW_00007, SRS_BSW_00009, SRS_BSW_00010, SRS_BSW_00158,
SRS_BSW_00160, SRS_BSW_00161, SRS_BSW_00162, SRS_BSW_00164, SRS_BSW_00172,
SRS_BSW_00344, SRS_BSW_00404, SRS_BSW_00405, SRS_BSW_00345, SRS_BSW_00159,
SRS_BSW_00167, SRS_BSW_00171, SRS_BSW_00170, SRS_BSW_00380, SRS_BSW_00419,
SRS_BSW_00381, SRS_BSW_00412, SRS_BSW_00383, SRS_BSW_00388, SRS_BSW_00389,
SRS_BSW_00390, SRS_BSW_00392, SRS_BSW_00393, SRS_BSW_00394, SRS_BSW_00395,
SRS_BSW_00396, SRS_BSW_00397, SRS_BSW_00398, SRS_BSW_00399, SRS_BSW_00400,
SRS_BSW_00375, SRS_BSW_00101, SRS_BSW_00416, SRS_BSW_00406, SRS_BSW_00168,
SRS_BSW_00407, SRS_BSW_00423, SRS_BSW_00424, SRS_BSW_00425, SRS_BSW_00426,
SRS_BSW_00427, SRS_BSW_00428, SRS_BSW_00429, SRS_BSW_00432, SRS_BSW_00433,
SRS_BSW_00336, SRS_BSW_00337, SRS_BSW_00369, SRS_BSW_00339, SRS_BSW_00422,
SRS_BSW_00417, SRS_BSW_00323, SRS_BSW_00409, SRS_BSW_00385, SRS_BSW_00386,
SRS_BSW_00452, SRS_BSW_00473, SRS_BSW_00458, SRS_BSW_00466)
23 of 23
Document ID 049:AUTOSAR_SWS_StandardTypes
- AUTOSAR confidential -
