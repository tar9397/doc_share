#!/usr/bin/python3
# -*- coding: utf-8 -*-
################################################################################
#                           GenTestAppRCar.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
import argparse
# import openpyxl
import json
from dataclasses import dataclass
from typing import List
from PDFNetPython3 import *

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------
line_ending = '\r\n'
# output_loc = './Output'
register_db = {}



@dataclass
class TestCaseConfig:
    port_name : str
    port_num : int
    port_pin : int
    reg_addr : str
    hold_time : int
    port_pin_list : List[int]

    def __init__(self, port_name: str, port_pin: int, reg_name: str, hold_time: int):
        # self.port_num = port_num
        self.port_name = port_name
        self.port_num = port_name.strip().split('_')[1]
        # self.port_pin_list = port_name.strip().split('_')[1]
        self.port_pin = port_pin
        self.reg_addr = register_db[reg_name]
        self.hold_time = hold_time
        self.port_pin_list = []

        tmp = port_name.strip().split('_')
        filter_data = tmp[tmp.index('BITS')+1:]
        if len(filter_data) > 1 and 'TO' in filter_data[1]:
            row_data = list(range(int(filter_data[0]), int(filter_data[2])+1))
            self.port_pin_list = row_data
        else:
            self.port_pin_list.append(filter_data)

        self.port_pin_mask = 0
        for pin_id in self.port_pin_list:
            if pin_id != self.port_pin:
                self.port_pin_mask += 1 << pin_id
                # print(pin_id)

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Handle parser create function
# port_id: array of int
# ------------------------------------------------------------------------------



# def update_arr_content(arr, *args):
#     return [data.format(*args) for data in arr]
def padded_hex_raw(dev_value, length):
    return "{0:#0{1}X}".format(dev_value, length+2 )

def padded_hex(dev_value, length=8):
    return padded_hex_raw(dev_value, length).replace('0X','0x')

def padded_hex_without_prefix(dev_value, length=8):
    return padded_hex_raw(dev_value, length).replace('0X','')





def get_input_command():
    """Get all input for init process
    Print help if command is not in valid command

    Returns:
        None -- It doesn't return any value
    """
    example = '''
    E.g: ./GenTestAppRCarGen3.py CFG001
    ./GenTestAppRCarGen3.py CFG009
    '''
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('Dev', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
                        help='valid target device: H3, V3M, V3H ...')
    parser.add_argument('-m', action='store_true', \
                        help='set max hold time', default=False, required=False)
    parser.add_argument('--run_tc', type=str, nargs='?',
                        help='run with specific test case or list of test case', default='', required=False)

    # parser.add_argument('cfg', type=str,
    #                     help='config to read')
    # parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
    #                     help='valid target module: Adc, Can, Dio ...')
    # parser.add_argument('TestType', type=str, help='type of test: DIT, TIT, VT')
    # parser.add_argument('TestDevice', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
    #                     help='target device: H3, V3M, V3H , M3, M3N, V3U, V3Hv2')
    # parser.add_argument('ARVersion', type=str, choices = ["4_2_2", "4.2.2"], \
    #                     help='autosar version: 4_2_2')
    # parser.add_argument('--nc', action='store_true', \
    #                     help='(optional) skip copy item in WorkspaceSetup.sh', default=False, required=False)
    # parser.add_argument('OSTest', type=str,
    #                     help='OS Test option: OS, No_OS, None(TIT only)')
    # parser.add_argument('--s0s1', type=str, nargs='?',
    #                     help='(optional) run S0/S1 or normal TC: True, False (default=False)', \
    #                     default='False', required=False)
    # parser.add_argument('--target_cfg', type=str, nargs='?',
    #                     help='(optional) run specific cfg(s)/config type: ex. CFG001,CFG002,DEF001(split by ","), \
    #                     CFGnnn(all CFG), RCnnn(all RC), DEFnnn(all DEF), xCFGnnn(exclude all CFG), \
    #                     xRCnnn(exclude all RC), xDEFnnn(exclude all DEF)', default='CFGxxx', required=False)
    # parser.add_argument('--keep_tc', type=str, nargs='?',
    #                     help='(optional: True or False) when --target_cfg is used do you want to keep original \
    #                     TC or not (defalt=False)', default='False', required=False)
    # parser.add_argument('--ignore_error', type=str, nargs='?',
    #                     help='(optional: True or False) allow to continue progress when error occurs (defalt=false)', \
    #                     default='False', required=False)
    # parser.add_argument('--ice_number', type=str, nargs='?',
    #                     help='(optional) id number of E2 Emulator to select debugger connection (defalt=null)', \
    #                     required=False)
    # parser.add_argument('--build_option', type=str, nargs='?',
    #                     help='(optional: all/generate_forward/build_only) allow to choose selected actions only \
    #                     to reduce execution time (defalt=all)', default='all', required=False)
    # parser.add_argument('--exe_op', type=str, nargs='?',
    #                     help='(optional: True or False) execution time optimization, not recommended, this should \
    #                     only be used when everything is confirmed OK (default=False)', default='False', required=False)
    # parser.add_argument('--cat_map', type=str, nargs='?',
    #                     help='(optional) use interrupt category in data.json', default='False', required=False)
    # parser.add_argument('--debug', type=str, nargs='?',
    #                     help='(optional) turn on script debug mode', default='False', required=False)

    args = parser.parse_args()
    # return str(args.cfg)
    return args

    # config.msn = str(args.Msn)  # MCAL Msn
    # config.test_type = str(args.TestType).upper()  # DIT, TIT
    # config.device = str(args.TestDevice)  # V3H, V3U..
    # config.ar_version = str(args.ARVersion).replace('_','.')  # 4_2_2, 4_3_1
    # config.skip_copy_workspace = args.nc

    # return True

def dumpAllText (reader):
    element = reader.Next()
    while element != None:
        type = element.GetType()
        if type == Element.e_text_begin:
            print("Text Block Begin")
        elif type == Element.e_text_end:
            print("Text Block End")
        elif type == Element.e_text:
            bbox = element.GetBBox()
            print("BBox: " + str(bbox.GetX1()) + ", " + str(bbox.GetY1()) + ", " 
                  + str(bbox.GetX2()) + ", " + str(bbox.GetY2()))
            textString = element.GetTextString()
            print(textString)
        elif type == Element.e_text_new_line:
            print("New Line")
        elif type == Element.e_form:
            reader.FormBegin()
            dumpAllText(reader)
            reader.End()
        element = reader.Next()


def main():
    """main function of the program"""

    # cmd_cfg = get_input_command()

    # # print("test")
    # if cmd_cfg.run_tc is not '':
    #     run_tc_list = cmd_cfg.run_tc.split(',')
    # else:
    #     run_tc_list = [tc for tc in run_config[cmd_cfg.Dev].keys()]
    
    # for run_tc_name in run_tc_list:
    #     print('*'*40)
    #     print("Process test case: ",run_tc_name)
    #     print('*'*40)
    #     run_tc = run_config[cmd_cfg.Dev][run_tc_name.strip()]
    #     # print(run_tc.port_pin_mask)
    #     result = gen_resource_command_serial(
    #             run_tc.reg_addr, 
    #             padded_hex(run_tc.port_pin_mask), 
    #             padded_hex_without_prefix(run_tc.hold_time, length=4))
    #     Send_Serial(result)

    process_folder = '431'
    # process_folder = '1911'

    # pdf_filename = "AUTOSAR_SRS_DIODriver.pdf"
    # pdf_filename = "AUTOSAR_SRS_BSWGeneral.pdf"
    # pdf_filename = "AUTOSAR_SWS_BSWGeneral.pdf"
    # pdf_filename = "AUTOSAR_SWS_DIODriver.pdf"
    # pdf_filename = "AUTOSAR_SWS_PlatformTypes.pdf"
    pdf_filename = "AUTOSAR_SWS_StandardTypes.pdf"

    output_name = pdf_filename.split('.')[0] + '.txt'

    path_to_pdf = os.path.join('./', process_folder, pdf_filename)
    path_to_output = os.path.join('./', process_folder, output_name)
    # output_name = 'output'
    # pdf_filename = "AUTOSAR_SWS_DIODriver_431.pdf"
    
    # output_name = 'output_srs_431'
    # pdf_filename = "AUTOSAR_SRS_DIODriver_431.pdf"
    
    # output_name = 'output_srs_1911'
    # pdf_filename = "AUTOSAR_SRS_DIODriver_1911.pdf"


    # output_name = "output"
    # pdf_filename = "AUTOSAR_SRS_DIODriver_1911.pdf"




    # PDFNet.Initialize()
    doc = PDFDoc(path_to_pdf)

    # convert to txt
    # page = doc.GetPage(1)

    # txt = TextExtractor()
    # txt.Begin(page) # Read the page

    # word = Word()
    # line = txt.GetFirstLine()
    # print('*'*80)
    # while line.IsValid():
    #     word = line.GetFirstWord()
    #     while word.IsValid():
    #         # word.GetString()
    #         word = word.GetNextWord()
    #         print(word.GetString())
    #         # print(type(word))
    #         exit(1)
    #     line = line.GetNextLine()
    # print('*'*80)
    # print(txt.GetAsText())
    
    output_txt = ''
    reader = ElementReader()
    itr  = doc.GetPageIterator()
    while itr.HasNext():
        # reader.Begin(itr.Current())
        # dumpAllText(reader)
        # reader.End()

        txt = TextExtractor()
        txt.Begin(itr.Current()) # Read the page
        print('*'*80)
        print(txt.GetAsText())
        output_txt += '*'*80
        output_txt += line_ending
        output_txt += txt.GetAsText()
        itr.Next()

    with open(path_to_output, 'w', encoding="utf-8") as f_out:
        f_out.write(output_txt)



if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################