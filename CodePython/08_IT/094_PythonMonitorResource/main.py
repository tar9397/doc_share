from concurrent.futures import ThreadPoolExecutor
from os import listdir
import os 
from PIL import Image
from memory_monitor import *



def process_img(filename):
    """
    This method take the filename of the img file, performs a gray scale operation on the file, creates
    a thumbnail of the img and then saves the file in processed folder.
    :param filename: name of img file.
    :return: None
    """

    # img = Image.open(f'.\\imgs\\{filename}')
    img = Image.open(os.path.join('.','imgs',f'{filename}'))
    gray_img = img.convert('L')
    gray_img.thumbnail((200, 400))
    # gray_img.save(f'.\\processed\\{filename}')
    gray_img.save(os.path.join('.','processed',f'{filename}'))

def my_analysis_function():
    # file_names = listdir('.\\imgs')
    file_names = listdir(os.path.join('.','imgs'))
    for filename in file_names:
        process_img(filename)

with ThreadPoolExecutor() as executor:
    monitor = MemoryMonitor()
    mem_thread = executor.submit(monitor.measure_usage)
    try:
        fn_thread = executor.submit(my_analysis_function)
        result = fn_thread.result()
    finally:
        monitor.keep_measuring = False
        max_usage = mem_thread.result()
        
    print(f"Peak memory usage: {max_usage}")