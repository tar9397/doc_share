from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Ie("./IEDriverServer.exe")
driver.get("https://b2b-gateway.renesas.com/login/ssl")
print('Start wait for load')
driver.implicitly_wait(15)
# time.sleep(5)
print('Find LOGIN_ID')
# driver.find_element(By.NAME,'LOGIN_ID').send_keys('a5120009')
driver.find_element(By.NAME,'LOGIN_ID').send_keys('a5124170')
driver.find_element(By.CLASS_NAME,'btnCustom').click()
password=""

# SMX_BTN_0, SMX_BTN_1, SMX_BTN_2, SMX_BTN_3
# SMX_BTN_4, SMX_BTN_5, SMX_BTN_6, SMX_BTN_7
# SMX_BTN_8, SMX_BTN_9, SMX_BTN_10, SMX_BTN_11
# SMX_BTN_12, SMX_BTN_13, SMX_BTN_14, SMX_BTN_15

# 0 1 2 3           16 17 18 19         32 33 34 35         48 49 50 51
# 4 5 6 7           20 21 22 23         36 37 38 39         52 53 54 55
# 8 9 10 11         24 25 26 27         40 41 42 43         56 57 58 59
# 12 13 14 15       28 29 30 31         44 45 46 47         60 61 62 63

arr = ["SMX_BTN_0", "SMX_BTN_1", "SMX_BTN_2", "SMX_BTN_3", 
     "SMX_BTN_20", "SMX_BTN_21", "SMX_BTN_22", "SMX_BTN_23",
     "SMX_BTN_44", "SMX_BTN_45", "SMX_BTN_46", "SMX_BTN_47",
     "SMX_BTN_56", "SMX_BTN_57", "SMX_BTN_58", "SMX_BTN_59"]
     
for i in arr:
     temp= driver.find_element(By.ID,i).get_attribute("alt")
     password += temp
print("PASSWORD")
print(password)
# driver.find_element_by_name("PASSWORD").send_keys(password)
# button = driver.find_element_by_class_name("btnCustom").click()
driver.close()
driver.quit()
