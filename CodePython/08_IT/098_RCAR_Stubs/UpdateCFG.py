#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                           GenTestAppRCar.py                                  #
# Purpose: Change naming of container in CDF                                   #
################################################################################
import sys,os,re
import argparse
import openpyxl
import json

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------
line_ending = '\r\n'
output_loc = './Output'


# with open("./config.json", "r+") as read_file:
#     init_db = json.load(read_file)

# with open("./template.c", "r+") as read_file:
#     template_content = read_file.readlines()

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------
def write_to_file(file_name, content_array):
    if not os.path.exists(output_loc):
        os.makedirs(output_loc)

    with open(os.path.join(output_loc, file_name), "w+") as write_file:
        # write_file.writelines(content_array)
        # write_file.write(line_ending.join(content_array))
        # write_file.writelines(line_ending)
        write_file.writelines(content_array)

# # ------------------------------------------------------------------------------
# # Read dat form excel
# # ------------------------------------------------------------------------------
# def get_gen_data(file_name, sheet_name):
    # # ret = []
    # # cur_portname = None
    # # out_data = None
    # header = []
    # body = []
    # try:
        # wbook = openpyxl.load_workbook(file_name, data_only=True)
        # ws = wbook.get_sheet_by_name(sheet_name)
        # for row in ws.iter_rows():
            # if "Test Case" == str(row[0].value):
                # # cur_portname = str(row[1].value)
                # # port_mask = 0
                # # output_mask = 0
                # header = [ str(cell.value) if '.' not in str(cell.value) 
                                            # else str(cell.value).split('.')[-1] 
                                            # for cell in row[0:] ]
                # continue

            # tmp = []
            # for cell in row:
                # cell_val = str(cell.value)
                # if 'FALSE' in cell_val or 'TRUE' in cell_val or \
                        # 'False' in cell_val or 'True' in cell_val:
                    # # print(cell_val)
                    # tmp.append(cell_val.lower())
                # # elif 'PORTGROUP' in cell_val :
                # #     tmp.append(cell_val)
                # else :
                    # tmp.append(cell_val)

            # body.append(tmp)
                
                    
        # wbook.close()
    # except Exception as e:
        # print("[E]Issue occurs when opening xlsx file to read")
        # print(e)
        
    # return header, body

# def get_input_command():
#     """Get all input for init process
#     Print help if command is not in valid command

#     Returns:
#         None -- It doesn't return any value
#     """
#     example = '''
#     E.g: ./GenTestAppRCarGen3.py CFG001
#     ./GenTestAppRCarGen3.py CFG009
#     '''
#     parser = argparse.ArgumentParser(description=example)
#     parser.add_argument('cfg', type=str,
#                         help='config to read')
#     # parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
#     #                     help='valid target module: Adc, Can, Dio ...')
#     # parser.add_argument('TestType', type=str, help='type of test: DIT, TIT, VT')
#     # parser.add_argument('TestDevice', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
#     #                     help='target device: H3, V3M, V3H , M3, M3N, V3U, V3Hv2')
#     # parser.add_argument('ARVersion', type=str, choices = ["4_2_2", "4.2.2"], \
#     #                     help='autosar version: 4_2_2')
#     # parser.add_argument('--nc', action='store_true', \
#     #                     help='(optional) skip copy item in WorkspaceSetup.sh', default=False, required=False)
#     # parser.add_argument('OSTest', type=str,
#     #                     help='OS Test option: OS, No_OS, None(TIT only)')
#     # parser.add_argument('--s0s1', type=str, nargs='?',
#     #                     help='(optional) run S0/S1 or normal TC: True, False (default=False)', \
#     #                     default='False', required=False)
#     # parser.add_argument('--target_cfg', type=str, nargs='?',
#     #                     help='(optional) run specific cfg(s)/config type: ex. CFG001,CFG002,DEF001(split by ","), \
#     #                     CFGnnn(all CFG), RCnnn(all RC), DEFnnn(all DEF), xCFGnnn(exclude all CFG), \
#     #                     xRCnnn(exclude all RC), xDEFnnn(exclude all DEF)', default='CFGxxx', required=False)
#     # parser.add_argument('--keep_tc', type=str, nargs='?',
#     #                     help='(optional: True or False) when --target_cfg is used do you want to keep original \
#     #                     TC or not (defalt=False)', default='False', required=False)
#     # parser.add_argument('--ignore_error', type=str, nargs='?',
#     #                     help='(optional: True or False) allow to continue progress when error occurs (defalt=false)', \
#     #                     default='False', required=False)
#     # parser.add_argument('--ice_number', type=str, nargs='?',
#     #                     help='(optional) id number of E2 Emulator to select debugger connection (defalt=null)', \
#     #                     required=False)
#     # parser.add_argument('--build_option', type=str, nargs='?',
#     #                     help='(optional: all/generate_forward/build_only) allow to choose selected actions only \
#     #                     to reduce execution time (defalt=all)', default='all', required=False)
#     # parser.add_argument('--exe_op', type=str, nargs='?',
#     #                     help='(optional: True or False) execution time optimization, not recommended, this should \
#     #                     only be used when everything is confirmed OK (default=False)', default='False', required=False)
#     # parser.add_argument('--cat_map', type=str, nargs='?',
#     #                     help='(optional) use interrupt category in data.json', default='False', required=False)
#     # parser.add_argument('--debug', type=str, nargs='?',
#     #                     help='(optional) turn on script debug mode', default='False', required=False)

#     args = parser.parse_args()
#     return str(args.cfg)

#     # config.msn = str(args.Msn)  # MCAL Msn
#     # config.test_type = str(args.TestType).upper()  # DIT, TIT
#     # config.device = str(args.TestDevice)  # V3H, V3U..
#     # config.ar_version = str(args.ARVersion).replace('_','.')  # 4_2_2, 4_3_1
#     # config.skip_copy_workspace = args.nc

#     # return True



def main():
    """main function of the program"""

    
    parttern = "(DioChannel_PG[\d]{2}_[\d]{2})"
    start_index = 63
    new_value_parttern = "DioChannel_{}"

    template_file_name = "Dio_Cfg035.arxml"
    # template_file_name = data_body[template_row_index][file_name_col]

    with open(template_file_name, "r+") as read_file:
        template_content = read_file.readlines()

    # new_value = new_value_parttern.format(str(start_index).zfill(3))
    # print(new_value)

    new_template_content = list(template_content)
    # while True:

    for row_text, content in enumerate(new_template_content):
        tmp = re.search(parttern, content)
        if tmp is not None :
            # print(content)
            origin_data = tmp.group(1)
            # print(origin_data)
            new_data = new_value_parttern.format(str(start_index).zfill(3))
            new_template_content[row_text] = new_template_content[row_text].replace(origin_data, new_data)

            start_index += 1
            print(new_template_content[row_text])

    # for data in data_body:
    #     check_tag_cnt = 0

    #     if template_file_name == data[file_name_col]:
    #         continue
    #     new_file_name = data[file_name_col]
    #     new_template_content = list(template_content)
    #     for col_idx, cell_data in enumerate(data):
    #         if 1 >= col_idx:
    #             # skip first and second
    #             continue
    #         # print("col_idx: ",col_idx)
    #         # print("cell_data: ",cell_data)
    #         origin_tag = data_header[col_idx]
    #         origin_data = data_body[template_row_index][col_idx]
    #         new_data = cell_data

    #         for row_text, content in enumerate(new_template_content):
    #             if origin_tag in content:
    #                 # replace in next line
    #                 check_tag_cnt += 1
    #                 if origin_data in new_template_content[row_text + 1]:
    #                     new_template_content[row_text + 1] = new_template_content[row_text + 1].replace(origin_data, new_data)
    #                 else:
    #                     print("Cannot file value, exit now")
    #                     print("origin_tag: ",origin_tag)
    #                     print("origin_data:",origin_data)
    #                     print("new_data: ",new_data)
    #                     print("data: ",data)
    #                     print("new_file_name: ",new_file_name)
    #                     sys.exit(-1)
    #                 break
    #     print("Total tag found: ",check_tag_cnt)
        # write_to_file(new_file_name, new_template_content)

    new_file_name = "Dio_Cfg035_new.arxml"
    write_to_file(new_file_name, new_template_content)
    print("Done Write !!!")






if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################