#!/usr/bin/python -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = MCAL Automatic Evaluation System                              #
# Module       = reRunFailCase.py                                              #
# Version      = 1.00                                                          #
# Date         = 03-Dec-2020                                                   #
# AES Version  = 2.1.0(Official Version)                                       #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This script is used to re-run fail case in test report                       #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  20-Jan-2019     : Initial Version                                    #
# V1.01:  15-Aug-2020     : Update template for U2A16 release                  #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------

from xlrd import open_workbook
from copy import copy
import openpyxl
import subprocess
import shlex
import sys
import re
import os
import datetime
import json
import argparse
# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------
update_cdf_flags = False

# -----------------------------------------------------------
# constant section
# -----------------------------------------------------------


# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------
def get_input_command():
    """Get all input for init process
    Print help if command is not in valid command

    Returns:
        None -- It doesn't return any value
    """
    example = '''
    E.g: ./reRunFailCase.py Report_Ex.xls
         ./reRunFailCase.py Report_All.xls
    '''
    parser = argparse.ArgumentParser(description=example)
    # parser.add_argument(
    #     'Msn', type=str, help='module short name of target module: Adc, Can, Dio ...')
    parser.add_argument('TestReportFile', type=str, help='File name of test report generated by AES')
    # parser.add_argument('ARVersion', type=str,
    #                     help='autosar version: 4_2_2, 4_3_1')
    parser.add_argument('-u', '--update_cdf', action='store_true', \
                        help='(optional) update cdf from DescriptionFile', default=False, required=False)
    args = parser.parse_args()

    global update_cdf_flags
    update_cdf_flags = args.update_cdf

    if 'Report' in str(args.TestReportFile):
        return str(args.TestReportFile)
    else:
        return None

# def run_command(command, cwd):
#     """
#     To run cmd command on specific working dir

#     Arguments:
#         command -- the expected commandline and working dir
#         cwd -- the expected working dir

#     Returns:
#         Continues log output
#     """
#     output = ""
#     mes_line = ""
#     if cwd and not os.path.isdir(cwd): 
#         print(" [~] Error: Directory {} does not exist.".format(cwd))
#         sys.exit()
#     process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, cwd=cwd)
#     # wait until process complete and get output message
#     while "" != mes_line or process.poll() is None:
#         mes_line = process.stdout.readline()
#         output += mes_line
#         continue
#     rc = process.poll()
#     # if failed, print notification but not terminate
#     if 0 != rc:
#         print(" [~] Error: Running command '{}' failed.".format(command))
#         # sys.exit()

#     return rc , output.strip()


def run_command(command, cwd):
    """To run cmd command on specific working dir

    Arguments:
        command {string} -- the expected command line
        cwd {path} -- the expected working dir

    Returns:
        string -- the output or the error from cmd
    """
    # process = subprocess.Popen(
    #     shlex.split(str(command)), stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=str(cwd), shell=True)
    process = subprocess.Popen(
        shlex.split(str(command)), stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd)
    
    output, errout = process.communicate()
    process.wait()
    if output:
        print(str(output.decode()).strip())
    if errout:
        print(str(errout.decode()).strip())

    rc = process.poll()

    # return rc , output.strip(), errout.strip()
    return rc , output.strip()



def check_env_file():
    command = 'remake'
    rc, output_cmd = run_command("which {}".format(command), None)
    # print("output_cmd: ",output_cmd)
    if 0 != rc or "no {} in".format(command) in output_cmd:
        print("Not found command")
        return False
    else:
        return True

def get_fail_case_form_test_result(report_file):
    fail_case = []

    # get test result according to test app
    rs_rb = open_workbook(report_file)
    rs_ws = rs_rb.sheet_by_index(0)
    for row in range(3, rs_ws.nrows):
        # exp_dict.update({str(rs_ws.row_values(row)[0]).split("/")[1]: str(rs_ws.row_values(row)[1])})
        if 'NG' in str(rs_ws.row_values(row)[1]):
            fail_case.append(str(rs_ws.row_values(row)[0]).split("/")[1])
    return fail_case

def read_cdf_in_test_specs(test_case_name):
    LTestSpecPath = "Test_Spec.xls"
    LSheet_name = "Test_Spec"

    if os.path.isfile(LTestSpecPath):  # open Test_Spec.xls file
        LBook = open_workbook(LTestSpecPath)  # Open file
    else:
        print('Can not find parameter file.\r\n')
        sys.exit(1)

    try:
        LSheet = LBook.sheet_by_name(LSheet_name)  # Test_Spec sheet open
        # GStderrPrint(str(GSheet.ncols) + ' , ' + str(GSheet.nrows) + '\r\n')
    except:
        print('Can not open sheet:"Test_Spec".\r\n')
        sys.exit(1)

    cdf_column_index = -1
    spec_number_index = -1
    for col_idx in range(LSheet.ncols):
        col_arr = LSheet.col_values(col_idx)
        if 'CDF' in str(col_arr[0]):
            cdf_column_index = col_idx
        if 'Spec Number' in str(col_arr[0]):
            spec_number_index = col_idx
        if -1 != cdf_column_index and -1 != spec_number_index:
            break

    spec_num_target = convert_test_case_name_to_spec_number(test_case_name)
    for row in range(1, LBook.nrows):
        tmp_spec= str(LBook.row_values(row)[spec_number_index])
        if spec_num_target.upper() in tmp_spec.upper():
            return str(LBook.row_values(row)[cdf_column_index]).split()

    return None


def convert_test_case_name_to_spec_number(test_case_name):
    tmp1 = test_case_name[:-3]
    tmp2 = test_case_name[-3:]
    return '_'.join([tmp1, tmp2])


def rerun_test_case(test_case_name, update_cdf=False):

    # TODO: read test_specs to update all cdf
    if update_cdf:
        cdf_list = read_cdf_in_test_specs(test_case_name)
        if cdf_list is not None:
            for cdf_name in cdf_list:
                copy_cdf_name_test_case(test_case_name, cdf_name)
        else:
            print("cannot parse in test specs: ", test_case_name)
            sys.exit(1)

    print("rerun tc {}".format(test_case_name))
    run_command("remake {}".format(test_case_name), None)

def copy_cdf_name_test_case(test_case_name, cdf_name):
    src_path = "DescriptionFile/{}".format(cdf_name)
    des_path = "EvalEnvelope/{0}/cdf/{1}".format(test_case_name, cdf_name)
    run_command("cp -f {0} {1}".format(src_path, des_path), None)



def main():
    """main function of the program

    Returns:
        int -- status of the main function. 1 is successful
    """
    report_file = get_input_command()
    if report_file is not None:
        if check_env_file():
            fail_case_list = get_fail_case_form_test_result(report_file)
            print("fail_case_list: ",fail_case_list)
            # for test_case in fail_case_list:
            #     rerun_test_case(test_case, update_cdf_flags)

    return None

# main processing
if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
