#!/bin/sh
# :: AUTHOR : FUSA/TOOL VIETNGUYEN
# :: DATE   : NOV 11 2016
# ::                       CDFs RENEW TOOL.
# ::
# ::  RENEW CDFs BASE ON OLD CDFs AND NEW PDF.
# ::  - Convert from ECU SPECTRUM v4.0 to DAVINCI V5.12 format.
# ::  - Remove unused containers and parameters.
# ::  - Update new containers and parameters in new base PDF.
# ::  
# ::############################################################################

echo ""
echo "################################################################################"
echo "## Auto-generated                                                               "
echo "##                           UUID REPORT                                        "
echo "##                                                                              "
echo "## FUSA/TOOL"
echo "## Date  : $(date)"
echo "################################################################################"
echo ""

passpath="$(cygpath -C ANSI -u "${1}")"
# Extract only uuid
# List of new uuid in XML
UUIDLIST=()
# Check the unique of UUID.
uuid_contains () { 
    local uuid_list="$1[@]"
    local seeking=$2
    local in=1
    
    for element in "${!uuid_list}"; do
        if [[ $element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}

uuid_replace () {
    
    echo ${UUIDLIST[@]}
    if [[ -f uuid.tmp ]]; then
         read -a UUIDLIST < uuid.tmp
    fi

    for i in `grep ........\-....\-....\-............ "$1" | \
              sed -e s/\"/" "/g | \
              awk {'print $3'}`
    do
        # To ensure UUID is not dupplicated.
        let UUIDFound=1
        while [[ $UUIDFound == 1 ]]; do

             UUID=`uuidgen -r`
             
             uuid_contains UUIDLIST $UUID &&  let UUIDFound=1 || let UUIDFound=0
        done
        sed -i 0,/$i/{s/$i/$UUID/} "$1"
        UUIDLIST+=($UUID)
        echo $i '->' $UUID
    done

    # Convert from LF to CR+LF
    unix2dos $1

    echo "["$(basename "$1")"] END RENEW UUIDs."
    echo "---"
    # echo ${UUIDLIST[@]}
    echo ${UUIDLIST[@]} > uuid.tmp
}

# Installation check
which uuidgen > /dev/null 2>&1
if [ $? = 1 ];
then
  echo "ERROR: Please install uuidgen from the Cygwin package util-linux."
  exit 1
fi
echo "["$(basename "$1")"] START RENEW UUIDs."


echo "Creating UUID backup..."
if [ -d "$passpath/uuid_backups" ] ;then
    echo "---"
else
    echo "---"
    mkdir "$passpath/uuid_backups"
fi
cp -fr "${passpath}"/*arxml "$passpath/uuid_backups"
echo "backup DONE!"
echo "START RENEW UUIDs"

export -f uuid_replace
export -f uuid_contains

find "${passpath}" -maxdepth 1 -name '*.arxml' -type f -exec bash -c 'uuid_replace "{}"' \;
rm -fv uuid.tmp
echo "DONE!"
sleep 5
exit
