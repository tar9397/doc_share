#!/usr/bin/python -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = MCAL Automatic Evaluation System                              #
# Module       = report.py                                                     #
# Version      = 1.01                                                          #
# Date         = 15-Aug-2020                                                   #
# AES Version  = 2.1.0(Official Version)                                       #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This script is used to generate the report from all results in workspace     #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  20-Jan-2019     : Initial Version                                    #
# V1.01:  15-Aug-2020     : Update template for U2A16 release                  #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------

from xlrd import open_workbook
from copy import copy
import openpyxl
from openpyxl.styles import Alignment, Font
import subprocess
import shlex
import sys
import re
import os
import datetime
import json
import argparse

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------

# Msn = "" # MCAL msn
# TestType = ""  # DIT, TIT
# TestDeviceType = "" # 702300EBBG, 702300EBBB..
# TestDevice = "" # U2A16, E2UH, E2H, U2B
# ARVersion = "" # 4_2_2, 4_3_1
# MsnReportFile = "" # Name of output file
# MsnOriginalTP = "" # Name of module test spec
# TestCaseList = [] # List of TC item number
# RevisionInfo = [] # Revision info get from test spec
# TestVersion = "" # Version of test spec
# StackSize = 0 # Worth case of stack depth
# RamRomUsage = {} # Dictionary contain value of ROM/RAM usage
# ExecutionTime = {} # Dictionary of each API/ISR execution time
# TestTargetsInfo = [] # List of target info from test spec
# TestEnvironment = {} # Dictionary of test environment info from test spec
# -----------------------------------------------------------
# constant section
# -----------------------------------------------------------
# Load the data base stored in the file to the program
# try:
#     with open("./data.json", "r+") as read_file:
#         init_db = json.load(read_file)
# except:
#     print("An exception occurred: data.json is not existed")
# Root repo directory
# ROOT = r"./../../../../../"
# Directory to template xlsx file
# Constant string values
# EXE_TIME = "Execution time"
# ROM_DET = "ROM_DET"
# RAM_DET = "RAM_DET"
# ROM_NO_DET = "ROM_NO_DET"
# RAM_NO_DET = "RAM_NO_DET"
# STACK_DET = "STACK_DET"
# STACK_NO_DET = "STACK_NO_DET"
# ITEM_TYPE = [EXE_TIME, ROM_DET, RAM_DET, ROM_NO_DET, RAM_NO_DET, STACK_DET, STACK_NO_DET]
# MODULE_INDEX = {"ADC": 4 , "CAN": 5 , "DIO": 6 , "ETH": 7 , "FLS": 8 , "FR": 9, "GPT": 10, "ICU": 11,
#                 "LIN": 12, "MCU": 13, "PORT": 14, "PWM": 15, "SPI": 16, "WDG": 17}
# ------------------------------------------------------------------------------
# Class section
# ------------------------------------------------------------------------------
class Config_IT:
    msn = ""
    test_type = ""  # DIT, TIT, VT
    device = ""  # V3U, V3H.
    ar_version = ""  # 4_2_2, 4_3_1
    def __init__(self):
        pass


# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------


def get_input_command(config):
    """Get all input for init process
    Print help if command is not in valid command

    Returns:
        None -- It doesn't return any value
    """
    example = '''
    E.g: ./report.py Dio DIT V3U 4_2_2
    ./report.py Dio DIT V3H4_2_2
    '''
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
                        help='valid target module: Adc, Can, Dio ...')
    parser.add_argument('TestType', type=str, help='type of test: DIT, TIT, VT')
    parser.add_argument('TestDevice', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
                        help='target device: H3, V3M, V3H , M3, M3N, V3U, V3Hv2')
    parser.add_argument('ARVersion', type=str, choices = ["4_2_2", "4.2.2"], \
                        help='autosar version: 4_2_2')



    args = parser.parse_args()

    config.msn = str(args.Msn)  # MCAL Msn
    config.test_type = str(args.TestType).upper()  # DIT, TIT
    config.device = str(args.TestDevice)  # V3H, V3U..
    config.ar_version = str(args.ARVersion).replace('_','.')  # 4_2_2, 4_3_1

    return True

def run_command(command, cwd):
    """
    To run cmd command on specific working dir

    Arguments:
        command -- the expected commandline and working dir
        cwd -- the expected working dir

    Returns:
        Continues log output
    """
    output = ""
    mes_line = ""
    if cwd and not os.path.isdir(cwd): 
        print(" [~] Error: Directory {} does not exist.".format(cwd))
        sys.exit()
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, cwd=cwd)
    # wait until process complete and get output message
    while "" != mes_line or process.poll() is None:
        mes_line = process.stdout.readline()
        output += mes_line
        continue
    rc = process.poll()
    # if failed, print notification but not terminate
    if 0 != rc:
        print(" [~] Error: Running command '{}' failed.".format(command))
        # sys.exit()
    return output.strip()


def get_unique_list(_input_list):
    """
    Get list contains unique element from input list

    Arguments:
        _input_list -- input list

    Returns:
        sorted list contains unique element form input list
    """
    _output_list = []
    for element in _input_list:
        if not element in _output_list:
            _output_list.append(element)
    return sorted(_output_list)

def r_str_replace(input_str, old, new, count=1):
    return new.join(input_str.rsplit(old, count))

def get_test_result(config, test_specs='Test_spec.xlsx'):
    """
    Get test result from exported file

    Arguments:
        None

    Returns:
        Dictionary of test result for each test case
    """
    # global TestCaseList
    result_dict = {}
    exp_dict = {}
    test_case_list = {}
    sp_data = []
    # get test result according to test app
    # {"Test case item nuber": "Test case result(OK/NG/other)"}
    print("  Generate report file and get test result of all test items")

    report_ex_file = 'Report_Ex.xls'
    # TODO: check Report_Ex first
    if not os.path.exists(report_ex_file):
        run_command("make report", None)
    rs_rb = open_workbook(report_ex_file)
    rs_ws = rs_rb.sheet_by_index(0)
    for row in range(3, rs_ws.nrows):
        exp_dict.update({str(rs_ws.row_values(row)[0]).upper().split("/")[1]: str(rs_ws.row_values(row)[1])})
    # get list of test case according to original test spec
    # {"test case ID": ["Test case item name", "Test case description", "CFG001", ..]}
    print("  Get list of test case from test spec")
    sp_rb = open_workbook(test_specs)
    sp_ws = sp_rb.sheet_by_name("Test_Spec")

    header = sp_ws.row_values(0)
    sp_data = [sp_ws.row_values(row) for row in range(sp_ws.nrows)
                    if (sp_ws.cell(row, 0).value != '') and (sp_ws.cell(row, 0).value != '-') 
                    and str((sp_ws.cell(row, header.index(config.device.upper())).value).upper() == 'O') ]
    
    for row in range(1, len(sp_data)):
        if "" != str(sp_data[row][sp_data[0].index("Spec Number")]):
            test_case_ID = str(sp_data[row][sp_data[0].index("Spec Number")])
            # test_case_item = str(sp_data[row][sp_data[0].index("Category")]).replace("_", "") + \
            #                  "{0:03}".format(int(sp_data[row][sp_data[0].index("Item number")]))
            # test_case_list.update({test_case_ID: [test_case_item, sp_data[row][sp_data[0].index("Content")]]})
            test_case_list.update({test_case_ID: sp_data[row][sp_data[0].index("Content")]})


    # print(test_case_list)
    # print(exp_dict)
    # sys.exit(1)


    TestCaseList = sorted(test_case_list.keys())
    # combine content of 2 above lists and return result_dict after combination
    # {"test case ID": ["Test case description", {"Config ID": ["Test result(PASSED/FAILED/NONE)", "Remark for config"]}, ""Remark for test case]}
    print("  Merge list of test case and result of test items")
    for test_case in sorted(test_case_list.keys()):
        # init element for each test case
        result_dict.update({test_case: [test_case_list[test_case], "NONE", "-"]})
        test_case_remark = ""
        # test_case_cfg_dict = {}
        # test_case_tested = False
        test_case_passed = True
        # for cfg in range(2, len(test_case_list[test_case])):
        #     # if "DIT" == TestType:
        #     #     test_item_name = Msn.upper() + "_" + test_case_list[test_case][0] + "_" + \
        #     #                  test_case_list[test_case][cfg] + "_" + TestDevice
        #     # else:
        #     #     test_item_name = Msn.upper() + "_" + test_case_list[test_case][0] + "_" + TestDevice
        #     test_item_name = config.msn.upper() + "_" + test_case_list[test_case][0]
        #     # start checking result for each test item
        #     if test_item_name in exp_dict.keys():
        #         # test ID exists in exported report, good! check it out!
        #         # test_case_tested = True
        #         test_item_result = str(exp_dict[test_item_name])
        #         if "OK" not in test_item_result:
        #             test_case_cfg_dict.update({test_item_name: ["FAILED", "Test case is failed in debugging"]})
        #         else:
        #             test_case_cfg_dict.update({test_item_name: ["PASSED", "-"]})
        #     # test ID doesn't exist in exported report, maybe it was not tested nor passed in compilation 
        #     elif not os.path.isfile(os.path.join("./EvalEnvelope", test_item_name)+'/obj/target.out') \
        #             and not os.path.isfile(os.path.join("./EvalEnvelope", test_item_name)+'/Result.txt') \
        #             and os.path.isfile(os.path.join("./EvalEnvelope", test_item_name)+'/log/Build.log'):
        #         print("  >>Test case {0} failed compilation, test case name in TS: {1}".format(test_item_name, test_case))
        #         test_case_cfg_dict.update({test_item_name: ["FAILED", "Test case is failed in compilation"]})
        #         # test_case_tested = True
        #     else:
        #         test_case_cfg_dict.update({test_item_name: ["NONE", "Test case is not tested in " + test_case_list[test_case][cfg]]})


        test_item_name_in_report = r_str_replace(test_case, '_', '')
        # start checking result for each test item
        if test_item_name_in_report.upper() in exp_dict.keys():
            # test ID exists in exported report, good! check it out!
            # test_case_tested = True
            test_item_result = str(exp_dict[test_item_name_in_report.upper()])
            if "OK" in test_item_result:
                # test_case_cfg_dict.update({test_case: ["PASSED", "-"]})
                result_dict[test_case][1] = "PASSED"
                result_dict[test_case][2] = "-"
            else:
                # test_case_cfg_dict.update({test_case: ["FAILED", "Test case is failed in debugging"]})
                result_dict[test_case][1] = "FAILED"
                result_dict[test_case][2] = "Test case is failed in debugging"
        # test ID doesn't exist in exported report, maybe it was not tested nor passed in compilation 
        elif not os.path.isfile(os.path.join("./EvalEnvelope", test_item_name_in_report)+'/obj/target.out') \
                and not os.path.isfile(os.path.join("./EvalEnvelope", test_item_name_in_report)+'/Result.txt') \
                and os.path.isfile(os.path.join("./EvalEnvelope", test_item_name_in_report)+'/log/Build.log'):
            print("  >>Test case {0} failed compilation, test case name in TS: {1}".format(test_item_name_in_report, test_case))
            # test_case_cfg_dict.update({test_case: ["FAILED", "Test case is failed in compilation"]})
            result_dict[test_case][1] = "FAILED"
            result_dict[test_case][2] = "Test case is failed in debugging"
            # test_case_tested = True
        else:
            # test_case_cfg_dict.update({test_case: ["NONE", "Test case is not tested."]})
            result_dict[test_case][1] = "NONE"
            result_dict[test_case][2] = "Test case is not tested."
            # print(test_item_name_in_report)
            # print('*'*40)

    # print(exp_dict.keys())

        # # after calculating related info, move them into return dictionary
        # if False == test_case_tested:
        #     result_dict[test_case][2] = "Test case is not tested in any Configuration"
        # else:
        #     result_dict[test_case][1] = test_case_cfg_dict
    return result_dict

    
def update_test_result(config, _wbook):
    """
    Update test result into sheet 'Test_Result'

    Arguments:
        _wbook -- input sheet

    Returns:
        None
    """
    # Step 1: Update test result into sheet 'Test_Result'
    print("*"*120)
    print("[~] Step 1: Update test result into sheet 'Test_Result'")
    # parse test result from EvaEnvelope folder
    result_dict = get_test_result(config)
    wsheet = _wbook.get_sheet_by_name("Test_Result")
    # wsheet.cell(row = 3, column = 6).value = Msn.upper()
    # wsheet.cell(row = 4, column = 6).value = TestEnvironment[u'Tested Software Version']
    # wsheet.cell(row = 5, column = 6).value = TestEnvironment[u'Release Version']

    start_row_of_test_result_in_template = 17

    line_index = 1
    case_index = 1
    # loop for each test case and write the result into row
    for test_case in sorted(result_dict.keys()):
        st_row = start_row_of_test_result_in_template + line_index

        # if isinstance(result_dict[test_case][1], dict):
        #     for cfg_id in sorted(result_dict[test_case][1].keys()):
        #         wsheet.cell(row = st_row, column = 5).value = cfg_id
        #         wsheet.cell(row = st_row, column = 6).value = result_dict[test_case][1][cfg_id][0]
        #         wsheet.cell(row = st_row, column = 7).value = result_dict[test_case][1][cfg_id][1]
        #         for col in range(2, 8):
        #             wsheet.cell(row = st_row, column = col)._style = \
        #                         copy(wsheet.cell(row = start_row_of_test_result_in_template + 1, column = col)._style)
        #         line_index += 1
        #     wsheet.merge_cells('B{0}:B{1}'.format(st_row, 14 + line_index))
        #     wsheet.merge_cells('C{0}:C{1}'.format(st_row, 14 + line_index))
        #     wsheet.merge_cells('D{0}:D{1}'.format(st_row, 14 + line_index))
        #     wsheet.cell(row = st_row, column = 3).value = test_case
        #     wsheet.cell(row = st_row, column = 2).value = str(case_index)
        #     wsheet.cell(row = st_row, column = 4).value = result_dict[test_case][0]
        #     case_index += 1
        # else:
        wsheet.cell(row = st_row, column = 2).value = str(case_index)
        wsheet.cell(row = st_row, column = 3).value = test_case
        wsheet.cell(row = st_row, column = 4).value = result_dict[test_case][0]
        wsheet.cell(row = st_row, column = 4).alignment = Alignment(wrap_text=True)
        # wsheet.cell(row = st_row, column = 5).value = "-"
        wsheet.cell(row = st_row, column = 5).value = result_dict[test_case][1]
        wsheet.cell(row = st_row, column = 6).value = result_dict[test_case][2]
        for col in range(2, 8):
            wsheet.cell(row = st_row, column = col)._style = \
                            copy(wsheet.cell(row = start_row_of_test_result_in_template + 1, column = col)._style)
        line_index += 1
        case_index += 1
    print("  Sheet 'Test_Result' is updated")
    print("*"*120)
    return True


# def get_performance_spec():
#     """
#     Get list of measured item from spec

#     Arguments:
#         None

#     Returns:
#         Dictionay of measured item info
#     """
#     res_dict = {type : [] for type in ITEM_TYPE}
#     # get list of item performance info from spec as format:
#     # ["item name", "item type", "test case name used to measure item", "CFGxxx", "expected value for item"]
#     print("  Get performance spec from test spec")
#     sp_rb = open_workbook(MsnOriginalTP)
#     sp_ws = sp_rb.sheet_by_name("Performance_Spec")
#     try:
#         device_col_index = sp_ws.row_values(0).index(TestDevice)
#     except:
#         print("  [~] Error: Couldn't get performance specification for" + TestDevice)
#         sys.exit()
#     sp_data = [sp_ws.row_values(row)[1 : device_col_index + 1] for row in range(1, sp_ws.nrows) \
#                if (sp_ws.col_values(device_col_index)[row] != "-") and (sp_ws.col_values(device_col_index)[row] != "")]
#     # Validate spec data
#     for row in sp_data:
#         if "" == row[0] or "-" == row[0]:
#             print("  [~] Error: Item name is missing at item No. " + str(sp_data.index(row) + 1))
#             sys.exit()
#         elif row[1] not in ITEM_TYPE:
#             print("  [~] Error: Item type is missing or invalid at item No. " + str(sp_data.index(row) + 1))
#             sys.exit()
#         elif row[2] not in TestCaseList:
#             print("  [~] Error: Item test case ID is missing or invalid at item No. " + str(sp_data.index(row) + 1))
#             sys.exit()
#         elif not re.search("(CFG|RC)([\d]{3})", row[3]):
#             print("  [~] Error: Item CDF name is missing or invalid at item No. " + str(sp_data.index(row) + 1))
#             sys.exit()
#         else:
#             pass
#     if 0 == len(sp_data):
#         print("  [~] Error: No available spec found")
#         sys.exit()
#     # Procedure data before return value
#     # {"Type": ["item name", "name of executed test case", "expected value for item"]}
#     for type in ITEM_TYPE:
#         type_list = [sp_data[i] for i in range(len(sp_data)) if sp_data[i][1] == type]
#         if 0 != len(type_list):
#             for item in type_list:
#                 parsed_name = str(item[2].strip()).split("_")
#                 if 5 == len(parsed_name):
#                     test_name = parsed_name[0] + "_" + parsed_name[2] + parsed_name[3] + \
#                                 "{0:03}_".format(int(parsed_name[4])) + item[3] + "_" + TestDevice
#                 elif 4 == len(parsed_name):
#                     test_name = parsed_name[0] + "_" + parsed_name[2] + "{0:03}_".format(int(parsed_name[3])) + \
#                                 item[3] + "_" + TestDevice
#                 else:
#                     pass
#                 temp_list = [item[0].strip(), test_name, item[-1]]
#                 res_dict[type].append(temp_list)
#         else:
#             # Every type must have al least one available item
#             print("  [~] Error: There is no item for {} in performance spec".format(type))
#             sys.exit()
#     return res_dict


# def get_memory_value(_item, _type, _test_case):
#     """
#     Get performance value of item

#     Arguments:
#         _item -- input measured item
#         _type -- input measured type
#         _test_case -- test case name used in measurement

#     Returns:
#         None
#     """
#     result_value = ""
#     # switch type of item then get the result value according to _type
#     if  EXE_TIME == _type:
#         temp = [0]
#         try:
#             # open debugger test log and parse the result of each item
#             file = open("./EvalEnvelope/" + _test_case + "/log/MULTI_Debugger.log")
#             lines = [x for x in file.readlines() if _item in x]
#             for line in lines:
#                 if re.findall("\d+\.\d+", line):
#                     temp.extend(re.findall("\d+\.\d+", line))
#             file.close()
#         except:
#             print("  Parse execution time failed for " + _item)
#         # return the maximum value of execution time if item is executed many times
#         result_value = "{:10.4f}".format(float(max(temp)))
#     else:
#         if ROM_NO_DET == _type or ROM_DET == _type or RAM_NO_DET == _type or RAM_DET == _type:
#             try:
#                 # use gsize to parse value of memory type comsumption in the output file
#                 cmd = "./gsize -all " + init_db["workspace"] + "/EvalEnvelope/" + _test_case + "/obj/target.out"
#                 ret_list = run_command(cmd, init_db["compiler_dir"]).split()
#                 result_value = ret_list[ret_list.index("." + _item) + 1]
#             except:
#                 print("  Execute command: " + cmd + " in " + init_db["compiler_dir"])
#                 print("  Parsing ROM/RAM failed for " + _item)
#         elif STACK_NO_DET == _type or STACK_DET == _type:
#             try:
#                 # use gstack to parse value of stack size using by item
#                 cmd = "./gstack --legacy -ez "+ _item + " " + init_db["workspace"] + "/EvalEnvelope/" + _test_case + \
#                       "/obj/target.out"
#                 result_value = run_command(cmd, init_db["compiler_dir"])
#             except:
#                 print("  Execute command: " + cmd + " in " + init_db["compiler_dir"])
#                 print("  Parsing Stack depth failed for " + _item)
#         else:
#             pass
#     if "" != result_value:
#         pass
#     else:
#         result_value = "0"
#     return result_value


# def update_measurement_result(_wbook):
#     """
#     Update sheet 'Measurement_Result'

#     Arguments:
#         _wbook -- input sheet

#     Returns:
#         None
#     """
#     if "DIT" == TestType:
#         global StackSize
#         global RamRomUsage
#         global ExecutionTime
#         stack_depth_list = []
#         start_offset = 0
#         end_offset = 0
#         # Step 2: Update test result into sheet 'Measurement_Result'
#         print("*"*120)
#         print("[~] Step 2: Update typical config result into sheet 'Measurement_Result'")
#         item_info_dict = get_performance_spec()
#         wsheet = _wbook.get_sheet_by_name("Measurement_Result")
#         # Update content of section 4.1  TYPICAL CONFIGURATION
#         typ_config = wsheet.cell(row=3, column=2).value
#         api_exe_tc_names = [item[1] for item in item_info_dict[EXE_TIME] if not item[0].endswith("_ISR")]
#         typ_config = typ_config.replace("{API_EXE_TIME}", ', '.join(get_unique_list(api_exe_tc_names)))
#         isr_exe_tc_names = [item[1] for item in item_info_dict[EXE_TIME] if item[0].endswith("_ISR")]
#         typ_config = typ_config.replace("{ISR_EXE_TIME}", ', '.join(get_unique_list(isr_exe_tc_names)))
#         ram_rom_det_exe_tc_names = [item[1] for item in item_info_dict[RAM_DET]]
#         ram_rom_det_exe_tc_names.extend([item[1] for item in item_info_dict[ROM_DET]])
#         typ_config = typ_config.replace("{RAM_ROM_DET}", ', '.join(get_unique_list(ram_rom_det_exe_tc_names)))
#         ram_rom_no_det_exe_tc_names = [item[1] for item in item_info_dict[RAM_NO_DET]]
#         ram_rom_no_det_exe_tc_names.extend([item[1] for item in item_info_dict[ROM_NO_DET]])
#         typ_config = typ_config.replace("{RAM_ROM_NO_DET}", ', '.join(get_unique_list(ram_rom_no_det_exe_tc_names)))
#         stk_det_exe_tc_names = [item[1] for item in item_info_dict[STACK_DET]]
#         typ_config = typ_config.replace("{STK_DET}", ', '.join(get_unique_list(stk_det_exe_tc_names)))
#         stk_no_det_exe_tc_names = [item[1] for item in item_info_dict[STACK_NO_DET]]
#         typ_config = typ_config.replace("{STK_NO_DET}", ', '.join(get_unique_list(stk_no_det_exe_tc_names)))
#         wsheet.cell(row=3, column=2).value = str(typ_config)
#         # Update content of section 4.2  MEMORY USAGE
#         # Go with ROM&RAM with DET ON first
#         index = 0
#         expected_total = 0
#         actual_total = 0
#         RamRomUsage.update({ROM_DET: []})
#         end_offset += len(item_info_dict[ROM_DET])
#         wsheet.insert_rows(11 + start_offset, len(item_info_dict[ROM_DET]))
#         for item in item_info_dict[ROM_DET]:
#             actual_val = get_memory_value(item[0], ROM_DET, item[1])
#             wsheet.cell(row=11+start_offset+index, column=2).value = str(index+1)
#             wsheet.cell(row=11+start_offset+index, column=4).value = str(item[0])
#             wsheet.cell(row=11+start_offset+index, column=5).value = int(item[2])
#             wsheet.cell(row=11+start_offset+index, column=6).value = "-"
#             wsheet.cell(row=11+start_offset+index, column=7).value = int(actual_val)
#             wsheet.cell(row=11+start_offset+index, column=8).value = "-"
#             wsheet.cell(row=11+start_offset+index, column=9).value = "-"
#             wsheet.cell(row=11+start_offset+index, column=10).value = "-"
#             wsheet.cell(row=11+start_offset+index, column=11).value = "-"
#             for col in range(2, 12):
#                 wsheet.cell(row=11+start_offset+index, column = col)._style = \
#                                        copy(wsheet.cell(row = 11+end_offset, column = col)._style)
#             index +=1
#             expected_total += int(item[2])
#             actual_total += int(actual_val)
#             RamRomUsage[ROM_DET].append([item[0], int(actual_val)])
#         wsheet.merge_cells(start_row=11+start_offset, start_column=3, end_row=11+end_offset, end_column=3)
#         wsheet.cell(row=11+start_offset, column=3).value = "ROM"
#         wsheet.cell(row=11+start_offset+index, column=5).value = expected_total
#         wsheet.cell(row=11+start_offset+index, column=7).value = actual_total
#         wsheet.cell(row=11+start_offset+index, column=8).value = "=G{0}/E{0}".format(11+end_offset)
#         wsheet.cell(row=11+start_offset+index, column=9).value = \
#                     "=IF(OR(H{0}<0.9, H{0}>1.1),\"Need Justification\", \"Not Need Justification\")".format(11+end_offset)
#         wsheet.cell(row=11+start_offset+index, column=10).value = "-"
#         wsheet.cell(row=11+start_offset+index, column=11).value = ", ".join(get_unique_list(ram_rom_det_exe_tc_names))
#         start_offset += len(item_info_dict[ROM_DET])
#         sub_index = 0
#         expected_total = 0
#         actual_total = 0
#         RamRomUsage.update({RAM_DET: []})
#         end_offset += len(item_info_dict[RAM_DET])
#         wsheet.insert_rows(12 + start_offset, len(item_info_dict[RAM_DET]))
#         for item in item_info_dict[RAM_DET]:
#             actual_val = get_memory_value(item[0], RAM_DET, item[1])
#             wsheet.cell(row=12+start_offset+sub_index, column=2).value = str(index+1)
#             wsheet.cell(row=12+start_offset+sub_index, column=4).value = str(item[0])
#             wsheet.cell(row=12+start_offset+sub_index, column=5).value = int(item[2])
#             wsheet.cell(row=12+start_offset+sub_index, column=6).value = "-"
#             wsheet.cell(row=12+start_offset+sub_index, column=7).value = int(actual_val)
#             wsheet.cell(row=12+start_offset+sub_index, column=8).value = "-"
#             wsheet.cell(row=12+start_offset+sub_index, column=9).value = "-"
#             wsheet.cell(row=12+start_offset+sub_index, column=10).value = "-"
#             wsheet.cell(row=12+start_offset+sub_index, column=11).value = "-"
#             for col in range(2, 12):
#                 wsheet.cell(row=12+start_offset+sub_index, column = col)._style = \
#                                        copy(wsheet.cell(row = 12+end_offset, column = col)._style)
#             index +=1
#             sub_index +=1
#             expected_total += int(item[2])
#             actual_total += int(actual_val)
#             RamRomUsage[RAM_DET].append([item[0], int(actual_val)])
#         wsheet.merge_cells(start_row=12+start_offset, start_column=3, end_row=12+end_offset, end_column=3)
#         wsheet.cell(row=12+start_offset, column=3).value = "RAM"
#         wsheet.cell(row=12+start_offset+sub_index, column=5).value = expected_total
#         wsheet.cell(row=12+start_offset+sub_index, column=7).value = actual_total
#         wsheet.cell(row=12+start_offset+sub_index, column=8).value = "=G{0}/E{0}".format(12+end_offset)
#         wsheet.cell(row=12+start_offset+sub_index, column=9).value = \
#                     "=IF(OR(H{0}<0.9, H{0}>1.1),\"Need Justification\", \"Not Need Justification\")".format(12+end_offset)
#         wsheet.cell(row=12+start_offset+sub_index, column=10).value = "-"
#         wsheet.cell(row=12+start_offset+sub_index, column=11).value = ", ".join(get_unique_list(ram_rom_det_exe_tc_names))
#         start_offset += len(item_info_dict[RAM_DET])
#         # Continue with ROM&RAM with DET OFF
#         index = 0
#         expected_total = 0
#         actual_total = 0
#         RamRomUsage.update({ROM_NO_DET: []})
#         end_offset += len(item_info_dict[ROM_NO_DET])
#         wsheet.insert_rows(16 + start_offset, len(item_info_dict[ROM_NO_DET]))
#         for item in item_info_dict[ROM_NO_DET]:
#             actual_val = get_memory_value(item[0], ROM_NO_DET, item[1])
#             wsheet.cell(row=16+start_offset+index, column=2).value = str(index+1)
#             wsheet.cell(row=16+start_offset+index, column=4).value = str(item[0])
#             wsheet.cell(row=16+start_offset+index, column=5).value = int(item[2])
#             wsheet.cell(row=16+start_offset+index, column=6).value = "-"
#             wsheet.cell(row=16+start_offset+index, column=7).value = int(actual_val)
#             wsheet.cell(row=16+start_offset+index, column=8).value = "-"
#             wsheet.cell(row=16+start_offset+index, column=9).value = "-"
#             wsheet.cell(row=16+start_offset+index, column=10).value = "-"
#             wsheet.cell(row=16+start_offset+index, column=11).value = "-"
#             for col in range(2, 12):
#                 wsheet.cell(row=16+start_offset+index, column = col)._style = \
#                                        copy(wsheet.cell(row = 16+end_offset, column = col)._style)
#             index +=1
#             expected_total += int(item[2])
#             actual_total += int(actual_val)
#             RamRomUsage[ROM_NO_DET].append([item[0], int(actual_val)])
#         wsheet.merge_cells(start_row=16+start_offset, start_column=3, end_row=16+end_offset, end_column=3)
#         wsheet.cell(row=16+start_offset, column=3).value = "ROM"
#         wsheet.cell(row=16+start_offset+index, column=5).value = expected_total
#         wsheet.cell(row=16+start_offset+index, column=7).value = actual_total
#         wsheet.cell(row=16+start_offset+index, column=8).value = "=G{0}/E{0}".format(16+end_offset)
#         wsheet.cell(row=16+start_offset+index, column=9).value = \
#                     "=IF(OR(H{0}<0.9, H{0}>1.1),\"Need Justification\", \"Not Need Justification\")".format(16+end_offset)
#         wsheet.cell(row=16+start_offset+index, column=10).value = "-"
#         wsheet.cell(row=16+start_offset+index, column=11).value = ", ".join(get_unique_list(ram_rom_no_det_exe_tc_names))
#         start_offset += len(item_info_dict[ROM_NO_DET])
#         sub_index = 0
#         expected_total = 0
#         actual_total = 0
#         RamRomUsage.update({RAM_NO_DET: []})
#         end_offset += len(item_info_dict[RAM_NO_DET])
#         wsheet.insert_rows(17 + start_offset, len(item_info_dict[RAM_NO_DET]))
#         for item in item_info_dict[RAM_NO_DET]:
#             actual_val = get_memory_value(item[0], RAM_NO_DET, item[1])
#             wsheet.cell(row=17+start_offset+sub_index, column=2).value = str(index+1)
#             wsheet.cell(row=17+start_offset+sub_index, column=4).value = str(item[0])
#             wsheet.cell(row=17+start_offset+sub_index, column=5).value = int(item[2])
#             wsheet.cell(row=17+start_offset+sub_index, column=6).value = "-"
#             wsheet.cell(row=17+start_offset+sub_index, column=7).value = int(actual_val)
#             wsheet.cell(row=17+start_offset+sub_index, column=8).value = "-"
#             wsheet.cell(row=17+start_offset+sub_index, column=9).value = "-"
#             wsheet.cell(row=17+start_offset+sub_index, column=10).value = "-"
#             wsheet.cell(row=17+start_offset+sub_index, column=11).value = "-"
#             for col in range(2, 12):
#                 wsheet.cell(row=17+start_offset+sub_index, column = col)._style = \
#                                        copy(wsheet.cell(row = 17+end_offset, column = col)._style)
#             index +=1
#             sub_index +=1
#             expected_total += int(item[2])
#             actual_total += int(actual_val)
#             RamRomUsage[RAM_NO_DET].append([item[0], int(actual_val)])
#         wsheet.merge_cells(start_row=17+start_offset, start_column=3, end_row=17+end_offset, end_column=3)
#         wsheet.cell(row=17+start_offset, column=3).value = "RAM"
#         wsheet.cell(row=17+start_offset+sub_index, column=5).value = expected_total
#         wsheet.cell(row=17+start_offset+sub_index, column=7).value = actual_total
#         wsheet.cell(row=17+start_offset+sub_index, column=8).value = "=G{0}/E{0}".format(17+end_offset)
#         wsheet.cell(row=17+start_offset+sub_index, column=9).value = \
#                     "=IF(OR(H{0}<0.9, H{0}>1.1),\"Need Justification\", \"Not Need Justification\")".format(17+end_offset)
#         wsheet.cell(row=17+start_offset+sub_index, column=10).value = "-"
#         wsheet.cell(row=17+start_offset+sub_index, column=11).value = ", ".join(get_unique_list(ram_rom_no_det_exe_tc_names))
#         start_offset += len(item_info_dict[RAM_NO_DET])
#         # Update content of section 4.3  THROUGHPUT DETAIL
#         des_line = wsheet.cell(row=21+start_offset, column=2).value
#         des_line = des_line.replace("{MSN}", Msn.upper())
#         wsheet.cell(row=21+start_offset, column=2).value = des_line
#         # Update throughput detail table
#         index = 0
#         RamRomUsage.update({EXE_TIME: []})
#         end_offset += len(item_info_dict[EXE_TIME])-1
#         wsheet.insert_rows(25 + start_offset, len(item_info_dict[EXE_TIME])-1)
#         for item in item_info_dict[EXE_TIME]:
#             actual_val = get_memory_value(item[0], EXE_TIME, item[1])
#             wsheet.cell(row=24+start_offset+index, column=2).value = str(index+1)
#             wsheet.cell(row=24+start_offset+index, column=3).value = str(item[0])
#             wsheet.cell(row=24+start_offset+index, column=4).value = item[2]
#             wsheet.cell(row=24+start_offset+index, column=5).value = wsheet.cell(row = 24+start_offset, column = 5).value
#             wsheet.cell(row=24+start_offset+index, column=6).value = float(actual_val)
#             wsheet.cell(row=24+start_offset+index, column=7).value = "=F{0}/D{0}".format(24+start_offset+index)
#             wsheet.cell(row=24+start_offset+index, column=8).value = \
#                 "=IF(OR(G{0}<0.9, G{0}>1.1),\"Need Justification\", \"Not Need Justification\")".format(24+start_offset+index)
#             wsheet.cell(row=24+start_offset+index, column=9).value = "-"
#             wsheet.cell(row=24+start_offset+index, column=10).value = ", ".join(get_unique_list(api_exe_tc_names))
#             for col in range(2, 11):
#                 wsheet.cell(row=24+start_offset+index, column = col)._style = \
#                                        copy(wsheet.cell(row = 24+start_offset, column = col)._style)
#             index +=1
#             RamRomUsage[EXE_TIME].append([item[0], float(actual_val)])
#         start_offset += len(item_info_dict[EXE_TIME])-1
#         # Update content of section 4.4     STACK DEPTH
#         exp_stk_list = [item[2] for item in item_info_dict[STACK_DET]]
#         exp_stk_list.extend([item[2] for item in item_info_dict[STACK_NO_DET]])
#         des_line = wsheet.cell(row=28+start_offset, column=2).value
#         des_line = des_line.replace("{MSN}", Msn.upper())
#         first_start_des = start_offset
#         # Update stack depth table with DET enabled
#         index = 0
#         end_offset += len(item_info_dict[STACK_DET])-1
#         wsheet.insert_rows(33 + start_offset, len(item_info_dict[STACK_DET])-1)
#         for item in item_info_dict[STACK_DET]:
#             actual_val = get_memory_value(item[0], STACK_DET, item[1])
#             wsheet.cell(row=32+start_offset+index, column=2).value = str(index+1)
#             wsheet.cell(row=32+start_offset+index, column=3).value = str(item[0])
#             wsheet.cell(row=32+start_offset+index, column=4).value = item[2]
#             wsheet.cell(row=32+start_offset+index, column=5).value = wsheet.cell(row = 32+start_offset, column = 5).value
#             wsheet.cell(row=32+start_offset+index, column=6).value = int(actual_val)
#             wsheet.cell(row=32+start_offset+index, column=7).value = "=IF(D{0}=0,\"-\",F{0}/D{0})".format(32+start_offset+index)
#             wsheet.cell(row=32+start_offset+index, column=8).value = \
#                 "=IF(OR(G{0}<0.9, G{0}>1.1),\"Need Justification\", \"Not Need Justification\")".format(32+start_offset+index)
#             wsheet.cell(row=32+start_offset+index, column=9).value = "-"
#             wsheet.cell(row=32+start_offset+index, column=10).value = ", ".join(get_unique_list(stk_det_exe_tc_names))
#             for col in range(2, 11):
#                 wsheet.cell(row=32+start_offset+index, column = col)._style = \
#                                        copy(wsheet.cell(row = 32+start_offset, column = col)._style)
#             index +=1
#             stack_depth_list.append(int(actual_val))
#         start_offset += len(item_info_dict[STACK_DET])-1
#         # Update stack depth table with DET Disabled
#         index = 0
#         end_offset += len(item_info_dict[STACK_NO_DET])-1
#         wsheet.insert_rows(37 + start_offset, len(item_info_dict[STACK_NO_DET])-1)
#         for item in item_info_dict[STACK_NO_DET]:
#             actual_val = get_memory_value(item[0], STACK_NO_DET, item[1])
#             wsheet.cell(row=36+start_offset+index, column=2).value = str(index+1)
#             wsheet.cell(row=36+start_offset+index, column=3).value = str(item[0])
#             wsheet.cell(row=36+start_offset+index, column=4).value = item[2]
#             wsheet.cell(row=36+start_offset+index, column=5).value = wsheet.cell(row = 36+start_offset, column = 5).value
#             wsheet.cell(row=36+start_offset+index, column=6).value = int(actual_val)
#             wsheet.cell(row=36+start_offset+index, column=7).value = "=IF(D{0}=0,\"-\",F{0}/D{0})".format(36+start_offset+index)
#             wsheet.cell(row=36+start_offset+index, column=8).value = \
#                 "=IF(OR(G{0}<0.9, G{0}>1.1),\"Need Justification\", \"Not Need Justification\")".format(36+start_offset+index)
#             wsheet.cell(row=36+start_offset+index, column=9).value = "-"
#             wsheet.cell(row=36+start_offset+index, column=10).value = ", ".join(get_unique_list(stk_no_det_exe_tc_names))
#             for col in range(2, 11):
#                 wsheet.cell(row=36+start_offset+index, column = col)._style = \
#                                        copy(wsheet.cell(row = 36+start_offset, column = col)._style)
#             index +=1
#             stack_depth_list.append(int(actual_val))
#         start_offset += len(item_info_dict[STACK_NO_DET])
#         StackSize = max(stack_depth_list)
#         des_line = des_line.replace("{STK Value}", str(StackSize))
#         wsheet.cell(row=28+first_start_des, column=2).value = des_line
#         print("  Sheet 'Measurement_Result' is updated")
#         print("*"*120)
#     else:
#         print("*"*120)
#         print("[~] Step 2: Remove sheet 'Measurement_Result'")
#         wsheet = _wbook.get_sheet_by_name("Measurement_Result")
#         _wbook.remove_sheet(wsheet)
#         print("  Sheet 'Measurement_Result' is removed")
#         print("*"*120)
#     return True

# def update_msn_sheet(_wbook):
#     """
#     Update sheet 'MSN'

#     Arguments:
#         _wbook -- input sheet

#     Returns:
#         None
#     """
#     if "DIT" == TestType:
#         old_val = ""
#         start_offset = 0
#         end_offset = 0
#         # Step 3: Update test result into sheet 'MSN'
#         print("*"*120)
#         print("[~] Step 3: Update typical config result into sheet 'MSN'")
#         # Update 'MSN' sheet name and module index 
#         wsheet = _wbook.get_sheet_by_name("MSN")
#         wsheet.title = Msn.upper()
#         wsheet.cell(row=2, column=1).value = MODULE_INDEX[Msn.upper()]
#         # Update description for each section
#         old_val = str(wsheet.cell(row=6, column=3).value)
#         wsheet.cell(row=6, column=3).value = old_val.replace("{MSN}", Msn.upper()).replace("{DV}", \
#                                              TestDevice).replace("{DVT}", TestDeviceType)
#         old_val = str(wsheet.cell(row=21, column=3).value)
#         wsheet.cell(row=21, column=3).value = old_val.replace("{MSN}", Msn.upper()).replace("{DV}", \
#                                              TestDevice).replace("{DVT}", TestDeviceType)
#         old_val = str(wsheet.cell(row=27, column=3).value)
#         wsheet.cell(row=27, column=3).value = old_val.replace("{MSN}", Msn.upper()).replace("{DV}", \
#                                              TestDevice).replace("{DVT}", TestDeviceType)
#         # Update 6.2 Stack size value
#         wsheet.cell(row=23, column=8).value = StackSize
#         # Update 6.1 MSN ROM/RAM Usage table
#         index = 0
#         total_ram_rom_det = 0
#         total_ram_rom_no_det = 0
#         actual_total_det = 0
#         actual_total_no_det = 0
#         # end_offset += len(RamRomUsage[ROM_DET]) - 2
#         wsheet.insert_rows(12 + start_offset, len(RamRomUsage[ROM_DET]) - 2)
#         for item in range(len(RamRomUsage[ROM_DET])):
#             wsheet.cell(row=10+start_offset+index, column=5).value = str(RamRomUsage[ROM_DET][item][0])
#             wsheet.cell(row=10+start_offset+index, column=7).value = int(RamRomUsage[ROM_DET][item][1])
#             wsheet.cell(row=10+start_offset+index, column=8).value = int(RamRomUsage[ROM_NO_DET][item][1])
#             for col in range(1,9):
#                 wsheet.cell(row=10+start_offset+index, column = col)._style = \
#                                                  copy(wsheet.cell(row = 11+start_offset, column = col)._style)
#             index +=1
#             actual_total_det += int(RamRomUsage[ROM_DET][item][1])
#             actual_total_no_det += int(RamRomUsage[ROM_NO_DET][item][1])
#         wsheet.cell(row=10+start_offset+index, column=7).value = actual_total_det
#         wsheet.cell(row=10+start_offset+index, column=8).value = actual_total_no_det
#         start_offset += (len(RamRomUsage[ROM_DET]) - 2)
#         total_ram_rom_det += actual_total_det
#         total_ram_rom_no_det += actual_total_no_det
#         index = 0
#         actual_total_det = 0
#         actual_total_no_det = 0
#         # end_offset += len(RamRomUsage[RAM_DET]) - 2
#         wsheet.insert_rows(15 + start_offset, len(RamRomUsage[RAM_DET]) - 2)
#         for item in range(len(RamRomUsage[RAM_DET])):
#             wsheet.cell(row=13+start_offset+index, column=5).value = str(RamRomUsage[RAM_DET][item][0])
#             wsheet.cell(row=13+start_offset+index, column=7).value = int(RamRomUsage[RAM_DET][item][1])
#             wsheet.cell(row=13+start_offset+index, column=8).value = int(RamRomUsage[RAM_NO_DET][item][1])
#             for col in range(1,9):
#                 wsheet.cell(row=13+start_offset+index, column = col)._style = \
#                                                  copy(wsheet.cell(row = 14+start_offset, column = col)._style)
#             index +=1
#             actual_total_det += int(RamRomUsage[RAM_DET][item][1])
#             actual_total_no_det += int(RamRomUsage[RAM_NO_DET][item][1])
#         wsheet.cell(row=13+start_offset+index, column=7).value = actual_total_det
#         wsheet.cell(row=13+start_offset+index, column=8).value = actual_total_no_det
#         total_ram_rom_det += actual_total_det
#         total_ram_rom_no_det += actual_total_no_det
#         wsheet.cell(row=13+start_offset+index+1, column=7).value = total_ram_rom_det
#         wsheet.cell(row=13+start_offset+index+1, column=8).value = total_ram_rom_no_det
#         start_offset += (len(RamRomUsage[RAM_DET]) - 2)
#         # Update 6.3 MSN API Execution Time
#         index = 0
#         for item in range(len(RamRomUsage[EXE_TIME])):
#             wsheet.cell(row=30+start_offset+index, column=4).value = str(RamRomUsage[EXE_TIME][item][0])
#             wsheet.cell(row=30+start_offset+index, column=6).value = float(RamRomUsage[EXE_TIME][item][1])
#             wsheet.cell(row=30+start_offset+index, column=7).value = "-"
#             for col in range(1,9):
#                 wsheet.cell(row=30+start_offset+index, column = col)._style = \
#                                                  copy(wsheet.cell(row = 30+start_offset, column = col)._style)
#             index +=1
#         print("  Sheet '{0}' is updated".format(Msn.upper()))
#         print("*"*120)
#     else:
#         print("*"*120)
#         print("[~] Step 3: Remove sheet 'MSN'")
#         wsheet = _wbook.get_sheet_by_name("MSN")
#         _wbook.remove_sheet(wsheet)
#         print("  Sheet 'MSN' are removed")
#         print("*"*120)
#     return True


# def get_svn_revision(_input):
#     """
#     Get URL and revision of input file/folder

#     Arguments:
#         input {path} -- input file/folder

#     Returns:
#         string -- return string of URL and revision of input
#     """
#     p_url = subprocess.Popen(
#         "svn info --show-item=url " + _input, stdout=subprocess.PIPE, shell=True)
#     p_rev = subprocess.Popen("svnversion " + _input, stdout=subprocess.PIPE, shell=True)
#     url, err_url = p_url.communicate()
#     rev, err_rev = p_rev.communicate()
#     # if parsing failed, return error messages as result
#     url = err_url if err_url else url
#     rev = err_rev if err_rev else re.findall(r'\d+', str(rev))[0]
#     return url, rev


# def update_cover_sheet(_wbook):
#     """
#     Update cover sheet for test report

#     Arguments:
#         _wbook -- input sheet

#     Returns:
#         None
#     """
#     # Step 3.1: Update sheet "Cover sheet"
#     wsheet = _wbook.get_sheet_by_name("Cover")
#     doc_title = wsheet.cell(row=4, column=5).value
#     wsheet.cell(row=4, column=5).value = doc_title.format(Msn.upper(), ("Driver" if TestType == "DIT" \
#                                          else "Gentool"))
#     wsheet.cell(row=5, column=8).value = datetime.datetime.today().strftime("%m/%d/%Y")
#     for cel in range(len(RevisionInfo)):
#         wsheet.cell(row=18, column=cel+1).value = RevisionInfo[cel]
#     return True


# def update_intro_sheet(_wbook):
#     """
#     Update itroduction sheet for test report

#     Arguments:
#         _wbook -- input sheet

#     Returns:
#         None
#     """
#     # Step 3.3: Update sheet "Introduction"
#     wsheet = _wbook.get_sheet_by_name("Introduction")
#     purpose = wsheet.cell(row=3, column=2).value
#     scope = wsheet.cell(row=6, column=2).value
#     purpose = purpose.format(init_db[ARVersion], init_db[TestDevice]["device_family"][0], Msn.upper())
#     scope = scope.format(init_db[ARVersion], init_db[TestDevice]["device_family"][0], Msn.upper())
#     wsheet.cell(row=3, column=2).value = purpose
#     wsheet.cell(row=6, column=2).value = scope
#     wsheet.cell(row=25, column=3).value = MsnOriginalTP.split('/')[-1]
#     wsheet.cell(row=25, column=4).value = TestVersion
#     return True


# def update_env_sheet(_wbook):
#     """
#     Update test environment sheet for test report

#     Arguments:
#         _wbook -- input sheet

#     Returns:
#         None
#     """
#     # Step 3.4: Update sheet "Test_Environment"
#     wsheet = _wbook.get_sheet_by_name("Test_Environment")
#     for index in range(len(TestTargetsInfo) if len(TestTargetsInfo) < 5 else 5):
#         wsheet.cell(row=10 + index, column=2).value = TestTargetsInfo[index][0]
#         wsheet.cell(row=10 + index, column=3).value = TestTargetsInfo[index][1]
#         wsheet.cell(row=10 + index, column=4).value = TestTargetsInfo[index][2]
#     wsheet.cell(row=19, column=4).value = "RH850/" + TestDevice
#     wsheet.cell(row=20, column=4).value = "R7F" + TestDeviceType
#     wsheet.cell(row=21, column=4).value = TestEnvironment[u'Device Version mentioned on chip']
#     wsheet.cell(row=22, column=4).value = TestEnvironment[u'Evaluation Board']
#     wsheet.cell(row=23, column=4).value = TestEnvironment[u'IDE Name and Version']
#     wsheet.cell(row=24, column=4).value = TestEnvironment[u'Compiler']
#     wsheet.cell(row=25, column=4).value = TestEnvironment[u'Compiler Options']
#     wsheet.cell(row=26, column=4).value = TestEnvironment[u'Linker']
#     wsheet.cell(row=27, column=4).value = TestEnvironment[u'Linker Options']
#     wsheet.cell(row=28, column=4).value = TestEnvironment[u'GNU Make']
#     wsheet.cell(row=31, column=2).value = str(get_svn_revision(init_db["workspace"])[1])
#     wsheet.cell(row=34, column=2).value = str(get_svn_revision(init_db["workspace"])[0])
#     return True


# def update_rest_sheet(_wbook):
#     """
#     Update the rest information

#     Arguments:
#         _wbook -- input sheet

#     Returns:
#         None
#     """
#     # Step 4: Update remain sheets of test report
#     print("*"*120)
#     print("[~] Step 4: Update remain sheets of test report")
#     if update_cover_sheet(_wbook):
#         print("  Sheet 'Cover' is updated")
#         if update_intro_sheet(_wbook):
#             print("  Sheet 'Introduction' is updated")
#             if update_env_sheet(_wbook):
#                 print("  Sheet 'Test_Environment' is updated")
#     print("*"*120)
#     return True


def main():
    """main function of the program

    Returns:
        int -- status of the main function. 1 is successful
    """
    
    report_cfg = Config_IT()

    if get_input_command(report_cfg):
        test_spec_path = "U:/internal/RCar/{0}/modules/{1}/workspace/4.2.2/Test_spec.xlsx".format(report_cfg.device, report_cfg.msn)
        # report_template = "U:/internal/RCar/{0}/modules/{1}/workspace/4.2.2/Template/Report_Template.xlsx".format(report_cfg.device, report_cfg.msn)
        report_template = "D:\Workspace/repo/RCAR_SVN/RCar_Autosar_New/Common/14_Rule_and_Guideline/Template/05_Test_Report/IT/R-CarGen3_MCAL_MSN_DriverITReport.xlsx"
        report_file = "U:/internal/RCar/{0}/modules/{1}/workspace/4.2.2/R-CarV3U_MCAL_DIO_DriverITReport.xlsx".format(report_cfg.device, report_cfg.msn)

        wbook = openpyxl.load_workbook(report_template)
        if update_test_result(report_cfg, wbook):
            # if update_measurement_result(wbook):
            #     if update_msn_sheet(wbook):
            #         update_rest_sheet(wbook)
            #         print("[~] {0} rep  ort for {1} module is exported successfully!".format(TestType, Msn.upper()))
            print("[~] {0} report for {1} module is exported successfully!".format(report_cfg.test_type, report_cfg.msn.upper()))
        wbook.save(report_file)

    return None

# main processing
if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
