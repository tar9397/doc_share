from ntpath import join
import sys,os,re
import argparse
import json

def get_margin_info(regex_up_down, regex_left_right, content, margin_info):
    margin_up_down = ""
    margin_left_right =""
    tmp_up_down = re.search(regex_up_down, content)
    tmp_left_right = re.search(regex_left_right, content)
    if tmp_up_down is not None :
        margin_up_down = tmp_up_down.group(0)
        #print(margin_up_down)
        margin_info = margin_up_down
    if tmp_left_right is not None :
        margin_left_right = tmp_left_right.group(0)
        margin_info = margin_info + '\r\n' + margin_left_right
    if margin_info is not None:
        # print(margin_info)
        return margin_info
 
def get_input():
    input_file_name = "xdiagefi_eyesurf_log_hex.txt"

    with open(input_file_name, "r+") as read_file:
        file_content = read_file.readlines()

    file_content = list(file_content)
    parttern = 'Running eye drawing\s(\w*\W*\w*)'
    regex_up_down = 'Margin up :\s*(\d*.*)'
    regex_left_right = 'Margin left :\s*(\d*.*)'
    graph_obj = {}
    graph_data = []

    global margin_info
    margin_info = ""
    margin_info_e = ""
    margin_up_down = ""
    margin_left_right =""
    html = ""
    for row_text, content in enumerate(file_content):
        margin_info_e = get_margin_info(regex_up_down, regex_left_right, content, margin_info_e)
        if margin_info_e.endswith('UI'):
            margin_info = margin_info_e
            margin_info_e = ""
            print(margin_info)
        # tmp_up_down = re.search(regex_up_down, content)
        # tmp_left_right = re.search(regex_left_right, content)
        # if tmp_up_down is not None :
        #     margin_up_down = tmp_up_down.group(0)
        #     #print(margin_up_down)
        # margin_info = margin_up_down
        # if tmp_left_right is not None :
        #     margin_left_right = tmp_left_right.group(0)
        #     margin_info = margin_info + '\r\n' + margin_left_right
        # if margin_info is not None and margin_info.endswith('UI'):
        #     print(margin_info)

def main():
    """main function of the program"""

    get_input()

if __name__ == '__main__':
    main()