#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                           GenTestAppRCar.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
from ntpath import join
import sys,os,re
import argparse
import json

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------
line_ending = '\r\n'
output_loc = './Output'

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------
def write_to_file(file_name, content_array):
    # if not os.path.exists(output_loc):
    #     os.makedirs(output_loc)

    # with open(os.path.join(file_name), "w+") as write_file:
    #     write_file.write('\n'.join(content_array))
    pass

def get_margin_info(regex_up_down, regex_left_right, content):
    tmp_up_down = re.search(regex_up_down, content)
    tmp_left_right = re.search(regex_left_right, content)
    if tmp_up_down is not None :
        margin_up_down = tmp_up_down.group(0)
        print(margin_up_down)
    if tmp_left_right is not None :
        margin_left_right = tmp_left_right.group(0)
        print(margin_left_right)

def get_eye_picture(parttern, _graph_obj, _graph_data, content):
    start_graph_data = '|'
    tmp = re.search(parttern, content)
    if tmp is not None :
        _graph_obj['data'] = []
        _graph_data.clear()
        graph_name = tmp.group(1)
        _graph_obj['name'] = graph_name 
    if content.startswith(start_graph_data):
        # tmp = content.replace(start_graph_data, '').strip()
        # print('tmp ',tmp)
        tmp_list = content.replace(start_graph_data, '').strip().split(' ')
        _graph_data.append(' '.join(['0x'+str(num) for num in tmp_list]))    

    if content.startswith('----------------') and len(_graph_data) > 0:
        # process data 
        _graph_obj['data'] = _graph_data
        
        # print(len(_graph_data))
        # print(content)

        new_file_name = 'tmp_' + _graph_obj['name'] + '.txt'
        print('write to file ',new_file_name)

        #Save log to Output
        write_to_file(new_file_name, _graph_obj['data'])
    

def main():
    """main function of the program"""

    input_file_name = "xdiagefi_eyesurf_log_hex.txt"

    with open(input_file_name, "r+") as read_file:
        file_content = read_file.readlines()

    file_content = list(file_content)
    parttern = 'Running eye drawing\s(\w*\W*\w*)'
    regex_up_down = 'Margin up :\s*(\d*.*)'
    regex_left_right = 'Margin left :\s*(\d*.*)'
    graph_obj = {}
    # graph_obj['data'] = []
    # graph_obj['name'] = ''
    graph_data = []

    for row_text, content in enumerate(file_content):
        get_eye_picture(parttern, graph_obj, graph_data, content)
        # print(graph_obj)
       # get_margin_info(regex_up_down, regex_left_right, content)


if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################