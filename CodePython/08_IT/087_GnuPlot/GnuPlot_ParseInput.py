#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                           GenTestAppRCar.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
from ntpath import join
import sys,os,re
import argparse
import json
import codecs

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------
line_ending = '\r\n'
output_loc = './Output'

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------
def write_to_file(file_name, content_array):
    if not os.path.exists(output_loc):
        os.makedirs(output_loc)

    # with open(os.path.join(output_loc, file_name), "w+") as write_file:
    #     # write_file.write('\n'.join(content_array))
    #     write_file.write(line_ending.join(content_array))

    with codecs.open(os.path.join(output_loc, file_name), "w", "utf-8-sig") as write_file:
        write_file.write(line_ending.join(content_array))

def main():
    """main function of the program"""

    input_file_name = "xdiagefi_eyesurf_log_hex.txt"

    with open(input_file_name, "r") as read_file:
        # file_content = read_file.readlines()
        file_content = read_file.read().splitlines() 

    file_content = list(file_content)
    parttern = 'Running eye drawing\s(\w*\W*\w*)'
    graph_obj = {}
    graph_data = []
    start_graph_data = '|'

    for row_text, content in enumerate(file_content):        
        if content.startswith(start_graph_data):
            # tmp = content.replace(start_graph_data, '').strip()
            # print('tmp ',tmp)
            tmp_list = content.replace(start_graph_data, '').strip().split(' ')
            graph_data.append(' '.join(['0x'+str(num) for num in tmp_list]))
        elif content.startswith('----------------') and len(graph_data) > 0:
            # process data 
            graph_obj['data'] = graph_data

            new_file_name = 'tmp_' + graph_obj['name'] + '.txt'
            print('write to file ',new_file_name)
            write_to_file(new_file_name, graph_obj['data'])

            # print('name ',graph_obj['name'])
            # print('data ',graph_obj['data'])
        else :
            tmp = re.search(parttern, content)
            if tmp is not None :
                graph_obj = {}
                graph_data = []
                graph_name = tmp.group(1)
                # print(graph_name)
                graph_obj['name'] = graph_name


if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################