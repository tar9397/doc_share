#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                           GenTestAppRCar.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
from ntpath import join
import sys,os,re
import argparse
import openpyxl
import json
import codecs
from pygnuplot import gnuplot
import pandas as pd
import numpy as np

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------
line_ending = '\r\n'
output_loc = './Output'

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------
def write_to_file(file_name, content_array):
    if not os.path.exists(output_loc):
        os.makedirs(output_loc)

    # with open(os.path.join(output_loc, file_name), "w+") as write_file:
    #     # write_file.writelines(content_array)
    #     # print(content_array)
    #     # print(line_ending.join(content_array))
    #     # write_file.write(line_ending.join(content_array))
    #     write_file.write('\n'.join(content_array))
    #     # write_file.writelines(line_ending)

    with codecs.open(os.path.join(output_loc, file_name), "w", "utf-8-sig") as write_file:
        write_file.write(line_ending.join(content_array))

def update_file_content(temp_file, file_name, content_array):
    new_file = []

    data_pattern = {
        'file_name' : "<TEMPLATE_FILENAME>",
        'marco_name' : "<MARCO_FILE>",
        'func_code' : "<FUNCTION_CODE>"
    }

    for data in temp_file:
        if data_pattern['file_name'] in data:
            tmp_data = data.replace(data_pattern['file_name'], file_name.ljust(len(data_pattern['file_name'])))
            new_file.append(tmp_data.strip())
            # print(tmp_data)
        elif data_pattern['marco_name'] in data:
            msn_test = str(file_name.split('_')[0]).upper() + 'TEST'
            testcase_name =  '_'.join(str(file_name.split('.')[0]).split('_')[1:]).upper()
            tmp_data = data.replace(data_pattern['marco_name'], '_'.join([msn_test, testcase_name, 'C']))
            new_file.append(tmp_data.strip())
            # print(tmp_data)
        elif data_pattern['func_code'] in data:
            new_file.extend(content_array)
        else:
            new_file.append(data.strip())

    return new_file

# ------------------------------------------------------------------------------
# Handle parser create function
# port_id: array of int
# ------------------------------------------------------------------------------
def create_function_str(param, config_name, port_id = None):
    # tmp_header = [
    #     "void Port_Init(void)",
    #     "{{",
    #     "  /* Port config: {0} */",
    #     "{1}",
    #     "}}"
    # ]
    
    tmp_header = [
        "void Port_Init(void)",
        "{{",
        "{1}",
        "}}"
    ]

    tmp_cfg = []

    for data in param:
        port_num = data['PortName'].strip().split('_')[1]
        # print('process port: ',port_num)
        # print(port_id)
        # if not port_id or len(port_id) == 0:
        #     tmp_cfg.append(create_cfg_for_single_port(port_num, data['Config']))
        # elif int(port_num) in port_id:
        #     tmp_cfg.append(create_cfg_for_single_port(port_num, data['Config']))

        if not port_id or len(port_id) == 0 or int(port_num) in port_id:
            tmp_cfg.extend(create_cfg_for_single_port(port_num, data['Config']))
            tmp_cfg.append('')

        # return template_header.format(line_ending, (line_ending*2).join(tmp_cfg))
    # return line_ending.join(tmp_header).format(config_name, (line_ending*2).join(tmp_cfg))
    
    tmp_cfg.pop()
    return line_ending.join(tmp_header).format(config_name, (line_ending).join(tmp_cfg))
    # return [data.format(config_name, tmp_cfg) for data in tmp_header]

def create_cfg_for_single_port(port_num, port_config):
    '''
        {0}: id
        {1}: port mask
        {2}: output mask
    '''
    template_common = [
        "  /* Port {0} */",
        "  /* Set the polarity positive of port {0} pin*/",
        "  POSNEG{0} &= (uint32) ~{1};",
        "  /* Configuration port {0} as IO mode */",
        "  IOINTSEL{0} &= (uint32) ~{1};",
        "  /* Configuration direction for port {0} pin are output */",
        "  INOUTSEL{0} = (uint32) {2};"
    ]

    '''
        {0}: id
        {1}: output mask
        {2}: output init value
    '''
    tmp_output_general = [
        "  /* Select output data is output by OUTDT register */",
        "  OUTDTSEL{0} &= (uint32) ~{1};",
        "  /* Init level for pin */",
        "  OUTDT{0} = (uint32) {2};"
    ]
    
    tmp_output_high_low = [
        "  /* Select output data is output by OUTDTH/OUTDTL register */",
        "  OUTDTSEL{0} |= (uint32) {1};",
        "  /* Init level for pin */",
        "  OUTDTH{0} = (uint32) {2};"
    ]

    '''
        {0}: id
        {1}: input mask
        {2}: pull up mask
    '''
    # tmp_input = [
    #     "  /* Enable pull function */",
    #     "  PUEN{0} |= (uint32) {1};",
    #     "  /* Select input pin is pull-up/down */",
    #     "  PUD{0} |= (uint32) {2};",
    tmp_input = [
        "  /* Enable input */",
        "  INEN{0} = (uint32) {1};"
    ]

    # port_mask = port_config[1]
    # output_mask = port_config[2]
    # port_init_val = port_config[3]
    port_mask = port_config['port_mask']
    output_mask = port_config['output_mask']
    input_mask = port_config['input_mask']

    if output_mask is not None:
        output_str = update_arr_content(template_common, port_num, port_mask, output_mask)
    else: 
        output_str = update_arr_content(template_common, port_num, port_mask, '0')
        
    if output_mask is not None:
        port_init_out_val = port_config['port_init_out_val']
        if 'General' in port_config['out_data_select']:
            output_str += update_arr_content(tmp_output_general, port_num, port_mask, port_init_out_val)
        else:
            output_str += update_arr_content(tmp_output_high_low, port_num, port_mask, port_init_out_val)
        

    if input_mask is not None:
        port_init_in_val = port_config['port_init_in_val']
        output_str += update_arr_content(tmp_input, port_num, input_mask, port_init_in_val)

    # return line_ending.join(output_str).format(port_num, port_mask, output_mask, port_init_val)
    # return [data.format(port_num, port_mask, output_mask, port_init_val) for data in output_str]
    return output_str

def update_arr_content(arr, *args):
    return [data.format(*args) for data in arr]

def padded_hex(dev_value):
    return "{0:#0{1}X}".format(dev_value,10).replace('0X','0x')

# ------------------------------------------------------------------------------
# Read dat form excel
# ------------------------------------------------------------------------------
'''data struct content: [{"PortName": 'PORTGROUP_0_BITS_0_TO_27', "Config": [data]}, {}, ...]'''
def get_port_name(file_name, sheet_name):
    ret = []
    cur_portname = None
    out_data = None
    try:
        wbook = openpyxl.load_workbook(file_name, data_only=True)
        ws = wbook.get_sheet_by_name(sheet_name)
        for row in ws.iter_rows():
            if "PortName" == str(row[0].value):
                cur_portname = str(row[1].value)
                port_mask = 0
                output_mask = 0
                continue
            if cur_portname:
                if "OutSelect" in str(row[0].value):
                    if "1" in str(row[1].value):
                        out_data = 'HighLow' 
                    else:
                        out_data = 'General' 
                elif "Mode" in str(row[0].value):
                    bit_pos = 31
                    for cell in row[1:]:
                        cell_content = str(cell.value).upper()
                        if 'I' in cell_content or 'O' in cell_content:
                            valid_pin = 1
                            port_mask += int(pow(2, bit_pos))
                            if 'O' in cell_content:
                                output_mask += int(pow(2, bit_pos))
                        if bit_pos >= 0:
                            bit_pos -= 1
                        else:
                            break
                elif "State" in str(row[0].value):
                    port_init_out_val = 0
                    port_init_in_val = 0
                    bit_pos = 31
                    for cell in row[1:]:
                        cell_content = str(cell.value)
                        if '1' in cell_content:
                            if (output_mask & int(pow(2, bit_pos)) != 0):
                                port_init_out_val += int(pow(2, bit_pos))
                            else:
                                port_init_in_val += int(pow(2, bit_pos))
                        if bit_pos >= 0:
                            bit_pos -= 1
                        else:
                            break
                    
                    temp = {}
                    temp['PortName'] = cur_portname
                    # temp['Config'] = [out_data, padded_hex(port_mask), padded_hex(output_mask), padded_hex(port_init_out_val)]
                    temp_config_dict = {}
                    temp_config_dict['port_mask'] = padded_hex(port_mask)
                    temp_config_dict['output_mask'] = padded_hex(output_mask) if output_mask != 0 else None
                    temp_config_dict['input_mask'] = padded_hex(port_mask-output_mask) if port_mask != output_mask else None
                    temp_config_dict['port_init_out_val'] = padded_hex(port_init_out_val)
                    temp_config_dict['port_init_in_val'] = padded_hex(port_init_in_val)
                    temp_config_dict['out_data_select'] = out_data # string var
                    temp['Config'] = temp_config_dict

                    ret.append(temp)
                    cur_portname = None
                    out_data = None
                    
        wbook.close()
    except Exception as e:
        print("[E]Issue occurs when opening xlsx file to read")
        print(e)
        
    return ret

def get_input_command():
    """Get all input for init process
    Print help if command is not in valid command

    Returns:
        None -- It doesn't return any value
    """
    example = '''
    E.g: ./GenTestAppRCarGen3.py CFG001
    ./GenTestAppRCarGen3.py CFG009
    '''
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('cfg', type=str,
                        help='config to read')
    # parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
    #                     help='valid target module: Adc, Can, Dio ...')
    # parser.add_argument('TestType', type=str, help='type of test: DIT, TIT, VT')
    # parser.add_argument('TestDevice', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
    #                     help='target device: H3, V3M, V3H , M3, M3N, V3U, V3Hv2')
    # parser.add_argument('ARVersion', type=str, choices = ["4_2_2", "4.2.2"], \
    #                     help='autosar version: 4_2_2')
    # parser.add_argument('--nc', action='store_true', \
    #                     help='(optional) skip copy item in WorkspaceSetup.sh', default=False, required=False)
    # parser.add_argument('OSTest', type=str,
    #                     help='OS Test option: OS, No_OS, None(TIT only)')
    # parser.add_argument('--s0s1', type=str, nargs='?',
    #                     help='(optional) run S0/S1 or normal TC: True, False (default=False)', \
    #                     default='False', required=False)
    # parser.add_argument('--target_cfg', type=str, nargs='?',
    #                     help='(optional) run specific cfg(s)/config type: ex. CFG001,CFG002,DEF001(split by ","), \
    #                     CFGnnn(all CFG), RCnnn(all RC), DEFnnn(all DEF), xCFGnnn(exclude all CFG), \
    #                     xRCnnn(exclude all RC), xDEFnnn(exclude all DEF)', default='CFGxxx', required=False)
    # parser.add_argument('--keep_tc', type=str, nargs='?',
    #                     help='(optional: True or False) when --target_cfg is used do you want to keep original \
    #                     TC or not (defalt=False)', default='False', required=False)
    # parser.add_argument('--ignore_error', type=str, nargs='?',
    #                     help='(optional: True or False) allow to continue progress when error occurs (defalt=false)', \
    #                     default='False', required=False)
    # parser.add_argument('--ice_number', type=str, nargs='?',
    #                     help='(optional) id number of E2 Emulator to select debugger connection (defalt=null)', \
    #                     required=False)
    # parser.add_argument('--build_option', type=str, nargs='?',
    #                     help='(optional: all/generate_forward/build_only) allow to choose selected actions only \
    #                     to reduce execution time (defalt=all)', default='all', required=False)
    # parser.add_argument('--exe_op', type=str, nargs='?',
    #                     help='(optional: True or False) execution time optimization, not recommended, this should \
    #                     only be used when everything is confirmed OK (default=False)', default='False', required=False)
    # parser.add_argument('--cat_map', type=str, nargs='?',
    #                     help='(optional) use interrupt category in data.json', default='False', required=False)
    # parser.add_argument('--debug', type=str, nargs='?',
    #                     help='(optional) turn on script debug mode', default='False', required=False)

    args = parser.parse_args()
    return str(args.cfg)

    # config.msn = str(args.Msn)  # MCAL Msn
    # config.test_type = str(args.TestType).upper()  # DIT, TIT
    # config.device = str(args.TestDevice)  # V3H, V3U..
    # config.ar_version = str(args.ARVersion).replace('_','.')  # 4_2_2, 4_3_1
    # config.skip_copy_workspace = args.nc

    # return True

def main():
    """main function of the program"""

    # run_cfg = get_input_command()
    # config_name = "CFG001" if run_cfg == '' else run_cfg
    # pin_config_file = "PinConfig.xlsm"

    # data = get_port_name(pin_config_file, config_name)
    # print("Collect {0} in {1} ".format(config_name, pin_config_file))
    # # print("result: ",data) 
    # # ('result: ', [{'PortName': 'PORTGROUP_0_BITS_0_TO_27', 'Config': ['General', '0x0FFFFFFF', '0x003FFFFF', '0x0000000F']}, {'PortName': 'PORTGROUP_1_BITS_0_TO_30', 'Config': ['General', '0x7FFFFFFF', '0x0000FFFF', '0x00000000']}, {'PortName': 'PORTGROUP_2_BITS_0_TO_24', 'Config': ['General', '0x01FFFFFF', '0x0000000F', '0x00000000']}, {'PortName': 'PORTGROUP_3_BITS_0_TO_16', 'Config': ['General', '0x0001FFFF', '0x00000FFF', '0x00000000']}, {'PortName': 'PORTGROUP_4_BITS_0_TO_26', 'Config': ['General', '0x07FFFFFF', '0x003FFFFF', '0x00000000']}, {'PortName': 'PORTGROUP_5_BITS_0_TO_20', 'Config': ['General', '0x001FFFFF', '0x001FFFFF', '0x00000000']}, {'PortName': 'PORTGROUP_6_BITS_0_TO_20', 'Config': ['General', '0x001FFFFF', '0x001FE00F', '0x00000000']}, {'PortName': 'PORTGROUP_7_BITS_0_TO_20', 'Config': ['General', '0x001FFFFF', '0x0000FFFF', '0x00000000']}, {'PortName': 'PORTGROUP_8_BITS_0_TO_20', 'Config': ['HighLow', '0x001FFFFF', '0x001FFF00', '0x00000000']}, {'PortName': 'PORTGROUP_9_BITS_0_TO_20', 'Config': ['HighLow', '0x001FFFFF', '0x0010000F', '0x0000000C']}])
    
    # # text = create_function_str(data, config_name)
    # msn = str(init_db['msn'])
    # device = str(init_db['device']).upper()
    # # for testcase, port_num_arr in init_db[config_name].items():
    # for testcase in sorted(init_db[config_name].keys()):
    #     port_num_arr = init_db[config_name][testcase]
    #     text = create_function_str(data, config_name, port_num_arr)
    #     # print('*'*80)
    #     # print(text)
    #     # print('*'*80)
    #     tmp_file_name_ele = [msn.capitalize()]
    #     file_name = '_'.join([msn.capitalize(), ''.join(testcase.split('_')), device]) + '.c'
    #     new_file = update_file_content(template_content, file_name, text.split(line_ending))
    #     write_to_file(file_name, new_file)
    #     print("Create file {} success !".format(file_name))



    # input_file_name = "xdiagefi_eyesurf_log_hex.txt"

    # with open(input_file_name, "r+") as read_file:
    #     file_content = read_file.readlines()

    # file_content = list(file_content)
    # parttern = 'Running eye drawing\s(\w*\W*\w*)'
    # graph_obj = {}
    # graph_data = []
    # start_graph_data = '|'

    # for row_text, content in enumerate(file_content):        
    #     if content.startswith(start_graph_data):
    #         # tmp = content.replace(start_graph_data, '').strip()
    #         # print('tmp ',tmp)
    #         tmp_list = content.replace(start_graph_data, '').strip().split(' ')
    #         graph_data.append(' '.join(['0x'+str(num) for num in tmp_list]))
    #     elif content.startswith('----------------') and len(graph_data) > 0:
    #         # process data 
    #         graph_obj['data'] = graph_data

    #         new_file_name = 'tmp_' + graph_obj['name'] + '.txt'
    #         print('write to file ',new_file_name)
    #         write_to_file(new_file_name, graph_obj['data'])

    #         # print('name ',graph_obj['name'])
    #         # print('data ',graph_obj['data'])
    #     else :
    #         tmp = re.search(parttern, content)
    #         if tmp is not None :
    #             graph_obj = {}
    #             graph_data = []
    #             graph_name = tmp.group(1)
    #             # print(graph_name)
    #             graph_obj['name'] = graph_name





    # df = pd.DataFrame(data = {'col1': [1, 2],
    #                       'col2': [3, 4],
    #                       'col3': [5, 6]})
    # g = gnuplot.Gnuplot()
    # g.plot_data(df, 'using 1:2 with lines', 'using 1:3 with points')





    # # A demostration to generate pandas data frame data in python.
    # df = pd.read_csv('demo/finance_v2.dat', sep='\t', index_col = 0, parse_dates = True,
    #     names = ['date', 'open','high','low','close', 'volume','volume_m50',
    #         'intensity','close_ma20','upper','lower '])

    # # make subplot at first, now there is still no real plot.
    # sub1 = gnuplot.make_plot_data(df,
    #     'using 0:2:3:4:5 notitle with candlesticks lt 8',
    #     'using 0:9 notitle with lines lt 3',
    #     'using 0:10 notitle with lines lt 1',
    #     'using 0:11 notitle with lines lt 2',
    #     'using 0:8 axes x1y2 notitle with lines lt 4',
    #     title = '"Change to candlesticks"',
    #     logscale = 'y',
    #     xrange = '[50:86]',
    #     yrange = '[75:105]',
    #     format = 'x ""',
    #     xtics = '(66, 87, 109, 130, 151, 174, 193, 215, 235)',
    #     ytics = '(105, 100, 95, 90, 85, 80)',
    #     lmargin = '9',
    #     rmargin = '2',
    #     bmargin = '0',
    #     origin = '0, 0.3',
    #     size = ' 1, 0.7',
    #     grid = 'xtics ytics',
    #     ylabel = '"price" offset 1',
    #     label = ['1 "Acme Widgets" at graph 0.5, graph 0.9 center front',
    #         '2 "Courtesy of Bollinger Capital" at graph 0.01, 0.07',
    #         '3 "  www.BollingerBands.com" at graph 0.01, 0.03']
    #     )

    # sub2 = gnuplot.make_plot_data(df,
    #     'using 0:($6/10000) notitle with impulses lt 3',
    #     'using 0:($7/10000) notitle with lines lt 1',
    #     ytics = '500',
    #     xtics = '("6/03" 66, "7/03" 87, "8/03" 109, "9/03" 130, "10/03" 151, "11/03" 174, "12/03" 193, "1/04" 215, "2/04" 235)',
    #     ylabel = '"volume (0000)" offset 1',
    #     nologscale = 'y',
    #     autoscale = 'y',
    #     size = '1.0, 0.3',
    #     origin = '0.0, 0.0',
    #     bmargin = '',
    #     tmargin = '0',
    #     format = ['x', 'y "%1.0f"'])

    # # plot at one time.
    # gnuplot.multiplot(sub1, sub2,
    #     output = '"finance.13.png"',
    #     term = 'pngcairo font "arial,10" fontscale 1.0 size 900, 600')
    # # gnuplot.multiplot(sub1,
    # #     output = '"finance.13.png"',
    # #     term = 'pngcairo font "arial,10" fontscale 1.0 size 900, 600')
    # # gnuplot.multiplot(sub2,
    # #     output = '"finance.13.png"',
    # #     term = 'pngcairo font "arial,10" fontscale 1.0 size 900, 600')




    # #https://matplotlib.org/gallery/subplots_axes_and_figures/axes_demo.html#sphx-glr-gallery-subplots-axes-and-figures-axes-demo-py
    # #http://gnuplot.sourceforge.net/demo_5.2/bins.html

    # # 1) create some data to use for the plot
    # np.random.seed(19680801) # Fixing random state for reproducibility
    # dt = 0.001
    # t = np.arange(0.0, 10.0, dt)
    # r = np.exp(-t / 0.05)  # impulse response
    # x = np.random.randn(len(t))
    # s = np.convolve(x, r)[:len(x)] * dt  # colored noise
    # df = pd.DataFrame({'r': r, 'x': x, 's': s}, index = t)
    # df.index.name = 't'
    # #print(df.tail().to_csv())

    # # 2) Plot the data
    # main = gnuplot.make_plot_data(df.iloc[:1000],
    #         'using 1:4 with line lw 2 lc "web-blue"',
    #         title = '"Gaussian colored noise"',
    #         xlabel = '"time (s)"',
    #         ylabel = '"current (nA)"',
    #         xrange = '[0:1]',
    #         yrange = '[-0.015:0.03]',
    #         key = None,
    #         size = ' 1, 1',
    #         origin = '0, 0')
    # right = gnuplot.make_plot_data(df,
    #         'using 4 bins=400 with boxes title "20 bins" lw 2 lc "web-blue"',
    #         title = '"Probability"',
    #         xlabel = None,
    #         ylabel = None,
    #         tics = None,
    #         xrange = None,
    #         yrange = None,
    #         origin = '0.65, 0.56',
    #         size = '0.24, 0.32',
    #         object = 'rectangle from graph 0,0 to graph 1,1 behind fc "black" fillstyle solid 1.0')
    # left = gnuplot.make_plot_data(df,
    #         'using 1:2 with line lw 2 lc "web-blue"',
    #         title = '"Impulse response"',
    #         xrange = '[0:0.2]',
    #         origin = '0.15, 0.56',
    #         size = '0.24, 0.32')

    # gnuplot.multiplot(main, right, left,
    #         output = '"sphx_glr_axes_demo_001.png"',
    #         term = 'pngcairo font "arial,10" fontscale 1.0 size 640, 480',
    #         key = '')





    # # A demostration to generate pandas data frame data in python.
    # df = pd.read_csv('demo/finance.dat', sep='\t', index_col = 0, parse_dates = True,
    #         names = ['date', 'open','high','low','close', 'volume','volume_m50',
    #             'intensity','close_ma20','upper','lower '])

    # # Create a Gnuplot instance and set the options at first;
    # g = gnuplot.Gnuplot(log = True,
    #         output = '"finance.13.png"',
    #         term = 'pngcairo font "arial,10" fontscale 1.0 size 900, 600',
    #         multiplot = "")

    # g.plot_data(df,
    #         'using 0:2:3:4:5 notitle with candlesticks lt 8',
    #         'using 0:9 notitle with lines lt 3',
    #         'using 0:10 notitle with lines lt 1',
    #         'using 0:11 notitle with lines lt 2',
    #         'using 0:8 axes x1y2 notitle with lines lt 4',
    #         title = '"Change to candlesticks"',
    #         logscale = 'y',
    #         xrange = '[50:253]',
    #         yrange = '[75:105]',
    #         format = 'x ""',
    #         xtics = '(66, 87, 109, 130, 151, 174, 193, 215, 235)',
    #         ytics = '(105, 100, 95, 90, 85, 80)',
    #         lmargin = '9',
    #         rmargin = '2',
    #         bmargin = '0',
    #         origin = '0, 0.3',
    #         size = ' 1, 0.7',
    #         grid = 'xtics ytics',
    #         ylabel = '"price" offset 1',
    #         label = ['1 "Acme Widgets" at graph 0.5, graph 0.9 center front',
    #             '2 "Courtesy of Bollinger Capital" at graph 0.01, 0.07',
    #             '3 "  www.BollingerBands.com" at graph 0.01, 0.03']
    #         )

    # g.plot_data(df,
    #         'using 0:($6/10000) notitle with impulses lt 3',
    #         'using 0:($7/10000) notitle with lines lt 1',
    #         bmargin = '',
    #         size = '1.0, 0.3',
    #         origin = '0.0, 0.0',
    #         tmargin = '0',
    #         nologscale = 'y',
    #         autoscale = 'y',
    #         format = ['x', 'y "%1.0f"'],
    #         ytics = '500',
    #         xtics = '("6/03" 66, "7/03" 87, "8/03" 109, "9/03" 130, "10/03" 151, "11/03" 174, "12/03" 193, "1/04" 215, "2/04" 235)',
    #         ylabel = '"volume (0000)" offset 1')





    # df = pd.read_csv('demo/immigration.dat', index_col = 0, sep='\t', comment='#')
    # g = gnuplot.Gnuplot()
    # g.set(terminal = 'pngcairo transparent enhanced font "arial,10" fontscale 1.0 size 600, 400 ',
    #         output = '"histograms.1.png"',
    #         key = 'fixed right top vertical Right noreverse noenhanced autotitle nobox',
    #         style = 'data linespoints',
    #         datafile = ' missing "-"',
    #         xtics = 'border in scale 1,0.5 nomirror rotate by -45 autojustify norangelimit',
    #         title = '"US immigration from Europe by decade"')
    # g.plot_data(df, 'using 2:xtic(1), for [i=3:22] "" using i ')
    
    



    # df = pd.read_csv('demo/immigration.dat', index_col = 0, sep='\t', comment='#')
    # gnuplot.plot_data(df,
    #         'using 2:xtic(1), for [i=3:22] "" using i ',
    #         terminal = 'pngcairo transparent enhanced font "arial,10" fontscale 1.0 size 600, 400 ',
    #         output = '"histograms.1.png"',
    #         key = 'fixed right top vertical Right noreverse noenhanced autotitle nobox',
    #         style = 'data linespoints',
    #         datafile = ' missing "-"',
    #         xtics = 'border in scale 1,0.5 nomirror rotate by -45 autojustify norangelimit',
    #         title = '"US immigration from Europe by decade"')





    # gnuplot.splot('cos(u)+.5*cos(u)*cos(v),sin(u)+.5*sin(u)*cos(v),.5*sin(v) with lines',
    #     '1+cos(u)+.5*cos(u)*cos(v),.5*sin(v),sin(u)+.5*sin(u)*cos(v) with lines',
    #     terminal = 'pngcairo enhanced font "arial,10" fontscale 1.0 size 600, 400 ',
    #     output = '"surface2.9.png"',
    #     dummy = 'u, v',
    #     key = 'bmargin center horizontal Right noreverse enhanced autotitle nobox',
    #     style = ['increment default','data lines'],
    #     parametric = '',
    #     view = '50, 30, 1, 1',
    #     isosamples = '50, 20',
    #     hidden3d = 'back offset 1 trianglepattern 3 undefined 1 altdiagonal bentover',
    #     xyplane = 'relative 0',
    #     title = '"Interlocking Tori" ',
    #     urange = '[ -3.14159 : 3.14159 ] noreverse nowriteback',
    #     vrange = '[ -3.14159 : 3.14159 ] noreverse nowriteback')





    # # Black and white version
    # gnuplot.splot('"demo/whale.dat" w pm3d',
    #         term = 'pngcairo size 480,480',
    #         out = '"whale.png"',
    #         style = 'line 100 lw 0.1 lc "black"',
    #         pm3d = 'depth hidden3d ls 100',
    #         cbrange = '[-0.5:0.5]',
    #         palette = 'rgb -3,-3,-3',
    #         colorbox = None,
    #         border = None,
    #         key = None,
    #         zrange = '[-2:2]',
    #         tics = None,
    #         view = '60,185,1.5')





    input_file_name = "eye_sum_9_15.txt"
    # df = pd.read_csv(''+input_file_name+'', sep=' ', index_col = 0, parse_dates = True,
    #         names = ['data_0', 'data_1', 'data_2', 'data_3', 'data_4', 'data_5',
    #          'data_6', 'data_7', 'data_8', 'data_9', 'data_10', 'data_11', 'data_12', 
    #          'data_13', 'data_14', 'data_15', 'data_16', 'data_17', 'data_18', 
    #          'data_19', 'data_20', 'data_21', 'data_22', 'data_23', 'data_24', 
    #          'data_25', 'data_26', 'data_27', 'data_28', 'data_29', 'data_30', 'data_31'])

    # # Create a Gnuplot instance and set the options at first;
    # g = gnuplot.Gnuplot(log = True,
    #         output = '"eye_test.png"',
    #         term = 'pngcairo font "arial,10" fontscale 1.0 size 900, 600',
    #         multiplot = "")

    # g.plot_data(df,
    #         '',
    #         bmargin = '',
    #         size = '1.0, 0.3',
    #         origin = '0.0, 0.0',
    #         tmargin = '0',
    #         nologscale = 'y',
    #         autoscale = 'y',
    #         format = ['x', 'y "%1.0f"'])

    g = gnuplot.Gnuplot()
    g.cmd('set term png' )
    g.cmd( 'set pm3d map' )
    g.cmd( 'set pm3d interpolate 4,4' )
    g.cmd( 'set output "sum_9_15.png"' )
    g.cmd( 'set palette defined (0 0 0 0.5, 1 0 0 1, 2 0 0.5 1, 3 0 1 1, 4 0.5 1 0.5, 5 1 1 0, 6 1 0.5 0, 7 1 0 0, 8 0.5 0 0)' )
    g.splot('"'+input_file_name+'" matrix',
            term = 'pngcairo size 480,480',
            out = '"eye_test.png"',
            style = 'line 100 lw 0.1 lc "black"',
            pm3d = 'depth hidden3d ls 100',
            cbrange = '[-0.5:0.5]',
            palette = 'rgb -3,-3,-3',
            colorbox = None,
            border = None,
            key = None,
            zrange = '[-2:2]',
            tics = None,
            view = '60,185,1.5')

if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################