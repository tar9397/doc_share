#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                             SyncTestInfo.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
import openpyxl
import argparse


DEBUG = False
CASE_BY_CASE = False
SelectTc = ""
Specific_Tc_List = []
Except_Tc_List = []
Msn = ""
msn = ""
MSN = ""
MsnSpecFile = ""
MsnAppSrc = ""
OutputLocation = ""
TcDict = {}
RegexPattern = {
    "STEP_OLD" : "((Step|step)\s*_*\d+\.*\d*):",
    "CP_OLD" : "((Checkpoint|Testpoint|Test Point|TestCondition)\s*_*\d+\.*\d*):",
    "STEP_NEW" : "(Step_\d+):",
    "CP_NEW" : "(Checkpoint_\d+.\d+):"
}

def entry():
    """Start application"""
    example = '''
    E.g: ./SyncTestInfo.py Adc
    '''
    valid_msn = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"]
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('Msn', type=str, help='valid target module: Adc, Can, Dio ...')
    parser.add_argument('--opl', type=str, nargs='?',
                        help='(optional) specify desired output file name and location (default=./)', 
                        default='./', required=False)
    parser.add_argument('--select_tc', type=str, nargs='?',
                        help='(optional) all/cbc/<Specific TC(s) name> choose the way TC will be updated (default=all)', 
                        default='all', required=False)
    parser.add_argument('--debug', type=str, nargs='?',
                        help='(optional) True/False Enable debug data (default=False)', default='False', required=False)
    parser.add_argument('--all_except', type=str, nargs='?',
                        help='(optional) Run for all test case except (default=None)', default='None', required=False)
    args = parser.parse_args()
    if str(args.Msn) not in valid_msn:
        parser.print_help()
        return False
    else:
        global Msn
        global msn
        global MSN
        global MsnSpecFile
        global MsnAppSrc
        global OutputLocation
        global DEBUG
        global CASE_BY_CASE
        global SelectTc
        global Specific_Tc_List
        global Except_Tc_List
        Msn = str(args.Msn)  # MCAL Msn
        msn = Msn.lower()    # MCAL msn
        MSN = Msn.upper()    # MCAL MSN
        MsnSpecFile = u'U:\\internal\\Module\\{0}\\08_IT\\01_WorkProduct_D\\integration_test\\plan\\RH850_X2x_{1}_DIT_TS.xlsx'.format(msn,MSN)
        MsnAppSrc = u'U:\\internal\\Module\\{0}\\08_IT\\01_WorkProduct_D\\integration_test\\app\\src\\'.format(msn)
        OutputLocation = str(args.opl) + "output.xlsx"
        DEBUG = (True if str(args.debug) == "True" else False)
        SelectTc = str(args.select_tc)
        if SelectTc.lower() == "all":
            pass
        elif SelectTc.lower() == "cbc":
            CASE_BY_CASE = True
        else:
            Specific_Tc_List = SelectTc.split(",")
            CASE_BY_CASE = True
        Except_Tc_List = str(args.all_except).split(",")
    return True

def get_unique_dict(input_list):
    """Get dict contains unique element from input dict"""
    output_list = {}
    for element in sorted(input_list.keys()):
        if not element in output_list.keys():
            output_list.update({element: input_list[element]})
    return output_list

def get_tc_dict():
    """Get test case list from test spec"""
    global TcDict
    print("[I]Open cloned xlsx file of TP and get sheet 'Test_Spec'")
    try:
        wbook = openpyxl.load_workbook(MsnSpecFile, data_only=True)
        wsheet = wbook.get_sheet_by_name("Test_Spec")
    except:
        print("[E]Test spec should be converted manually into xlsx format file first, keep sheet 'Test_Spec' only! Don't forget mapping U drive")
        return False

    if SelectTc.lower() == "all" or SelectTc.lower() == "cbc":
        TcDict = get_unique_dict({\
                    str(wsheet.cell(row_idx, 16).value) : [row_idx,""] for row_idx in range(2,wsheet.max_row) \
                    if wsheet.cell(row_idx, 16).value and wsheet.cell(row_idx, 16).value != "" and \
                    wsheet.cell(row_idx, 16).value != "-" and wsheet.cell(row_idx, 16).value not in Except_Tc_List})
    else:
        TcDict = get_unique_dict({\
                    str(wsheet.cell(row_idx, 16).value) : [row_idx,""] for row_idx in range(2,wsheet.max_row) \
                    if wsheet.cell(row_idx, 16).value and wsheet.cell(row_idx, 16).value != "" and \
                    wsheet.cell(row_idx, 16).value != "-" and wsheet.cell(row_idx, 16).value in Specific_Tc_List and \
                    wsheet.cell(row_idx, 16).value not in Except_Tc_List})
    if 0 == len(TcDict.keys()):
        print("[E]Couldn't get any TC as input order.")
        return False
    wbook.close()
    return True

def get_comment(_app_name, _tc_name):
    """ Get content form test app comment """
    global TcDict
    shorted_app_name = _app_name.split("/")[-1]
    print("[I]Start with test app file " + shorted_app_name)
    file = open(_app_name,'r')
    if file:
        comment_dict = {}
        line_idx = 0
        # use readline() to read the first line
        line = file.readline()
        line_idx += 1
        while line:
            print_debug(line)
            # process for each line
            if re.search(RegexPattern["STEP_OLD"], line) or \
                re.search(RegexPattern["CP_OLD"], line):
                if line.strip().startswith(r"/*"):
                    if line.strip("\r\n").strip().endswith(r"*/"):
                        curComment = line.strip("\r\n").strip().strip(r"/*").strip(r"*/").strip().strip("-")
                    else:
                        curComment = line.strip("\r\n").strip(r"\/").strip().strip(r"/*").strip().strip("-") + " "
                        line = file.readline()
                        line_idx += 1
                        while not line.strip("\r\n").strip().endswith(r"*/"):
                            curComment += line.strip("\r\n").strip(r"\/").strip().strip("-") + " "
                            line = file.readline()
                            line_idx += 1
                        curComment += line.strip("\r\n").strip().strip(r"*/").strip().strip("-")
                    comment_dict.update({line_idx: curComment})
                elif line.strip().startswith(r"//"):
                    curComment = line.strip().strip(r"\/").strip().strip("-")
                    comment_dict.update({line_idx: curComment})
                elif line.strip().startswith(r"*"):
                    curComment = line.strip().strip(r"\/").strip().strip("-")
                    comment_dict.update({line_idx: curComment})
                else:
                    print("[W]Comment type at line {} is not supported.".format(line_idx))
                    pass
            else:
                pass     
            # use readline() to read next line
            line = file.readline()
            line_idx += 1
        file.close()
        print_debug(comment_dict)
        step_idx = 0
        cp_idx = 0
        step_bf = False
        step_str = ""
        cp_str = ""
        for comment in sorted(comment_dict.keys()):
            print_debug(comment_dict[comment])
            match_step = re.search(RegexPattern["STEP_OLD"], comment_dict[comment])
            match_cp = re.search(RegexPattern["CP_OLD"], comment_dict[comment])
            if match_step:
                step_idx += 1
                cp_idx = 1
                comment_dict[comment] = comment_dict[comment].replace(match_step.group(0), "Step_{0:02}:".format(step_idx))
                step_str += comment_dict[comment] + "\n"
                step_bf = True
            elif match_cp:
                if step_bf == True:
                    comment_dict[comment] = comment_dict[comment].replace(match_cp.group(0), \
                                            "Checkpoint_{0:02}.{1:01}:".format(step_idx, cp_idx))
                    cp_idx += 1
                    cp_str += comment_dict[comment] + "\n"
                else:
                    print("[W]Checkpoint '{0}' presents before Step in {1}, Check it out".format(comment_dict[comment], _app_name))
            else:
                print("[E]Something's wrong here!")
        print_debug(comment_dict)
        content_sum = "[Test program]\n{0}\n[Expected]\n{1}".format(step_str, cp_str)
        if TcDict[_tc_name][1] != "":
            if content_sum != TcDict[_tc_name][1]:
                print("[W]Test content is not the same among test app files of test case " + _tc_name)
            else:
                print("[I]Test content is still consistent among test app files of test case " + _tc_name)
        else:
            TcDict[_tc_name][1] = content_sum
            print("[I]New 'Test description and Expected' for test case {} is:".format(_tc_name))
            print("- "*40)
            print(content_sum)
            print("- "*40)
        ans = print_question("[Q]Do you want to update comments in test app {0} and info of \
                            test case {1} with new Step and Checkpoint? - Yes/No/App_Only/Spec_Only (y/n/a/s)".format(\
                            shorted_app_name, _tc_name))
        if False == ans or "y" == ans.lower() or "a" == ans.lower():
            with open(_app_name,'r+') as file:
                lines = file.readlines()
                for l in comment_dict.keys():
                    new_step_index = re.search(RegexPattern["STEP_NEW"], comment_dict[l])
                    new_cp_index = re.search(RegexPattern["CP_NEW"], comment_dict[l])
                    old_line = lines[l-1]
                    old_step_index = re.search(RegexPattern["STEP_OLD"], old_line)
                    old_cp_index = re.search(RegexPattern["CP_OLD"], old_line)
                    if new_step_index and old_step_index:
                        lines[l-1] = old_line.replace(old_step_index.group(0),new_step_index.group(0))
                    if new_cp_index and old_cp_index:
                        lines[l-1] = old_line.replace(old_cp_index.group(0),new_cp_index.group(0))
                file.close()
                file = open(_app_name, "w+")
                file.writelines(lines)
                print("[I]File {} was updated with new comments.".format(shorted_app_name))
                file.close()
            # except:
            #     print("[E]Trouble in opening file {}".format(shorted_app_name))
            if True == ans and "a" == ans.lower():
                print("[I]Update file {} only!".format(shorted_app_name))
                return False
        elif True == ans and "n" == ans.lower():
            print("[I]Skipped updating both file {0} and info of {1}!".format(shorted_app_name, _tc_name))
            return False
        elif True == ans and "s" == ans.lower():
            print("[I]Update info of test case {0} only!".format(_tc_name))
            return True
        else:
            print("[E]Invalid option '{}'".format(ans))
            return False
    else:
        print("[E]Can not open file {}".format(_app_name))
        return False
    return True

def set_comment(_app_name, _wsheet):
    """ Use values get from test app, modify test ap and update test spec """
    for tc_name in sorted(TcDict.keys()):
         _wsheet.cell(TcDict[tc_name][0], 23).value = TcDict[tc_name][1]
    return True

def update_tc():
    """ Go through all listed test case and update each one """
    global TcDict
    try:
        wbook = openpyxl.load_workbook(MsnSpecFile,data_only=True)
        wsheet_read_only = wbook.get_sheet_by_name("Test_Spec")
        wbook.close()
    except:
        print("[E]Issue occurs when opening xlsx file to read")
        return False
    try:
        wbook = openpyxl.load_workbook(MsnSpecFile)
        wsheet_write = wbook.get_sheet_by_name("Test_Spec")
    except:
        print("[E]Issue occurs when opening xlsx file to update")
        return False
    for tc_name in sorted(TcDict.keys()):
        print("\n[I]Process test case "+tc_name + "\n")
        assigned_app = str(wsheet_read_only.cell(TcDict[tc_name][0], 29).value)
        try:
            tc_index =  int(re.search(r'_(\d+).c', assigned_app).group(1))
        except:
            print("[E]Invalid test app name "+assigned_app)
            continue
        mod_tc_name = assigned_app.replace("{0:04}.c".format(tc_index),"{0:03}[x00-x7f]+.c".format(tc_index))
        print_debug("[I]Modified test case name "+mod_tc_name)
        for dirpath, dirnames, filenames in os.walk(MsnAppSrc):
            for filename in [f for f in filenames if re.match(mod_tc_name,f)]:
                if get_comment(os.path.join(dirpath, filename), tc_name):
                    set_comment(os.path.join(dirpath, filename), wsheet_write)
                else:
                    pass
    # save and close file
    print("[I]Saving and closing xlsx file")
    try:
        wbook.save(OutputLocation)
    except:
        print("[E]Couldn't save output file as " + OutputLocation)
    wbook.close()
    return True

def print_debug(_input):
    """ Print info if debugging is enabled """
    if DEBUG:
        print(_input)
    return True

def print_question(_input):
    """ Print question and get answer if case by case processing is enabled """
    if CASE_BY_CASE:
        output = str(raw_input(_input))
        return output
    else:
        return False

def main():
    """main function of the program"""
    if entry():
        if get_tc_dict():
            print_debug(TcDict)
            if update_tc():
                print("[I]File '{0}' is updated with new test description and expected".format(OutputLocation))
            else:
                print("[E]Proccessing problem")
        else:
            print("[E]Parsing problem")
    else:
        print("[E]Input problem")


if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################