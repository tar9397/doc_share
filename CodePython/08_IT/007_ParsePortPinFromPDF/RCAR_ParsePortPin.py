#!/usr/bin/python -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = MCAL Automatic Evaluation System                              #
# Module       = report.py                                                     #
# Version      = 1.01                                                          #
# Date         = 15-Aug-2020                                                   #
# AES Version  = 2.1.0(Official Version)                                       #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This script is used to generate the report from all results in workspace     #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  20-Jan-2019     : Initial Version                                    #
# V1.01:  15-Aug-2020     : Update template for U2A16 release                  #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------
from __future__ import print_function
from xlrd import open_workbook
from copy import copy
# import openpyxl
import subprocess
import shlex
import sys
import re
import os
import datetime
import json
import argparse
# import pandas as pd

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------

Msn = "" # MCAL msn
device = "" # U2A16, E2UH, E2H, ALL

# -----------------------------------------------------------
# constant section
# -----------------------------------------------------------
# Load the data base stored in the file to the program


# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------


def get_input_command():
    """Get all input for init process
    Print help if command is not in valid command
    Put this script same directory as excel file and run it

    Returns:
        None -- It doesn't return any value
    """
    example = '''
    E.g: ./report.py Dio 4_3_1
    '''
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
                        help='valid target module: Adc, Can, Dio ...')
    parser.add_argument('device', type=str, choices = ["U2A16", "E2UH", "E2H", "ALL"], \
                        help='device select: U2A16, E2UH, E2H, ALL')
    args = parser.parse_args()

    global Msn
    global device
    
    Msn = str(sys.argv[1]) # MCAL Msn
    device = ["E2M", "E2UH", "E2H", "U2A16"]  if str(args.device) == 'ALL' else [str(args.device)] # U2A16, E2UH, E2H, ALL

    return True


def run_command(command, cwd):
    """
    To run cmd command on specific working dir

    Arguments:
        command -- the expected commandline and working dir
        cwd -- the expected working dir

    Returns:
        Continues log output
    """
    output = ""
    mes_line = ""
    if cwd and not os.path.isdir(cwd): 
        print(" [~] Error: Directory {} does not exist.".format(cwd))
        sys.exit()
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, cwd=cwd)
    # wait until process complete and get output message
    while "" != mes_line or process.poll() is None:
        mes_line = process.stdout.readline()
        output += mes_line
        continue
    rc = process.poll()
    # if failed, print notification but not terminate
    if 0 != rc:
        print(" [~] Error: Running command '{}' failed.".format(command))
        # sys.exit()
    return output.strip()


# def get_unique_list(_input_list):
#     """
#     Get list contains unique element from input list

#     Arguments:
#         _input_list -- input list

#     Returns:
#         sorted list contains unique element form input list
#     """
#     _output_list = []
#     for element in _input_list:
#         if not element in _output_list:
#             _output_list.append(element)
#     return sorted(_output_list)



def get_svn_revision(_input):
    """
    Get URL and revision of input file/folder

    Arguments:
        input {path} -- input file/folder

    Returns:
        string -- return string of URL and revision of input
    """
    p_url = subprocess.Popen(
        "svn info --show-item=url " + _input, stdout=subprocess.PIPE, shell=True)
    p_rev = subprocess.Popen("svnversion -c " + _input, stdout=subprocess.PIPE, shell=True)
    url, err_url = p_url.communicate()
    rev, err_rev = p_rev.communicate()
    # if parsing failed, return error messages as result
    url = err_url if err_url else url
    rev = err_rev if err_rev else re.findall(r'\d+', str(rev))[0]
    return url, rev



def get_file_list(msn, device):
    """
    Get test result from exported file

    Arguments:
        None

    Returns:
        Dictionary of test result for each test case
    """
    sp_data = []
    start_row = None
    end_row = None

    folder_list=os.listdir(os.getcwd())
    for file_name in folder_list:
        if msn.upper() in file_name and 'xlsm' in file_name and '~' not in file_name:
            print("[~] open file {0} ".format(file_name))
            wbook = open_workbook(file_name)
            rs_ws = wbook.sheet_by_name("EvidenceList")
            for row in range(11, rs_ws.nrows):
                test_type = rs_ws.row_values(row)[3]
                if 'Integration Test' in test_type:
                    if device in test_type and start_row is None:
                        start_row = row
                    if device in test_type and start_row is not None:
                        end_row = row
                    if start_row is not None and end_row == row:
                        cell_value = rs_ws.row_values(row)[19]
                        # print('cell_value: ',cell_value[0].encode('utf8'))
                        # print(type(cell_value[0].encode('utf8')))
                        if '-' is not cell_value[0].encode('utf8'):
                            sp_data.append(cell_value)

            # print('start_row: ',start_row)
            # print('end_row: ',end_row)
            # for row in range(start_row, end_row+1):
            #     cell_value = rs_ws.row_values(row)[19]
            #     print('cell_value: ',cell_value)
            #     print(type(cell_value))

            #     if u'-' is not cell_value:
            #         sp_data.append(cell_value)
            
            # print(sp_data)

    return start_row, end_row+1, sp_data


def get_file_version(target_file_names):
    """
    Arguments:
        _wbook -- input sheet
    """
    svn_rev = None
    temp_version = {}
    result = []

    for subdir, dirs, files in os.walk("U:/internal/Module/{0}/08_IT".format(Msn.lower())):
        for file_name in files:
            for target_file_name in target_file_names:
                # print('file_name: ',file_name)
                # print('target_file_name: ',target_file_name)
                target_file_without_extension = os.path.splitext(target_file_name)[0]
                local_file_without_extension = os.path.splitext(file_name)[0]
                if target_file_without_extension == local_file_without_extension:
                    file_path = os.path.join(subdir, file_name)
                    svn_rev = get_svn_revision(file_path)[1]
                    # print(svn_rev)
                    temp_version.update({target_file_name: svn_rev})
                    break

    # print(temp_version.keys())
    # print(len(temp_version.keys()))

    for target_file_name in target_file_names:
        # print('target_file_name: ',target_file_name)
        if target_file_name in temp_version.keys():
            svn_revision = temp_version[target_file_name]
            result.append([target_file_name, svn_rev])
        else:
            print("Warnings: File version is missing: {0}".format(target_file_name))

    return result



def main():
    """main function of the program

    Returns:
        int -- status of the main function. 1 is successful
    """
    # if get_input_command():
    #     for dev in device:
    #         print("*"*120)
    #         start_row, end_row, file_names = get_file_list(Msn, dev)
    #         result = get_file_version(file_names)
    #         # print(result)
    #         for file_result in result:
    #             print("File :{0}, Rev: {1}".format(file_result[0],file_result[1]))
    sum_data = []
    with open('pin.txt') as reader:
        data_read = reader.readlines()
    for data in data_read:
        tmp = data.strip().split('_')
        filter_data = tmp[tmp.index('BITS')+1:]
        if len(filter_data) > 1 and 'TO' in filter_data[1]:
            row_data = range(int(filter_data[0]), int(filter_data[2])+1)
            sum_data.append(row_data)
        else:
            sum_data.append(filter_data)
    # print(sum_data)
    print(len(sum_data))    
    total_pin = 0

    cont_pin = 0
    str_format = ""
    # format_tempate = "DioPort_{0:03}/DioChannel_{1:03}                                Container\r\n"
    # format_tempate += "DioPort_{0:03}/DioChannel_{1:03}/DioChannelBitPosition         {2}\r\n"
    # format_tempate += "DioPort_{0:03}/DioChannel_{1:03}/DioChannelId                  {2}\r\n\r\n"

    format_tempate = "DioPort{0}/DioChannel{1}                               Container\r\n"
    format_tempate += "DioPort{0}/DioChannel{1}/DioChannelBitPosition         {2}\r\n"
    format_tempate += "DioPort{0}/DioChannel{1}/DioChannelId                  {2}\r\n"

    for index, data in enumerate(sum_data):
        print("Element {0} has : {1}".format(index, data))
        print("Count element {0}".format(len(data)))
        total_pin += len(data)
        for pin_index in data:
            if 0 == index:
                if 0 == cont_pin:
                    str_format += format_tempate.format('', '', pin_index)
                else:
                    str_format += format_tempate.format('', '_' + str(cont_pin).zfill(3), pin_index)
            else:
                str_format += format_tempate.format('_' + str(index).zfill(3), '_' + str(cont_pin).zfill(3), pin_index)
            cont_pin += 1
        str_format += '\r\n'

    print("Total pin: {}".format(total_pin))    
    print("*"*80)    
    print(str_format)
    with open('./output.txt', 'w') as f:
        f.write(str_format)    


        # wbook = openpyxl.load_workbook('RH850_device_module_AMDC_Justification.xlsx')
        # if update_result_sheet(wbook, file_name, data):
        #     MsnReportFile = "RH850_{}_{}.xlsx".format(Msn.upper(), 'R'+ ''.join(ARVersion.split('_')))
        #     wbook.save(MsnReportFile)
        #     print("[~] {0} report is exported successfully! {1} ".format(Msn.upper(),MsnReportFile))

    #     wbook = openpyxl.load_workbook(TEMPLATE_DIR)       
    #     if update_test_result(wbook):
    #         if update_measurement_result(wbook):
    #             if update_msn_sheet(wbook):
    #                 update_rest_sheet(wbook)
    #                 print("[~] {0} report for {1} module is exported successfully!".format(TestType, Msn.upper()))
    #     wbook.save(MsnReportFile)
    # return None
    


# main processing
if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
