import openpyxl
import subprocess, re,os
from openpyxl.styles import Font
from openpyxl.styles import colors
from openpyxl.styles import Color

##################_INPUT_##################

wb_name = "D:/RCar_Autosar_New/xOS2/trunk/02_Basic_Design/R-CarGen3_AUTOSAR_MCAL_xOS2_WorkProductTable.xlsx"
ws_name = "CDDEMM"

startRow = 30
endRow = 150

root = "http://172.29.143.27/SS2/RCar_Autosar_New"
local_path = "D:\RCar_Autosar_New"

Current_SVN_Git_path = "AA"
Document_file_name = "AB"

##############_INPUT/OUTPUT_##############
SVN_revision_or_date = "AC"

#################_OUTPUT_###################

Document_number = "W"
Revision = "X"
Date = "Y"

Created_by_Organizer = "AI"
Approved_by = "AJ"

Review_type = "AM"

####################################################################
ft = Font(color = '00FF0000', size=10)

####################################################################
#Open input Work Product Table
try:
    book = openpyxl.load_workbook(wb_name,data_only=True)
    sheet = book[ws_name]
except:
    print ("Can not open input file, please check are your workbook name and sheet name corect?")

#Function to process Common excel file to collect needed information
def CommonExcelFunciton(local_file_path,svn_path):
    global history_sheet
    #load work book
    try:        
        book = openpyxl.load_workbook(local_file_path,data_only=True)
    except:
        #print ("Clean up local folder...")
        #print (("svn cleanup " + local_file_path.split('\\',2)[0] + "\\" + local_file_path.split('\\',2)[1]))
        #os.popen("svn cleanup " + local_file_path.split('\\',2)[0] + "\\" + local_file_path.split('\\',2)[1]).read()
        print ("Running SVN checkout...")
        os.popen("svn co "+ svn_path.rsplit('/', 1)[0] + " " + local_file_path.rsplit('\\', 1)[0]).read()
        print (("svn co "+ svn_path.rsplit('/', 1)[0] + " " + local_file_path.rsplit('\\', 1)[0]))
        #message = subprocess.check_output("svn co "+ svn_path.rsplit('/', 1)[0])
        print ("Done run SVN check out, and trying to open sheet")
        book = openpyxl.load_workbook(local_file_path,data_only=True)
    #variable for history sheet name first time is empty
    history_sheet = 'empty'
    #check if this wb content history sheet or not
    for item in book.sheetnames:
        if item.find('istory') != -1: #HISTORY or hISTORY
            history_sheet = item
            break
    if history_sheet == 'empty':     
        #print ("No history sheet!")
        return ("-","-","-","-") #doc_rev,doc_date,doc_author,doc_approver
    else:
        #print ('Curent history sheet name is: ',history_sheet)
        sheet = book[history_sheet]
        # to find cell "Date" and it's row
        cell_date_row = None
        cell_date_col = None
        cell_rev_col = None
        cell_author_col = None
        cell_approver_col = None
        for row in sheet.iter_rows(min_row=2, max_col=10, max_row=10, min_col=2): 
            for cell in row:
                value = str(cell.value)
                if (value.find('Date') != -1) or (value.find('date') != -1):
                    #print ('cell date is: ', cell.value)
                    cell_date_row = cell.row
                    cell_date_col = cell.column
                    break
            else:
                continue
            # to break the first for loop
            break    

        print ("Row of cell 'date' is: ",cell_date_row)

        # to find cell "doc_rev" and it's col
        for row in sheet.iter_rows(min_row=cell_date_row, max_col=10, max_row= cell_date_row , min_col=1): 
            for cell in row:
                #print (cell.value)
                value = str(cell.value)
                if cell.column == cell_date_col:
                    continue
                if (value.find('sion') != -1) or (value.find('Ver') != -1) or (value.find('Rev') != -1): #doc_rev or verSION
                    print ('cell doc_rev is: ', cell.value)
                    cell_rev_col = cell.column
                if (value.find('Author') != -1) or (value.find('hanged by') != -1) or (value.find('By') != -1):  #Author or cHANGED BY or CHANGED BY
                    print ('cell Author is: ', cell.value)
                    cell_author_col = cell.column
                if value.find('pprover') != -1: #aPPROVER or APPROVER
                    print ('cell Approver is: ', cell.value)
                    cell_approver_col = cell.column

        #print ("Col of cell 'doc_rev' is: ",cell_rev_col)

        max_row = len(sheet['C'])
        #print ("initial max row: ", max_row)
        #print ("max row value: ",sheet[('C'+ str(max_row))].value)
        while (sheet[('C'+ str(max_row))].value) is None or (sheet[('C'+ str(max_row))].value)=="-":
            max_row = max_row-1
            #print ("initial max row: ", max_row)
            #print ("max row value: ",sheet[('C'+ str(max_row))].value)
        #doc_rev = sheet[('D'+ str(max_row))].value
        doc_rev = sheet.cell(row = max_row, column=cell_rev_col).value
        doc_date = sheet.cell(row = max_row, column=cell_date_col).value
        doc_author = sheet.cell(row = max_row, column=cell_author_col).value
        try:
            doc_approver = sheet.cell(row = max_row, column=cell_approver_col).value
        except:
            doc_approver = "Not required"
        

        #empty filter for None cell
        if doc_rev is None:
            doc_rev = "-"
        if doc_date is None:
            doc_date = "-"
        if doc_author is None:
            doc_author = "-"
        if doc_approver is None:
            doc_approver = "-"   
        #print result  
        #print ("Document rev: ",doc_rev)
        #print ("Document date: ",doc_date)
        #print ("Document author: ",doc_author)
        #print ("Document approver: ",doc_approver)
        
        return (doc_rev,doc_date,doc_author,doc_approver)


#function update from PeerReviewMinutes.xlsm
def update_PRM(local_file_path,svn_path):
    print ("---PEERREVIEWMINUTES---")
    try:        
        book = openpyxl.load_workbook(local_file_path,data_only=True)
    except:
        #print ("Clean up local folder...")
        #print (("svn cleanup " + local_file_path.split('\\',2)[0] + "\\" + local_file_path.split('\\',2)[1]))
        #os.popen("svn cleanup " + local_file_path.split('\\',2)[0] + "\\" + local_file_path.split('\\',2)[1]).read()
        print ("Running SVN checkout...")
        os.popen("svn co "+ svn_path.rsplit('/', 1)[0] + " " + local_file_path.rsplit('\\', 1)[0]).read()
        print (("svn co "+ svn_path.rsplit('/', 1)[0] + " " + local_file_path.rsplit('\\', 1)[0]))
        #message = subprocess.check_output("svn co "+ svn_path.rsplit('/', 1)[0])
        print ("Done run SVN check out, and trying to open sheet")
        book = openpyxl.load_workbook(local_file_path,data_only=True)

    
    print (book.sheetnames[0])

    sheet = book[book.sheetnames[0]]
    sheet_name = book.sheetnames[0]

    doc_num = sheet['U3'].value
    doc_date = sheet['U5'].value
    if (sheet['U12'].value) is None or (sheet['U12'].value) =="-":
        doc_approver = "-"
        actual_enddate = "-"
    else:
        doc_approver = (sheet['U12'].value).split('\n')[0]
        actual_enddate = (sheet['U12'].value).split('\n')[1]
    if (sheet['X12'].value) is None or (sheet['X12'].value) =="-":
        doc_author = "-"
        actual_startdate = "-"
    else:
        doc_author = (sheet['X12'].value).split('\n')[0]
        actual_startdate = (sheet['X12'].value).split('\n')[1]
    
    review_type = "-"
    doc_rev = "-"

    if sheet_name.find("Passaround") != -1:
        review_type = "Pass around"
    elif sheet_name.find("Inspection") != -1:
        review_type = "Inspection"

    #print (doc_num)
    #print (doc_date)
    #print (doc_approver)
    #print (doc_author)
    #print (actual_startdate)
    #print (actual_enddate)
    #print (review_type)
    #print (doc_rev)

    return (doc_rev,doc_date,doc_author,doc_approver,doc_num,review_type)

##########____MAIN____#############

for x in range(startRow, endRow+1):
    try:
        print (x)
        a1 = sheet[('AA'+ str(x))]
        a2 = sheet[('AB'+ str(x))]
        if a1.value is None:
            print ("Skip this line")
            continue
        svn_path = root + (a1.value).strip() + (a2.value).strip()
        svn_path_fol = root + (a1.value).strip()
        local_svn_path = local_path + (a1.value).strip() + (a2.value).strip()
        try:
            svn_path_common = svn_path.replace(((re.findall(r"\/(.*?)\/",a1.value))[0]+"/trunk"), "Common")
            local_svn_path_common = local_svn_path.replace(((re.findall(r"\/(.*?)\/",a1.value))[0]+"/trunk"), "Common")
        except:
            print ("Wrong file path structure!! Skip this line item..")
            continue

        #uniform paths
        svn_path = svn_path.replace("\\","/")
        svn_path_common = svn_path_common.replace("\\","/")
        local_svn_path = local_svn_path.replace("/","\\")
        local_svn_path_common = local_svn_path_common.replace("/","\\")


        print (svn_path)
        print (svn_path_common)
        print (local_svn_path)
        print (local_svn_path_common)

        #start: colect svn infor for current svn_path
        try:
            print ("svn info -r HEAD --show-item last-changed-revision "+ svn_path)
            svn_info = subprocess.check_output("svn info -r HEAD --show-item last-changed-revision "+ svn_path)  
        except:
            #start: if can not find on current path, goto Common path to check
            try:
                print ("retrying on Common path...")
                print ("svn info -r HEAD --show-item last-changed-revision "+ svn_path_common)
                svn_info = subprocess.check_output("svn info -r HEAD --show-item last-changed-revision "+ svn_path_common)
                
            except:
                print ("error")
            else:
                if local_svn_path.find(".xlsx") != -1:
                    doc_rev,doc_date,doc_author,doc_approver = CommonExcelFunciton(local_svn_path_common,svn_path_common)
                    #print (result)
                    #print (doc_rev)
                    #print (doc_date)
                    #print (doc_author)
                    #print (doc_approver)

                    sheet[Revision + str(x)].font = ft
                    sheet[Revision + str(x)] = doc_rev

                    sheet[Date + str(x)].font = ft
                    sheet[Date + str(x)] = doc_date

                    sheet[Created_by_Organizer + str(x)].font = ft
                    sheet[Created_by_Organizer + str(x)] = doc_author

                    sheet[Approved_by + str(x)].font = ft
                    sheet[Approved_by + str(x)] = doc_approver

                    sheet[SVN_revision_or_date + str(x)].font = ft
                    sheet[SVN_revision_or_date + str(x)] = svn_info
                    
                elif local_svn_path.find("PeerReviewMinutes.xlsm") != -1:
                    update_PRM (local_svn_path_common,svn_path_common)
                    doc_rev,doc_date,doc_author,doc_approver,doc_num,review_type = update_PRM (local_svn_path_common,svn_path_common)

                    sheet[Revision + str(x)].font = ft
                    sheet[Revision + str(x)] = doc_rev

                    sheet[Date + str(x)].font = ft
                    sheet[Date + str(x)] = doc_date

                    sheet[Created_by_Organizer + str(x)].font = ft
                    sheet[Created_by_Organizer + str(x)] = doc_author

                    sheet[Approved_by + str(x)].font = ft
                    sheet[Approved_by + str(x)] = doc_approver

                    sheet[SVN_revision_or_date + str(x)].font = ft
                    sheet[SVN_revision_or_date + str(x)] = svn_info

                    sheet[Document_number + str(x)].font = ft
                    sheet[Document_number + str(x)] = doc_num

                    sheet[Review_type + str(x)].font = ft
                    sheet[Review_type + str(x)] = review_type
                else:
                    print ("---DOCTYPE---")
                print ("doc_rev: "+str(svn_info,'utf-8'))
            #end: if can not find on current path, goto Common path to check
        else:
            if local_svn_path.find(".xlsx") != -1:
                doc_rev,doc_date,doc_author,doc_approver = CommonExcelFunciton(local_svn_path,svn_path)
                #print (result)
                #print (doc_rev)
                #print (doc_date)
                #print (doc_author)
                #print (doc_approver)                   

                sheet[Revision + str(x)].font = ft
                sheet[Revision + str(x)] = doc_rev

                sheet[Date + str(x)].font = ft
                sheet[Date + str(x)] = doc_date

                sheet[Created_by_Organizer + str(x)].font = ft
                sheet[Created_by_Organizer + str(x)] = doc_author

                sheet[Approved_by + str(x)].font = ft
                sheet[Approved_by + str(x)] = doc_approver

                sheet[SVN_revision_or_date + str(x)].font = ft
                sheet[SVN_revision_or_date + str(x)] = svn_info

            elif local_svn_path.find("PeerReviewMinutes.xlsm") != -1:
                doc_rev,doc_date,doc_author,doc_approver,doc_num,review_type = update_PRM (local_svn_path,svn_path)

                sheet[Revision + str(x)].font = ft
                sheet[Revision + str(x)] = doc_rev

                sheet[Date + str(x)].font = ft
                sheet[Date + str(x)] = doc_date

                sheet[Created_by_Organizer + str(x)].font = ft
                sheet[Created_by_Organizer + str(x)] = doc_author

                sheet[Approved_by + str(x)].font = ft
                sheet[Approved_by + str(x)] = doc_approver

                sheet[SVN_revision_or_date + str(x)].font = ft
                sheet[SVN_revision_or_date + str(x)] = svn_info

                sheet[Document_number + str(x)].font = ft
                sheet[Document_number + str(x)] = doc_num

                sheet[Review_type + str(x)].font = ft
                sheet[Review_type + str(x)] = review_type

            else:
                print ("---DOCTYPE---")
            print ("Done run")
            print ("doc_rev: "+str(svn_info,'utf-8'))

    except:
        print("unknow error, high ligh from svn, please contact author to fix it, go to next line item...")
        continue
        #end: colect svn infor for current svn_path
    
book.save("testing.xlsx")
##########____END_MAIN____#############



