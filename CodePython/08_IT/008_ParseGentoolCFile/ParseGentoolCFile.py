#!/usr/bin/python -B
# -*- coding: utf-8 -*-

################################################################################
# Project      = MCAL Automatic Evaluation System                              #
# Module       = report.py                                                     #
# Version      = 1.01                                                          #
# Date         = 15-Aug-2020                                                   #
# AES Version  = 2.1.0(Official Version)                                       #
################################################################################
#                                  COPYRIGHT                                   #
################################################################################
# Copyright(c) 2019 Renesas Electronics Corporation. All rights reserved.      #
################################################################################
# Purpose:                                                                     #
# This script is used to generate the report from all results in workspace     #
#                                                                              #
################################################################################
#                                                                              #
# Unless otherwise agreed upon in writing between your company and             #
# Renesas Electronics Corporation the following shall apply!                   #
#                                                                              #
# Warranty Disclaimer                                                          #
#                                                                              #
# There is no warranty of any kind whatsoever granted by Renesas. Any warranty #
# is expressly disclaimed and excluded by Renesas, either expressed or implied,#
# including but not limited to those for non-infringement of intellectual      #
# property, merchantability and/or fitness for the particular purpose.         #
#                                                                              #
# Renesas shall not have any obligation to maintain, service or provide bug    #
# fixes for the supplied Product(s) and/or the Application.                    #
#                                                                              #
# Each User is solely responsible for determining the appropriateness of       #
# using the Product(s) and assumes all risks associated with its exercise      #
# of rights under this Agreement, including, but not limited to the risks      #
# and costs of program errors, compliance with applicable laws, damage to      #
# or loss of data, programs or equipment, and unavailability or                #
# interruption of operations.                                                  #
#                                                                              #
# Limitation of Liability                                                      #
#                                                                              #
# In no event shall Renesas be liable to the User for any incidental,          #
# consequential, indirect, or punitive damage (including but not limited       #
# to lost profits) regardless of whether such liability is based on breach     #
# of contract, tort, strict liability, breach of warranties, failure of        #
# essential purpose or otherwise and even if advised of the possibility of     #
# such damages. Renesas shall not be liable for any services or products       #
# provided by third party vendors, developers or consultants identified or     #
# referred to the User by Renesas in connection with the Product(s) and/or the #
# Application.                                                                 #
#                                                                              #
################################################################################
# Environment:                                                                 #
#              Devices:        X2x                                             #
################################################################################

################################################################################
##                      Revision Control History                              ##
################################################################################
#                                                                              #
# V1.00:  20-Jan-2019     : Initial Version                                    #
# V1.01:  15-Aug-2020     : Update template for U2A16 release                  #
################################################################################

# -----------------------------------------------------------
# include section
# -----------------------------------------------------------
from __future__ import print_function
from xlrd import open_workbook
from copy import copy
# import openpyxl
import subprocess
import shlex
import sys
import re
import os
import datetime
import json
import argparse
# import pandas as pd

# -----------------------------------------------------------
# global variables section
# -----------------------------------------------------------

Msn = "" # MCAL msn
device = "" # U2A16, E2UH, E2H, ALL

# -----------------------------------------------------------
# constant section
# -----------------------------------------------------------
# Load the data base stored in the file to the program


# -----------------------------------------------------------
# Function section
# -----------------------------------------------------------



def run_command(command, cwd):
    """
    To run cmd command on specific working dir

    Arguments:
        command -- the expected commandline and working dir
        cwd -- the expected working dir

    Returns:
        Continues log output
    """
    output = ""
    mes_line = ""
    if cwd and not os.path.isdir(cwd): 
        print(" [~] Error: Directory {} does not exist.".format(cwd))
        sys.exit()
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE, cwd=cwd)
    # wait until process complete and get output message
    while "" != mes_line or process.poll() is None:
        mes_line = process.stdout.readline()
        output += mes_line
        continue
    rc = process.poll()
    # if failed, print notification but not terminate
    if 0 != rc:
        print(" [~] Error: Running command '{}' failed.".format(command))
        # sys.exit()
    return output.strip()


# def get_unique_list(_input_list):
#     """
#     Get list contains unique element from input list

#     Arguments:
#         _input_list -- input list

#     Returns:
#         sorted list contains unique element form input list
#     """
#     _output_list = []
#     for element in _input_list:
#         if not element in _output_list:
#             _output_list.append(element)
#     return sorted(_output_list)



def get_svn_revision(_input):
    """
    Get URL and revision of input file/folder

    Arguments:
        input {path} -- input file/folder

    Returns:
        string -- return string of URL and revision of input
    """
    p_url = subprocess.Popen(
        "svn info --show-item=url " + _input, stdout=subprocess.PIPE, shell=True)
    p_rev = subprocess.Popen("svnversion -c " + _input, stdout=subprocess.PIPE, shell=True)
    url, err_url = p_url.communicate()
    rev, err_rev = p_rev.communicate()
    # if parsing failed, return error messages as result
    url = err_url if err_url else url
    rev = err_rev if err_rev else re.findall(r'\d+', str(rev))[0]
    return url, rev



def main():
    """main function of the program

    Returns:
        int -- status of the main function. 1 is successful
    """
    # if get_input_command():
    #     for dev in device:
    #         print("*"*120)
    #         start_row, end_row, file_names = get_file_list(Msn, dev)
    #         result = get_file_version(file_names)
    #         # print(result)
    #         for file_result in result:
    #             print("File :{0}, Rev: {1}".format(file_result[0],file_result[1]))
    sum_data = []
    found_child_value_in_next_line = None
    element_parttern = r'^\s*/\*\s*Index:\s(\d*)'
    result_template = "{0}[{1}].{2} == {3}"



    # parent_name = 'Dio_GaaPortGroup'
    # child_parttern_dict = {}
    # child_parttern_dict.update({'pPortAddress':r'^\s*/\*\s*pPortAddress'})
    # child_parttern_dict.update({'ucPortType':r'^\s*/\*\s*ucPortType'})
    # child_parttern_dict.update({'ulModeMask':r'^\s*/\*\s*ulModeMask'})
    # child_parttern_dict.update({'ulPortGroupNum':r'^\s*/\*\s*ulPortGroupNum'})
    # child_value_pattern = r'^\s*(\(.*\)|)(\w*[UL|U])'


    # # #Dio_GaaPortChannel
    # parent_name = 'Dio_GaaPortChannel'
    # child_parttern_dict = {}
    # child_parttern_dict.update({'usMask':r'^\s*/\*\s*usMask'})
    # child_parttern_dict.update({'ucPortIndex':r'^\s*/\*\s*ucPortIndex'})
    # child_value_pattern = r'^\s*(\(.*\)|)(\w*[UL|U])'
    # # child_value_pattern_bak2 = r'^\s*(\w*[UL|U])'
    # # child_value_pattern_bak = r'^\s*(0x\w*U)'


    parent_name = 'Dio_GaaChannelGroupData'
    child_parttern_dict = {}
    child_parttern_dict.update({'usMask':r'^\s*/\*\s*usMask'})
    child_parttern_dict.update({'ucOffset':r'^\s*/\*\s*ucOffset'})
    child_parttern_dict.update({'ucPortIndex':r'^\s*/\*\s*ucPortIndex'})
    child_value_pattern = r'^\s*(\(.*\)|)(\w*[UL|U])'
    child_value_pattern_bak2 = r'^\s*(\w*[UL|U])'



    with open('input.txt') as reader:
        data_read = reader.readlines()
    for data in data_read:
        element_id_group = re.search(element_parttern, data)
        if element_id_group:
            element_id = element_id_group.group(1)
            # print(element_id)
            continue
        if found_child_value_in_next_line:
            # print('process: ',data)
            child_value_pattern_search = re.search(child_value_pattern, data)
            if child_value_pattern_search:
                # print(dir(child_value_pattern_search))
                # print(child_value_pattern_search.groups())
                # sys.exit()
                # child_value = child_value_pattern_search.group(1)
                child_value = child_value_pattern_search.groups()[-1]
                # result = parent_name + '[' + str(element_id) +'].' + found_child_value_in_next_line + ' == ' + child_value
                result = result_template.format(parent_name, str(element_id), found_child_value_in_next_line, child_value)
                sum_data.append(result)
                found_child_value_in_next_line = None
                continue
        for id_name, id_parttern in child_parttern_dict.items():  
            if re.search(id_parttern, data):
                found_child_value_in_next_line = id_name
                break


    print('result')
    for index, data in enumerate(sum_data):
        # print("Element {0} has : {1}".format(index, data))
        # print("Count element {0}".format(len(data)))
        print(data)
    
    


# main processing
if __name__ == '__main__':
    main()

# -----------------------------------------------------------
# end of script
# -----------------------------------------------------------
