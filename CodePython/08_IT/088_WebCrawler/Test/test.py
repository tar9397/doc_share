#!/usr/bin/python3

from requests_html import HTMLSession
import codecs
import sys,os,re

# output_loc = './Output'
line_ending = '\r\n'

def write_to_file(output_loc, file_name, content_str):
    import codecs, os
    if not os.path.exists(output_loc):
        os.makedirs(output_loc)

    with codecs.open(os.path.join(output_loc, file_name), "w", "utf-8-sig") as write_file:
        write_file.write(content_str)

def CrawPage(page_str):
    pass

session = HTMLSession()
resp = session.get("https://www.aliexpress.com/category/200216607/tablets.html")
resp.html.render()
# print(resp.html.html)
# write_to_file('./Output', 'web_tmp.html',resp.html.html)

script_tags = resp.html.find("script")
script_text_list = [tag.text for tag in script_tags]

# display_parttern = r'"displayTitle":"([\w*\s\.&]*)"'
display_parttern = r'"displayTitle":"((\\"|[^"])*)"'
results = []
print('Total script_text_list: ', len(script_text_list))
for script_id, scrips_text in enumerate(script_text_list):
    # print('-'*40)
    # print(scrips_text)
    # print('-'*40)
    tmp_list = re.findall(display_parttern, scrips_text)
    for tmp in tmp_list:
        if tmp is not None:
            scrips_data = tmp[0]
            results.append(scrips_data)
            print(scrips_data)

print('Total items: ', len(results))
if len(results) > 0:
    write_to_file('./Output', 'results.txt', line_ending.join(results))    



print('='*40)
print('DONE')
print('='*80)

