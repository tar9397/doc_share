import scrapy
from ..items import SampleprojectItem

num_cnt = 1
line_ending = '\r\n'

class AliexpressTabletsSpider(scrapy.Spider):
    name = 'aliexpress_tablets'
    allowed_domains = ['aliexpress.com']
    start_urls = ['https://www.aliexpress.com/category/200216607/tablets.html']

    # custom_settings={ 'FEED_URI': "aliexpress_%(time)s.csv",
    #                    'FEED_FORMAT': 'csv'}

    def parse(self, response):
        # pass
        global num_cnt
        print('*'*80)
        print("procesing:"+response.url)
        #Extract data using css selectors
        # product_name=response.css('.product::text').extract()
        # price_range=response.css('.value::text').extract()
        #Extract data using xpath
        # orders=response.xpath("//em[@title='Total Orders']/text()").extract()
        # company_name=response.xpath("//a[@class='store $p4pLog']/text()").extract()

        # display_parttern = r'"displayTitle":"((\\"|[^"])*)"'
        # price_parttern = r'"formatted_price":"((\\"|[^"])*)"'
        # shiping_parttern= r'"logisticsDesc":"((\\"|[^"])*)"'
        # store_name_parttern = r'"storeName":"((\\"|[^"])*)"'
        # store_id_parttern = r'"storeId":((\d)*)'

        middle_text = "seoCrossLink"
        # object_parttern = r'("title":".*)'
        # object_parttern = r'"items":\[(.*)\]'
        object_parttern = r'"items":\[{(.*)\}]'
        # empty_object_parttern = r'"items":\[(.*)\]'
        empty_object_parttern = r'"items":\[\]'

        display_parttern = r'"title":"((\\"|[^"])*)"'
        price_parttern = r'"price":"((\\"|[^"])*)"'
        shiping_parttern= r'"logisticsDesc":"((\\"|[^"])*)"'
        store_name_parttern = r'"storeName":"((\\"|[^"])*)"'
        store_id_parttern = r'"storeId":([\d]*)'
        # display_parttern = r'"displayTitle":"([\w*\s\.]*)"'
        # url_parttern = r'searchAjaxUrl:\s\'//([.-z&]*)\''
        import re
        scrips_text_list = response.css('script::text').extract()
        # scrips_data = response.css('script::text').re_first(display_parttern)

        print('*'*40)
        file_name = 'response_' + str(num_cnt) +'.html'
        print("Save {} to debug: {}".format(file_name, response.url))
        write_to_file('./Output', file_name, response.text)
        num_cnt = num_cnt + 1

        product_name = []
        product_price = []
        shipping_info = []
        company_name = []
        company_id= []
        for scrips_text in scrips_text_list:
            allObjects = getObjectStr(object_parttern, scrips_text)
            if allObjects is not None:
                # delimiter = '"title"'
                # write_to_file('./Output', 'allObjects.txt', allObjects)
                delimiter = '"imageWidth"'
                element_list = splitWithoutRemove(delimiter, allObjects)
                print("Total element in page: ",len(element_list))
                for element in element_list:
                    product_name.append(
                        updateParseValue(display_parttern, element, default='None'))
                    product_price.append(
                        updateParseValue(price_parttern, element, '0000'))
                    shipping_info.append(
                        updateParseValue(shiping_parttern, element, 'None'))
                    company_name.append(
                        updateParseValue(store_name_parttern, element, 'None'))
                    company_id.append(
                        updateParseValue(store_id_parttern, element, '0000'))
            else:
                # print("not found object keywork in html")
                # work-around for not load
                if isEmptyObjects(empty_object_parttern, scrips_text):
                    print("Force reload due to empty parse")
                    yield scrapy.Request(
                        response.url,
                        callback=self.parse)   
                else:
                    print("not found object keywork in html")
                
        debug_data_list = packageDebugData(product_name, product_price, shipping_info,
                        company_name, company_id)
        write_to_file('./Output', 'debug_data.txt', line_ending.join(debug_data_list))
        
        print('Total product_name: ', len(product_name))
        print('Total product_price: ', len(product_price))
        print('Total shipping_info: ', len(shipping_info))
        print('Total company_name: ', len(company_name))
        print('Total company_id: ', len(company_id))
   
        row_data=zip(product_name, product_price, shipping_info, company_name, company_id)

        print('*'*80)
        #Making extracted data row wise
        for item in row_data:
            #create a dictionary to store the scraped info
            scraped_info = {
                #key:value
                'page': response.url,
                'product_name' : item[0], #item[0] means product in the list and so on, index tells what value to assign
                'price' : item[1],
                'shipping' : item[2],
                'company_name' : item[3],
                'company_id' : item[4],
            }

            # #yield or give the scraped info to scrapy
            yield scraped_info

            # NEXT_PAGE_SELECTOR = '.ui-pagination-active + a::attr(href)'
            # next_page = response.css(NEXT_PAGE_SELECTOR).extract_first()
            # if next_page:
            #     yield scrapy.Request(
            #     response.urljoin(next_page),
            #     callback=self.parse)
            
            NEXT_PAGE_SELECTOR = 'link[rel*=next]::attr(href)'
            next_page = response.css(NEXT_PAGE_SELECTOR).extract_first()
            if next_page:
                yield scrapy.Request(
                next_page,
                callback=self.parse)   


def write_to_file(output_loc, file_name, content_str):
    import codecs, os
    if not os.path.exists(output_loc):
        os.makedirs(output_loc)

    with codecs.open(os.path.join(output_loc, file_name), "w", "utf-8-sig") as write_file:
        write_file.write(content_str)

def parseListPattern(partten, content_str):
    import re
    result = []
    tmp_list = re.findall(partten, content_str)
    for tmp in tmp_list:
        if tmp is not None:
            if type(tmp) == list or type(tmp) == tuple:
                scrips_data = tmp[0]
            else:
                scrips_data = tmp
            result.append(scrips_data)
    return result

def parsePattern(partten, content_str):
    import re
    tmp = re.search(partten, content_str)
    if tmp is not None:
        return tmp.group(1)
    else:
        return None

def packageDebugData(*args):
    result = []
    max_len = max([len(e) for e in args])
    for cnt in range(max_len):
        tmp = ""
        for e in args:
            if cnt < len(e):
                tmp += str(e[cnt])
            else:
                tmp += "    "
            tmp += " | "
        tmp += "####"    
        result.append(tmp)
    return result

def getObjectStr(obj_pattern, text):
    import re
    tmp = re.search(obj_pattern, text)
    return None if tmp is None else tmp.group(1)
    # if tmp is not None:
    #     return tmp.group(0)
    # else:
    #     return None

def isEmptyObjects(obj_pattern, text):
    import re
    tmp = re.search(obj_pattern, text)
    # if tmp is not None and '' == tmp.group(0):
    if tmp is not None:
        return True
    else:    
        return False

def splitWithoutRemove(delimiter, text):
    return [delimiter+e for e in text.split(delimiter) if e]

def updateParseValue(pattern, text_input, default='None'):  
    tmp = parsePattern(pattern, text_input)
    if tmp is not None:
        return tmp
    else:
        return default