# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
# from SampleProject.exporters import CsvCustomSeperator
from scrapy.exporters import CsvItemExporter
from SampleProject.settings import CSV_FILE_NAME

class SampleprojectPipeline:

    def __init__(self):
        file_name = (CSV_FILE_NAME)
        import time
        timestr = time.strftime("%Y%m%d-%H%M%S")
        file_name_only = file_name.split('.')[0]
        file_name_ext = file_name.split('.')[-1]
        file_name = file_name_only + '_' + timestr + '.' + file_name_ext
        self.file = open(file_name, 'wb')
        self.exporter = CsvItemExporter(self.file, encoding = 'utf-8-sig')
        self.exporter.start_exporting()

    def close_spider(self,spider):
        self.exporter.finish_exporting()
        self.file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item

    # def open_spider(self,spider):
    #     self.file = open('demo.txt', 'w',encoding='utf-8-sig')

    # def close_spider(self,spider):
    #     self.file.close()

    # def process_item(self, item, spider):
    #     # print("item: ",item)
    #     try:
    #         page=item['page']
    #         product_name = item["product_name"]
    #         price =item["price"]
    #         shipping = item["shipping"]
    #         company_name = item["company_name"]
    #         self.file.write('page: {}, product name: {}, price: {}, shipping: {}, company name: {}\n'.format(
    #             page,product_name,price,shipping,company_name)
    #         )
    #     except:
    #         pass