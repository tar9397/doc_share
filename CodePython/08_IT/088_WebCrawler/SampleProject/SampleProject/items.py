# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class SampleprojectItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    page = scrapy.Field()
    product_name = scrapy.Field()
    price = scrapy.Field()
    shipping = scrapy.Field()
    company_name = scrapy.Field()
    company_id = scrapy.Field()

    # pass
