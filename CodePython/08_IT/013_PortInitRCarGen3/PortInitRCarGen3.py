#!/usr/bin/python
# -*- coding: utf-8 -*-
################################################################################
#                             SyncTestInfo.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
import openpyxl


line_ending = '\r\n'



def create_function_str(param):
    template_header = "void Port_Init(void){0}" + '{{' + "{0}{1}{0}"+ '}}'
    tmp_cfg = []

    for data in param:
        tmp_cfg.append(create_cfg_for_single_port(data['PortName'], data['Config']))
    return template_header.format(line_ending, (line_ending*2).join(tmp_cfg))

def create_cfg_for_single_port(port_str, port_config):
    temple1 = "  /* Port {1} */{0}  /* Set the polarity positive of port {1} pin*/{0}  POSNEG{1} &= (uint32) ~{2};{0}\
  /* Configuration port {1} as IO mode */{0}  IOINTSEL{1} &= (uint32) ~{2};{0}\
  /* Configuration direction for port {1} pin are output */{0}  INOUTSEL{1} |= (uint32) {3};{0}\
  /* Select output data is output by OUTDT register */{0}  OUTDTSEL{1} &= (uint32) ~{2};{0}\
  /* Init level for pin */{0}  OUTDT{1} = (uint32) {4};"
  
    temple2 = "  /* Port {1} */{0}  /* Set the polarity positive of port {1} pin*/{0}  POSNEG{1} &= (uint32) ~{2};{0}\
  /* Configuration port {1} as IO mode */{0}  IOINTSEL{1} &= (uint32) ~{2};{0}\
  /* Configuration direction for port {1} pin are output */{0}  INOUTSEL{1} |= (uint32) {3};{0}\
  /* Select output data is output by OUTDTH/OUTDTL register */{0}  OUTDTSEL{1} |= (uint32) {2};{0}\
  /* Init level for pin */{0}  OUTDTH{1} = (uint32) {4};"


    port_num = port_str.strip().split('_')[1]

    port_mask = port_config[1]
    output_mask = port_config[2]
    port_init_val = port_config[3]
    if 'General' in port_config[0]:
        return temple1.format(line_ending, port_num, port_mask, output_mask, port_init_val)
    else:
        return temple2.format(line_ending, port_num, port_mask, output_mask, port_init_val)
        
        



def padded_hex(dev_value):
    return "{0:#0{1}X}".format(dev_value,10).replace('0X','0x')


# data struct content: [{"PortName": 'PORTGROUP_0_BITS_0_TO_27', "Config": [data]}, {}, ...]
def get_port_name(file_name, sheet_name):
    ret = []
    cur_portname = None
    out_data = None
    try:
        wbook = openpyxl.load_workbook(file_name, data_only=True)
        ws = wbook.get_sheet_by_name(sheet_name)
        for row in ws.iter_rows():
            if "PortName" == str(row[0].value):
                cur_portname = str(row[1].value)
              # print("Portname: ",cur_portname)
                port_mask = 0
                output_mask = 0
                continue
            if cur_portname:
                if "OutSelect" in str(row[0].value):
                    if "1" in str(row[1].value):
                        out_data = 'HighLow' 
                    else:
                        out_data = 'General' 
                elif "Mode" in str(row[0].value):
                    bit_pos = 31
                    for cell in row[1:]:
                        cell_content = str(cell.value).upper()
                        if 'I' in cell_content or 'O' in cell_content:
                            valid_pin = 1
                            # port_mask = port_mask*2 + 1
                            port_mask += int(pow(2, bit_pos))
                            # output_mask = output_mask*2
                            if 'O' in cell_content:
                                # output_mask += 1
                                output_mask += int(pow(2, bit_pos))
                        if bit_pos >= 0:
                            bit_pos -= 1
                        else:
                            break
                elif "State" in str(row[0].value):
                    # for cell in row[1:]:
                    port_init_val = 0
                    bit_pos = 31
                    for cell in row[1:]:
                        cell_content = str(cell.value)
                        # port_init_val = port_init_val*2
                        if '1' in cell_content and (output_mask & int(pow(2, bit_pos)) != 0):
                            # port_init_val += 1
                            port_init_val += int(pow(2, bit_pos))
                        if bit_pos >= 0:
                            bit_pos -= 1
                        else:
                            break
                    
                    temp = {}
                    temp['PortName'] = cur_portname
                    temp['Config'] = [out_data, padded_hex(port_mask), padded_hex(output_mask), padded_hex(port_init_val)]
                    ret.append(temp)
                    cur_portname = None
                    out_data = None
                    
        wbook.close()
    except Exception as e:
        print("[E]Issue occurs when opening xlsx file to read")
        print(e)
        
    return ret



def main():
    """main function of the program"""

    data = get_port_name("PinConfig.xlsm", "CFG001")
    # print("result: ",data)
    
    text = create_function_str(data)
    print('*'*80)
    print(text)
    print('*'*80)


if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################