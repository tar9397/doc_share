#!/usr/bin/python3
# -*- coding: utf-8 -*-
################################################################################
#                           GenTestAppRCar.py                                  #
# Purpose: Synchronize test case content from test app into test specification #
################################################################################
import sys,os,re
import argparse
# import openpyxl
import json
from dataclasses import dataclass
from typing import List

# ------------------------------------------------------------------------------
# global variables section
# ------------------------------------------------------------------------------
line_ending = '\r\n'
# output_loc = './Output'
register_db = {}

# H3
register_db['OUTDT6']='0xE6055408'
# V3H
register_db['OUTDT0']='0xE6050008'


@dataclass
class TestCaseConfig:
    port_name : str
    port_num : int
    port_pin : int
    reg_addr : str
    hold_time : int
    port_pin_list : List[int]

    def __init__(self, port_name: str, port_pin: int, reg_name: str, hold_time: int):
        # self.port_num = port_num
        self.port_name = port_name
        self.port_num = port_name.strip().split('_')[1]
        # self.port_pin_list = port_name.strip().split('_')[1]
        self.port_pin = port_pin
        self.reg_addr = register_db[reg_name]
        self.hold_time = hold_time
        self.port_pin_list = []

        tmp = port_name.strip().split('_')
        filter_data = tmp[tmp.index('BITS')+1:]
        if len(filter_data) > 1 and 'TO' in filter_data[1]:
            row_data = list(range(int(filter_data[0]), int(filter_data[2])+1))
            self.port_pin_list = row_data
        else:
            self.port_pin_list.append(filter_data)

        self.port_pin_mask = 0
        for pin_id in self.port_pin_list:
            if pin_id != self.port_pin:
                self.port_pin_mask += 1 << pin_id
                # print(pin_id)



# xOS2
run_config = {}

run_config_H3 = {}
run_config_H3['ETC_086'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_H3['ETC_087'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_H3['ETC_089'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_H3['ETC_090'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_H3['ETC_092'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_H3['ETC_093'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_H3['ETC_095'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_H3['ETC_096'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)

run_config_V3H = {}
run_config_V3H['ETC_134'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3H['ETC_135'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)
run_config_V3H['ETC_136'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3H['ETC_137'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)
run_config_V3H['ETC_138'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3H['ETC_139'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)
run_config_V3H['ETC_140'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3H['ETC_141'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)

run_config_M3 = {}
run_config_M3['ETC_086'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3['ETC_087'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_M3['ETC_089'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3['ETC_090'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_M3['ETC_092'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3['ETC_093'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_M3['ETC_095'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3['ETC_096'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)

run_config_M3N = {}
run_config_M3N['ETC_086'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3N['ETC_087'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_M3N['ETC_089'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3N['ETC_090'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_M3N['ETC_092'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3N['ETC_093'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)
run_config_M3N['ETC_095'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 15)
run_config_M3N['ETC_096'] = TestCaseConfig('PORTGROUP_6_BITS_0_TO_31', 11, 'OUTDT6', 65535)

run_config_V3U = {}
run_config_V3U['ETC_134'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3U['ETC_135'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)
run_config_V3U['ETC_136'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3U['ETC_137'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)
run_config_V3U['ETC_138'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3U['ETC_139'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)
run_config_V3U['ETC_140'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 15)
run_config_V3U['ETC_141'] = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 11, 'OUTDT0', 65535)

# run_config = {
#     'ETC_134': TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 7, 'OUTDT0', 15)
# }

# PORTGROUP_0_BITS_0_TO_21
# PORTGROUP_1_BITS_0_TO_27
# PORTGROUP_2_BITS_0_TO_29
# PORTGROUP_3_BITS_0_TO_16
# PORTGROUP_4_BITS_0_TO_24
# PORTGROUP_5_BITS_0_TO_14

run_config['H3']=run_config_H3
run_config['V3H']=run_config_V3H
run_config['M3']=run_config_M3
run_config['M3N']=run_config_M3N
run_config['V3U']=run_config_V3U

# ------------------------------------------------------------------------------
# Function section
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Handle parser create function
# port_id: array of int
# ------------------------------------------------------------------------------



# def update_arr_content(arr, *args):
#     return [data.format(*args) for data in arr]
def padded_hex_raw(dev_value, length):
    return "{0:#0{1}X}".format(dev_value, length+2 )

def padded_hex(dev_value, length=8):
    return padded_hex_raw(dev_value, length).replace('0X','0x')

def padded_hex_without_prefix(dev_value, length=8):
    return padded_hex_raw(dev_value, length).replace('0X','')


# ------------------------------------------------------------------------------
# Gen resource command serial
# ------------------------------------------------------------------------------
def gen_resource_command_serial(reg_addr, reg_val, hld_time, mfisclk=1):
    result = []
    result.append("setlock {0} {1} {2}".format(mfisclk, reg_addr, reg_val))    # (MFISLCKR1, OUTDT0, FlipBitsMask)
    result.append("settttime 00003A98")     # (15s)
    result.append("setmontime 0064")        # (100us)
    result.append("setsmpltime 0001")       # (1ms)
    result.append("sethldtime {0}".format(hld_time))         # (us)
    result.append("exelock")                # (execute lock register)
    
    return result
    # return line_ending.join(result)



def get_input_command():
    """Get all input for init process
    Print help if command is not in valid command

    Returns:
        None -- It doesn't return any value
    """
    example = '''
    E.g: ./GenTestAppRCarGen3.py CFG001
    ./GenTestAppRCarGen3.py CFG009
    '''
    parser = argparse.ArgumentParser(description=example)
    parser.add_argument('Dev', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
                        help='valid target device: H3, V3M, V3H ...')
    parser.add_argument('-m', action='store_true', \
                        help='set max hold time', default=False, required=False)
    parser.add_argument('--run_tc', type=str, nargs='?',
                        help='run with specific test case or list of test case', default='', required=False)

    # parser.add_argument('cfg', type=str,
    #                     help='config to read')
    # parser.add_argument('Msn', type=str, choices = ["Adc", "Can", "Dio", "Eth", "Fls", "Fr", "Gpt", "Icu", "Lin", "Mcu", "Port", "Pwm", "Spi", "Wdg"], \
    #                     help='valid target module: Adc, Can, Dio ...')
    # parser.add_argument('TestType', type=str, help='type of test: DIT, TIT, VT')
    # parser.add_argument('TestDevice', type=str, choices = ["H3", "V3M", "V3H", "M3", "M3N", "V3U", "V3Hv2"], \
    #                     help='target device: H3, V3M, V3H , M3, M3N, V3U, V3Hv2')
    # parser.add_argument('ARVersion', type=str, choices = ["4_2_2", "4.2.2"], \
    #                     help='autosar version: 4_2_2')
    # parser.add_argument('--nc', action='store_true', \
    #                     help='(optional) skip copy item in WorkspaceSetup.sh', default=False, required=False)
    # parser.add_argument('OSTest', type=str,
    #                     help='OS Test option: OS, No_OS, None(TIT only)')
    # parser.add_argument('--s0s1', type=str, nargs='?',
    #                     help='(optional) run S0/S1 or normal TC: True, False (default=False)', \
    #                     default='False', required=False)
    # parser.add_argument('--target_cfg', type=str, nargs='?',
    #                     help='(optional) run specific cfg(s)/config type: ex. CFG001,CFG002,DEF001(split by ","), \
    #                     CFGnnn(all CFG), RCnnn(all RC), DEFnnn(all DEF), xCFGnnn(exclude all CFG), \
    #                     xRCnnn(exclude all RC), xDEFnnn(exclude all DEF)', default='CFGxxx', required=False)
    # parser.add_argument('--keep_tc', type=str, nargs='?',
    #                     help='(optional: True or False) when --target_cfg is used do you want to keep original \
    #                     TC or not (defalt=False)', default='False', required=False)
    # parser.add_argument('--ignore_error', type=str, nargs='?',
    #                     help='(optional: True or False) allow to continue progress when error occurs (defalt=false)', \
    #                     default='False', required=False)
    # parser.add_argument('--ice_number', type=str, nargs='?',
    #                     help='(optional) id number of E2 Emulator to select debugger connection (defalt=null)', \
    #                     required=False)
    # parser.add_argument('--build_option', type=str, nargs='?',
    #                     help='(optional: all/generate_forward/build_only) allow to choose selected actions only \
    #                     to reduce execution time (defalt=all)', default='all', required=False)
    # parser.add_argument('--exe_op', type=str, nargs='?',
    #                     help='(optional: True or False) execution time optimization, not recommended, this should \
    #                     only be used when everything is confirmed OK (default=False)', default='False', required=False)
    # parser.add_argument('--cat_map', type=str, nargs='?',
    #                     help='(optional) use interrupt category in data.json', default='False', required=False)
    # parser.add_argument('--debug', type=str, nargs='?',
    #                     help='(optional) turn on script debug mode', default='False', required=False)

    args = parser.parse_args()
    # return str(args.cfg)
    return args

    # config.msn = str(args.Msn)  # MCAL Msn
    # config.test_type = str(args.TestType).upper()  # DIT, TIT
    # config.device = str(args.TestDevice)  # V3H, V3U..
    # config.ar_version = str(args.ARVersion).replace('_','.')  # 4_2_2, 4_3_1
    # config.skip_copy_workspace = args.nc

    # return True

def Send_Serial(cmd_list):
    for cmd in cmd_list:
        # print(cmd+line_ending)
        print(cmd)


def main():
    """main function of the program"""

    # result = gen_resource_command_serial('0xE6050008', '0xFFFFF7FF', '000F')
    # Send_Serial(result)
    # # print(line_ending.join(result))

    # for test_case_name in run_config.keys():
    #     print('*'*40)
    #     print("Run for : {}".format(test_case_name))
    #     print('*'*40)
    #     test_case = run_config[test_case_name]
    #     result = gen_resource_command_serial(test_case.reg_addr, '0xFFFFF7FF', test_case.hold_time)
    #     Send_Serial(result)

    cmd_cfg = get_input_command()

    # test_case = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 7, 'OUTDT0', 15)
    #  0xFFFFF7FF
    # test_case = TestCaseConfig('PORTGROUP_0_BITS_0_TO_21', 7, '0xE6050008', padded_hex_without_prefix(15))
    # result = gen_resource_command_serial(test_case.reg_addr, '0xFFFFF7FF', test_case.hold_time)
    # result = gen_resource_command_serial('0xE6050008', '0xFFFFF7FF', test_case.hold_time)

    # print("Sample")
    # if cmd_cfg.m:
    #     result = gen_resource_command_serial('0xE6050008', '0xFFFFF7FF', padded_hex_without_prefix(65535, length=4))
    # else:
    #     result = gen_resource_command_serial('0xE6050008', '0xFFFFF7FF', padded_hex_without_prefix(15, length=4))
    # Send_Serial(result)

    # print("test")
    if cmd_cfg.run_tc is not '':
        run_tc_list = cmd_cfg.run_tc.split(',')
    else:
        run_tc_list = [tc for tc in run_config[cmd_cfg.Dev].keys()]
    
    for run_tc_name in run_tc_list:
        print('*'*40)
        print("Process test case: ",run_tc_name)
        print('*'*40)
        run_tc = run_config[cmd_cfg.Dev][run_tc_name.strip()]
        # print(run_tc.port_pin_mask)
        result = gen_resource_command_serial(
                run_tc.reg_addr, 
                padded_hex(run_tc.port_pin_mask), 
                padded_hex_without_prefix(run_tc.hold_time, length=4))
        Send_Serial(result)






    # config_name = "CFG001" if run_cfg == '' else run_cfg
    # pin_config_file = "PinConfig.xlsm"

    # data = get_port_name(pin_config_file, config_name)
    # print("Collect {0} in {1} ".format(config_name, pin_config_file))
    
    # # text = create_function_str(data, config_name)
    # msn = str(init_db['msn'])
    # device = str(init_db['device']).upper()
    # # for testcase, port_num_arr in init_db[config_name].items():
    # for testcase in sorted(init_db[config_name].keys()):
    #     port_num_arr = init_db[config_name][testcase]
    #     text = create_function_str(data, config_name, port_num_arr)
    #     # print('*'*80)
    #     # print(text)
    #     # print('*'*80)
    #     tmp_file_name_ele = [msn.capitalize()]
    #     file_name = '_'.join([msn.capitalize(), ''.join(testcase.split('_')), device]) + '.c'
    #     new_file = update_file_content(template_content, file_name, text.split(line_ending))
    #     write_to_file(file_name, new_file)
    #     print("Create file {} success !".format(file_name))




if __name__ == '__main__':
    main()

################################################################################
#                               End of script                                  #
################################################################################